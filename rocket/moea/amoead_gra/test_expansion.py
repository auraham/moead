# test_expansion.py
from __future__ import print_function
from rocket.plot import load_rcparams
from rocket.moea.amoead_gra.expansion import expand
from rocket.moea.amoead_gra.expansion import get_base_simplex, move_base_simplex, move_reduce_base_simplex, reduce_configs
from rocket.moea.amoead_gra.expansion import plot_configs,  plot_weights
import numpy as np

if __name__ == "__main__":

    load_rcparams()
    
    weights = np.genfromtxt("weights_m_3_21.txt")
    m_objs  = weights.shape[1]
    p       = 5                                             # number of division of original simplex weights_m_3_21.txt
    scale   = 0.5
    save    = False
    
    # create base simplex
    base_simplex = get_base_simplex(m_objs, p, scale)
    
    # --- first expansion ---
    
    # get all valid configurations (removing duplicates)
    center_id   = 12                 # this value can change
    configs     = move_reduce_base_simplex(center_id, weights, base_simplex)
   
    # check result (removing duplicates)
    plot_configs(center_id, scale, weights, base_simplex, configs, save, "multiple_expansion_1_12.png")
    
    # get new weights from configs
    new_weights = reduce_configs(configs, weights)

    # plot result
    plot_weights(new_weights, save=False)
    
    
    # --- second expansion ---
    
    # get all valid configurations (removing duplicates)
    weights     = new_weights.copy()
    center_id   = 8                 # this value can change
    configs     = move_reduce_base_simplex(center_id, weights, base_simplex)
   
    # check result (removing duplicates)
    plot_configs(center_id, scale, weights, base_simplex, configs, save, "multiple_expansion_2_8.png")
    
    # get new weights from configs
    new_weights = reduce_configs(configs, weights)

    # plot result
    plot_weights(new_weights, save=False)
    
    # --- third expansion (no vectors were added) ---
    
    # get all valid configurations (removing duplicates)
    weights     = new_weights.copy()
    center_id   = 8                 # this value can change
    configs     = move_reduce_base_simplex(center_id, weights, base_simplex)
   
    # check result (removing duplicates)
    plot_configs(center_id, scale, weights, base_simplex, configs, save, "multiple_expansion_3_8.png")
    
    # get new weights from configs
    new_weights = reduce_configs(configs, weights)

    # plot result
    plot_weights(new_weights, save=False)
    
    # --- fourth expansion (only one vector was added) ---
    
    # get all valid configurations (removing duplicates)
    weights     = new_weights.copy()
    center_id   = 30                 # this value can change
    configs     = move_reduce_base_simplex(center_id, weights, base_simplex)
   
    # check result (removing duplicates)
    plot_configs(center_id, scale, weights, base_simplex, configs, save, "multiple_expansion_4_30.png")
    
    # get new weights from configs
    new_weights = reduce_configs(configs, weights)

    # plot result
    plot_weights(new_weights, save, "multiple_expansion_final.png")
    
    
    
    # --- same result ---
    center_ids  = [12, 8, 8, 30]
    weights     = np.genfromtxt("weights_m_3_21.txt")
    p           = 5                                             # number of division of original simplex weights_m_3_21.txt
    scale       = 0.5
    new_weights = expand(center_ids, weights, p, scale)
    
    # plot result
    plot_weights(new_weights, save, "multiple_expansion_final2.png")
    
    
