# plot_poi.py
from __future__ import print_function
import matplotlib.pyplot as plt
import numpy as np
from rocket.plot import load_rcparams

def plot_poi_record(poi_record):
    
    load_rcparams((8, 10))
    
    fig = plt.figure()
    ax = fig.add_subplot(111)
    plt.subplot(1,1,1)
    
    vmin = poi_record.min()
    vmax = poi_record.max()
    plt.matshow(poi_record, fignum=False, cmap="Blues", vmin=vmin, vmax=vmax, aspect="auto")
    
    plt.title("Poi Record")
    plt.colorbar()
    plt.grid(False)
    plt.xlabel("Generation $t$")
    plt.ylabel("Subproblem $i$")
    
    #ax = plt.axes()
    ax.xaxis.set_ticks_position("bottom")
    
    plt.show()
    
if __name__ == "__main__":
    
    
    poi_record = np.array([
                        [1, 0, 1, 0],
                        [1, 2, 0, 0],
                        [1, 2, 3, 2],
                        [1, 0, 0, 2],
                        [0, 0, 1, 0],
                        [0, 0, 1, 0],
                        [0, 0, 1, 2],
                        [0, 0, 1, 2],
                        ])
                        
                        
    poi_record = np.genfromtxt("poi_record.txt", dtype=int)
                        
    plot_poi_record(poi_record)
    
