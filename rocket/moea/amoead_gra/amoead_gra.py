# moead_gra.py
from __future__ import print_function
from rocket.moea import MOEAD_GRA
from rocket.moea.moead_gra.genetic_operations import genetic_operations
from rocket.moea.moead_gra.replacement import replacement
from rocket.moea.amoead_gra.expansion import expand_ext
from rocket.moea.amoead_gra.plot_poi import plot_poi_record
import numpy as np
from numpy.random import RandomState
import matplotlib.pyplot as plt

# debug
from rocket.dev.rand import Rand

class AMOEAD_GRA(MOEAD_GRA):
    
    def __init__(self, params):
        
        MOEAD_GRA.__init__(self, params)
        
        # @todo: add checks
        self.expansion_step = params.get("amoead-gra_expansion_step", None)     # number of generations for expanding weight vectors
        self.expansion_p    = params.get("amoead-gra_expansion_p", None)        # number of divisions
    
    def update_improvement(self, fit_old_pop, fit_new_pop):
        """
        Implementation of Eq. (5) of [Zhou16]
        
        This function computes the relative improvement update of each subproblem according Eq. (5)
        
        Input
        fit_old_pop
        fit_new_pop
        
        Output
        poi
        """
        
        nsp = self.pop_size         # number of subproblems
        u   = np.zeros((nsp, ))     # relative improvement
        poi = np.zeros((nsp, ))     # probability of improvement vector
        eps = 1e-50                 # to avoid zero division
        
        almost_zero = 0.00001       # reset poi threshold
        
        # for each subproblem
        for i in range(nsp):                                                # @todo: is it possible to use a matrix division instead? something like
                                                                            #  (fit_old_pop - fit_new_pop) / (fit_old_pop+eps)
            u[i] = (fit_old_pop[i] - fit_new_pop[i]) / (fit_old_pop[i]+eps) # we need to add eps to avoid zero division

        # maximum relative improvement
        u_max = u.max()
        
        # check for reset
        if u_max < almost_zero:
            
            
            if self.verbose: print("reseting poi (u_max: %.4f)" % u_max)    # @todo: add log support here
        
            poi = np.ones((nsp, ))          # now, each subproblem has a probability of 1 to be selected for further improvement
            return poi.copy()


        # update poi 
        for i in range(nsp):
            poi[i] = (u[i] + eps) / (u_max + eps)       # we need two for loops, one to compute u_max and other to compute poi
        
        
        # added to amoead_gra
        to_keep = poi < eps
        indices = np.arange(nsp)
        
        if to_keep.sum() > 0:
            # these subproblems:
            # - are on local minima
            # - are on the real front
            print("stagnation on %s subproblems" % to_keep.sum())
            print("%s" % indices[to_keep])
            print(" ---- ")
        
        return poi.copy()
    
    
    def expand_pop(self, chroms, objs, weights):
        
        
        n_genes     = chroms.shape[1]
        m_objs      = objs.shape[1]
        pop_size    = chroms.shape[0]
        p           = self.expansion_p
        center_ids  = np.arange(pop_size, dtype=int)
        
        # create new weights
        new_weights, counter = expand_ext(center_ids, weights, p, scale=0.5)
        
        # preserve previous pop
        new_pop_size = new_weights.shape[0]
        
        new_chroms = np.zeros((new_pop_size, n_genes))
        new_objs   = np.zeros((new_pop_size, m_objs))
        
        new_chroms[:pop_size, :] = chroms.copy()            # copy previous chroms
        new_objs[:pop_size, :]   = objs.copy()              # copy previous objs
        
        
        # add and evaluate new chroms
        start = pop_size
        end = 0
        for i in range(pop_size):
            
            if counter[i] > 0:
                
                nc  = counter[i]         # number of copies of chroms[i] to be added to new_chroms
                end = start + nc
                
                block_chroms    = chroms[i].reshape((1, n_genes)).repeat(nc, axis=0)
                #block_weights  = new_weights[start:end]
                block_objs      = self.evaluate_pop_mop(block_chroms)
                
                new_chroms[start:end, :] = block_chroms
                new_objs[start:end, :]   = block_objs 
                
                start = end
            
            
        return new_chroms.copy(), new_objs.copy(), new_weights.copy()
        
    
    def run(self, chroms, seed, run_id):
        """
        Independent run
        """
        
        self.rand   = RandomState(seed)
        
        # debug
        # chroms = np.genfromtxt("init_chroms.txt")
        self.rand = Rand() 
        
        
        pop_size    = self.pop_size
        weights     = self.pop_weights
        neighbors   = self.create_neighborhood(weights)
        objs        = self.evaluate_pop_mop(chroms)
        
        # added to moead_gra
        poi         = np.ones((pop_size, ))                                 # init as ones vector
        fit_old_pop = self.evaluate_pop_scalar_method(objs, weights)        # fitness = scalarizing function value
        fit_new_pop = fit_old_pop.copy()
        poi_evals   = np.zeros((pop_size, ))                                # for stats only
        poi_record  = np.zeros((pop_size, self.iters))
        
        t = 1
        self.stats(chroms, objs, t, run_id)
        
        t = 2
        while t <= self.iters:
            
            for k in range(pop_size):
                
                # added to moead_gra
                i = self.get_next_subproblem(poi, k)
                
                # added to moead_gra
                child = genetic_operations(i, neighbors, chroms, self.lbound, self.ubound, self.pop_size, self.params_crossover["prob"], self.params_mutation["prob"], self.rand)       # @todo: use crossover and mutation frontends instead
                
                child = self.check_pop_bounds(child)            
                
                child_objs = self.evaluate_pop_mop(child)
                
                # modified for moead_gra, use all the weights, not only a subset
                scalar_child = self.evaluate_pop_scalar_method(child_objs, weights)
                scalar_pop   = self.evaluate_pop_scalar_method(objs, weights)
                
                
                # added to moead_gra
                to_replace = replacement(scalar_child, scalar_pop)
                
                
                chroms[to_replace, :] = child.copy()
                objs[to_replace, :] = child_objs.copy()
                
                # stats
                poi_evals[i] += 1
                poi_record[i, t-1] += 1        # t-1, debe ser cero-basado        
            
            
            # @note: we output messages AFTER evaluating child on self.evaluate_pop_mop
            # this way, the value of self.fevals is the expected value
            if self.verbose:            # @todo: add log support here
                if t%100 == 0:
                    print ("t:", t, " fevals:", self.fevals)
            
            # compute stats
            self.stats(chroms, objs, t, run_id)
            
            # added to moead_gra
            if t % self.poi_delta == 0:
                
                fit_old_pop = fit_new_pop.copy()
                fit_new_pop = self.evaluate_pop_scalar_method(objs, weights) 
                
                poi = self.update_improvement(fit_old_pop, fit_new_pop)
                
            
            # added to amoead_gra
            if self.expansion_step:
                if t % self.expansion_step == 0:
                    print ("Expanding pop...")
                    
                    # expand pop 
                    new_chroms, new_objs, new_weights = self.expand_pop(chroms.copy(), objs.copy(), weights.copy())
                    
                    # update pop and weights
                    chroms = new_chroms.copy()
                    objs = new_objs.copy()
                    weights = new_weights.copy()
                    
                    # update pop_size
                    pop_size = chroms.shape[0]
                    self.pop_size = pop_size
                    
                    # update neighborhood
                    neighbors   = self.create_neighborhood(weights)
                    
                    # update poi
                    fit_old_pop = self.evaluate_pop_scalar_method(objs, weights) 
                    fit_new_pop = self.evaluate_pop_scalar_method(objs, weights) 
                    
                    # option 1: reset
                    #poi = np.ones((pop_size, ))
                    
                    # option 2: extend
                    slots     = pop_size - poi.shape[0]
                    poi       = np.hstack((poi, np.ones((slots, ))))
                    poi_evals = np.hstack((poi_evals, np.zeros((slots, ))))
                    poi_record= np.vstack((poi_record, np.zeros((slots, self.iters))))
                    
            # next generation
            t+=1
            
            
        # save stats
        self.poi_evals  = poi_evals.copy()
        self.poi_record = poi_record.copy() 
           
        return chroms.copy(), objs.copy()
        
    
    def plot_run(self, run_id):
        
        # @todo: put MOEAD_GRA.plot_run in plot_poi.py or something similar
        
        plot_poi_record(self.poi_record)
        #np.savetxt("poi_record.txt", self.poi_record, fmt="%d")
