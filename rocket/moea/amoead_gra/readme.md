# Adaptive MOEA/D-GRA

This package contains an implementation of our AMOEA/D-GRA (Generalized Resource Allocation). This hybrid is based on two key features:

- Generalized Resource Allocation [Zhou16]:
- Adaptation of referentce vectors [Jain13,Deb14]:




# Update

This MOEA is functional, but is not finished. Their performace is not as good as expected. However, we hope to take it back in the future. *Why did not it work?* Well, the expansion is a good idea, but it was not well applied. Instead of expanding both weight vectors (`weights`) and individuals (`chroms,objs`), we should expand weight vectors only. This way, `pop_size` will not increase.

# Intuition

The idea of [Jain13] is to add weight vectors around a $m$-dimensioanl point in a uniform manner. To this end, the authors construct a *primary simplex* which consists of $m$ weight vectors uniformly spaced as shown Fig. 1. In the following, the term *primary simplex* will be replaced by *base simplex* for simplicity.



![](base_simplex.png)
*Figure 1. Location of base simplex.*

This procedure can be divided into two parts:

- Construct a base simplex (`get_base_simplex()`)
- Move the base simplex around $m$ points (`move_base_simplex()`)


## Construct a base simplex


To create the base simplex, we proceed as follows (see Fig. 2):

- Create a set of $m$ extreme points. When $m=3$, this set is composed of:

  - $a = (1, 0, 0)$
  - $b = (0, 1, 0)$
  - $c =(0, 0, 1)$.

- Select one of these extreme points as the origin. In this example, $a$ is the origin.

- Translate the remaining $m-1$ of the points *near* to the origin. For $w \in \{b, c\}$, we proceed as follows:

  - Compute the direction vector from $a$ to $w$:
    $$
    d_w = a - w
    $$

  - Define a scale factor according to $p$, where $p$ is the spacing parameter of the original simplex. In this example, $p=5$.

    $$
    f = (p-1) \times (1/p)
    $$

  - Scale the direction vector $d_w$ as follows:

    $$
    v_w = f \times d_w
    $$

  - Transtale $w$ using $v_w$:

    $$
    t_w = w + v_w
    $$















The intuition of this procedure is to move $m-1$ extreme points near to one of them, the origin, which remains unchanged. In this example, we move $w \in \{b, c\}$ near to the origin $a$. To this end, a vector $v_w$ is employed to move these vectors near the origin. First, we need the direction of the translation. This direction is given by $a - w$. Then, we need to scale this direction vector. As the original simplex contains $p$ divisions on each edge, we employ this value as reference.  See Fig. 2 (Right). The edge delimited by $a$ and $c$ is divided into $p$ parts. From these, $d_c$ will cover only the first four parts from $c$ to $a$. To this end, we employ the scale factor $f = (p-1)*(1/p)$, wich means *take $p-1$ divisions of size $(1/p)$*. After scaling $d_c$, $v_c = f \times d_c$, we add $v_c$ to $c$. This operation means *translate $c$ using $v_c$ as direction*.



![](construction_notes.png)



*Figure 2. Construction of base simplex. (Left) An $m$-dimensional simplex is created using $m$ extreme points: $(1, 0, 0)$,  $(0, 1, 0)$, and $(0, 0, 1)$. (Right)  $m-1$ extremes, labeled as $b$ and $c$, are translated to $t_b$ and $t_c$, respectively. The origin $a$ remains unchanged. The final base simplex is constructed from $a$, $t_b$, and $t_c$*



We can shrink even more a given base simplex using this formula:
$$
f = (p-1 + (1-s)) \times (1/p)
$$
where $s \in [0, 1]$ is an additional scaling factor which means *shirnk this base simplex to $s$*. Figure 3 shows the intuition behind this additional scaling.


![](scale.png)

*Figure 3. Additional scaling of the base simplex using a scale of 0.25, 0.5, and 0.75.*


## Move the base simplex around $m$ points 


Remember, the idea of the adaptation method of [Jain13] is to create additional weight vectors around a point, as illustrated in Fig 4. To this end, we will create these additional weight vectors using the base simplex defined above.

The idea consists of create *configurations*. Figure 4 shows three configurations of the base simplex:

- Configuration 1: vectors 8, 2, 3.
- Configuration 2: vectors 8, 7, 12.
- Configuration 3: vectors 8, 9, 13.

![](expansion_8.png)

*Figure 4. Move the base simplex arond the weight vector 8*.



Note that these three configurations are created around the vector 8, which we will call *center point*. That is, *we are adding vectors around this point*. Also, note that the simplex vector is located in the left bottom corner. We will move this base simplex around the *center point*. This translation requires a direction vector. Although, there are $m$ ways to define such vector:

- $d_{8, 20} = w_8 - w_{20}$
- $d_{8, 18} = w_8 - w_{18}$
- $d_{8, 19} = w_8 - w_{19}$

So, we have $m$ direction vectors, each of them moves the base simplex around the center point. After performing $m$ translations, the regionr around the center point will contain more weight vectors. This procedure is implemented as follows:



```python
# expansion.py

def move_base_simplex(center_id, weights, base_simplex):
    
    m_objs = weights.shape[1]
    configs = []
    
    a = weights[center_id]          # destiny
                                    # the base simplex will be translated (moved) around this vector
    
    for i in range(m_objs):
 
        b   = base_simplex[i]       # source
        d   = a - b                 # translation (direction) vector
        
        config = base_simplex + d   # configuration
        
        # check
        if (config < 0).sum() > 0:  # if config contains negative values, it is skipped
            continue
            
        configs.append(config)
    
    # return list of configurations
    return configs
```



# Example

This snippet shows how to use both `get_base_simplex` and `move_base_simplex` functions.

```python
# expansion.py

if __name__ == "__main__":

    load_rcparams()
    
    weights = np.genfromtxt("weights_m_3_21.txt")
    m_objs  = weights.shape[1]
    p       = 5                          # number of division of original simplex weights_m_3_21.txt
    scale   = 0.25
    
    # create base simplex
    base_simplex = get_base_simplex(m_objs, p, scale)
    
    # get all valid configurations
    center_id   = 8                 # change this value
    configs     = move_base_simplex(center_id, weights, base_simplex)
   
    # check result
    plot_configs(center_id, scale, weights, base_simplex, configs, save=True)
```

Three configurations are created around vector 8 using a scale of 0.25. The base simplex is located in the bottom left corner.

![](expansion_8_0.25.png)

# More examples

These figures show how some vectors are added around a given point.

![](configs_5.png)

![](configs_0.png)

![](configs_12.png)

![](configs_18.png)

![](configs_20.png)

![](configs_3.png)



# Multiple expansions

The script `test_expansion.py` depicts four consecutive expansions. The goal of this script is to show you how new vectors are added to the original set of vectors.



![](multiple_expansion_1_12.png)

**First expansion, six vectors** Six new vectors are added around vector 12.

![](multiple_expansion_2_8.png)

**Second expansion, five vectors** Six new vectors are around vector 8 but only five of them are new (one of the vectors of config 2 is the same as vector 21). 

![](multiple_expansion_3_8.png)

**Third expansion, zero vectors** Six new vectors are added around vector 8, the same as in second expansion. Although, all of them are already in the  set of weights, so this expansion do not add new vectors.

![](multiple_expansion_4_30.png)

**Fourth expansion, one vector** Six new vectors are added around vector 30. However, only one of them is new (vector 32 in next figure).

![](multiple_expansion_final.png)

**Final expansion** This figure shows the final set of weight vectors. After four consecutive expansions, 6+5+0+1 new vectors were added, obtaining 33 vectors in the final set.

The same result can be obtained using `expand` as follows:

```python
center_ids  = [12, 8, 8, 30]
weights     = np.genfromtxt("weights_m_3_21.txt")
p           = 5      # number of divisions
scale       = 0.5
new_weights = expand(center_ids, weights, p, scale)

# plot result
plot_weights(new_weights)
```



# Funny bug

I was debugging `amoead_gra.py` using two configurations: (1) expansion disabled and (2) expansion enabled:

```python
# bug_test_moea_config.py (expansion enabled every 101 generations)
params = {
	...
    # AMOEA/D-GRA
    "amoead-gra_expansion_step": 101,
    "amoead-gra_expansion_p": 39, 
}
```

```python
# bug_test_moea_config_baseline.py (expansion disabled)
params = {
    ...
	# AMOEA/D-GRA
    #"amoead-gra_expansion_step": 101,
    #"amoead-gra_expansion_p": 39,
}
```

The difference among both configurations is whether the population will grow after 101 generations. This expansion is made in `amoead_gra.py` as follows:

```python
# added to amoead_gra
if self.expansion_step:
    if t % self.expansion_step == 0:
        
        new_chroms, new_objs, new_weights = self.expand_pop(chroms.copy(), objs.copy(), weights.copy())

        # buggy section
        chroms  = new_chroms.copy()
        objs    = new_objs.copy()
        weights = new_weights.copy()
        
        # pop_size is not updated
```

However, I thought that both configurations *should* return the same output (`chroms`, `objs`) because the three lines does not change the first `pop_size` rows of `chroms`, `objs`, and `weights`, so the evolutionary process should remain the same. *It was not*. It turns out that these lines:

```python
# modified for moead_gra, use all the weights, not only a subset
scalar_child = self.evaluate_pop_scalar_method(child_objs, weights)
scalar_pop   = self.evaluate_pop_scalar_method(objs, weights)
```

uses `weights`, but remember, we have updated `weights` after expansion. So, it changed its shape from `(40, 2)` to `(79, 2)`, by adding `39` new weight vectors to the bottom of `weights`. 

**Expansion disabled**

![](expansion_disabled_poi.png)



![](expansion_disabled_pop.png)

**Expansion enabled**

![](expansion_enabled_poi.png)



![](expansion_enabled_pop.png)

The previous images show the populations obtained for each configuration. They are different! At first I was thinking *well, if `pop_size` is not updated, the remaining process will use the first `pop_size=40` rows of `chroms`, `objs`, and `weights` and the output of both configurations will be the same*. However, the output of the previous images is not the same. 

```python
# buggy section
chroms  = new_chroms.copy()
objs    = new_objs.copy()
weights = new_weights.copy()

# pop_size is not updated
```

So, where is the error? It turns out that when these lines were evalauted using both approaches we obtained different results:

```python
if t == 102 and k == 0:
	import ipdb; ipdb.set_trace()

# modified for moead_gra, use all the weights, not only a subset
scalar_child = self.evaluate_pop_scalar_method(child_objs, weights)
scalar_pop   = self.evaluate_pop_scalar_method(objs, weights)
```

If we do not use `expansion`, both `scalar_child` and `scalar_pop` have a shape of `(40, )`. However, if we do use `expansion`, their shape is `(79, )`. Thus, the value of `to_replace` will not be the same in both configurations. In order to *patch* this bug, we would have to replace these lines:

```python
# buggy section
chroms  = new_chroms[:pop_size].copy()
objs    = new_objs[:pop_size].copy()
weights = new_weights[:pop_size].copy()
```

After this change and evaluating the expansion enabled configuration, we obtained this pop, the same as the expansion disabled configuration:

![](expansion_enabled_patched_pop.png)



Obviously, the *patch* is only for debugging purposes. We need these lines in order to use expansion:

```python
chroms  = new_chroms.copy()
objs    = new_objs.copy()
weights = new_weights.copy()
```

**tl;dr** expansion will change both `chroms`, `objs`, and `weights`. As they change their size, `scalar_child` and `scalar_pop` will change too, changing the value of `to_replace`. This change impacts on the search process.

# TODO

- Implement adaptation strategy of [Deb14], currenty we only have the strategy of [Jain13].
- Improve the *duplicate detection* implemented in `is_v_in_weights()` in `expansion.py`. There should be a better way to know it a vector `v` is in a matrix `weights`.
- How can I remove `p` parameter from `expand`?



# References

- [Zhang07] *MOEA/D: A Multiobjective Evolutionary Algorithm Based on Decomposition*.
- [Jain13] *An Improved Adaptive Approach for Elitist Nondominated Sorting Genetic Algorithm for Many-Objective Optimization (A$^{2}$-NSGA-III).*
- [Deb14] *An Evolutionary Many-Obejctive Optimization Algorithm Using Reference-Point-Based Nondominated Sorting Approach, Part II: Handling Constrained and Extending to an Adaptive Approach.*