"""
expansion.py
"""
from __future__ import print_function
from rocket.plot.colors import colors, basecolors
from rocket.plot import load_rcparams
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
from scipy.spatial.distance import cdist
from numpy.linalg import norm
from expansion import get_base_simplex, move_base_simplex

  
# --- figures ---

def plot_fig_1(weights, p, save=False):
    """
    Plot figure 1
    
    Input
    center_id           int, index
    weights             (n_points, m_objs) array of weights (simplex)
    base_simplex        (m_objs, ) array with base simplex to move around weights[center_id] 
    configs             list of configurations
    """
    
    load_rcparams()
    
    fig = plt.figure()
    ax = fig.add_subplot(111, projection="3d")
    ax.set_xlabel("$x$")
    ax.set_ylabel("$y$")
    ax.set_zlabel("$z$")
    ax.set_title("Location of Base Simplex")
    
    ax.plot(weights[:, 0], weights[:, 1], weights[:, 2], c=colors[0],
        mec=basecolors["almost_black"], ls="none", marker="o", markeredgewidth=0.15)
    
    
    # unscaled base simplex
    bs = get_base_simplex(m_objs, p)
    ax.plot(bs[:, 0], bs[:, 1], bs[:, 2], c=colors[2], label="Base simplex",
        mec=basecolors["almost_black"], ls="none", marker="o", markeredgewidth=0.15)
    
    
    # set view
    ax.view_init(15, 45)
    
    # legend
    leg = ax.legend(loc="lower right")
    
    if save:
        figname = "base_simplex.png"
        fig.savefig(figname, dpi=300)
    
    plt.show()
    
    return fig, ax
    
def plot_fig_2a(ax, weights):
    """
    Plot figure 2
    
    Input
    center_id           int, index
    weights             (n_points, m_objs) array of weights (simplex)
    base_simplex        (m_objs, ) array with base simplex to move around weights[center_id] 
    configs             list of configurations
    """
    
    #fig = plt.figure()
    #ax = fig.add_subplot(111, projection="3d")
    ax.set_xlabel("$x$")
    ax.set_ylabel("$y$")
    ax.set_zlabel("$z$")
    ax.set_title("Unscaled Base Simplex")
    
    ax.plot(weights[:, 0], weights[:, 1], weights[:, 2], c=colors[0],
        mec=basecolors["almost_black"], ls="none", marker="o", markeredgewidth=0.15)
    
    
    m_objs = weights.shape[1]
    base_simplex    = np.eye(m_objs, dtype=float)
    
    ax.plot(base_simplex[:, 0], base_simplex[:, 1], base_simplex[:, 2], c=colors[2], label="Unscaled Base simplex",
        mec=basecolors["almost_black"], ls="none", marker="o", markeredgewidth=0.15)
    
    # set view
    ax.view_init(15, 45)
    
    # legend
    leg = ax.legend(loc="lower right")
    
def plot_fig_2b(ax, weights, p):
    """
    Plot figure 3
    
    Input
    center_id           int, index
    weights             (n_points, m_objs) array of weights (simplex)
    base_simplex        (m_objs, ) array with base simplex to move around weights[center_id] 
    configs             list of configurations
    """
    
    #fig = plt.figure()
    #ax = fig.add_subplot(111, projection="3d")
    ax.set_xlabel("$x$")
    ax.set_ylabel("$y$")
    ax.set_zlabel("$z$")
    ax.set_title("Translation of Extremes")
    
    ax.plot(weights[:, 0], weights[:, 1], weights[:, 2], c=colors[0],
        mec=basecolors["almost_black"], ls="none", marker="o", markeredgewidth=0.15)
    
    
    # unscaled base simplex
    m_objs = weights.shape[1]
    base    = np.eye(m_objs, dtype=float)
    bs      = base.copy()
    
    #ax.plot(bs[:, 0], bs[:, 1], bs[:, 2], c=colors[2], label="Unscaled Base simplex",
    #    mec=basecolors["almost_black"], ls="none", marker="o", markeredgewidth=0.15)
    
    
    
    factor  = (p-1)*(1.0/p)             # scale factor 
    print ("factor", factor)
    letters = ("a", "b", "c")

    palete = (
            ("#4163B3", "#87CAF1"),
            ("#85227C", "#D588CD"))
            
    # from
    x, y, z = base[0]
    ax.text(x, y, z, "$%s$" % letters[0], size=16)

    for i in range(1, m_objs):
        
        # from
        x, y, z = base[i]
        ax.text(x, y, z, "$%s$" % letters[i], size=16)
        
        d = base[0] - base[i]           # translation (direction) vector
        v = factor*d                    # scaled translation vector
        base[i] = base[i] + v           # move vector
        
        # to
        x, y, z = base[i]
        ax.text(x, y, z, "$t_{%s}$" % letters[i], size=16)
        
        print ("i", i)
        print("src base[0]:  %s" % base[0])
        print("dst base[%i]: %s" % (i, base[i]))
        print("direction vector   (d): %s" % d)
        print("translation vector (v): %s" % v)
        print("translated  vector:     %s" % base[i])
        print("")
        
        d = d.reshape((1, m_objs))
        v = v.reshape((1, m_objs))
        t = base[i].reshape((1, m_objs))
        
        ax.plot(bs[[i], 0], bs[[i], 1], bs[[i], 2], c=palete[i-1][0], label="$%s$" % letters[i],
            mec=basecolors["almost_black"], ls="none", marker="o", markeredgewidth=0.15)
    
        ax.plot(t[:, 0], t[:, 1], t[:, 2], c=palete[i-1][1], label="$t_{%s}$" % letters[i],
            mec=basecolors["almost_black"], ls="none", marker="o", markeredgewidth=0.15)
    
    
    
    # set view
    ax.view_init(15, 45)
    
def plot_fig_3a(ax, weights, p, scale):
    """
    Plot figure 3
    
    Input
    center_id           int, index
    weights             (n_points, m_objs) array of weights (simplex)
    base_simplex        (m_objs, ) array with base simplex to move around weights[center_id] 
    configs             list of configurations
    """
    
    #fig = plt.figure()
    #ax = fig.add_subplot(111, projection="3d")
    ax.set_xlabel("$x$")
    ax.set_ylabel("$y$")
    ax.set_zlabel("$z$")
    ax.set_title("Additional Scale (%.2f)" % scale)
    
    ax.plot(weights[:, 0], weights[:, 1], weights[:, 2], c=colors[0],
        mec=basecolors["almost_black"], ls="none", marker="o", markeredgewidth=0.15)
    
    
    # unscaled base simplex
    m_objs = weights.shape[1]
    base    = np.eye(m_objs, dtype=float)
    bs      = base.copy()
    
    #ax.plot(bs[:, 0], bs[:, 1], bs[:, 2], c=colors[2], label="Unscaled Base simplex",
    #    mec=basecolors["almost_black"], ls="none", marker="o", markeredgewidth=0.15)
    
    
    
    factor  = (p-1)*(1.0/p)             # scale factor 
    print ("factor", factor)
    letters = ("a", "b", "c")

    factor = (p-1 + (1-scale))*(1.0/p)

    palete = (
            ("#4163B3", "#87CAF1"),
            ("#85227C", "#D588CD"))
            
    # from
    x, y, z = base[0]
    ax.text(x, y, z, "$%s$" % letters[0], size=16)

    for i in range(1, m_objs):
        
        # from
        x, y, z = base[i]
        ax.text(x, y, z, "$%s$" % letters[i], size=16)
        
        d = base[0] - base[i]           # translation (direction) vector
        v = factor*d                    # scaled translation vector
        base[i] = base[i] + v           # move vector
        
        # to
        x, y, z = base[i]
        ax.text(x, y, z, "$t_{%s}$" % letters[i], size=16)
        
        print ("i", i)
        print("src base[0]:  %s" % base[0])
        print("dst base[%i]: %s" % (i, base[i]))
        print("direction vector   (d): %s" % d)
        print("translation vector (v): %s" % v)
        print("translated  vector:     %s" % base[i])
        print("")
        
        d = d.reshape((1, m_objs))
        v = v.reshape((1, m_objs))
        t = base[i].reshape((1, m_objs))
        
        ax.plot(bs[[i], 0], bs[[i], 1], bs[[i], 2], c=palete[i-1][0], label="$%s$" % letters[i],
            mec=basecolors["almost_black"], ls="none", marker="o", markeredgewidth=0.15)
    
        ax.plot(t[:, 0], t[:, 1], t[:, 2], c=palete[i-1][1], label="$t_{%s}$" % letters[i],
            mec=basecolors["almost_black"], ls="none", marker="o", markeredgewidth=0.15)
    
    
    
    # set view
    ax.view_init(15, 45)
    
def plot_fig_2(weights, p, save=False):
    
    load_rcparams((12, 5))
    
    fig = plt.figure()
    axes = (
        fig.add_subplot(121, projection="3d"),
        fig.add_subplot(122, projection="3d"),
        )
   
    plot_fig_2a(axes[0], weights)
    plot_fig_2b(axes[1], weights, p)
    
    if save:
        figname = "construction.png"
        fig.savefig(figname, dpi=300)
    
    plt.show()
    
def plot_fig_3(weights, p, save=False):
    
    load_rcparams((19, 5))
    
    fig = plt.figure()
    axes = (
        fig.add_subplot(131, projection="3d"),
        fig.add_subplot(132, projection="3d"),
        fig.add_subplot(133, projection="3d"),
        )
   
    plot_fig_3a(axes[0], weights, p, scale=0.25)
    plot_fig_3a(axes[1], weights, p, scale=0.5)
    plot_fig_3a(axes[2], weights, p, scale=0.75)
    
    if save:
        figname = "scale.png"
        fig.savefig(figname, dpi=300)
    
    plt.show()


def plot_configs(ax, center_id, scale, weights, base_simplex, configs, save=False):
    """
    Plot a list of configurations over a simplex.
    
    Input
    center_id           int, index
    scale       Scale factor from [Jain12]
    weights             (n_points, m_objs) array of weights (simplex)
    base_simplex        (m_objs, ) array with base simplex to move around weights[center_id] 
    configs             list of configurations
    """
    
    #fig = plt.figure()
    #ax = fig.add_subplot(111, projection="3d")
    ax.set_xlabel("$x$")
    ax.set_ylabel("$y$")
    ax.set_zlabel("$z$")
    ax.set_title("Move around %d (scale: %.2f)" % (center_id, scale))
    
    # for callback
    lined = {}
    
    ax.plot(weights[:, 0], weights[:, 1], weights[:, 2], c=colors[0],
        mec=basecolors["almost_black"], ls="none", marker="o", markeredgewidth=0.15)
    
    label = "Base simplex"
    lined[label], = ax.plot(base_simplex[:, 0], base_simplex[:, 1], base_simplex[:, 2], c=colors[2], label=label,
        mec=basecolors["almost_black"], ls="none", marker="o", markeredgewidth=0.15)
    
    palete = ("#4163B3", "#77133A", "#652D7B")
    
    # translations
    for i, config in enumerate(configs):
        label = "Config %d" % (i+1, )
        lined[label], = ax.plot(config[:, 0], config[:, 1], config[:, 2], c=palete[i], label=label,
            mec=basecolors["almost_black"], ls="none", marker="o", markeredgewidth=0.15)
    
    # plot ids
    for i, w in enumerate(weights):
        x, y, z = w
        ax.text(x, y, z, str(i), color="#404040")
    
    # set view
    ax.view_init(15, 45)
    
    # legend
    leg = ax.legend(loc="lower right")
    
    

def plot_fig_4(center_id, save=False):
    
    load_rcparams((19, 5))
    
    # check result
    fig = plt.figure()
    axes = (fig.add_subplot(141, projection="3d"),
            fig.add_subplot(142, projection="3d"),
            fig.add_subplot(143, projection="3d"),
            fig.add_subplot(144, projection="3d"),
            )
            
    for i, scale in enumerate((0.25, 0.50, 0.75, 1.00)):
        
        # create base simplex
        base_simplex = get_base_simplex(m_objs, p, scale)
        
        # get all valid configurations
        configs     = move_base_simplex(center_id, weights, base_simplex)
        
        plot_configs(axes[i], center_id, scale, weights, base_simplex, configs, save=False)

    plt.subplots_adjust(
        left  = 0.02,   # the left side of the subplots of the figure
        right = 0.97,    # the right side of the subplots of the figure
        wspace = 0.05,   # the amount of width reserved for blank space between subplots
    ) 


    if save:
        figname = "configs_%d.png" % center_id
        fig.savefig(figname, dpi=300)

    
if __name__ == "__main__":
    
    weights = np.genfromtxt("weights_m_3_21.txt")
    m_objs  = weights.shape[1]
    p       = 5

    plot_fig_1(weights, p, save=False)

    plot_fig_2(weights, p, save=False)
    
    plot_fig_3(weights, p, save=False)
    
    plot_fig_4(center_id=0, save=False)
    plot_fig_4(center_id=5, save=False)
    plot_fig_4(center_id=12, save=False)
    plot_fig_4(center_id=18, save=False)
    plot_fig_4(center_id=20, save=False)
    plot_fig_4(center_id=3, save=False)

    
    
