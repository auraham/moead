# test_moea_config.py
from __future__ import print_function
from rocket.moea import AMOEAD_GRA
from rocket.helpers import get_n_genes, get_bounds
from rocket.labs import get_experiment_path
import numpy as np
import os

# define experiment output path
script_path = os.path.dirname(os.path.abspath(__file__))
output_path = get_experiment_path(script_path, name="sample")

if __name__ == "__main__":
    
    weights = np.genfromtxt("weights_m_2_240.txt")      # large set
    weights = np.genfromtxt("weights_m_2_40.txt")       # small set
    pop_size, m_objs = weights.shape
    
    mop_name = "t2"
    mop_name = "dtlz1"
    n_genes, wfg_k, wfg_l = get_n_genes(mop_name, m_objs)
    
    params = {
        "name"              : "amoead-gra",                                         # name of moea
        "label"             : "AMOEA/D-GRA",                                        # label of moea
        "runs"              : 1,                                                    # number of independent runs
        "iters"             : 400,                                                  # number of generations
        "m_objs"            : m_objs,                                               # number of objectives
        "pop_size"          : pop_size,                                             # number of individuals
        "params_mop"        : {"name": mop_name, "k": wfg_k, "degrees":180},        # mop parameters
        "params_crossover"  : {"name": "moead-gra_crossover", "prob": 0.8},        # crossover parameters
        "params_mutation"   : {"name": "moead-gra_mutation",  "prob": 0.1},        # mutation parameters
        "params_stats"      : {"output_path": output_path},                         # stats parameters
        "verbose"           : True,                                                 # log verbosity
        
        # MOEA/D params
        "neigh_size"        : 20,
        "pop_weights"       : weights,
        "params_form"       : {"name": "te"},
        
        # MOEA/D-GRA
        "moead-gra_poi_delta": 10,
    }
    
    moea = AMOEAD_GRA(params)
    #chroms, objs, fig, ax = moea.evaluate(plot_output=True, plot_output_name="amoead_gra.png", plot_run=True)
    chroms, objs, fig, ax = moea.evaluate(plot_output=True, plot_output_name="", plot_run=True)
    
    
