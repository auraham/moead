# __init__.py
from .nsga2.nsga2           import NSGA2
from .nsga3.nsga3           import NSGA3
from .moead.moead           import MOEAD
from .moead_gra.moead_gra   import MOEAD_GRA

# proposed/modified moeas
from .moead_batch.moead_batch import MOEAD_Batch
from .moead_dc.moead_dc import MOEAD_DC
from .amoead.amoead import AMOEAD
from .amoead.amoead2 import AMOEAD2
from .amoead.amoead3 import AMOEAD3
#from .amoead.amoead4 import AMOEAD4
from .amoead.amoead5 import AMOEAD5
from .amoead.amoead6 import AMOEAD6
from .amoead.amoead7 import AMOEAD7
from .amoead.amoead8 import AMOEAD8
from .amoead.amoead9 import AMOEAD9
from .amoead.amoead10 import AMOEAD10
from .amoead.amoead11 import AMOEAD11

# not finished
from .amoead_gra.amoead_gra import AMOEAD_GRA
