# About this package

This package contains methods for Pareto-dominance comparison:

- `dominance.py` contains the classic Pareto-dominance comparison




# TODO

- Implement PISA's version in `dominance.py` (just for comparison)
- Add relaxed forms of dominance here.
- Add referenes



# References