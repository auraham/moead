# moead_batch.py
from __future__ import print_function
import numpy as np
from numpy.random import RandomState
from rocket.moea import MOEAD

class MOEAD_Batch(MOEAD):
    
    def __init__(self, params):
        
        MOEAD.__init__(self, params)
        
        
    def run(self, chroms, seed, run_id):
        """
        Independent run
        """
        
        self.rand   = RandomState(seed)
        
        pop_size    = self.pop_size
        weights     = self.pop_weights
        neighbors   = self.create_neighborhood(weights)
        objs        = self.evaluate_pop_mop(chroms)
        
        # for moead_batch only
        batch       = [0 for l in range(self.pop_size)]     # each entry is a batch
        
        t = 1
        self.stats(chroms, objs, t, run_id)
        
        t = 2
        while t <= self.iters:
            
            for i in range(pop_size):
                
                neigh = neighbors[i]
                
                neigh_objs = objs[neigh]
                
                neigh_weights = weights[neigh]
                
                child = self.genetic_operations(i, neigh, chroms)
                
                child = self.check_pop_bounds(child)            
                
                child_objs = self.evaluate_pop_mop(child)                   # no me convencen los nuevos nombres con pop
                
                scalar_child = self.evaluate_pop_scalar_method(child_objs, neigh_weights)
                scalar_neigh = self.evaluate_pop_scalar_method(neigh_objs, neigh_weights)
                
                to_replace = neigh[scalar_child <= scalar_neigh]
                
                # MOEA/D update pop
                #chroms[to_replace, :] = child.copy()
                #objs[to_replace, :] = child_objs.copy()
            
                # MOEA/D-batch update
                batch[i] = (to_replace, child, child_objs)
            
            
            # MOEA/D-batch update pop
            for i in range(self.pop_size):
                
                to_replace, child, child_objs = batch[i]
                
                chroms[to_replace, :] = child.copy()
                objs[to_replace, :] = child_objs.copy()
                
                # clean up
                batch[i] = 0
            
            
            # @note: we output messages AFTER evaluating child on self.evaluate_pop_mop
            # this way, the value of self.fevals is the expected value
            if self.verbose:            # @todo: add log support here
                if t%100 == 0:
                    print ("t:", t, " fevals:", self.fevals)
            
            
            self.stats(chroms, objs, t, run_id)
            
            t+=1
            
           
        return chroms.copy(), objs.copy()
