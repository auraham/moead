# About this package

This package contains a modified version of `MOEAD` (`moea.moead/moead.py`).



## MOEA/D update pop

```python
def run(self, chroms, seed, run_id):
    t = 2
    while t <= self.iters:

        for i in range(pop_size):

            neigh = neighbors[i]

            neigh_objs = objs[neigh]

            neigh_weights = weights[neigh]

            child = self.genetic_operations(i, neigh, chroms)

            child = self.check_pop_bounds(child)            

            child_objs = self.evaluate_pop_mop(child)

            scalar_child = self.evaluate_pop_scalar_method(child_objs, neigh_weights)
            scalar_neigh = self.evaluate_pop_scalar_method(neigh_objs, neigh_weights)

            # update pop
            to_replace = neigh[scalar_child <= scalar_neigh]
            chroms[to_replace, :] = child.copy()
            objs[to_replace, :] = child_objs.copy()
```



## MOEA/D-Batch update pop

```python
def run(self, chroms, seed, run_id):
    t = 2
    while t <= self.iters:

        for i in range(pop_size):

            neigh = neighbors[i]

            neigh_objs = objs[neigh]

            neigh_weights = weights[neigh]

            child = self.genetic_operations(i, neigh, chroms)

            child = self.check_pop_bounds(child)            

            child_objs = self.evaluate_pop_mop(child)

            scalar_child = self.evaluate_pop_scalar_method(child_objs, neigh_weights)
            scalar_neigh = self.evaluate_pop_scalar_method(neigh_objs, neigh_weights)

            to_replace = neigh[scalar_child <= scalar_neigh]

            # MOEA/D update pop
            #chroms[to_replace, :] = child.copy()
            #objs[to_replace, :] = child_objs.copy()

            # MOEA/D-batch update
            batch[i] = (to_replace, child, child_objs)


        # MOEA/D-batch update pop
        for i in range(self.pop_size):

            to_replace, child, child_objs = batch[i]

            chroms[to_replace, :] = child.copy()
            objs[to_replace, :] = child_objs.copy()

            # clean up
            batch[i] = 0
```



## Difference

MOEA/D (`moead.py`) updates `chroms` and `objs` on each iteration:

```python
for i in range(pop_size):
    # ...

    # update pop
    to_replace = neigh[scalar_child <= scalar_neigh]
    chroms[to_replace, :] = child.copy()
    objs[to_replace, :] = child_objs.copy()
```


MOEA/D-Batch (`moead_batch.py`) holds a list of replacements (`batch`). After this list is complete, `chroms` and `objs` are updated.

```python
for i in range(pop_size):
    # ...
    
    # MOEA/D-batch update
    batch[i] = (to_replace, child, child_objs)


# MOEA/D-batch update pop
for i in range(self.pop_size):

    to_replace, child, child_objs = batch[i]
    chroms[to_replace, :] = child.copy()
    objs[to_replace, :] = child_objs.copy()

    # clean up
    batch[i] = 0
```

