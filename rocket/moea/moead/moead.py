# moead.py
from __future__ import print_function
from rocket.moea.moea_base import MOEA
from rocket.forms import evaluate_form
from rocket.genetics import crossover, mutation
from scipy.spatial.distance import cdist
import numpy as np
from numpy.random import RandomState

class MOEAD(MOEA):
    
    def __init__(self, params):
        
        MOEA.__init__(self, params)
        
        # @todo: add checks
        self.pop_weights    = params["pop_weights"]
        self.neigh_size     = params["neigh_size"]
        self.params_form    = params["params_form"]

    
    def create_neighborhood(self, weights):
        
        T           = self.neigh_size
        dist_matrix = cdist(weights, weights)
        
        by_row      = 1
        sortd_ids   = dist_matrix.argsort(axis=by_row)
        
        neighbors   = sortd_ids[:, :T]
        
        return neighbors.copy()
    
    
    def parent_selection(self, i, neighbors, chroms):
        """
        Select two parents (indices) from neighbors. These indices are different of the i index.
        """
        
        selected = np.array([i, i])
        
        for ind in range(2):
            
            while True:
                selected[ind] = self.rand.permutation(neighbors)[0]
                if selected[ind] != i:                                  # @todo: add parent_selection() from m2m to avoid selecting the same parents
                    break
                    
        
        p1 = chroms[selected[0]]
        p2 = chroms[selected[1]]
        
        parents = (p1, p2)
        
        return parents
    
    
    def genetic_operations(self, i, neighbors, chroms):
        
        lb = self.lbound
        ub = self.ubound
        
        # crossover
        parents = self.parent_selection(i, neighbors, chroms)
        c1, __  = crossover(parents, self.lbound, self.ubound, self.params_crossover, self.rand) 
        
        # mutation
        parents = (c1, )
        c1,    = mutation(parents, self.lbound, self.ubound, self.params_mutation, self.rand)
    
        return c1.copy()
        
        
    def evaluate_pop_scalar_method(self, objs, weights):
        
        z_ideal = self.z_ideal.flatten()
        z_nadir = self.z_nadir.flatten()
        
        input_objs   = objs.copy()            # objs to be evaluated
        flag_reshape = False                    
        
        # if objs contains a single obj vector
        # we need to reshape it before calling form
        if objs.ndim == 1:
            flag_reshape = True
            
            rows = weights.shape[0]
            cols = len(objs)
            
            input_objs = input_objs.reshape((1, cols)).repeat(rows, axis=0)
        
        
        
        # evaluate input_objs on form
        #scalar = form(input_objs, weights, z_ideal, z_nadir)#, params)  # @todo: how to add params? self.form_params?
        scalar = evaluate_form(input_objs, weights, z_ideal, z_nadir, self.params_form)
        #, params)  # @todo: how to add params? self.form_params?
        
        
        # if this flag is activated (that means that objs is a single vector)
        # we need to reshape it before returning it
        #if flag_reshape:
        #    return scalar.copy().flatten()
        
        
        return scalar.copy().flatten()
    
    
    def run(self, chroms, seed, run_id):
        """
        Independent run
        """
        
        self.rand   = RandomState(seed)
        
        pop_size    = self.pop_size
        weights     = self.pop_weights
        neighbors   = self.create_neighborhood(weights)
        objs        = self.evaluate_pop_mop(chroms)
        
        t = 1
        self.stats(chroms, objs, t, run_id)
        
        t = 2
        while t <= self.iters:
            
            for i in range(pop_size):
                
                neigh = neighbors[i]
                
                neigh_objs = objs[neigh]
                
                neigh_weights = weights[neigh]
                
                child = self.genetic_operations(i, neigh, chroms)
                
                child = self.check_pop_bounds(child)            
                
                child_objs = self.evaluate_pop_mop(child)                   # no me convencen los nuevos nombres con pop
                
                scalar_child = self.evaluate_pop_scalar_method(child_objs, neigh_weights)
                scalar_neigh = self.evaluate_pop_scalar_method(neigh_objs, neigh_weights)
                
                to_replace = neigh[scalar_child <= scalar_neigh]
                
                chroms[to_replace, :] = child.copy()
                objs[to_replace, :] = child_objs.copy()
            
            # @note: we output messages AFTER evaluating child on self.evaluate_pop_mop
            # this way, the value of self.fevals is the expected value
            if self.verbose:            # @todo: add log support here
                if t%100 == 0:
                    print ("t:", t, " fevals:", self.fevals)
            
            
            self.stats(chroms, objs, t, run_id)
            
            t+=1
            
           
        return chroms.copy(), objs.copy()
