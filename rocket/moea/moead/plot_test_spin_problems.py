# plot_test_spin_problems.py
from __future__ import print_function
from rocket.fronts import get_real_front
from rocket.plot import plot_pops
from rocket.helpers import rotate_matrix
import numpy as np
import os

if __name__ == "__main__":
    
    
    output_path = "test_spin"
    m_objs = 3
    theta = np.pi
    
    # (mop_name, lower_lims, upper_lims)
    problems = (
            ("dtlz1", (0, 0, 0), (0.5, 0.5, 0.5)),
            #("dtlz2", (0, 0, 0), (1, 1, 1)),  
            #("dtlz3", (0, 0, 0), (1, 1, 1)),  
            #("dtlz4", (0, 0, 0), (1, 1, 1)),  
            
            ("dtlz1-spin", (0, 0, 0), (0.5, 0.5, 0.5)),
            #("dtlz2-spin", (0, 0, 0), (1, 1, 1)),  
            #("dtlz3-spin", (0, 0, 0), (1, 1, 1)),  
            #("dtlz4-spin", (0, 0, 0), (1, 1, 1)),  
             
            
            #("wfg1", (0, 0, 0), (7, 7, 7)),
            #("wfg2", (0, 0, 0), (7, 7, 7)),
            #("wfg3", (0, 0, 0), (7, 7, 7)),
            #("wfg4", (0, 0, 0), (7, 7, 7)),
            #("wfg5", (0, 0, 0), (7, 7, 7)),
            #("wfg6", (0, 0, 0), (7, 7, 7)),
            #("wfg7", (0, 0, 0), (7, 7, 7)),
            #("wfg8", (0, 0, 0), (7, 7, 7)),
            ("wfg9", (0, 0, 0), (7, 7, 7)),
            
            #("wfg1-spin", (0, 0, 0), (7, 7, 7)),
            #("wfg2-spin", (0, 0, 0), (7, 7, 7)),
            #("wfg3-spin", (0, 0, 0), (7, 7, 7)),
            #("wfg4-spin", (0, 0, 0), (7, 7, 7)),
            #("wfg5-spin", (0, 0, 0), (7, 7, 7)),
            #("wfg6-spin", (0, 0, 0), (2, 4, 6)),
            #("wfg7-spin", (0, 0, 0), (2, 4, 6)),
            #("wfg8-spin", (0, 0, 0), (2, 4, 6)),
            ("wfg9-spin", (0, 0, 0), (7, 7, 7)),
        )
        
    alg_name = "moead-pbi"
    alg_label = "MOEA/D-PBI"
    
    # (alg_name, alg_label)
    moeas = (
        ("moead-pbi", "MOEA/D-PBI"),
        ("nsga2", "NSGA-II"),
        )
        
    for mop_name, lower_lims, upper_lims in problems:
        
        pops = []
        labels = []
        
        title = mop_name.upper()
        real_front, p = get_real_front(mop_name, m_objs, params={"degrees":180})
        
        
        # reverted spin
        rpops   = []
        rlabels = []
        rfront  = rotate_matrix(real_front, theta)
        
        for alg_name, alg_label in moeas:
        
            filename = "objs_%s_m_%d_run_0_final.txt" % (alg_name, m_objs)
            filepath = os.path.join(output_path, mop_name, alg_name, "pops", filename)

            objs = np.genfromtxt(filepath)
            
            pops.append(objs)
            labels.append(alg_label)
            
            # reverted spin
            robjs = rotate_matrix(objs, theta)
            rpops.append(robjs)
            rlabels.append("%s*" % (alg_label, ))
        
                
        plot_pops(pops, labels, real_front=real_front, p=p, title=title, lower_lims=lower_lims, upper_lims=upper_lims)
         
        
        # this block plot the un-spinned objs
        if mop_name.count("spin") > 0:
            plot_pops(rpops, rlabels, real_front=rfront, p=p, title=title+"*", lower_lims=lower_lims, upper_lims=upper_lims)
          
    
