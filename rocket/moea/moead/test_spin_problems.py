# test_moea_config.py
from __future__ import print_function
from rocket.moea import MOEAD, NSGA2
from rocket.helpers import get_n_genes, get_bounds
from rocket.labs import get_experiment_path
import numpy as np
import os

# define experiment output path
script_path = os.path.dirname(os.path.abspath(__file__))
output_path = get_experiment_path(script_path, name="test_spin")

    
if __name__ == "__main__":
    
    weights     = np.genfromtxt("weights_m_3.txt")
    pop_size    = weights.shape[0]
    m_objs      = 3
    
    problems = (
            "dtlz1", "dtlz2", "dtlz3", "dtlz4", 
            "dtlz1-spin", "dtlz2-spin", "dtlz3-spin", "dtlz4-spin", 
            "wfg1", "wfg2", "wfg3", "wfg4", "wfg5", "wfg6", "wfg7", "wfg8", "wfg9",
            "wfg1-spin", "wfg2-spin", "wfg3-spin", "wfg4-spin", "wfg5-spin", "wfg6-spin", "wfg7-spin", "wfg8-spin", "wfg9-spin"
        )
    
    for mop_name in problems:
        
        n_genes, wfg_k, _wfg_l = get_n_genes(mop_name, m_objs)
    
        params = {
            "name"              : "moead-pbi",                                              # name of moea
            "label"             : "MOEA/D-PBI",                                             # label of moea
            "runs"              : 1,                                                        # number of independent runs
            "iters"             : 400,                                                      # number of generations
            "m_objs"            : m_objs,                                                   # number of objectives
            "pop_size"          : pop_size,                                                 # number of individuals
            "params_mop"        : {"name": mop_name, "k": wfg_k, "degrees":180},            # mop parameters
            "params_crossover"  : {"name": "sbx", "prob": 1.0, "eta": 20},                  # crossover parameters
            "params_mutation"   : {"name": "polymutation", "prob": 1./n_genes, "eta": 15},  # mutation parameters
            "params_stats"      : {"output_path": output_path},                             # stats parameters
            "verbose"           : True,                                                     # log verbosity
            
            
            # MOEAD params
            "neigh_size"        : 20,
            "pop_weights"       : weights,
            "params_form"       : {"name": "pbi", "pbi-theta": 5},
        }
        
        #moea = MOEAD(params)
        #chroms, objs, fig, ax = moea.evaluate(plot_output=False)
        
        
        # nsga2
        params = {
            "name"              : "nsga2",                                                  # name of moea
            "label"             : "NSGA-II",                                                # label of moea
            "runs"              : 1,                                                        # number of independent runs
            "iters"             : 400,                                                      # number of generations
            "m_objs"            : m_objs,                                                   # number of objectives
            "pop_size"          : pop_size+1,                                               # number of individuals
            "params_mop"        : {"name": mop_name, "k": wfg_k, "degrees":180},            # mop parameters
            "params_crossover"  : {"name": "sbx", "prob": 1.0, "eta": 20},                  # crossover parameters
            "params_mutation"   : {"name": "polymutation", "prob": 1./n_genes, "eta": 15},  # mutation parameters
            "params_stats"      : {"output_path": output_path},                             # stats parameters
            "verbose"           : True,                                                     # log verbosity
        }    

        moea = NSGA2(params)
        chroms, objs, fig, ax = moea.evaluate(plot_output=False)
