# About this module



# Structure

```
moea
	nsga2
		nsga2.py  # add support for running from command line
		...
	nsga3
		nsga3.py
		...
	moead
		moead.py
		...
		
	moea_base.py
	test_moea_config.py
```



# Output structure

Each experiment has this structure:

```
PREFIX_MOP-NAME_m_OBJ
    MOEA-NAME        (1)
        performance    (2)
        pops           (3)
```

where 

- `PREFIX` is an optional parameter to group directories. This prefix is useful, for instance, when you are interested on evaluate the same algorithms with 2, 3, 5 objectives and group that outputs. In that case, three directories will be created using the same prefix, for instance `"experiment1"`:
```
experiment1_
```


```
# actual, un grupo por problema
output_dtlz1_m_3
    moead-ipbi5        
        performance    
        pops
    nsga2
        performance
        pops
        
output_dtlz2_m_3
    moead-ipbi5        
        performance    
        pops
    nsga2
        performance
        pops
```


```
# nueva, un grupo por experimento
output
    dtlz1
        moead-ipbi5
            performance
            pops
        nsga2
            performance
            pops
    dtlz2
        moead-ipbi5
            performance
            pops
        nsga2
            performance
            pops
    
```


where

1. This directory contains the popula




# How to add a MOEA





# Conventions

## About pop functions

As a conventions, the functions with `pop` in their name, such as `evaluate_pop`, `check_pop_bounds`, etc., allows us to use either a matrix or vector as input. For instance,

```

```