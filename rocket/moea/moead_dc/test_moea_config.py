# test_moea_config.py
from __future__ import print_function
from rocket.moea import MOEAD_DC
from rocket.helpers import get_n_genes, get_bounds
from rocket.labs import get_experiment_path
import numpy as np
import os

# define experiment output path
script_path = os.path.dirname(os.path.abspath(__file__))
output_path = get_experiment_path(script_path, name="sample")

    
if __name__ == "__main__":
    
    weights = np.genfromtxt("weights_m_3.txt")
    weights = np.genfromtxt("weights_m_3_d.txt")
    pop_size = weights.shape[0]
    
    mop_name = "dtlz1-spin"
    m_objs = 3
    n_genes, wfg_k, wfg_l = get_n_genes(mop_name, m_objs)
    
    params = {
        "name"              : "moead_dc-pbi",                                           # name of moea
        "label"             : "MOEA/D-DC-PBI",                                          # label of moea
        "runs"              : 1,                                                        # number of independent runs
        "iters"             : 400,                                                      # number of generations
        "m_objs"            : m_objs,                                                   # number of objectives
        "pop_size"          : pop_size,                                                 # number of individuals
        "params_mop"        : {"name": mop_name, "k": wfg_k, "degrees":180},            # mop parameters
        "params_crossover"  : {"name": "sbx", "prob": 1.0, "eta": 20},                  # crossover parameters
        "params_mutation"   : {"name": "polymutation", "prob": 1./n_genes, "eta": 15},  # mutation parameters
        "params_stats"      : {"output_path": output_path, "step": None},               # stats parameters
        "verbose"           : True,                                                     # log verbosity
        
        
        # MOEAD params
        "neigh_size"        : 20,
        "pop_weights"       : weights,
        "params_form"       : {"name": "pbi", "pbi-theta": 5},
        
        # MOEAD-DC params
        "moead-dc_forms"    : (
                                {"name": "pbi", "pbi-theta": 5},
                                {"name": "asf", "asf-z_ref": np.array([0, 0, 0])},
                            )
    }
    
    moea = MOEAD_DC(params)
    chroms, objs, fig, ax = moea.evaluate(plot_output=True)
    
    
