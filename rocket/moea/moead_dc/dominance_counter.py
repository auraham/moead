# dominance_counter.py
from __future__ import print_function
from rocket.moea.dominance import compare_min, A_DOM_B

def dominance_counter(child_objs, objs, to_replace):
    
    a = child_objs.flatten()
    
    dominated = 0
    total = len(to_replace)
    
    if total == 0:
        return 0
        
    for i in to_replace:
        
        b = objs[i].flatten()
        
        if compare_min(a, b) == A_DOM_B:
            dominated += 1

    ratio = (dominated*1.0)/total

    return ratio
            
