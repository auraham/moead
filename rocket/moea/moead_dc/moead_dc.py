# moead_dc.py
from __future__ import print_function
import numpy as np
from numpy.random import RandomState
from rocket.moea import MOEAD
from rocket.forms import evaluate_form
from rocket.moea.moead_dc.dominance_counter import dominance_counter
from numpy.random import RandomState

class MOEAD_DC(MOEAD):
    
    def __init__(self, params):
        
        MOEAD.__init__(self, params)
        
        
        self.forms       = params["moead-dc_forms"]
        self.form_labels = (params["name"] for params in self.forms)
    
    
    def evaluate_pop_scalar_method(self, objs, weights, params_form):
        
        z_ideal = self.z_ideal.flatten()
        z_nadir = self.z_nadir.flatten()
        
        input_objs   = objs.copy()            # objs to be evaluated
        flag_reshape = False                    
        
        # if objs contains a single obj vector
        # we need to reshape it before calling form
        if objs.ndim == 1:
            flag_reshape = True
            
            rows = weights.shape[0]
            cols = len(objs)
            
            input_objs = input_objs.reshape((1, cols)).repeat(rows, axis=0)
        
        # evaluate input_objs on form
        scalar = evaluate_form(input_objs, weights, z_ideal, z_nadir, params_form)
        
        return scalar.copy().flatten()
    
    
    def get_best_form(self, batch, objs):
        
        
        best_k  = 0
        l_forms = len(batch)
        dc      = np.zeros((l_forms, ))     # dominance counter array
    
        for i in range(l_forms):
            
            to_replace, child, child_objs = batch[i]
            
            dc[i] = dominance_counter(child_objs, objs, to_replace)
        
        best_k = dc.argmax()
        
        # @todo: if sum(dc) == 0: return -1
        
        return best_k
        
        
    def run(self, chroms, seed, run_id):
        """
        Independent run
        """
        
        self.rand   = RandomState(seed)
        
        
        # debug
        import ipdb; ipdb.set_trace()
        chroms      = np.genfromtxt("better_chroms.txt")
        self.rand   = RandomState(100)
        
        pop_size    = self.pop_size
        weights     = self.pop_weights
        neighbors   = self.create_neighborhood(weights)
        objs        = self.evaluate_pop_mop(chroms)
        
        # for moead_dc only
        forms       = self.forms
        l_forms     = len(forms)
        batch       = [0 for p in range(l_forms)]     # (l_forms, )
        last_k      = 0
        
        
        t = 1
        self.stats(chroms, objs, t, run_id)
        
        t = 2
        while t <= self.iters:
            
            for i in range(pop_size):
                
                neigh = neighbors[i]
                
                neigh_objs = objs[neigh]
                
                neigh_weights = weights[neigh]
                
                child = self.genetic_operations(i, neigh, chroms)
                
                child = self.check_pop_bounds(child)            
                
                child_objs = self.evaluate_pop_mop(child)                   # no me convencen los nuevos nombres con pop
                
                
                # for moead_dc only: eval forms
                #import ipdb; ipdb.set_trace()
                for j, form in enumerate(forms):
                    
                    scalar_child = self.evaluate_pop_scalar_method(child_objs, neigh_weights, form)
                    scalar_neigh = self.evaluate_pop_scalar_method(neigh_objs, neigh_weights, form)
                    
                    to_replace = neigh[scalar_child <= scalar_neigh]
                
                    batch[j] = (to_replace, child, child_objs)
                
                k = self.get_best_form(batch, objs)
                
                """
                if k == -1:
                    k = last_k      # para evitar seleccionar una form que empeore la busqueda, seleccionamos la anterior
                """
                
                
                # actual replacement
                to_replace, child, child_objs = batch[k]
                chroms[to_replace, :]   = child.copy()
                objs[to_replace, :]     = child_objs.copy()
                last_k                  = k
                #form_counter[self.form_labels[k]] += 1
            
            # -----------------------
            
            # @note: we output messages AFTER evaluating child on self.evaluate_pop_mop
            # this way, the value of self.fevals is the expected value
            if self.verbose:            # @todo: add log support here
                if t%100 == 0:
                    print ("t:", t, " fevals:", self.fevals)
            
            
            self.stats(chroms, objs, t, run_id)
            
            t+=1
            
           
        return chroms.copy(), objs.copy()
