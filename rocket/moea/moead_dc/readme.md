# About this package

This package contains a modified version of `MOEAD` (`moea.moead/moead.py`).

# Goal

Replicar los resultados de `research_emo` rev 142 usado la siguiente configuración:

- *Baselines* MOEA/D-PBI, IPBI, ASF
- *Proposal* MOEA/D-(PBI, ASF)
- *Problem* DTLZ1-spin
- *Weights* Set D

Resultados:
- MOEA/D-PBI obtuvo una mala distribución, las soluciones se concentraron en un costado del frente
- MOEA/D-ASF obtuvo la mejor distribución
- MOEA/D-(PBI,ASF) logró obtener una buena distribución, similar a la de MOEA/D-ASF, ie, parece que sí logró identificar la mejor formulación.


# TODO

- Renombrar los parametros con el prefijo `moead`:

```
"neigh_size"        : 20,
"pop_weights"       : weights,
"params_form"       : {"name": "pbi", "pbi-theta": 5}, 
```

- En `moead_dc.py`, dejar `params_form` a `None` y agregar una validacion en `moead.py`


