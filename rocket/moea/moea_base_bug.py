# moea_base.py
from __future__ import print_function
from rocket.helpers import get_n_genes, get_bounds
from rocket.labs import prepare_experiment_dirs
from rocket.problems import evaluate_mop
from rocket.plot import plot_pops
from rocket.fronts import get_real_front

from numpy.random import RandomState
import numpy as np
from distutils.dir_util import mkpath
import os
import logging

from timeit import default_timer as timer


class MOEA(object):
    
    def __init__(self, params):
        
        # @todo: add check params
        # ok = self.check_params(params)

        # moea params
        self.name               = params["name"]                            # name of moea
        self.label              = params["label"]                           # label of moea
        self.runs               = params.get("runs", 1)                     # number of independent runs
        self.iters              = params["iters"]                           # number of generations                 
        self.m_objs             = params["m_objs"]                          # number of objectives
        self.pop_size           = params["pop_size"]                        # number of individuals
        self.params_mop         = params["params_mop"]                      # mop parameters
        self.params_crossover   = params["params_crossover"]                # crossover parameters
        self.params_mutation    = params["params_mutation"]                 # mutation parameters
        self.params_stats       = params["params_stats"]                    # stats parameters
        self.verbose            = params.get("verbose", False)              # log verbosity

        
        # internals (do not override)
        self.n_genes, __, __     = get_n_genes(self.params_mop["name"], self.m_objs)    # number of decision variables according to mop["name"]
        self.lbound, self.ubound = get_bounds(self.params_mop["name"], self.n_genes)    # lower and upper bounds of decision variables
        self.fevals = 0                                                                 # counter of function evaluations
        self.z_ideal = np.ones((self.m_objs, )) * np.inf                                # initialization of z_ideal (worst values at the beginning)
        self.z_nadir = np.ones((self.m_objs, )) * (-1)*np.inf                           # initialization of z_nadir (best values at the beginning)
                                                                                        # @todo: I'm not sure if this initialization is the best method, maybe it would be a good idea to initialize it from objs
        
        # stats
        self.stats_step         = self.params_stats.get("step", 100)                            # we compute stats every 100 generations by default
        #self.stats_indicators   = self.params_stats.get("indicators", (, ))                     # there is no performance indicators by default
        
        # create output directories
        paths = prepare_experiment_dirs(self.params_stats["output_path"], self.params_mop["name"], self.name) 
        self.paths = paths
    
    
    def update_z(self, objs):
        """
        Update the reference vectors
        
        Input
        objs        (pop_size, m_objs) objs matrix
        """
        
        pop_size, m_objs = objs.shape

        # @todo: check whether these approaches return the same result
        # approach 1
        for row in range(pop_size):
        
            for col in range(m_objs):
                
                if objs[row, col] < self.z_ideal[col]:
                    self.z_ideal[col] = objs[row, col]
                    
                if objs[row, col] > self.z_nadir[col]:
                    self.z_nadir[col] = objs[row, col]
                    
        
        # approach 2
        min_objs = objs.min(axis=0)
        max_objs = objs.max(axis=0)
    
        for col in range(m_objs):
            
            if min_objs[col] < self.z_ideal[col]:
                self.z_ideal[col] = min_objs[col]
                
            if max_objs[col] > self.z_nadir[col]:
                self.z_nadir[col] = max_objs[col]
                
    
    def check_pop_bounds(self, chroms):
        """
        Check each decision vector of chroms chroms in a give mop. After evaluation, the reference vectors are updated.
        
        Input
        chroms      (pop_size, n_genes) chroms matrix or
                    (n_genes) chrom vector
                    
        Output
        new_chroms  (pop_size, n_genes) chroms matrix or
                    (n_genes, ) chrom vector
        """
        
        input_chroms = chroms.copy()            # chroms to be checked
        flag_reshape = False                    
        
        # if chroms contains a single chrom vector
        # we need to reshape it before calling evaluate_mop
        if chroms.ndim == 1:
            flag_reshape = True
            n = len(chroms)
            input_chroms = input_chroms.reshape((1, n))
        
        # check bounds
        new_chroms = input_chroms.copy()
        pop_size, n_genes = new_chroms.shape
        
        for row in range(pop_size):
            
            for col in range(n_genes):
                
                if input_chroms[row, col] < self.lbound[col]:
                    new_chroms[row, col] = self.lbound[col]
                    
                if input_chroms[row, col] > self.ubound[col]:
                    new_chroms[row, col] = self.ubound[col]
        
        # if this flag is activated (that means that objs is a single vector)
        # we need to reshape it before returning it
        if flag_reshape:
            return new_chroms.copy().flatten()
        
        return new_chroms.copy()


    def evaluate_pop_mop(self, chroms):
        """
        Evaluate chroms in a give mop. After evaluation, the reference vectors are updated.
        
        Input
        chroms      (pop_size, n_genes) chroms matrix or
                    (n_genes) chrom vector
        
        Output
        objs        (pop_size, m_objs) objs matrix or
                    (m_objs, ) obj vector
        """
        
        input_chroms = chroms.copy()            # chroms to be evaluated
        flag_reshape = False                    
        
        # if chroms contains a single chrom vector
        # we need to reshape it before calling evaluate_mop
        if chroms.ndim == 1:
            flag_reshape = True
            n = len(chroms)
            input_chroms = input_chroms.reshape((1, n))
        
        # evaluate input_chroms on a mop
        objs = evaluate_mop(input_chroms, self.m_objs, self.params_mop)
        
        # update z_ideal and z_nadir
        self.update_z(objs)
        
        # if this flag is activated (that means that objs is a single vector)
        # we need to reshape it before returning it
        if flag_reshape:
            
            # update fevals
            self.fevals += 1
            return objs.copy().flatten()
        
        
        # update fevals
        pop_size, __ = objs.shape
        self.fevals += pop_size
        
        return objs.copy()
    
    def plot_run(self, run_id):
        """
        Add a plot function
        This function will be called after invoking self.run() on self.evaluate()
        """
        pass    
        
    def run(self, chroms, seed, run_id):
        """
        Independent run of this moea
        """
        print("override this function on your moea")        # @todo: add log support here
        
        
    def evaluate(self, chroms=None, plot_output=False, plot_output_name=None, plot_run=False):
        """
        Evaluate the performance of moea a number of independent times
        """
        
        mop_name        = self.params_mop["name"]
        m_objs          = self.m_objs
        pop_size        = self.pop_size
        n_genes         = self.n_genes
        lb              = self.lbound
        ub              = self.ubound
        list_chroms     = [None] * self.runs
        list_objs       = [None] * self.runs
        list_labels     = [None] * self.runs
        real_front, p   = get_real_front(mop_name, m_objs, n_genes, self.params_mop)
        
        for i in range(self.runs):
            
            seed = i*100
            rand = RandomState(seed)
            
            # reset counter
            self.fevals = 0
            
            # initial pop
            if chroms is None:
                chroms = (ub - lb) * rand.random_sample((pop_size, n_genes)) + lb
            
            # start timer
            start = timer()
            
            # independent run
            chroms, objs = self.run(chroms, seed, i)
            
            # end timer
            end = timer()
            
            spent_time = end - start
            
            # save spent_time
            self.save_spent_time(spent_time, i)

            # save pop on file
            self.save_final_pop(chroms, objs, i)
            
            # save them
            list_chroms[i]      = chroms.copy()
            list_objs[i]        = objs.copy()
            list_labels[i]      = "run: %d" % (i+1, ) 
            
            
            # plot run
            if plot_run and i < 5:
                # only call plot_run for the first five times to avoid 
                # a great number of plots on memory
                self.plot_run(i)
            
            print("run %d/%d, fevals: %d, time: %.5f" % (i+1, self.runs, self.fevals, spent_time))
            print("")
        
        # plot
        fig, ax = None, None
        if plot_output:
            title="%s - %s" % (self.params_mop["name"].upper(), self.label)
            fig, ax = plot_pops(list_objs, list_labels, real_front=real_front, p=p, title=title, loc="upper right", output=plot_output_name)
                
        
            
        return list_chroms, list_objs, fig, ax
            
    
    def stats(self, chroms, objs, t, run_id, suffix=""):
        """
        Compute stats and save pop
        
        Input
        chroms      (pop_size, n_genes) chroms matrix
        objs        (pop_size, m_objs) objs matrix
        t           int, generation number
        run_id      int, independent run number
        sufix       str, optional suffix for filename
        """
        
        # this block is useful to avoid gathering stats (ie, just for a quick test)
        if self.stats_step is None:
            return
        
        if t % self.stats_step == 0:
            
            # compute performance indicators here...
            """
            for indicator in self.stats_indicators:
                
                if indicator == "igd":
                    pass
            """        
                    
            # save pop
            filename = "chroms_%s_m_%d_run_%d_t_%d%s.txt" % (self.name, self.m_objs, run_id, t, suffix)
            filepath = os.path.join(self.paths["path_pops"], filename)
            np.savetxt(filepath, chroms)
    
            filename = "objs_%s_m_%d_run_%d_t_%d%s.txt" % (self.name, self.m_objs, run_id, t, suffix)
            filepath = os.path.join(self.paths["path_pops"], filename)
            np.savetxt(filepath, objs)
            
            

    def save_final_pop(self, chroms, objs, run_id):
        """
        Save final population. Call this function within self.evaluate() only
        
        Input
        chroms      (pop_size, n_genes) chroms matrix
        objs        (pop_size, m_objs) objs matrix
        t           generation number
        run_id      independent run number
        """
        
        filename = "chroms_%s_m_%d_run_%d_final.txt" % (self.name, self.m_objs, run_id)
        filepath = os.path.join(self.paths["path_pops"], filename)
        np.savetxt(filepath, chroms)

        filename = "objs_%s_m_%d_run_%d_final.txt" % (self.name, self.m_objs, run_id)
        filepath = os.path.join(self.paths["path_pops"], filename)
        np.savetxt(filepath, objs)
        
        print("objs: %s" % (filepath, ))
        
    
    def save_spent_time(self, spent_time, run_id):
        """
        Save the spent_time of this independent run. Call this function within self.evaluate() only
        
        Input
        spent_time  execution time of this independent run
        run_id      independent run number
        """
        
        filename = "time_%s_m_%d.txt" % (self.name, self.m_objs)
        filepath = os.path.join(self.paths["path_stats"], filename)
        line     = "%.10f\n" % spent_time
        mode     = "w" if run_id == 0 else "a"
        
        # create empty file if run_id == 0
        # append file       if run_id >  0
        with open(filepath, mode) as log:
            log.write(line)
        
        #print("Spent time:", line, end="")
        
    

            
    
    # mopy
    # @todo: add evaluate_bounds
    # @todo: add update_z_vectors o como se llame en moead
    # @todo: add moead from moea_research
    
    
    
    # pymoea3
    # @todo: add evaluate_pop? o checar la fuuncion que ocupaba en moead
        
