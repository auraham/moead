# test_moea_config.py
from __future__ import print_function
from rocket.moea import MOEAD
from rocket.helpers import get_n_genes, get_bounds
from rocket.labs import get_experiment_path
import numpy as np
import os

# define experiment output path
script_path = os.path.dirname(os.path.abspath(__file__))
#output_path = get_experiment_path(script_path, name=None)
output_path = get_experiment_path(script_path, name="sample")


def get_weights():
    
    pop_size = 100
    span = np.linspace(0, 1, pop_size)
    
    weights = np.zeros((pop_size, 2))
    weights[:, 0] = span.copy()
    weights[:, 1] = span[::-1].copy()

    return weights.copy()
    
if __name__ == "__main__":
    
    weights  = get_weights()
    pop_size = weights.shape[0]
    
    
    params = {
        "name"              : "moead",                                              # name of moea
        "label"             : "MOEAD",                                              # label of moea
        "runs"              : 1,                                                    # number of independent runs
        "iters"             : 400,                                                  # number of generations
        "m_objs"            : 2,                                                    # number of objectives
        "pop_size"          : pop_size,                                             # number of individuals
        "params_mop"        : {"name": "dtlz1"},                                    # mop parameters
        "params_crossover"  : {"name": "sbx", "prob": 1.0, "eta": 20},              # crossover parameters
        "params_mutation"   : {"name": "polymutation", "prob": 1./6, "eta": 15},    # mutation parameters
        "params_stats"      : {"output_path": output_path},                         # stats parameters
        "verbose"           : True,                                                 # log verbosity
        
        
        # MOEAD params
        "neigh_size"        : 20,
        "pop_weights"       : weights,
        "params_form"       : {"name": "pbi", "pbi-theta": 5},
    }
    
    moea = MOEAD(params)
    chroms, objs = moea.evaluate()             # run moea 5 independent times
    
    
