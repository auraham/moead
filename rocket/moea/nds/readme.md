# About this package

This package contains methods for non dominating sorting:

- `fnds.py` contains the method proposed by [Deb02]




# TODO

- Add more nds methods here (naive, cont_update, Mishra, Kung, etc)
- Add a function `get_first_front()` or `get_fronts(int limit)` to get the fronts of the first `limit` solutions to avoid processing other fronts.

# References

- [Deb02] *A fast and elitist multiobjective genetic algorithm: NSGA-II*.