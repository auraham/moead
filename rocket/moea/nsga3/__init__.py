# __init__.py
from .normalize import norm_matrix
from .group import get_groups_from_fronts
from .associate import associate
