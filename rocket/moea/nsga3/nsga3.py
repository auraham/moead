# nsga3.py
from __future__ import print_function
from rocket.moea import NSGA2
from rocket.moea.nds import fnds
from rocket.moea.moea_base import MOEA
from rocket.genetics import crossover, mutation
from rocket.moea.nsga3.normalize import norm_matrix
from rocket.moea.nsga3.associate import associate
from rocket.moea.nsga3.niching import niching
from rocket.moea.nsga3.group import get_groups_from_fronts
import numpy as np

class NSGA3(NSGA2):
    
    def __init__(self, params):
        
        NSGA2.__init__(self, params)
        
        # nsga3 params
        self.weights                    =  params["nsga3_weights"]
        self.parent_selection_method    =  params.get("nsga3_parent_selection_method", "nsga3")
        
    def update_selection(self, objs):
        """
        Rank (parent and child's) objs and return a vector of sorted indices
        
        Input
        objs        (pop_size*2, m_objs) objs matrix
        
        Output
        to_keep     (pop_size, ) int array of sorted indices to keep
        """
        
        # fast non-dominated sorting
        fronts, _ranks = fnds(objs)
        
        # get s (np+lf), np (next_pop), and lf (last_front) sets
        s_set, np_set, lf_set = get_groups_from_fronts(fronts, self.pop_size)
        
        # normalization
        norm_objs = norm_matrix(objs)
        
        # associate
        pie, rho_np, rho_lf = associate(s_set, norm_objs, self.weights, np_set)
        
        # niching
        k_slots = self.pop_size - len(np_set)                           # k_slots available for new individuals in last_front
        to_add  = niching(k_slots, pie, rho_np, rho_lf, lf_set, self.weights, norm_objs, self.rand)
        
        # merge sets
        to_keep = np.hstack((np_set, to_add))
        
        return np.array(to_keep, dtype=int)
        
        
    def genetic_operations(self, chroms, objs, fitness):
        """
        Create a new pop from parent pop (chroms)
        This function has been replaced from nsga2 to use custom parent selection methods
        
        Input
        chroms          (pop_size, n_genes) matrix
        objs            (pop_size, m_objs) matrix
        fitness         (pop_size, 2) ranks and crowding dist matrix
        
        Output
        child_chroms    (pop_size, n_genes) matrix
        """
        
        new_chroms = np.zeros(chroms.shape)
        
        
        if self.parent_selection_method == "nsga2_tournament":
            
            # nsga2's method (binary tournament selection) for default (ranks and crowding distance)
            new_chroms = NSGA2.genetic_operations(self, chroms, objs, fitness)                      # @todo: check if create_child_pop uses nsga3's crossover and mutation params
            
        elif self.parent_selection_method == "nsga3_tournament":
            
            # [Seada2016]'s for parent selection
            print("error: nsga3_tournament will be implemented later!")
            
        else:
            
            # @todo: add nsga3' original method
            
            print("error on parent selection method, using nsga2_tournament instead")
            
            # nsga2's method (binary tournament selection) for default (ranks and crowding distance)
            new_chroms = NSGA2.genetic_operations(self, chroms, objs, fitness)                      # @todo: check if create_child_pop uses nsga3's crossover and mutation params
            
            
        return new_chroms.copy()
        
        
        
        
