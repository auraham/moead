# normalize
from __future__ import print_function
import numpy as np

def norm_matrix(objs):
    """
    Normalize objs matrix (we assume minimization)
    
    Input
    objs        (pop_size, m_objs) objs matrix
    
    Output
    norm_objs   (pop_size, m_objs)
    """
    
    z_ideal = objs.min(axis=0)
    z_nadir = objs.max(axis=0)

    norm_objs = (objs - z_ideal) / (z_nadir - z_ideal)
    
    return norm_objs.copy()
    

if __name__ == "__main__":
    
    objs = np.array([
        [90, 5],
        [45, 65],
        [96, 30],
        [35, 90],
        [40, 30],
        [98, 40],
        [10, 90],
        [20, 20],
        [70, 15],
        [65, 35],
        [50, 60],
        [30, 50]])
        
    norm_objs = norm_matrix(objs)
