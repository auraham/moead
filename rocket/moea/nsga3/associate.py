# associate.py
from __future__ import print_function
from rocket.helpers import rad2deg
from numpy.linalg import norm
import numpy as np
from rocket.moea.nsga3.debug import debug_plot_objs_associate


def get_angle(u, v):
    """
    Compute angle between vectors (degrees)
    
    Input
    u       (m, ) array
    v       (m, ) array
    
    Output
    deg     angle between u and v
    """
    
    rad = np.arccos(np.dot(u, v) / (norm(u) * norm(v))) 
    
    deg = rad2deg(rad)
    
    return deg
    
    
def get_pdist(u, v):
    """
    Compute perpendicular distance between objective vector u and weight vector v.
    In other words, compute the length of the orthogonal (perpendicular) vector between u and v.
    
    Input
    u       (m, ) array
    v       (m, ) array
    
    Output
    deg     perpendicular distance between u and v
    """
    
    # perpendicular vector
    #     (vector) - (          scalar          )  * (vector)
    wp =  u        - (np.dot(u, v) / (norm(v)**2)) * v

    # lenght
    d2 = norm(wp)

    return d2


def associate(s_indices, norm_objs, weights, np_set, debug=False, lf_indices=None):
    """
    Associate weights to solutions in pie
    
    Input
    s_indices       (s_size, ) tuple, each index represents an individual in S set, @todo: s_size > pop_size, s_size = np_size + lf_size?
    norm_objs       (pop_size*2, m_objs) normalized objs (parent+offspring)
    weights         (n_weights, m_objs) weights matrix
    np_set          (np_size, ) tuple, each index represents an individual in next_pop set @todo: np_size < pop_size
    debug           If true, displays a progress plot
    lf_indices      (lf_size, ) tuple, each index represents an individual in last_front set (needed for debugging only) @todo: ls_size < pop_size, 
    
    Output
    pie             (pop_size*2, )  pie[i]=j  (association dict) returns the index (j) of the weight associated to the i-th individual in S set
    rho_np          (n_weights, )   rho_np[i] (counter dict)     returns the number of individuals of S in next_pop   associated to the i-th weight vector
    rho_lf          (n_weights, )   rho_lf[i] (counter dict)     returns the number of individuals of S in last_front associated to the i-th weight vector
    """
    
    objs_size   = norm_objs.shape[0]
    n_weights   = weights.shape[0]
    pie         = np.ones((objs_size, ), dtype=int) * -1       # if pie[i] = -1, then the i-th individual was not assigned to any weight vector
    rho_np      = np.zeros((n_weights, ), dtype=int)
    rho_lf      = np.zeros((n_weights, ), dtype=int)
    
    # for each obj vector s_i in s_indices
    for i, s in enumerate(norm_objs):

        if i not in s_indices:
            # we only associate individuals in s_indices
            # ie, if the i-th individual is not in s_indices, it is skipped
            continue
            
        # reset historial
        min_pdist       = np.inf        # min perpendicular dist (pdist)
        argmin_pdist    = 0
    
        # for each weight vector w_j in weights
        for j, w in enumerate(weights):

            # @note: in the original paper, Deb et al. uses perpendicular distance between s_i and w_j to associate individuals,
            # however, in this implementation we are using the angle between s_i and w_j to associate individuals instead
            curr_pdist = get_angle(s, w)                # replace only this line if you want to use perpendicular dist (ie, curr_pdist = get_pdist(s, w))
                                                        # the rest of the code does not need any additional change
            
            #curr_pdist = get_pdist(s, w)
            
            # update historial
            if curr_pdist < min_pdist:
                min_pdist = curr_pdist
                argmin_pdist = j
                
        # update pie
        pie[i] = argmin_pdist                           # i-th individual is associated to w_j (j = argmin_pdist, ie the nearest w_j)
        
        # update rho
        if i in np_set:
            
            # i-th individual is in next_pop
            rho_np[argmin_pdist] += 1                   # increment the number of individuals of next_pop associated to w_j

        else:
            
            # i-th individual is in last_front
            # note that we assert it because the if-block (if i not in s_indices)
            # that is, we only associate individuals in s_indices (S set = next_pop set + last_front set)
            # so, it the i-th individual in S is not in next_pop, then it MUST be in last_front 
            
            rho_lf[argmin_pdist] += 1
            
            
        if debug:
            
            debug_plot_objs_associate(norm_objs, s_indices, np_set, lf_indices, None, weights, rho_np, rho_lf, None, "debug-associate")
            
            
    return pie.copy(), rho_np.copy(), rho_lf.copy()


