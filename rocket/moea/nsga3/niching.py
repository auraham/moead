# niching.py
from __future__ import print_function
from rocket.moea.nsga3.associate import get_angle
from rocket.moea.nsga3.debug import debug_plot_objs_niching
import numpy as np

def get_j_min_set(rho_np, to_ignore):
    """
    Identify which weight vectors (indices) have the lower number
    of associated solutions from next_pop
    
    Input
    rho_np                  (n_weights, )   rho_np[i] (counter dict)     returns the number of individuals of S in next_pop   associated to the i-th weight vector
    to_ignore               list of weight vectors (indices) to ignore in this function
    
    Output
    alone_vector_list       list of indices, this list represents the set of weight vectors
                            with the lower number of associated solutions from next_pop
    
    """

    n_weights = rho_np.shape[0]
    
    # argmin
    # this block gets the j-th vector (indice j_argmin)
    # with the lower number of associated solutions from next_pop
    j_argmin = np.inf
    for i in range(n_weights):
        
        if i in to_ignore:      # important! this avoid to consider (more than one time) weight vectors
            continue            # with no assigned solutions
        
        if rho_np[i] < j_argmin:
            j_argmin = rho_np[i]
            
    # alone_vector_list
    # this list contains all of those weights vectors
    # with exactly j_argmin solutions associated from next_pop
    # @todo: i think this block could be more pythonic
    alone_vector_list = []
    for i in range(n_weights):
        
        if i in to_ignore:
            continue
            
        if rho_np[i] == j_argmin:
            alone_vector_list.append(i)
            
    return list(alone_vector_list)

def get_argmin_from_vector(indices, s_objs, weight):
    """
    Given a set of indices (from s_objs), return the nearest obj vector from s_objs to weight
    @note: I dont remember if this function is the same as the paper (it was perp distance or angle?)
    
    """
    
    selected = 0
    min_angle = 360
    
    for ind in indices:
        
        obj_vector = s_objs[ind]
        theta = get_angle(obj_vector, weight)
        
        if theta < min_angle:
            min_angle = theta
            selected = ind
            
    return selected
    
def get_random_value_from_vector(vector, rand):
    
    # rand_id = rand.random_integers(0, len(vector)-1)      # random_integers(low, high) is deprecated; low and high are inclusive
    rand_id = rand.randint(0, len(vector))                   # randint(low, high) is recommended; low is inclusive, high is exclusive
    
    return vector[rand_id]
        
def niching(k_slots, pie, rho_np, rho_lf, lf_set, weights, norm_objs, rand, debug=False, fronts=[]):
    
    to_add      = np.zeros((k_slots, ), dtype=int)
    to_ignore   = []
    last_front  = [ind for ind in lf_set]           # copy for eliminations
    next_j      = 0                                 # debug

    k = 0
    while k < k_slots:
        
        # get j_min set considering next_pop only (rho_np)
        alone_vector_list = get_j_min_set(rho_np, to_ignore)
        
        # select j index randomly
        j = get_random_value_from_vector(alone_vector_list, rand)
        
        # this line emulates the lines 5 and 6 of Algorithm 4 of [Deb14]
        if rho_lf[j] > 0:
            
            new_member = None
            
            if rho_np[j] == 0:
                
                new_member = get_argmin_from_vector(last_front, norm_objs, weights[j])
                
            else:
                
                new_member = get_random_value_from_vector(last_front, rand)
                
                
            to_add[k] = new_member
            k += 1
            
            # important!
            rho_np[j] += 1
            
            # delete new_member from last_front to avoid adding it again
            last_front = [ind for ind in last_front if ind != new_member]  # @todo: more pythonic? use a boolean array instead: to_skip[new_member]=True?
            
            if debug:
                
                debug_plot_objs_niching(norm_objs, fronts, weights, rho_np, to_add[:k], "niching")
                
                print("alone_vector_list:", alone_vector_list)
                print("j:", j, "new_member:", new_member)
                
        else:
            
            # this avoid to consider selecting the j-th weight vector again
            # ie, avoid a loop
            to_ignore.append(j)
            
            if debug:
                print("to_ignore:", to_ignore)
                
    return to_add.copy()
    
    
if __name__ == "__main__":
    
    from rocket.moea.nds import fnds
    from rocket.moea.nsga3 import get_groups_from_fronts, associate
    from numpy.random import RandomState
    from debug import plot_objs_niching

    
    rand = RandomState(100)
    pop_size = 8
    norm_objs = np.array([
                    [0.90, 0.05],
                    [0.45, 0.65],
                    [0.96, 0.30],
                    [0.35, 0.90],
                    #[0.40, 0.30],
                    [0.98, 0.40],
                    [0.98, 0.20],
                    [0.10, 0.90],
                    [0.20, 0.20],
                    [0.70, 0.15],
                    [0.65, 0.35],
                    [0.50, 0.60],
                    [0.30, 0.50],
                    [0.25, 0.40],
                    [0.75, 0.10],
                    [0.40, 0.50],
                    [0.80, 0.80],
                    ])
    
    weights = np.array([
                    [0.000, 1.000],
                    [0.125, 0.875],
                    [0.250, 0.750],
                    [0.375, 0.625],
                    [0.500, 0.500],
                    [0.625, 0.375],
                    [0.750, 0.250],
                    [0.875, 0.125],
                    [1.000, 0.000],
                    ])
    
    # fast non-dominated sorting
    fronts, _ranks = fnds(norm_objs)
    
    # grouping
    s_set, np_set, lf_set = get_groups_from_fronts(fronts, pop_size)
    
    # associate
    pie, rho_np, rho_lf = associate(s_set, norm_objs, weights, np_set)
    
    # copy
    rho_np_prev = rho_np.copy()
    
    print("rho_np", rho_np)
    print("rho_lf", rho_lf)
    
    # niching
    k_slots = pop_size - len(np_set)                    # k_slots available for new individuals in last_front
    to_add  = niching(k_slots, pie, rho_np, rho_lf, lf_set, weights, norm_objs, rand)
    
    # merge sets
    to_keep = np.hstack((np_set, to_add))
    
    print("rho_np", rho_np)
    print("rho_lf", rho_lf)
    
    plot_objs_niching(norm_objs, s_set, np_set, lf_set, weights, rho_np_prev, rho_np, to_add, to_keep, fronts, "niching-complete")

    
    
     
    

