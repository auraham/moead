# group.py
from __future__ import print_function
import numpy as np

def get_groups_from_fronts(fronts, pop_size):
    """
    Extract three sets from fronts:
        s
        np (next pop)
        lf (last front)
        
    s contains the union of np and lf.
    np contains the |np| indices of survival solutions, where |np| < pop_size, and slots = pop_size - |np|
    lf works as a pool for selecting the remaining slots
        
    Input
    fronts      list of lists
    
    Output
    s           (np_size + lf_size, ) tuple of indices
    np          (np_size, ) tuple of indices
    lf          (lf_size, ) tuple of indices
    """
    
    s  = [] ; s_size = 0
    np = []
    lf = []
    
    i  = 0
    while True:
        
        # fronts[i]
        pfront = fronts[i]
        front_size = len(pfront)
        
        # append fronts[i] to s
        s.extend(list(pfront))
        s_size += front_size
        
        if s_size >  pop_size:
            
            # fronts[i] is the last front to be added
            lf.extend(list(pfront))
            break
        
        else:
            
            # fronts[i] is not the last front to be added
            # thus, it is added to next_pop instead
            np.extend(list(pfront))
            
        i += 1
        
    return tuple(s), tuple(np), tuple(lf)


def plot_objs(objs, s, np, lf):
    
    import matplotlib.pyplot as plt    
    
    colors = ["#FBEF88", "#3863AA", "#F7523A",  "#5A5A5A"]
    
    fig = plt.figure("group")
    axes = (fig.add_subplot(121), fig.add_subplot(122))
    
    for i, ax in enumerate(axes):
        
        ax.cla()
        ax.set_xlim(0, 1.2)
        ax.set_ylim(0, 1.2)
        ax.set_xlabel("$f_1$")
        ax.set_ylabel("$f_2$")

        if i == 0:
            ax.plot(objs[:, 0], objs[:, 1],   marker="o", c=colors[0], ls="none", label="$Q_t$ (objs)", ms=10)
            ax.plot(objs[s, 0], objs[s, 1],   marker="o", c=colors[1], ls="none", label="$S_t$ (s)")

            ax.legend(loc="upper right")

        if i ==  1:
            ax.plot(objs[:, 0], objs[:, 1],   marker="o", c=colors[0], ls="none", label="$Q_t$ (objs)", ms=10)
            ax.plot(objs[np, 0], objs[np, 1], marker="o", c=colors[2], ls="none", label="$N_{t+1}$ (np)")
            ax.plot(objs[lf, 0], objs[lf, 1], marker="o", c=colors[3], ls="none", label="$F_{l}$ (lf)")

            ax.legend(loc="upper right")
            plt.show()
            
            
if __name__ == "__main__":
    
    from rocket.moea.nds import fnds
    
    pop_size    = 8
    norm_objs   = np.array([
                    [0.90, 0.05],
                    [0.45, 0.65], 
                    [0.96, 0.30], 
                    [0.35, 0.90], 
                    [0.40, 0.30], 
                    [0.98, 0.40], 
                    [0.10, 0.90], 
                    [0.20, 0.20], 
                    [0.70, 0.15], 
                    [0.65, 0.35], 
                    [0.50, 0.60], 
                    [0.30, 0.50], 
                    [0.25, 0.40], 
                    [0.75, 0.10], 
                    [0.40, 0.50], 
                    ])
                    
    fronts, ranks = fnds(norm_objs)
    s, np, lf = get_groups_from_fronts(fronts, pop_size)
    
    plot_objs(norm_objs, s, np, lf)

