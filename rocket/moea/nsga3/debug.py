# debug.py
from __future__ import print_function
from numpy.linalg import norm
import numpy as numpy
import matplotlib.pyplot as plt

colors = ("#FBEF88", "#3863AA", "#F7523A", "#5A5A5A", "#0759AB", "7DBC7B")


def get_theta(u, v):
    
    theta = numpy.arccos(numpy.dot(u, v) / (norm(u) * norm(v)))

    return theta

def get_polar_coord(theta, radius):
    
    return numpy.array([radius*numpy.cos(theta), radius*numpy.sin(theta)])

def draw_lines(ax, weights, rho, c="#505050"):
    
    u = numpy.array([1, 0])             # x axis
    for i, w in enumerate(weights):
        
        theta = get_theta(u, w)         # angle between w and x axis
        
        polar_x, polar_y = get_polar_coord(theta, 5)
        
        x = [0, polar_x]
        y = [0, polar_y]
        
        if rho[i] > 0:
            ax.plot(x, y, c=c)              # this line is occupied for a solution from rho
        else:
            ax.plot(x, y, c="#c0c0c0")      # this line is not occupied for any solution from rho

        # weight labels
        x, y = get_polar_coord(theta, 1.2)
        ax.text(x, y, "$w_{%d}$" % (i, ), size=16)
        ax.text(x+0.05, y, "$(%d)$" % (rho[i], ), size=12)
        
def draw_busy_lines(ax, weights, rho, c="#505050", ls="-"):
    
    u = numpy.array([1, 0])             # x axis
    for i, w in enumerate(weights):
        
        theta = get_theta(u, w)         # angle between w and x axis
        
        polar_x, polar_y = get_polar_coord(theta, 5)
        
        x = [0, polar_x]
        y = [0, polar_y]
        
        if rho[i] > 0:
            ax.plot(x, y, c=c)              # this line is occupied for a solution from rho
    
def draw_count_lines(ax, weights, rho_counter, c="#505050", ls="-"):
    
    u = numpy.array([1, 0])             # x axis
    for i, w in enumerate(weights):
        
        theta = get_theta(u, w)         # angle between w and x axis
        
        polar_x, polar_y = get_polar_coord(theta, 5)
        
        x = [0, polar_x]
        y = [0, polar_y]
        
        # weight labels
        x, y = get_polar_coord(theta, 1.2)
        ax.text(x, y, "$w_{%d}$" % (i, ), size=16)
        ax.text(x+0.05, y, "$(%d)$" % (rho_counter[i], ), size=12)
     
def draw_free_lines(ax, weights):
    
    u = numpy.array([1, 0])             # x axis
    for i, w in enumerate(weights):
        
        theta = get_theta(u, w)         # angle between w and x axis
        
        polar_x, polar_y = get_polar_coord(theta, 5)
        
        x = [0, polar_x]
        y = [0, polar_y]
        
        ax.plot(x, y, c="#c0c0c0")

def debug_plot_objs_associate(objs, s, np, lf, bf, weights, rho_np, rho_lf, rho_bf, figname):
    
    fig = plt.figure(figname)
    ax = fig.add_subplot(111)
    
    ax.cla()
    ax.set_xlim(-0.05, 1.25)
    ax.set_ylim(-0.05, 1.25)
    ax.set_xlabel("$f_1$")
    ax.set_ylabel("$f_2$")
    ax.set_title(figname)
    
    # direction search lines
    draw_free_lines(ax, weights)
    draw_busy_lines(ax, weights, rho_np, c=colors[2], ls="-")
    draw_busy_lines(ax, weights, rho_lf, c=colors[3], ls="-")
    
    
    if rho_bf is None:
        draw_count_lines(ax, weights, rho_np+rho_lf)
    else:
        draw_busy_lines(ax, weights, rho_bf, c=colors[5])
        draw_count_lines(ax, weights, rho_np+rho_lf+rho_bf)
    
    
    ax.plot(objs[:, 0], objs[:, 1], marker="o", c=colors[0], ls="none", label="$Q_t$ (objs)", ms=10)
    ax.plot(objs[np, 0], objs[np, 1], marker="o", c=colors[2], ls="none", label="$P_{t+1}$ (np)")
    ax.plot(objs[lf, 0], objs[lf, 1], marker="o", c=colors[3], ls="none", label="$F_l$ (lf)")
    
    
    if rho_bf is not None:
        ax.plot(objs[bf, 0], objs[bf, 1], marker="o", c=colors[5], ls="none", label="$F_b$ (bf)")
    
    # obj labels
    for j, u in enumerate(objs):
        x, y = u[0]+0.02, u[1]
        ax.text(x, y, str(j))
        
    ax.legend(loc="upper right")
    
    plt.show()

def draw_fronts(ax, fronts, objs):
    
    for front in fronts:
        
        pobjs   = objs[front, :]
        
        ids     = pobjs[:, 0].argsort()
        sobjs   = pobjs[ids, :]
        n       = sobjs.shape[0]-1
        
        for i in range(n):
            
            u = sobjs[i]
            v = sobjs[i+1]
            
            x = [u[0], v[0]]
            y = [u[1], v[1]]

            ax.plot(x, y, ":", c="#404040")

def debug_plot_objs_niching(objs, fronts, weights, rho_np, to_add, figname):
    
    fig = plt.figure(figname)
    ax = fig.add_subplot(111)
    
    ax.cla()
    ax.set_xlim(-0.05, 1.25)
    ax.set_ylim(-0.05, 1.25)
    ax.set_xlabel("$f_1$")
    ax.set_ylabel("$f_2$")

    # draw direction search lines
    draw_lines(ax, weights, rho_np)
    
    # draw fronts
    draw_fronts(ax, fronts, objs)
    
    
    ax.plot(objs[:, 0], objs[:, 1], marker="o", c=colors[0], ls="none", label="$Q_t$ (objs)", ms=10)
    ax.plot(objs[to_add, 0], objs[to_add, 1], marker="o", c=colors[4], ls="none", label="$F_l$ (to_add)")
    
    # objs labels
    for j, u in enumerate(objs):
        x, y = u[0]+0.02, u[1]
        ax.text(x, y, str(j))
        
    ax.legend(loc="upper right")
    
    plt.show()
    
    
def plot_objs_niching(objs, s_set, np_set, lf_set, weights, rho_np_prev, rho_np, to_add, to_keep, fronts, figname):
    
    fig = plt.figure(figname)
    axes = (fig.add_subplot(121),
            fig.add_subplot(122))
            
    for i, ax in enumerate(axes):
        
        ax.cla()
        ax.set_xlabel("$f_1$")
        ax.set_ylabel("$f_2$")
        ax.set_xlim(-0.05, 1.8)
        ax.set_ylim(-0.05, 1.8)
        
        
        if i == 0:
            
            # draw direction search lines
            draw_lines(ax, weights, rho_np_prev)
            
            # draw fronts
            draw_fronts(ax, fronts, objs)
    
            
            ax.plot(objs[:, 0], objs[:, 1], marker="o", c=colors[0], ls="none", label="$Q_t$ (objs)", ms=10)
            ax.plot(objs[np_set, 0], objs[np_set, 1], marker="o", c=colors[2], ls="none", label="$P_{t+1}$ (np)")
            ax.plot(objs[lf_set, 0], objs[lf_set, 1], marker="o", c=colors[3], ls="none", label="$F_{l}$ (lf)")
            ax.plot(objs[to_add, 0], objs[to_add, 1], marker="o", c=colors[4], ls="none", label="$F_{l}$ (to_add)")
            
            # objs labels
            for j, u in enumerate(objs):
                x, y = u[0]+0.02, u[1]
                ax.text(x, y, str(j))
                
            ax.legend(loc="upper right")
            
            
        else:
            
            # draw direction search lines
            draw_lines(ax, weights, rho_np)
            
            # draw fronts
            draw_fronts(ax, fronts, objs)
    
            
            ax.plot(objs[:, 0], objs[:, 1], marker="o", c=colors[0], ls="none", label="$Q_t$ (objs)", ms=10)
            ax.plot(objs[to_keep, 0], objs[to_keep, 1], marker="o", c=colors[2], ls="none", label="$P_{t+1}$ (complete)")
            
            # objs labels
            for j, u in enumerate(objs):
                x, y = u[0]+0.02, u[1]
                ax.text(x, y, str(j))
                
            ax.legend(loc="upper right")
            
            plt.show()
            
    
    
