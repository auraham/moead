# test_moea_config.py
from __future__ import print_function
from rocket.moea import NSGA3
from rocket.helpers import get_n_genes, get_bounds
from rocket.labs import get_experiment_path
import numpy as np
import os

# define experiment output path
script_path = os.path.dirname(os.path.abspath(__file__))
#output_path = get_experiment_path(script_path, name=None)
output_path = get_experiment_path(script_path, name="sample")


def get_weights():
    
    pop_size = 100
    span = np.linspace(0, 1, pop_size)
    
    weights = np.zeros((pop_size, 2))
    weights[:, 0] = span.copy()
    weights[:, 1] = span[::-1].copy()

    return weights.copy()
    
if __name__ == "__main__":
    
    #weights  = get_weights()
    weights = np.genfromtxt("weights_m_3.txt")
    pop_size = weights.shape[0]+1                                                   # pop_size must be even
    
    
    params = {
        "name"              : "nsga3",                                              # name of moea
        "label"             : "NSGA-III",                                           # label of moea
        "runs"              : 1,                                                    # number of independent runs
        "iters"             : 500,                                                  # number of generations
        "m_objs"            : 3,                                                    # number of objectives
        "pop_size"          : pop_size,                                             # number of individuals
        "params_mop"        : {"name": "dtlz1"},                                   # mop parameters
        #"params_mop"        : {"name": "dtlz1-spin", "degrees":180},                # mop parameters
        #"params_mop"        : {"name": "dtlz1-spin-cube", "rotation":"positive"},  # mop parameters
        "params_crossover"  : {"name": "sbx", "prob": 1.0, "eta": 30},              # crossover parameters
        "params_mutation"   : {"name": "polymutation", "prob": 1./7, "eta": 20},    # mutation parameters
        "params_stats"      : {"output_path": output_path},                         # stats parameters
        "verbose"           : True,                                                 # log verbosity
        
        
        # NSGA3 params
        "nsga3_weights"                 : weights,
        "nsga3_parent_selection_method" : "nsga2_tournament",
    }
    
    moea = NSGA3(params)
    chroms, objs, fig, ax = moea.evaluate(plot_output=True)
    
    
