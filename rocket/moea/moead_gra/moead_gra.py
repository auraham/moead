# moead_gra.py
"""
# baseline
243 - rand, r: 0.5589
244 - rand, r: 0.8726
245 - permutation, perm: [ 35 206  80 111 130 170 120 191  46 162  54 117  89 172 203 169  90  53
 
# new
243 - rand, r: 0.5589
244 - rand, r: 0.8726
245 - rand, r: 0.0137

"""

from __future__ import print_function
from rocket.moea import MOEAD
from rocket.moea.moead_gra.genetic_operations import genetic_operations
from rocket.moea.moead_gra.replacement import replacement
import numpy as np
from numpy.random import RandomState
import matplotlib.pyplot as plt

# debug
# from rocket.dev.rand import Rand

class MOEAD_GRA(MOEAD):
    
    def __init__(self, params):
        
        MOEAD.__init__(self, params)
        
        # @todo: add checks
        self.poi_delta    = params["moead-gra_poi_delta"]   # probability of improvement delta


    def update_improvement(self, fit_old_pop, fit_new_pop):
        """
        Implementation of Eq. (5) of [Zhou16]
        
        This function computes the relative improvement update of each subproblem according Eq. (5)
        
        Input
        fit_old_pop
        fit_new_pop
        
        Output
        poi
        """
        
        nsp = self.pop_size         # number of subproblems
        u   = np.zeros((nsp, ))     # relative improvement
        poi = np.zeros((nsp, ))     # probability of improvement vector
        eps = 1e-50                 # to avoid zero division
        
        almost_zero = 0.00001       # reset poi threshold
        
        # for each subproblem
        for i in range(nsp):                                                # @todo: is it possible to use a matrix division instead? something like
                                                                            #  (fit_old_pop - fit_new_pop) / (fit_old_pop+eps)
            u[i] = (fit_old_pop[i] - fit_new_pop[i]) / (fit_old_pop[i]+eps) # we need to add eps to avoid zero division

        # maximum relative improvement
        u_max = u.max()
        
        # check for reset
        if u_max < almost_zero:
            
            
            if self.verbose: print("reseting poi (u_max: %.4f)" % u_max)    # @todo: add log support here
        
            poi = np.ones((nsp, ))          # now, each subproblem has a probability of 1 to be selected for further improvement
            return poi.copy()


        # update poi 
        for i in range(nsp):
            poi[i] = (u[i] + eps) / (u_max + eps)       # we need two for loops, one to compute u_max and other to compute poi
        
        # @todo: this version returns negative probabilities
        
        return poi.copy()
        
        
    def get_next_subproblem(self, poi, i):
        """
        Select the next subproblem (index i) to evaluate according to this rule:
        
            - If rand() < poi[i], the i-th subproblem is chosen
            - Else, pick another j-th subproblem randomly and check again 
        
        Input
        poi
        i
        
        Output
        next_sp     index of next subproblem to evaluate
        """
        
        next_sp = i             # next subproblem to evaluate
        
        if self.rand.rand() < poi[i]:
            
            # if rand() < poi[i], the i-th subproblem is chosen
            next_sp = i
            
        else:
            
            # else, pick another j-th subproblem randomly and check again
            
            while True:
                
                j = self.rand.randint(0, self.pop_size)
                
                if self.rand.rand() < poi[j]:
                    
                    next_sp = j
                    break
        
        return next_sp
            
        
    def run(self, chroms, seed, run_id):
        """
        Independent run
        """
        
        self.rand   = RandomState(seed)
        
        # debug
        # chroms = np.genfromtxt("init_chroms.txt")
        # self.rand = Rand() 
        
        
        pop_size    = self.pop_size
        weights     = self.pop_weights
        neighbors   = self.create_neighborhood(weights)
        objs        = self.evaluate_pop_mop(chroms)
        
        # added to moead_gra
        poi         = np.ones((pop_size, ))                                 # init as ones vector
        fit_old_pop = self.evaluate_pop_scalar_method(objs, weights)        # fitness = scalarizing function value
        fit_new_pop = fit_old_pop.copy()
        poi_evals   = np.zeros((pop_size, ))                                # for stats only
        
        t = 1
        self.stats(chroms, objs, t, run_id)
        
        t = 2
        while t <= self.iters:
            
            for k in range(pop_size):
                
                # added to moead_gra
                i = self.get_next_subproblem(poi, k)
                
                # added to moead_gra
                child = genetic_operations(i, neighbors, chroms, self.lbound, self.ubound, self.pop_size, self.params_crossover["prob"], self.params_mutation["prob"], self.rand)       # @todo: use crossover and mutation frontends instead
                
                child = self.check_pop_bounds(child)            
                
                child_objs = self.evaluate_pop_mop(child)
                
                # modified for moead_gra, use all the weights, not only a subset
                scalar_child = self.evaluate_pop_scalar_method(child_objs, weights)
                scalar_pop   = self.evaluate_pop_scalar_method(objs, weights)
                
                # added to moead_gra
                to_replace = replacement(scalar_child, scalar_pop)
                
                chroms[to_replace, :] = child.copy()
                objs[to_replace, :] = child_objs.copy()
                
                # stats
                poi_evals[i] += 1
            
            # @note: we output messages AFTER evaluating child on self.evaluate_pop_mop
            # this way, the value of self.fevals is the expected value
            if self.verbose:            # @todo: add log support here
                if t%100 == 0:
                    print ("t:", t, " fevals:", self.fevals)
            
            # compute stats
            self.stats(chroms, objs, t, run_id)
            
            # added to moead_gra
            if t % self.poi_delta == 0:
                
                fit_old_pop = fit_new_pop.copy()
                fit_new_pop = self.evaluate_pop_scalar_method(objs, weights) 
                
                poi = self.update_improvement(fit_old_pop, fit_new_pop)
            
            # next generation
            t+=1
            
        # save stats
        self.poi_evals = poi_evals.copy()
           
        return chroms.copy(), objs.copy()
        

    def plot_run(self, run_id):
        
        # add check for max numer of runs to avoid a lot of plots!
        # add a function to moea_base.py
        
        n = self.poi_evals.shape[0]
        x = np.arange(n)
        y = self.poi_evals
        
        fig_name = "poi_evals_run_%d" % run_id
        fig = plt.figure(fig_name)
        
        ax = fig.add_subplot(111)
        ax.cla()
        ax.set_title("Budget, probability of improvement vector")
        ax.set_xlabel("Subproblem ID")
        ax.set_xlabel("Evaluations")
        
        ax.plot(x, y, c="#A1123F")
        
        
        for t in (y.argmin(), y.argmax()):
            
            tx, ty = x[t], y[t]
            
            ax.text(tx, ty, "ID: %d, %d" % (t, ty))
            ax.plot(tx, ty, marker="o", c="#A1123F")
            
            #ax.plot(pop[:, 0], pop[:, 1], pop[:, 2], label=labels[i], c=colors[i], 
            #            mec=basecolors["almost_black"], ls="none", marker="o", markeredgewidth=0.15))
            
        
        plt.show()
        
        
        
