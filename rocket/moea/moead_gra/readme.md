# MOEA/D-GRA

This package contains an implementation of MOEA/D-GRA (Generalized Resource Allocation) proposed by Zhou and Zhang [Zhou16].

**Difference between this code and original algorithm** 

- The original algorithm [Zhou16] is based on MOEA/D-DE [Li09], an improved version of MOEA/D for complicated Pareto sets. On the other hand, our implementation is based on MOEA/D [Zhang07].  **Update**: both versions, [Zhou16] and our `moead_gra.py`, employs the same genetic operators (Algorithm 2 in [Zhou16]) and replacement criteria (Algorithm 3 in [Zhou16]), so both versions are similar.

- [Zhou16] specifies a way to define weight vectors that is not implemented in this version:

  > Let $w^i = (w_1^i, \dots, w_m^i)$, $i=1, \dots, N$, be a set of evenly distributed vectors that satisfy $\sum_{j=1}^{m}w_j^i = 1$, and $w_j^i>0$. The weight vectors are defined as:

$$
\lambda_{j} = \frac{\frac{1}{w_j^i}}{\sum_{k=1}^{m}{w_j^i}}
$$

  > where $j=1, \dots, m$, and $i=1, \dots, N$. In the original MOEA/D, the vectors $w^i$ are used as the weights directly. Some recent studies show that the settings in (1) will lead to better distributions on the Pareto Front [Giagkiosis13,Giagkiosis14]. For this reason, we define the weight vectors by (1) for MOEA/D and its variants in this paper.




# Files

- `moead_gra.py` main file.
- `genetic_operations.py` contains crossover and mutation operators specific for this MOEA.
- `replacement.py` contains the replacement rule for survival selection.
- `test_moea_config.py` contains an example.


# How to use

`test_moea_config.py` shows you how to use `moead_gra`:

```python
# test_moea_config.py
from __future__ import print_function
from rocket.moea import MOEAD_GRA
from rocket.helpers import get_n_genes, get_bounds
from rocket.labs import get_experiment_path
import numpy as np
import os

# define experiment output path
script_path = os.path.dirname(os.path.abspath(__file__))
output_path = get_experiment_path(script_path, name="sample")

if __name__ == "__main__":
    
    weights = np.genfromtxt("weights_m_2_240.txt")
    pop_size, m_objs = weights.shape
    
    mop_name = "t2"
    n_genes, wfg_k, wfg_l = get_n_genes(mop_name, m_objs)
    
    params = {
        "name"              : "moead-gra",                                          # name of moea
        "label"             : "MOEA/D-GRA",                                         # label of moea
        "runs"              : 4,                                                    # number of independent runs
        "iters"             : 200,                                                  # number of generations
        "m_objs"            : m_objs,                                               # number of objectives
        "pop_size"          : pop_size,                                             # number of individuals
        "params_mop"        : {"name": mop_name, "k": wfg_k, "degrees":180},        # mop parameters
        "params_crossover"  : {"name": "moead-gra_crossover", "prob": 0.8},         # crossover parameters
        "params_mutation"   : {"name": "moead-gra_mutation",  "prob": 0.1}, 		# mutation parameters
        "params_stats"      : {"output_path": output_path},                         # stats parameters
        "verbose"           : True,                                                 # log verbosity
        
        # MOEA/D params
        "neigh_size"        : 20,
        "pop_weights"       : weights,
        "params_form"       : {"name": "te"},
        
        # MOEA/D-GRA
        "moead-gra_poi_delta": 10,
    }
    
    moea = MOEAD_GRA(params)
    chroms, objs, fig, ax = moea.evaluate(plot_output=True, plot_output_name="moead_gra.png")
    
    
```

This figure is obtained as output after 200 generations:

![](moead_gra.png)





# Update improvement



# Selection of next subproblem



# Genetic operations



# Replacement



# TODO



- [x] Add `poi_plot` to check the improvement of each subproblem.
- Implement other utility functions from [Zhou16], e.g., Equations (7-12).
- `update_improvement` return negative probabilities. Fix needed?
- Implement weights modification, Equation (3).



# References

- [Zhang07] *MOEA/D: A Multiobjective Evolutionary Algorithm Based on Decomposition*.
- [Li09] *Multiobjective Optimization Problems with Complicated Pareto Sets*.
- [Zhou16] *Are All the Subproblems Equally Important? Resource Allocation in Decomposition-Based Multiobjective Evolutionary Algorithms*.
- [Giagkiosis13] *Generalized Decomposition*
- [Giagkiosis14](http://www.sciencedirect.com/science/article/pii/S0020025514006057)  *Generalized Decomposition and Cross Entropy Methods for Many-Objective*