# replacement.py
from __future__ import print_function
import numpy as np

def replacement(scalar_child, scalar_pop):
    """
    This function implements Algorithm 3 of [Zhou16]
    
    Determine which individual (index k) should be replaced for child (child is created outside this function).
    First, we identify the k-th subproblem where child is the best.
    Then, check whether scalar_child[k] is better (lower) than scalar_pop[k]
    """
    
    to_replace  = []
    eps         = 1e-50     # to avoid zero division
    
    k = ((scalar_pop - scalar_child) / (scalar_pop + eps)).argmax() 
    
    if scalar_child[k] < scalar_pop[k]:
        to_replace = k
        
    return to_replace
