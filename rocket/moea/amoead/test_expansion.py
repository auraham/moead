# test_expansion.py
from __future__ import print_function
from rocket.moea.amoead.expansion import expand_ext
from rocket.plot.colors import colors, basecolors

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np

def plot_weights(weights, new_weights, title, save=False):
    
    fig = plt.figure()
    ax = fig.add_subplot(111, projection="3d")
    ax.set_xlabel("$f_1$")
    ax.set_ylabel("$f_2$")
    ax.set_zlabel("$f_3$")
    ax.set_title(title)
    
    ax.set_xlim(0, 1.1)
    ax.set_ylim(0, 1.1)
    ax.set_zlim(0, 1.1)
    
    lined = {}
    
              
    # weights
    l, = ax.plot(new_weights[:, 0], new_weights[:, 1], new_weights[:, 2], c="#B0E2AB", label="new weights",
                            mec=basecolors["almost_black"], ls="none", marker="o", markeredgewidth=0.15)
    lined["new weights"] = l
             
    # weights
    l, = ax.plot(weights[:, 0], weights[:, 1], weights[:, 2], c="#9E0E42", label="weights",
                            mec=basecolors["almost_black"], ls="none", marker="o", markeredgewidth=0.15)
    lined["weights"] = l
   
   
        
                  
    ax.view_init(15, 45)
    
    leg = ax.legend(loc="lower left")
    
    # for callback
    for legtxt in leg.get_texts():
        legtxt.set_picker(5)
        
    # inner callback
    def on_pick(event):
        
        legtxt, line = None, None
        
        obj = event.artist
        is_callable = callable(getattr(obj, "get_text", None))
        
        if is_callable:
            
            legtxt = event.artist
            line = lined[legtxt.get_text()]
            
        else:
            
            legtxt = None
            line = event.artist
            
        is_visible = line.get_visible()
        
        # fade a little bit
        if is_visible:
            
            line.set_visible(False)
            
            if legtxt is not None:
                legtxt.set_alpha(0.2)
                
        else:
            
            line.set_visible(True)
            
            if legtxt is not None:
                legtxt.set_alpha(None)
                
        fig.canvas.draw()
    
    # register callback
    fig.canvas.mpl_connect("pick_event", on_pick)
    
    
    if save:
        output = "weights.png"
        fig.savefig(output, dpi=300)

    plt.show()


if __name__ == "__main__":
    
    weights     = np.genfromtxt("weights_m_3.txt")
    n_weights   = weights.shape[0]
    
    center_ids  = np.arange(n_weights, dtype=int)
    
    new_weights, counter = expand_ext(center_ids, weights.copy(), p=12, scale=0.5)
        
    plot_weights(weights, new_weights, "weights", save=False)
