#!/usr/bin/env bash

PATH="$HOME/rocket/performance/hv-1.3-src:$PATH"
export PATH

m_objs=3
experiment_path="/media/data/experiment_a"

# UNCOMMENT for dtlz1 and dtlz1-spin
for mop_name in dtlz1 dtlz1-spin ; do

# UNCOMMENT for inv-dtlz1
#for mop_name in inv-dtlz1 ; do

    for form in asf pbi ipbi ; do
    
        for param in 2 4 6 8 10 ; do
        
            moea_name=amoead-"$form"-"$param"
            
            echo "$mop_name" "$moea_name"
            
            for run_id in {0..29} ; do
        
                output_filename=hv_"$moea_name"_m_"$m_objs"_run_"$run_id".txt
                output_filepath="$experiment_path"/"$mop_name"/"$moea_name"/stats/"$output_filename"
                
                # eliminar output_filepath si existe
                if [ -f "$output_filepath" ] ; then
                    echo deleting previous file "$output_filepath"
                    rm "$output_filepath"
                fi

                # UNCOMMENT for dtlz1 and dtlz1-spin
                for step in {5..300..5} ; do
                
                # UNCOMMENT for inv-dtlz1
                #for step in {5..600..5} ; do
                
                    input_filename=objs_"$moea_name"_m_"$m_objs"_run_"$run_id"_t_"$step".txt
                    input_filepath="$experiment_path"/"$mop_name"/"$moea_name"/pops/"$input_filename"
                    
                    if [ ! -f "$input_filepath" ] ; then
                    
                        echo "$input_filepath" does no exist
                    
                    else
                        
                        #echo "uncomment to compute"
                        # -q quiet
                        # -r ref point
                        hv -q -r '1.1 1.1 1.1' "$input_filepath" >> "$output_filepath"
                    fi
                    
                done
                
            done
            
        done
        
    done
    
done
