# debug_associate.py
from __future__ import print_function
from rocket.plot.colors import  basecolors
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from rocket.plot import load_rcparams
from rocket.fronts import get_real_front
import numpy as np
from rocket.moea.amoead.associate import associate
from rocket.moea.amoead.normalize import norm_matrix

#np.seterr(all='raise')

def plot_weights(objs, weights, rho, pie, title, figname, save=False, to_keep=None):
    
    fig = plt.figure()
    ax = fig.add_subplot(111, projection="3d")
    ax.set_xlabel("$f_1$")
    ax.set_ylabel("$f_2$")
    ax.set_zlabel("$f_3$")
    ax.set_title(title)
    
    ax.set_xlim(0, 1.1)
    ax.set_ylim(0, 1.1)
    ax.set_zlim(0, 1.1)
    
    # objs
    colors = [
        "#74CE74",
        "#2EB5B7",
        "#F7523A",
        "#5A5A5A",
        "#0759AB",
        "y",
        "m",
        "g",
        "c",
        "#F9FF81",
        ]
    
    plot_objs = False
    plot_objs_id = True
    if to_keep is None:
        to_keep = np.arange(rho.shape[0], dtype=int)
        plot_objs_id = False
        plot_objs = True
    
    lined = {}
    
    # weights
    l, = ax.plot(weights[:, 0], weights[:, 1], weights[:, 2], mfc="none", label="weights",
                            mec=basecolors["almost_black"], ls="none", marker="o", markeredgewidth=0.15)
    lined["weights"] = l
             
    
    # objs
    if plot_objs:
        l, = ax.plot(objs[:, 0], objs[:, 1], objs[:, 2], label="objs", c=colors[0], 
                            mec=basecolors["almost_black"], ls="none", marker="o", markeredgewidth=0.15)
        lined["objs"] = l

    # weights
    vmin, vmax = rho.min(), rho.max()
    for i, w in enumerate(weights):
        
        if i in to_keep:
            x, y, z = w
            label = "%d (%d)" % (i, rho[i])
            #label = "%d" % rho[i]
            
            if rho[i] > 0:
                ax.text(x, y, z, label, color="#404040", size=10)
            
            alpha = (rho[i])/vmax
            
            ax.plot(weights[[i], 0], weights[[i], 1], weights[[i], 2], c="#9E0E42", 
                                mec=basecolors["almost_black"], ls="none", marker="o", markeredgewidth=0.15, alpha=alpha)
          
    
    
    indices = np.arange(objs.shape[0])

    if plot_objs_id:
        
        for wi, weight_index in enumerate(to_keep):
            
            to_plot = indices[pie == weight_index]
        
            if len(to_plot) > 0:
                
                for i in to_plot:
                    
                    obj = objs[i]
                    
                    x, y, z = obj
                    label = "%d" % i
                    ax.text(x, y, z, label, color="#4D8861", size=10)
                    
        
                # objs
                label = "Objs %d" % weight_index
                l, = ax.plot(objs[to_plot, 0], objs[to_plot, 1], objs[to_plot, 2], label=label, c=colors[wi], 
                                    mec=basecolors["almost_black"], ls="none", marker="o", markeredgewidth=0.15)
                lined[label] = l
                      
    ax.view_init(15, 45)
    
    leg = ax.legend(loc="lower left")
    
    # for callback
    for legtxt in leg.get_texts():
        legtxt.set_picker(5)
        
    # inner callback
    def on_pick(event):
        
        legtxt, line = None, None
        
        obj = event.artist
        is_callable = callable(getattr(obj, "get_text", None))
        
        if is_callable:
            
            legtxt = event.artist
            line = lined[legtxt.get_text()]
            
        else:
            
            legtxt = None
            line = event.artist
            
        is_visible = line.get_visible()
        
        # fade a little bit
        if is_visible:
            
            line.set_visible(False)
            
            if legtxt is not None:
                legtxt.set_alpha(0.2)
                
        else:
            
            line.set_visible(True)
            
            if legtxt is not None:
                legtxt.set_alpha(None)
                
        fig.canvas.draw()
    
    # register callback
    fig.canvas.mpl_connect("pick_event", on_pick)
    
    if save:
        fig.savefig(figname, dpi=300)

def pretty(x):
    
    return "[" + " ".join(["%d" % i for i in x ]) + "]"

def summary(rho, pie, to_keep=None):
    
    if to_keep is None:
        to_keep = np.arange(rho.shape[0], dtype=int)
    
    for i, count in enumerate(rho):
        
        if i in to_keep:
        
            count = rho[i]
            index = i
            if count > 0:
                print("w %2d: %2d, %s" % (index, count, np.where(pie == index)[0])) 

if __name__ == "__main__":
    
    load_rcparams((10, 8))
    
    objs        = np.genfromtxt("sample/dtlz1-spin/amoead-pbi/pops/objs_amoead-pbi_m_3_run_0_final.txt")
    weights     = np.genfromtxt("weights_m_3.txt")
    criteria    = "pdist"
    
    rho, pie, _ = associate(objs, weights, criteria)
    plot_weights(objs, weights, rho, pie, "association", "association.png", save=False)
    summary(rho, pie)
    
    
