# expansion_layers.py
from __future__ import print_function
from rocket.moea import MOEAD
from rocket.moea.amoead.expansion import expand_ext
from rocket.moea.amoead.associate import associate, associate_ext, associate_frontend
from rocket.moea.amoead.normalize import norm_matrix
from rocket.moea.amoead.utility import utility_matrix
from rocket.moea.amoead.sort_subproblems import sort_subproblems_frontend
from rocket.moea.amoead.replacement import replacement

from rocket.plot import load_rcparams, colors, basecolors, plot_pops
import numpy as np
from numpy.random import RandomState
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

import os

def expand_weights_p(center_ids, weights, expansion_p, layer_indices):
    
    final_weights = weights.copy()
    final_counter = None
    scales = (0.5, 0.25)
    
    final_counter = { ind: 0 for ind in center_ids }        # todos inician con cero
    
    for i, p in enumerate(expansion_p):
        
        to_keep = np.where(layer_indices == i)[0]
        
        w = weights[to_keep]                        # vectors from i-th layer
        
        # filtro, solo los que estan indices de center_ids que estan en to_keep
        to_expand = np.array([ ind for ind in center_ids if ind in to_keep ])
        
        if len(to_expand) > 0:
        
            # create new weights
            new_weights, new_counter = expand_ext(to_expand, final_weights.copy(), p, scale=scales[i])
            
            final_weights = new_weights.copy()
            
            for j, ind in enumerate(to_expand):
                
                final_counter[ind] = new_counter[j]
            
            
    final_counter = np.array([ final_counter[ind] for ind in center_ids ])  # conversion de dict a array en orden
            
    return final_weights.copy(), final_counter.copy()
    
def plot_layers(weights, layer_indices):
    
    vals = np.unique(layer_indices)
    
    pops = []
    labels = []
    
    # get layers
    for i in vals:
        
        indices = np.where(layer_indices == i)
        w = weights[indices]
        
        label = "Layer %d" % i
        
        pops.append(w.copy())
        labels.append(label)
      
    # plot layers
    fig, ax = plot_pops(pops, labels)
    
    # plot ids
    for i, v in enumerate(weights):
        
        x, y, z = v
        
        ax.text(x, y, z, str(i), color="#404040")
        
    plt.show()

def plot_expansion(weights, new_weights):
    
    pops = []
    labels = []
   
    pops = (new_weights, weights)
    labels = ("B", "A")
      
    # plot layers
    fig, ax = plot_pops(pops, labels)
    
    # plot ids
    for i, v in enumerate(weights):
        
        x, y, z = v
        
        ax.text(x, y, z, str(i), color="#404040")
        
    plt.show()
    
def plot_expansion(weights, new_weights, layer_indices, center_ids, counter, save=False):
    
    pops = []
    labels = []
    
    for i in (0, 1):            # layer ids
        
        to_keep = np.where(layer_indices == i)[0]
        w = weights[to_keep]
        label = "Layer %d" % i
        
        pops.append(w.copy())
        labels.append(label)
        
    # plot original weights
    fig, ax = plot_pops(pops, labels)
    
    # plot ids
    for i, v in enumerate(weights):
        
        x, y, z = v
        
        ax.text(x, y, z, str(i), color="#404040", size=10)
    
    
    # plot new weights
    ncolors = ("#D67922", "#367B2C")
    n_weights = weights.shape[0]
    start, end = n_weights, 0
    for i, ind in enumerate(center_ids):
        
        count = counter[i]
        
        end = start + count
        
        w = new_weights[start:end, :]
        
        start = end
        
        
        ax.plot(w[:, 0], w[:, 1], w[:, 2], c=ncolors[1], 
            mec=basecolors["almost_black"], ls="none", marker="o", markeredgewidth=0.15)
    
    
        print("# of vectors around %d: %d" % (ind, count))
    
    
    if save:
        fig.savefig("expansion_layers.png", dpi=300, bbox_inches="tight")
    
    
    plt.show()
    

if __name__ == "__main__":
    
    load_rcparams()
    
    weights = np.genfromtxt("weights_m_3_16_zeros.txt")
    layer_indices = np.genfromtxt("weights_m_3_16_ids.txt", dtype=int)
    
    #plot_layers(weights, layer_indices)
    
    #center_ids = np.array([0, 2, 8, 10, 12, 14])
    center_ids = np.array([0, 2, 8, 10, 12, 14])
    center_ids = np.array([5])
    #center_ids = np.array([13])
    center_ids = np.array([13, 3])
    expansion_p = (3, 2)
    new_weights, counter = expand_weights_p(center_ids, weights, expansion_p, layer_indices)


    plot_expansion(weights, new_weights, layer_indices, center_ids, counter, save=False)
    
    
    
