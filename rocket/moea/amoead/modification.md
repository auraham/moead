# Modificación de amoead

- `associate_frontend` no creo que requiera modificación
- `sort_subproblems_frontend` tampoco



```python
self.rand     = RandomState(seed)
self.pop_size = chroms.shape[0]        # @todo: [como remover este] reset necesario para multiples invocaciones de run

pop_size    = chroms.shape[0]
weights     = self.pop_weights.copy()
neighbors   = self.create_neighborhood(weights)
objs        = self.evaluate_pop_mop(chroms)

# AGREGADO
budget      = pop_size							# el budget sigue siendo el mismo
sorted_sp   = np.arange(budget, dtype=int)	
sorted_sp   = repeat_array(sorted_sp.copy(), budget) # debemos repetirlo algunas veces, no importa que se pase de budget
                
t = 2
while t <= self.iters:

    for j in range(budget):

        # added to amoead
        i = sorted_sp[j]

        neigh = neighbors[i]

        neigh_objs = objs[neigh]

        neigh_weights = weights[neigh]

        child = self.genetic_operations(i, neigh, chroms)

        child = self.check_pop_bounds(child)            

        child_objs = self.evaluate_pop_mop(child)
        
        # moead-gra
        scalar_child    = self.evaluate_pop_scalar_method(child_objs, weights)
        scalar_pop      = self.evaluate_pop_scalar_method(objs, weights)
        to_replace      = replacement(scalar_child, scalar_pop)
        chroms[to_replace, :] = child.copy()
        objs[to_replace, :] = child_objs.copy()
      
	if self.use_adaptation:
        
        rho, pie, _ = associate_frontend(self.association_a_type, objs, weights, is_principal, neighbors)
        sorted_sp = sort_subproblems_frontend(self.sort_sp_a_type, rho, budget, self.pop_size, self.rand)

        # AGREGADO
        sorted_sp = repeat_array(sorted_sp.copy(), budget)
                
                
     	# indices a expandir
        threshold   = self.expansion_threshold
        # quiza debamos permitir expandir los vectores secundarios!
        ids         = indices[rho[is_principal] > threshold]            # (0, 1, ..., budget-1 a lo mas)
        center_ids  = ids[np.invert(to_ignore[ids])]                    # (0, 1, ..., budget-1 a lo mas)
        slots       = center_ids.shape[0]                               # numero de vectores a expandir, puede ser cero

        if slots > 0:

            # crear nuevos vectores
            weights, counter = self.expand_weights(center_ids, weights.copy())
            new_vectors = counter.sum()

            # update
            to_ignore[center_ids] = True
                    
            if new_vectors > 0:
                        
           		# actualizar vecindario
                # (n_weigths+new_vectors, neigh_size)
                neighbors = self.create_neighborhood(weights)        

                # actualizar pop
                chroms, objs = self.expand_pop(chroms.copy(), objs.copy(), new_vectors)

                # actualizar rho
                n_weights = weights.shape[0]
                rho = np.zeros((n_weights, ))       # no importa si se sobreescribe

                # update
                is_principal = np.zeros((n_weights, ), dtype=bool)
                is_principal[:budget] = True

                rho, pie, _ = associate_frontend(self.association_b_type, objs, weights, is_principal, neighbors)
                sorted_sp = sort_subproblems_frontend(self.sort_sp_b_type, rho, budget, self.pop_size, self.rand)

               	# AGREGADO
        		sorted_sp = repeat_array(sorted_sp.copy(), budget)
        
            # @note: we output messages AFTER evaluating child on self.evaluate_pop_mop
            # this way, the value of self.fevals is the expected value
            if self.verbose:            # @todo: add log support here
                if t%100 == 0:
                    print ("t:", t, " fevals:", self.fevals)
            
            # compute stats
            if self.use_adaptation:
            
                # added to reduce subsamplings!
                if t % self.stats_step == 0:
                    
                    # subsampling before stats!
                    tmp_rho, _ , _ = associate_frontend(self.association_c_type, objs, weights, is_principal, neighbors)
                    tmp_sorted_sp = sort_subproblems_frontend(self.sort_sp_c_type, tmp_rho, budget, self.pop_size, self.rand)

                    # aqui no se que hacer para el subsampling
                    # quiza guardar solo los primeros pop_size
                    
                    tmp_to_keep = tmp_sorted_sp[:budget]
                    
                    input_chroms = chroms[tmp_to_keep]
                    input_objs = objs[tmp_to_keep]
                
                    self.stats(input_chroms, input_objs, t, run_id)
                    
                    # added to save rho
                    rho, _, _ = associate_frontend("nearest", objs, weights, is_principal, neighbors, normalize_objs=True)
                    
                    filename = "rho_%s_m_%d_run_%d_t_%d.txt" % (self.name, self.m_objs, run_id, t)
                    filepath = os.path.join(self.paths["path_pops"], filename)
                    np.savetxt(filepath, rho, fmt="%d")
                
            else:
                
                self.stats(chroms, objs, t, run_id)
            
            
```



# Posible bug

```python
k = None

if is_principal[q]:
    k = q                            
else:

    neigh   = neighbors[q]                          
    to_keep = is_principal[neigh]                  
    
    # debemos mover esta linea aqui en caso de que la condicion
    # to_keep.sum() > 0:
    # no se cumpla!
    k = q                                       # association by default

    
    if to_keep.sum() > 0:

        pweights        = neigh[to_keep]            
        pdists          = closeness[i, pweights]   
        sorted_pweights = pweights[pdists.argsort()]

        ###k = q                                       # association by default

        for pw in sorted_pweights:

            if tmp_rho[pw] == 0:
                k = pw
                break

                # association
                rho[k] += 1
                pie[i] = k


```



Para hoy

- Crear un notebook con las versiones de `amoead.py` y su propósito. Por ahora, limitarnos a `amoead11.py` con población dispersa y `amoead12.py` con simplex de tres escalas.
- Agregar una nueva función para mover un *simplex* de 18 puntos (tres factores de escala: 0.25, 0.50, 0.75) en lugar de uno con 6 puntos. Esto lo hare para resolver el problema de falta de densidad cuando partimos de una población dispersa.  
- Hacer la animacion/debug en 2d con ilustracion del vecindario de MOEA/D.











