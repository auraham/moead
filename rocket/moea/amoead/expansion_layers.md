# Expansión en capas

![](expansion_layers.png)



La figura anterior muestra cómo se expanden los vectores de referencia alrededor de `center_ids=[13, 3]`.



**Convención**

- La capa externa es la `0`
- La capa interna es la `1`



**Notas para recordar**

- La diferencia entre expansión de vectores entre (1) una capa y (2) dos capas es el factor de escala de `expand_weights_p`. Para la capa externa se debe usar `scale=0.5`, mientras que para la capa interna se debe usar `scale=0.25`.
- Recuerda que los vectores de la capa interna ya están escalados con `0.5`, siguiendo la recomendación de [Deb14], Fig. 4.

**Algunas notas de desempeño**
- La función `expand_weights_p` *debe* devolver `final_counter` en el orden de `center_ids`. En el ejemplo de `expansion_layers.py` (changeset 333), los vectores centrales son:


```python
center_ids = [
  13,		# capa interna
  3			# capa externa
]
```

de modo que `final_counter` debe estar en orden de acuerdo a `center_ids`. Así:

```
counter[0] = 4	# vectores generados alrededor de 13
counter[1] = 2	# vectores generados alrededor de 3
```

Por ahora uso un diccionario para preservar el orden, pero no sé si eso impacte en el desempeño de la expansión cuando se usen más vectores con más capas. Es decir, se podría devolver un nuevo `center_ids` en orden, primero los indices de la capa externa y luego los de la interna, algo como:

```
def expand_weights_p(center_ids, weights, expansion_p, layer_indices):
	...
	return final_weights.copy(), final_counter.copy(), new_center_ids.copy()
```

donde `new_center_ids` esté en orden. Lo malo de esto es que tendríamos que cambiar `amoead9.py` para que use `new_center_ids` en lugar de `center_ids`. 



# Referencias

- [Deb14] *An Evolutionary Many-Objective Optimization Algorithm Using Reference-Point-Based Nondominated Sorting Approach, Part I: Solving Problems With Box Constraints*

