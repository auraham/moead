# amoead2.py
from __future__ import print_function
from rocket.moea import MOEAD
from rocket.moea.amoead.expansion import expand_ext
from rocket.moea.amoead.associate import associate
from rocket.moea.amoead.normalize import norm_matrix
from rocket.moea.amoead.utility import utility_matrix
from rocket.moea.amoead.replacement import replacement

import numpy as np
from numpy.random import RandomState
import matplotlib.pyplot as plt

# debug
from rocket.dev.rand import Rand

class AMOEAD3(MOEAD):
    
    def __init__(self, params):
        
        MOEAD.__init__(self, params)
        
        self.expansion_p = 12 # para los 91 vectores!
        self.expansion_done = False
        
    
    def sort_subproblems(self, rho):
        """
        Return a sorted array of indices, where
        
        rho[i] = 1 is better than rho[i] == 2
        rho[i] = 2 is better than rho[i] == 3
        etc
        
        Input
        rho         (n_weights, ) association counter array
        
        Output
        sorted_rho  (n_weights, ) array of indices
        """
        
        tmp         = rho.copy()
        tmp[rho==0] = rho.max()+1
        sorted_rho  = tmp.argsort()
        
        return sorted_rho.copy() 
        
        
    def update_pop(self, merged_objs, weights, rho):
        
        
        to_keep = np.zeros((self.pop_size, ), dtype=int)
        
        z_ideal = merged_objs.min(axis=0)
        z_nadir = merged_objs.max(axis=0)
        utility = utility_matrix(merged_objs, weights, z_ideal, z_nadir, self.params_form)      # (pop_size+1, n_weights)
        
        if weights.shape[0] == self.pop_size:
            
            # si no ha habido expansion de vectores ...
            per_col = 0
            to_keep = utility.argmin(axis=per_col)
            
        else:
            
            # si ya hubo expansion de vectores ...
            sorted_sp = self.sort_subproblems(rho)
            
            for i in range(self.pop_size):
                
                k = sorted_sp[i]
                to_keep[i] = utility[:, k].argmin()     # el argmin de la k-th col 
        
        return to_keep.copy()
        
        
    def create_neighborhood_objs(self, objs, weights, params_form):
        """
        Devuelve un vector closer de (n_weights, ), donde closer[i]=j
        indica que la mejor solucion para weights[i] es objs[j]
        
        Para definir "mejor" usamos la funcion de escalarizacion
        
        Input
        objs            (pop_size,  m_objs) objs matrix
        weights         (n_weights, m_objs) weights matrix, n_weights >= pop_size
        params_form     dict
        
        Output
        closer
        
        NO FUNCIONO
        """
        
        utility = utility_matrix(objs, weights, self.z_ideal, self.z_nadir, params_form)      # (pop_size, weights)
    
        per_col = 0
        closer  = utility.argmin(axis=per_col)
        
        return closer.copy()
        
          
    def get_mating_pool(self, neigh):
        
        indices = np.arange(self.pop_size, dtype=int)
        pool = np.zeros(neigh.shape, dtype=int)
        t = int(neigh.shape[0] / 2)
        
        rand_neigh = self.rand.permutation(neigh.copy())
        rand_pool  = self.rand.permutation(indices)
        
        pool[:t] = rand_neigh[:t]
        pool[t:] = rand_pool[:t]
        
        return pool.copy()
        
        
    def expand_weights(self, center_ids, weights):
        
        p = self.expansion_p
        n_weights = weights.shape[0]
        counter = np.zeros((n_weights, ))
        
        
        # create new weights
        weights, counter = expand_ext(center_ids, weights.copy(), p, scale=0.5)
        
        # como evitar que genere sobre los mismos center_ids una y otra vez?
        # counter debe ser zeros en ese caso
        
        return weights.copy(), counter.copy()
                
            
    def update_neighborhood_objs_prev(self, center_ids, counter, neighborhood_weights, prev_neighborhood_objs, neighbors_base):
        
        new_neighborhood_objs = neighborhood_weights.copy()
        m_objs = neighborhood_weights.shape[1]
                                                    # num_new_vectors = neighborhood_weights.shape[0] - prev_neighborhood_objs.shape[0]
        start = prev_neighborhood_objs.shape[0]     # debe ser menor a neighborhood_weights.shape[0] 
        end = 0
        for i, wi in enumerate(center_ids):
            
            if counter[i] > 0:
                
                # indica que weights[wi] genero counter[i] vectores de pesos
                # cuales? los siguientes counter[i] vectores a partir de start
                
                nc = counter[i]
                end = start+nc
                
                # checar contenido y shape
                #new_neigh = neighborhood_weights[wi].reshape((1, m_objs)).repeat(nc, axis=0)
                new_neigh = neighborhood_weights[wi].reshape((1, m_objs)).repeat(nc, axis=0)
                
                new_neighborhood_objs[start:end, :] = new_neigh.copy()
                
                start = end
                
        return new_neighborhood_objs.copy()
                
        
    def expand_pop(self, chroms, objs, slots):
        
        # select random pool
        size = chroms.shape[0]
        rand_pool = self.rand.permutation(size)[:slots]     # 
        
        
        if slots > size:
            
            # si hay mas slots que llenar que individuos de donde copiar...
            
            rand_pool = self.rand.permutation(slots)     # indices aleatorios entre [0, slots-1], slots > pop_size 
            pool = np.zeros((slots, ), dtype=int)
            
            for i in range(slots):
                
                pool[i] = rand_pool[i] % size
            
            rand_pool = pool.copy()
        
        
        # create new blocks
        new_chroms_block = chroms[rand_pool].copy()     # seguro que aqui el copy si es necesario para no copiar referencias sino contenido!
        new_objs_block = objs[rand_pool].copy()
        
        # update pop
        new_chroms = np.vstack((chroms, new_chroms_block))
        new_objs = np.vstack((objs, new_objs_block))
        
        # update pop_size
        self.pop_size = new_chroms.shape[0]
        
        return new_chroms.copy(), new_objs.copy()
                
    
    def run(self, chroms, seed, run_id):
        """
        Independent run
        """
        
        self.rand   = RandomState(seed)
        
        pop_size    = self.pop_size
        weights     = self.pop_weights
        neighbors   = self.create_neighborhood(weights)
        objs        = self.evaluate_pop_mop(chroms)
        
        # added to amoead
        budget      = self.pop_size
        norm_objs   = norm_matrix(objs)
        rho, _, _   = associate(norm_objs, weights)
        sorted_sp   = self.sort_subproblems(rho)
        rho_record  = np.zeros((weights.shape[0], self.iters))      # stats, aunque el numero de rows crecera luego!
        rho_record[:, 0] = rho.copy()
        
        # added to amoead (neighborhood)
        neighbors_weights   = self.create_neighborhood(weights)
        neighbors_objs      = neighbors_weights.copy()            # por ahora
        neighbors_base      = neighbors_weights.copy()            # dont edit
        
        
        t = 1
        self.stats(chroms, objs, t, run_id)
        
        t = 2
        while t <= self.iters:
            
            for j in range(budget):
                
                #print("j", j)
                
                # added to amoead
                i = sorted_sp[j]

                # moead                
                #neigh = neighbors[i]
                #neigh_objs = objs[neigh]
                #neigh_weights = weights[neigh]
                
                # amoead
                ind_neigh_weights   = neighbors_weights[i]          # (neigh_size, )
                neigh_weights       = weights[ind_neigh_weights]
                
                ind_neigh_objs      = neighbors_objs[i]
                neigh_objs          = objs[ind_neigh_objs] 
                
                neigh = ind_neigh_objs                          # neigh funciona como nuestro mating pool
                                                                # y como medio de actualizacion
                
                child = self.genetic_operations(i, neigh, chroms)
                
                child = self.check_pop_bounds(child)            
                
                child_objs = self.evaluate_pop_mop(child)

                # moead                
                #scalar_child = self.evaluate_pop_scalar_method(child_objs, neigh_weights)
                #scalar_neigh = self.evaluate_pop_scalar_method(neigh_objs, neigh_weights)
                #to_replace = neigh[scalar_child <= scalar_neigh]
                #chroms[to_replace, :] = child.copy()
                #objs[to_replace, :] = child_objs.copy()
        
                # moead-gra # continua aqui
                scalar_child = self.evaluate_pop_scalar_method(child_objs, weights)
                scalar_pop   = self.evaluate_pop_scalar_method(objs, weights)
                to_replace = replacement(scalar_child, scalar_pop)
                chroms[to_replace, :] = child.copy()
                objs[to_replace, :] = child_objs.copy()
                
            
            # added to amoead
            rho, pie, _ = associate(objs, weights)
            rho_record[:, t-1] = rho.copy()
            sorted_sp = self.sort_subproblems(rho)  # tiene sentido hacer el sort porque el numero de vectores aumentara
                                                    # y solo evaluaremos los primeros pop_size de ellos!
            
            
            
            # expansion
            to_expand  = rho > 0                    # (n_weights, ) boolean array
            slots      = to_expand.sum()            # numero de vectores a expandir
            indices    = np.arange(self.pop_size, dtype=int)
            center_ids = indices[to_expand]         # indices de los vectores a expandir
            
            if slots > 0:
                
                
                
                #import ipdb; ipdb.set_trace()
                
                # crear nuevos vectores
                weights, counter = self.expand_weights(center_ids, weights.copy())
                new_vectors = counter.sum()
                
                # debug
                print("expansion, new vectors: %d, total vectors: %d, center_ids: %s," % (new_vectors, weights.shape[0], center_ids))
                
                # darle oportunidad a otros subproblemas
                sp = sorted_sp.copy()
                l = int(budget/2)
                sp[-l:] = self.rand.permutation(self.pop_size)[-l:]
                
                sorted_sp = sp.copy()
                
                # mas extremo
                sorted_sp = self.rand.permutation(self.pop_size)
                
                
                if new_vectors > 0:
                    
                    # actualizar vecindario
                    # (n_weigths+new_vectors, neigh_size)
                    neighbors_weights = self.create_neighborhood(weights)        
        
                    # actualizar vecindario
                    # (n_weigths+new_vectors, neigh_size)
                    # las nuevas slots entradas seran iguales a los vecindarios de los vectores center_ids 
                    neighbors_objs = neighbors_weights.copy()
                    
                    # actualizar pop
                    chroms, objs = self.expand_pop(chroms.copy(), objs.copy(), new_vectors)
                    
                    # actualizar rho, rho_record
                    n_weights = weights.shape[0]
                    rho = np.zeros((n_weights, ))       # no importa si se sobreescribe
                    
                    rows = n_weights - rho_record.shape[0]
                    cols = rho_record.shape[1]
                    rho_block = np.zeros((rows, cols))
                    rho_record = np.vstack((rho_record, rho_block))
                
                
                    rho, pie, _ = associate(objs, weights)  # tengo que volver a asociar para hacer el sort
                    #rho_record[:, t-1] = rho.copy()
                    sorted_sp = self.sort_subproblems(rho)  # tiene sentido hacer el sort porque el numero de vectores aumentara
                                                            # y solo evaluaremos los primeros pop_size de ellos!
                
                    # darle oportunidad a otros subproblemas
                    sp = sorted_sp.copy()
                    l = int(budget/2)
                    sp[-l:] = self.rand.permutation(self.pop_size)[-l:]
                    
                    sorted_sp = sp.copy()
                
                
           
            # debug
            if t in (30, 50, 200, ):
                
                
                from rocket.fronts import get_real_front
                from rocket.plot import plot_pops, basecolors
                
                mop_name        = self.params_mop["name"]
                real_front, p   = get_real_front(mop_name, self.m_objs, self.n_genes, self.params_mop)
                
                W = weights
                pops = (objs, W)
                labels = ("objs", "weights")
                
                pops = (W, )
                labels = ("weights", )
                
                
                fig, ax = plot_pops(pops, labels, real_front=real_front, p=p)
                
                
                indices = np.where(rho > 0)[0]
                
                for ind in indices:
                    w = W[ind]
                    x, y, z = w
                    #ax.text(x, y, z, str(ind), color="#404040", size=10)
                    
                    ax.plot(W[[ind], 0], W[[ind], 1], W[[ind], 2, ], c="#95234E", 
                        mec=basecolors["almost_black"], ls="none", marker="o", markeredgewidth=0.15)
                    
                    to_plot = np.where(pie == ind)[0]
                    
                    if len(to_plot):
                        for tp in to_plot:
                            
                            x, y, z = objs[tp]
                            #ax.text(x, y, z, str(tp), color="#92C3B8", size=10)
                    
                            
                plt.show()
                
                #import ipdb; ipdb.set_trace()
        
                
            
            
                
            # @note: we output messages AFTER evaluating child on self.evaluate_pop_mop
            # this way, the value of self.fevals is the expected value
            if self.verbose:            # @todo: add log support here
                if t%1 == 0:
                    print ("t:", t, " fevals:", self.fevals)
            
            # compute stats
            self.stats(chroms, objs, t, run_id)
            
            # next iter
            t+=1
            
        # debug
        #filepath = "rho/rho_amoead3_%s.txt" % self.params_mop["name"]
        #np.savetxt(filepath, rho_record, fmt="%d")
        
        
        # subsampling
        rho, pie, _ = associate(objs, weights)
        sorted_sp   = self.sort_subproblems(rho)  # tiene sentido hacer el sort porque el numero de vectores aumentara
                                                    # y solo evaluaremos los primeros pop_size de ellos!
        
        to_keep = sorted_sp[:budget]
        chroms = chroms[to_keep]
        objs = objs[to_keep]
        
           
        return chroms.copy(), objs.copy()

    
    
