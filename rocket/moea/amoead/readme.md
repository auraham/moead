# Adaptive MOEA/D

This package contains an implementation of our A-MOEA/D. This hybrid is based on theses key features:

- Adaptation of referentce vectors [Jain13,Deb14]




# Association

This figure shows the number of solutions associated to each weight vector (`plot_weights.py`). As can be seen, only a few vectors are associated to one solution only. On the other hand, there are vectors associated up to 10 solutions. This uniformity difficults the spreading of solutions over the real front.

![](weights.png)

This is the same figure but hiding the population. The darker of the color of the vector `w_i`, the greater of the value of `rho[i]`. 

![](weights_no_objs.png)

The aim of showing this figure is to illustrate those regions in the objective space  *where* to expand vectors.





La finalidad de la figura anterior es mostrar en qué regiones conviene realizar la expansión, en aquellas donde `rho[i]` es pequeño.



# Aceleración

Podríamos acelerar `amoead.py` de este modo:

1. Iniciar con un conjunto disperso de ~30 vectores de referencia. El `budget` seguirá siendo 91 como antes para tres objetivos.
2. `sorted_sp` tendrá este patron, `0, 1, ..., 29, 0, 1, ..., 29, 0, 1, ...` hasta completar una longitud de `budget`. Este patron solo se usara al inicio, en la primera generacion.
3. En las siguientes generaciones, usaremos `associate` para estimar la densidad de los vectores y expandir aquellos con mayor densidad. Con algo de suerte, el conjunto disperso se convertira en denso (como si se tratara del conjunto original), pero reduciendo el costo de la busqueda (realmente se reduce el costo de la busqueda? lo que *sí* se reducirá será el costo de invocar `associate`, ya que habra menos vectores no utilizados)
4. Bajo este modelo, podriamos usar un umbral `thres` en donde expadimos luego de `thres` generaciones, por decir algo, aunque iria en contra de `center_ids`.


**Para qué serviría? (Motivación)**

- Para evitar usar el modelo de vectores en capas (que ya vimos que no sirven si se quiere diversidad en tres objetivos).


















# Use association correctly



```
case a, rho [ 0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  1  0  0  0  0  0  0
  0  0  0  0  4  2  6  0  0  0  0  0  0  0 15  3  4  1  0  0  0  0  0 17  4
  4  0  0  0  0  0  6  2  2  2  0  0  0  2 10  6  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0]
```

```
case b, rho [0 0 0 0 0 2 0 0 0 0 0 0 0 0 0 0 0 3 0 2 0 2 4 0 0 0 0 0 6 3 1 0 1 1 0 0 0
 0 8 7 0 1 1 2 0 1 0 0 9 1 2 0 3 0 0 0 5 0 0 1 1 0 0 2 1 1 1 1 0 0 1 4 5 2
 0 2 0 0 4 0 0 0 0 0 0 0 0 0 0 0 0]
 
```

```
case c, rho [ 0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  1  0  0  0  0  0  0
  0  0  0  0  4  2  6  0  0  0  0  0  0  0 15  3  4  1  0  0  0  0  0 17  4
  4  0  0  0  0  0  6  2  2  2  0  0  0  2 10  6  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0]
  
```

```
case d, rho [0 0 0 0 0 2 0 0 0 0 0 0 0 0 0 0 0 3 0 2 0 2 4 0 0 0 0 0 6 3 1 0 1 1 0 0 0
 0 8 7 0 1 1 2 0 1 0 0 9 1 2 0 3 0 0 0 5 0 0 1 1 0 0 2 1 1 1 1 0 0 1 4 5 2
 0 2 0 0 4 0 0 0 0 0 0 0 0 0 0 0 0]
```

```
case d, rho [ 0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  1  0  0  0  0  0  0
  0  0  0  0  4  2  6  0  0  0  0  0  0  0 15  3  4  1  0  0  0  0  0 17  4
  4  0  0  0  0  0  6  2  2  2  0  0  0  2 10  6  0  0  0  0  0  0  0  0  0
  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0]
```







# TODO

- [ ] Hacer una figura como la anterior comparando diferentes métodos para asociar vectores: distancia perpendicular, coseno.



# References

- [Zhang07] *MOEA/D: A Multiobjective Evolutionary Algorithm Based on Decomposition*.
- [Jain13] *An Improved Adaptive Approach for Elitist Nondominated Sorting Genetic Algorithm for Many-Objective Optimization (A$^{2}$-NSGA-III).*
- [Deb14] *An Evolutionary Many-Obejctive Optimization Algorithm Using Reference-Point-Based Nondominated Sorting Approach, Part II: Handling Constrained and Extending to an Adaptive Approach.*


# Parametrización de `amoead.py`

- [x] Crear una version de `amoead6.py` que permita parametrizar estos métodos:

- [x] **Normalización antes de asociación**  

```python
params["amoead-association_norm_objs"] = True | False

# uso
rho, pie = associate_frontend(self.association_type, self.association_norm_objs, objs, weights, is_principal, neighbors)

```




- [x] **Tipo de asociación**


```python
params["amoead-association_type"] = "nearest" | "hierarchy"

# uso
rho, pie = associate_frontend(self.association_type, self.association_norm_objs, objs, weights, is_principal, neighbors)
```



- [x] **Tipo de ordenamiento (antes y después de generar vectores)**

```python
# sorting antes de expandir
params["amoead-sort_sp_a_type"] = "best_first" | "half" | "permutation" | "hierarchy"

# sorting despues de expandir (si la expansion ocurre)
params["amoead-sort_sp_b_type"] = "best_first" | "half" | "permutation" | "hierarchy"

# uso
sorted_sp = sort_subproblems_frontend(cat, rho)
```



Guardar `objs`, `chroms`, `rho`, `pie`, `to_keep` por cada configuración.



# Nuevo método de ordenamiento

```
Col 1. Conteo de asociacion
Col 2. is_principal


ej.

w_0 [0, 1]
w_1 [0, 1]
w_2 [2, 1]   <- center
w_3 [3, 1]   <- center
w_4 [0, 1]
w_5 [0, 1]

w_6 [1, 0]   <- around w_2
w_7 [1, 0]

w_8 [1, 0]   <- around w_3
w_9 [2, 0]


transform

w_0 [inf, 1]
w_1 [inf, 1]
w_2 [  2, 1]   <- center
w_3 [  3, 1]   <- center
w_4 [inf, 1]
w_5 [inf, 1]

w_6 [1, 0]   <- around w_2
w_7 [1, 0]

w_8 [1, 0]   <- around w_3
w_9 [2, 0]


sort

w_3  mas count, por ser primarios
w_2  menos count

w_9  mas count, por ser secundarios
w_6, w_7, w_8, menos count



```

clorodamida



- [x] termina `sort_subproblems_frontend` con los cuatro metodos de ordenamiento
- [x] prueba los cuatro metodos, en especial `hierarchy`, para saber si este último es el mejor en dtlz1-spin y dtlz1, mi hipótesis es que sí lo será. *por qué?* porque 
      - [x] (1) primero recorre los vectores primarios asociados (primero los que tienen mayor rho, luego los que tienen menor rho)*, 
      - [x] (2) luego recorre los vectores secundarios asociados  (primero los que tienen mayor rho, luego los que tienen menor rho)*, 
      - [x] (3) luego recorre los vectores primarios no asociados (el rho es cero, no importa el criterio de desempate)
      - [x] (4) por último, recorre los vectores secundarios no asociados (el rho es cero, no importa el criterio de desempate)



* ( * ) se puede cambiar a *(primero los que tienen menor rho, luego los que tienen mayor rho)*, pero no creo que sea bueno, mejor hay que darle prioridad a las regiones con mas, o eso seria contraproducente para poblar el centro?



por qué creo que funcionará en dtlz1?

- porque (1) dará preferencia a los vectores principales, así las primeras `budget` entradas de `sorted_sp` corresponderan en su mayoria a vectores principales (aunque seguro el problema del overhead aun seguira! porque los vectores secundarios seguiran ahi, aunque sin estar asociados)



por qué creo que funcionará en dtlz1-spin?

- porque (1) abarcará solo los vectores principales de las aristas del frente, dando algunos espacios disponibles para vectores secundarios. Si hay `k` vectores principales en las aristas, entonces habrán `budget-k` espacios para vectores secundarios
- con (2) podemos llenar los `budget-k` espacios con vectores secundarios




```
 criteria[sorted_sp]
array([[ -1, -10],
       [ -1,  -8],
       [ -1,  -6],
       [ -1,  -5],
       [ -1,  -4],
       [ -1,  -4],
       [ -1,  -2],
       [ -1,  -2],
       [ -1,  -2],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [  0, -15],
       [  0, -14],
       [  0, -10],
       [  0, -10],
       [  0,  -8],
       [  0,  -8],
       [  0,  -7],
       [  0,  -6],
       [  0,  -6],
       [  0,  -6],
       [  0,  -4],
       [  0,  -4],
       [  0,  -4],
       [  0,  -4],
       [  0,  -4],
       [  0,  -3],
       [  0,  -2],
       [  0,  -2],
       [  0,  -2],
       [  0,  -2],
       [  0,  -2],
       [  0,  -2],
       [  0,  -2],
       [  0,  -2],
       [  0,  -2],
       [  0,  -2],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0]])
```

debe quedar asi

```
 criteria[sorted_sp]
 
 basta con hacer un mask antes
 
array([
	   ----------
	   [ -1, -10],
       [ -1,  -8],
       [ -1,  -6],
       [ -1,  -5],
       [ -1,  -4],
       [ -1,  -4],
       [ -1,  -2],
       [ -1,  -2],
       [ -1,  -2],
       ----------
       [  0, -15],
       [  0, -14],
       [  0, -10],
       [  0, -10],
       [  0,  -8],
       [  0,  -8],
       [  0,  -7],
       [  0,  -6],
       [  0,  -6],
       [  0,  -6],
       [  0,  -4],
       [  0,  -4],
       [  0,  -4],
       [  0,  -4],
       [  0,  -4],
       [  0,  -3],
       [  0,  -2],
       [  0,  -2],
       [  0,  -2],
       [  0,  -2],
       [  0,  -2],
       [  0,  -2],
       [  0,  -2],
       [  0,  -2],
       [  0,  -2],
       [  0,  -2],
       ----------
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       [ -1,   0],
       ----------
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0],
       [  0,   0]])
```







- [x] programar el nuevo metodo de sorting `neigh`, probarlo y, si no funciona en dtlz1, pasarnos a chronos.


- [x] instala `pip3` en `chronos` para instalar `numpy`, `scipy`, y demás.
- [x] pasa `rocket` a `chronos` para acelerar las pruebas
- [x] probar las diferentes configuraciones de `amoead8.py` en `dtlz1-spin`, `dtlz1` para ver cuales son similares (y funcionan) y cuales no funcionan.




- [x] Ahora, debemos crear/agregar un parametro `params["amoead_associate_norm"]=True,False` para determinar si se normalizaran los objetivos antes de asociar. Creo que esto (si es `True`) ayudará a resolver `min-dtlz1`.









# TODO

- Quitar parametro `self.expansion_p`, `self.expansion_done`
- Checar por qué `amoead3.py` genera más vectores de pesos de los permitidos durante la expansión en `min-dtlz1`.
- Volver a ejecutar `compare.py` con `ipbi` en `min-dtlz1`.
- Checar cómo resuelve `moead.py` con `pbi, ipbi` en `min-dtlz1`, según recuerdo lo hacía bien pero con falta de diversidad.
   ```

   ```