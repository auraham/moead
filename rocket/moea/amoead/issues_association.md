# Issues about association

En este documento describo un problema encontrado al usar la asociación entre vectores de referencia y  vectores objetivo  (de aquí en adelante, entre vectores `weights` y soluciones `objs`, por simplicidad). El script `associate.py` realiza dicha asociación. Como resultado, devuelve un vector `rho` con tamaño `(n_weights, )`, donde `n_weights` es el número de vectores de referencia. El vector `rho` es un contador, en donde `rho[i]=j` indica que el vector `weights[i]` está asociado a `j` soluciones. Sin embargo, el contenido de `rho` parece depender de si las soluciones `objs` se encuentran normalizadas o no.



```
(a) No norm objs 
w 18:  1, [8]
w 29:  4, [ 6  7 19 20]
w 30:  2, [ 9 21]
w 31:  6, [10 11 12 22 23 24]
w 39: 15, [ 1  2  3  4  5 15 16 17 18 28 29 30 31 40 41]
w 40:  3, [32 42 43]
w 41:  4, [33 34 44 45]
w 42:  1, [35]
w 48: 17, [ 0 13 14 25 26 27 36 37 38 39 46 47 48 49 50 57 58]
w 49:  4, [51 52 59 60]
w 50:  4, [53 54 61 62]
w 56:  6, [55 56 63 64 65 71]
w 57:  2, [66 73]
w 58:  2, [67 68]
w 59:  2, [69 75]
w 63:  2, [70 76]
w 64: 10, [72 77 78 81 82 85 86 88 89 90]
w 65:  6, [74 79 80 83 84 87]
(b) Norm objs    
w  5:  2, [7 8]
w 17:  3, [ 6 19 20]
w 19:  2, [ 9 21]
w 21:  2, [10 22]
w 22:  4, [11 12 23 24]
w 28:  6, [ 3  4 16 17 29 30]
w 29:  3, [ 5 18 31]
w 30:  1, [32]
w 32:  1, [33]
w 33:  1, [34]
w 38:  8, [ 0 13 14 25 26 27 38 39]
w 39:  7, [ 1  2 15 28 40 41 50]
w 41:  1, [42]
w 42:  1, [43]
w 43:  2, [44 45]
w 45:  1, [35]
w 48:  9, [36 37 46 47 48 49 56 57 58]
w 49:  1, [59]
w 50:  2, [51 52]
w 52:  3, [53 54 62]
w 56:  5, [55 63 64 65 71]
w 59:  1, [60]
w 60:  1, [61]
w 63:  2, [70 76]
w 64:  1, [72]
w 65:  1, [66]
w 66:  1, [67]
w 67:  1, [68]
w 70:  1, [81]
w 71:  4, [77 82 85 88]
w 72:  5, [73 78 86 89 90]
w 73:  2, [74 80]
w 75:  2, [69 75]
w 78:  4, [79 83 84 87]

```




![](no_norm_objs.png)

![](norm_objs.png)


Hay varias cosas que resaltan:

- Hay vectores que son ocupados en (b) pero no en (a), por ejemplo, el 38, 28, 17 y 5. Esto indica que al normalizar `objs`, se pueden ocupar más vectores, pero no sé si sea correcto.
- El número de vectores asociados cambia. Por ejemplo, el conteo del vector 48 en (a) es 17, mientras que en (b) es 9. Es decir, se redujo el número de soluciones asociadas.

Para encontrar la razón de esta inconsistencia, revisaremos el conteo de dos vectores, 38 y 48, en ambos caso, sin normalización y con normalización.

Esta es la salida de `associate_issues.py` considerado los vectores de pesos`to_keep=[38, 48]`:

```
--- to keep ---
(a) No norm objs 
w 48: 17, [ 0 13 14 25 26 27 36 37 38 39 46 47 48 49 50 57 58]
(b) Norm objs    
w 38:  8, [ 0 13 14 25 26 27 38 39]
w 48:  9, [36 37 46 47 48 49 56 57 58]
```



Las siguientes figuras muestran los vectores 48 y 38, y las soluciones asociadas a ambos. En (a) los objetivos no están normalizados, mientras que en (b) sí lo están. 

Observe que el conjuto de soluciones asignado es casi el mismo. En (a) el vector 48 está asociado a:

```
[ 0 13 14 25 26 27 36 37 38 39 46 47 48 49 50 57 58]
```

mientras que en (b), los vectores 38 y 48 están asociados a casi el mismo conjunto anterior (a excepción de la solucion 50):

```
[ 0 13 14 25 26 27 38 39][36 37 46 47 48 49 56 57 58]
```





![](no_norm_objs_to_keep.png)

![](norm_objs_to_keep.png)

Aún no sé por qué la normalización parece generar una asociación diferente. Lo único que puedo decir es que, luego de la normalización, los objectivos se *expanden* un poco. Si se comparan ambas figuras previas, se puede ver cómo los objetivos en (b) ocupan un poco más de espacio en en (a). Esta expansión puede provocar que ocupen un mayor número de vectores de pesos. No sé si esto impacte negativamente en el manejo de diversidad, pero creo que conviene no hacer la normalización antes de la asociación. 

# TODO

- Parece que normalizar antes de asociar no es buena idea porque abarca más vectores de pesos.
- Checar el método de normalización de [Chen16], creo que normalizar como se hace ahora no ayudará a la asociación.



- [Chen16] *A Reference Vector Guided Evolutionary Algorithm for Many-Objective Optimization*