# get_results_experiment_a.py
from __future__ import print_function
import numpy as np
import os
from rocket.performance import hv
import matplotlib.pyplot as plt
from rocket.plot import load_rcparams, plot_pops
from rocket.fronts import get_real_front

def create_summary_hv_from_experiment(mop_name, moea_name, m_objs, runs, output_path, step, iters, force_write=False):
    
    rows    = runs
    cols    = int(iters/step)
    summary = np.zeros((rows, cols))    # each row contains cols hv measurements (ie, each row is an independent run)
    
    for run_id in range(runs):
        
        filename = "hv_%s_m_%d_run_%d.txt" % (moea_name, m_objs, run_id)
        filepath = os.path.join(output_path, mop_name, moea_name, "stats", filename)
        
        if os.path.isfile(filepath):
            
            values = np.genfromtxt(filepath)        # (cols, ) array
            
            summary[run_id, :] = values
            
        else:
            
            print("filepath does not exist:", filepath)
            
    # save
    # @todo: save as dataframe
    # @todo: add replacement check
    filename = "summary_hv_%s_m_%d.txt" % (moea_name, m_objs)
    filepath = os.path.join(output_path, mop_name, moea_name, "stats", filename)
        
    file_exists = os.path.isfile(filepath)
    write = True
    stat = ""

    if file_exists and force_write:
        write = True
        stat = "(replaced)"
        
    elif not file_exists:
        write = True
        stat = "(new)"
    else:
        write = False
        stat = "(unmodified)"
        
    if write:
        np.savetxt(filepath, summary)
        
    print("summary file %s: %s" % (stat, filepath))
    

def plot_summary_hv_from_experiment(mop_name, moea_names, m_objs, runs, output_path, step, iters, title):
    
    rows = len(moea_names)
    cols = int(iters/step)
    medians = np.zeros((rows, cols))
    
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.set_title(title)
    
    xticks = np.arange(step, iters+1, step)
    ax.xaxis.set_ticks(np.arange(50, iters+1, 50))
    
    for row, moea_name in enumerate(moea_names):
        
        # load summary
        filename = "summary_hv_%s_m_%d.txt" % (moea_name, m_objs)
        filepath = os.path.join(output_path, mop_name, moea_name, "stats", filename)
        summary = np.genfromtxt(filepath)
        
        x = xticks
        y = np.median(summary, axis=0)
        
        ax.plot(x, y, label=moea_name)
        
    ax.legend(loc="upper left")
    
    plt.show()


def plot_pops_from_experiment(mop_name, moea_names, m_objs, runs, output_path, step, iters, title):
    
    steps = int(iters/step)
    
    pops = []
    labels = []
    
    for moea_name in moea_names:
        
        # load summary
        filename = "summary_hv_%s_m_%d.txt" % (moea_name, m_objs)
        filepath = os.path.join(output_path, mop_name, moea_name, "stats", filename)
        summary = np.genfromtxt(filepath)
        
        # get best run
        run_id = summary[:, steps-1].argmax()       # ultima generacion, el mejor (argmax) hv de la ultima generacion
        
        # debug
        print("%s - %s best run: %d" % (mop_name, moea_name, run_id))
        
        # load objs
        #filename = "objs_%s_m_%d_run_%d_t_%d.txt" % (moea_name, m_objs, run_id, iters)
        filename = "objs_%s_m_%d_run_%d_final.txt" % (moea_name, m_objs, run_id)
        filepath = os.path.join(output_path, mop_name, moea_name, "pops", filename)
        objs = np.genfromtxt(filepath)
        
        pops.append(objs)
        labels.append(moea_name)
        
    real_front, p = get_real_front(mop_name, m_objs, params={"degrees":180})
    plot_pops(pops, labels, real_front=real_front, p=p, title=title, lower_lims=(0,0,0), upper_lims=(1,1,1))
    
    
if __name__ == "__main__":
    
    load_rcparams()
    
    mop_names = ("dtlz1", "dtlz1-spin", "inv-dtlz1")
    m_objs = 3
    runs = 30
    output_path = "/media/data/experiment_a"
    step = 5
    iters = 300
    
    for mop_name in mop_names:
        
        iters = 600 if mop_name == "inv-dtlz1" else 300
        
        # -- create summary hv --
        
        moea_names = ("amoead-asf-2", "amoead-asf-4", "amoead-asf-6", "amoead-asf-8", "amoead-asf-10", )
        for moea_name in moea_names:
            create_summary_hv_from_experiment(mop_name, moea_name, m_objs, runs, output_path, step, iters)
            
        moea_names = ("amoead-pbi-2", "amoead-pbi-4", "amoead-pbi-6", "amoead-pbi-8", "amoead-pbi-10", )
        for moea_name in moea_names:
            create_summary_hv_from_experiment(mop_name, moea_name, m_objs, runs, output_path, step, iters)
            
        moea_names = ("amoead-ipbi-2", "amoead-ipbi-4", "amoead-ipbi-6", "amoead-ipbi-8", "amoead-ipbi-10", )
        for moea_name in moea_names:
            create_summary_hv_from_experiment(mop_name, moea_name, m_objs, runs, output_path, step, iters)
        
        # -- plot --
        
        title = "%s final - asf" % mop_name
        moea_names = ("amoead-asf-2", "amoead-asf-4", "amoead-asf-6", "amoead-asf-8", "amoead-asf-10", )
        plot_summary_hv_from_experiment(mop_name, moea_names, m_objs, runs, output_path, step, iters, title)
        plot_pops_from_experiment(mop_name, moea_names, m_objs, runs, output_path, step, iters, title)

        title = "%s final - pbi" % mop_name
        moea_names = ("amoead-pbi-2", "amoead-pbi-4", "amoead-pbi-6", "amoead-pbi-8", "amoead-pbi-10", )
        plot_summary_hv_from_experiment(mop_name, moea_names, m_objs, runs, output_path, step, iters, title)
        plot_pops_from_experiment(mop_name, moea_names, m_objs, runs, output_path, step, iters, title)
        
        title = "%s final - ipbi" % mop_name
        moea_names = ("amoead-ipbi-2", "amoead-ipbi-4", "amoead-ipbi-6", "amoead-ipbi-8", "amoead-ipbi-10", )
        plot_summary_hv_from_experiment(mop_name, moea_names, m_objs, runs, output_path, step, iters, title)
        plot_pops_from_experiment(mop_name, moea_names, m_objs, runs, output_path, step, iters, title)
