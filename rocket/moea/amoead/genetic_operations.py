# genetic_operations.py
from __future__ import print_function
import numpy as np

def parent_selection(pop_size, neighbors, i, rand, ps_delta=0.8):
    
    
    # set mating pool
    mating_pool = np.arange(0, pop_size, dtype=int)     # all pop by default
    
    if rand.rand() < ps_delta:
        mating_pool = neighbors.copy()                  # only the neighborhood

    # choose parents
    p1, p2 = 0, 0
    while True:
        
        p1 = rand.permutation(mating_pool)[0]
        p2 = rand.permutation(mating_pool)[0]
        
        if p1 != i and p2 != i and p1 != p2:
            break
            
    return p1, p2
    
    
def crossover(p0, p1, p2, prob_crossover, rand, F=0.5):
    
    
    child = p0.copy()
    
    if rand.rand() < prob_crossover:
        
        child = p0 + F*(p1 - p2)
        
    return child.copy()
    
    
    
def repair(parent, child, lbound, ubound, rand):
    
    n_genes = child.shape[0]
    new_child = child.copy()
    
    for n in range(n_genes):
        
        if child[n] < lbound[n]:
            new_child[n] = parent[n] - rand.rand() * (parent[n] - lbound[n])
            
        if child[n] > ubound[n]:
            new_child[n] = parent[n] + rand.rand() * (ubound[n] - parent[n])
            
    return new_child.copy()
    

def mutation(child, rand, prob_mutation, lbound, ubound, eta=20):
    
    n_genes = child.shape[0]
    new_child = child.copy()
    
    for n in range(n_genes):
        
        # if flip() is True and child is under lbound/ubound
        if rand.rand() < prob_mutation and child[n] >= lbound[n] and child[n] <= ubound[n]:
            
            r           = rand.rand()
            mut_delta   = 0
            
            if r < 0.5:
                frac    = ((ubound[n] - child[n]) / (ubound[n] - lbound[n])) ** (eta+1)
                power   = (1.0/(eta+1))
                base    = 2*r + (1-2*r) * frac
                mut_delta = (base**power) - 1
        
            else:
                frac    = ((child[n] - lbound[n]) / (ubound[n] - lbound[n])) ** (eta+1)
                power   = (1.0/(eta+1))
                base    = 2 - 2*r + (2*r - 1) * frac
                mut_delta = 1 - (base**power)
                
            new_child[n] = child[n] + mut_delta * (ubound[n] - lbound[n])
            
    return new_child.copy()
    

def genetic_operations(i, neighborhood, chroms, lbound, ubound, pop_size, prob_crossover, prob_mutation, rand):
    
    # parent selection
    p1, p2 = parent_selection(pop_size, neighborhood[i], i, rand)

    # crossover
    child = crossover(chroms[i], chroms[p1], chroms[p2], prob_crossover, rand)
    
    # repair (bounds check)
    child = repair(chroms[i], child, lbound, ubound, rand)  
    
    # mutation
    child = mutation(child, rand, prob_mutation, lbound, ubound)
    
    return child.copy()        
            
            
            
    
            
        
        
    
