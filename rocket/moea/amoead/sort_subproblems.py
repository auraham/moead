# sort_subproblems.py
from __future__ import print_function
import numpy as np


def sort_subproblems_frontend(cat, rho, budget, pop_size, rand):
    """
    Devuelve un array de indices sorted_sp ordenado de acuerdo al criterio cat y 
    el arreglo count rho
    
    Input
    cat         string, "best_first", "half", "permutation", "hierarchy"
    rho         (n_weights, ) array, rho[i]=j indica que el vector w_i tiene j soluciones asociadas
    
    Output
    sorted_sp   (n_weights, ) array de indices ordenados, n_weights < budget
    """
    
    sorted_sp = None
    
    if cat == "best_first_a":
        
        tmp         = rho.copy()
        tmp[rho==0] = rho.max()+1           # asigna el mayor valor a los vectores no asociados
        sorted_sp   = tmp.argsort()         # primero los vectores con menos soluciones asociadas
        
        
    elif cat == "best_first_b":
        
        tmp         = rho.copy()
        sorted_sp   = tmp.argsort()[::-1]   # primero los vectores con mas soluciones asociadas
        
        
    elif cat == "half_first_a":
        
        tmp         = rho.copy()
        tmp[rho==0] = rho.max()+1           # asigna el mayor valor a los vectores no asociados
        sorted_sp   = tmp.argsort()         # primero los vectores con menos soluciones asociadas
        
        sp = sorted_sp.copy()
        l = int(budget/2)
        sp[-l:] = rand.permutation(pop_size)[-l:]
        sorted_sp = sp.copy()
        
    elif cat == "half_first_b":
        
        tmp         = rho.copy()
        sorted_sp   = tmp.argsort()[::-1]   # primero los vectores con mas soluciones asociadas
        
        sp = sorted_sp.copy()
        l = int(budget/2)
        sp[-l:] = rand.permutation(pop_size)[-l:]
        sorted_sp = sp.copy()
        
    
    elif cat == "permutation":
        
        sorted_sp = rand.permutation(pop_size)
        
    elif cat == "hierarchy":
        
        n_weights = rho.shape[0]
        
        is_p = np.zeros((n_weights, ), dtype=int)
        is_p[:budget] = 1                               # asumimos que los primeros budget vectores son principales
        
        indices = np.arange(n_weights, dtype=int)
        
        mask_a = indices[(is_p == 1) & (rho >  0)]
        mask_b = indices[(is_p == 0) & (rho >  0)]
        mask_c = indices[(is_p == 1) & (rho == 0)]
        mask_d = indices[(is_p == 0) & (rho == 0)]
        
        sorted_sp = np.hstack((mask_a, mask_b, mask_c, mask_d))
    

    elif cat == "hierarchy_rand":
        
        n_weights = rho.shape[0]
        
        is_p = np.zeros((n_weights, ), dtype=int)
        is_p[:budget] = 1                               # asumimos que los primeros budget vectores son principales
        
        indices = np.arange(n_weights, dtype=int)
        
        mask_a = indices[(is_p == 1) & (rho >  0)]
        mask_b = indices[(is_p == 0) & (rho >  0)]
        mask_c = indices[(is_p == 1) & (rho == 0)]
        mask_d = indices[(is_p == 0) & (rho == 0)]
        
        sorted_sp = np.hstack((mask_a, mask_b, mask_c, mask_d))
    
    
        if rand.rand() < .2:
            #print("rand permutation")
            sorted_sp = rand.permutation(n_weights)
            
    elif cat == "fixed":

        sorted_sp = np.arange(budget, dtype=int)
    
    
         
            
    
        

    else:
        
        print("error on sort_subproblems")
        
        
        
    """
    elif cat == "neigh":
        
        
        indices = rand.permutation(budget)      # para recorrer los vectores principales en desorden
        
        i = 0
        counter = 0
        
        while counter < budget:
            
            if rho[]
    """  
    
    
    return sorted_sp.copy()
            
    
    
if __name__ == "__main__":
    
    
    rho = np.array([ 0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
                    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
                    0,  0,  0,  0,  0,  5,  0,  6,  0,  0,  0,  0,  0,  0,  0,  0,  0,
                   10,  0,  0,  0,  0,  0,  0,  4,  0,  4,  2,  0,  0,  0,  8,  2,  0,
                    0,  2,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
                    0,  0,  0,  0,  0,  0,  0,  2,  0,  0,  0,  0,  0,  0,  2,  0,  0,
                    6,  0,  0,  2,  0,  0,  0,  0,  6,  2,  2,  0,  0,  0,  0,  3,  2,
                    0,  2,  0,  0,  0,  0,  6,  0,  0,  0,  0,  0,  0, 10,  2,  0,  0,
                    0,  0,  0,  0,  2,  4,  0,  0,  4,  7,  0,  8,  0,  4,  0, 14,  8,
                    4,  0,  4, 10,  0,  0,  0,  2,  0,  0,  0,  0,  0,  0,  0,  0,  0,
                    0,  0,  0, 15,  0,  0])
                
                
    is_p = np.array([1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
               1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
               1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
               1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0,
               0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])
           
           
           
    indices = np.arange(len(is_p), dtype=int)
    
    
    criteria = np.stack((is_p, rho)).T
    
    # ----
    
    mask_a = indices[(is_p == 1) & (rho >  0)]
    mask_b = indices[(is_p == 0) & (rho >  0)]
    mask_c = indices[(is_p == 1) & (rho == 0)]
    mask_d = indices[(is_p == 0) & (rho == 0)]

    tmp_rho = np.hstack((
                        rho[mask_a],
                        rho[mask_b])) * -1
                        
    tmp_is_p = np.hstack((
                        is_p[mask_a],
                        is_p[mask_b])) * -1
                        
    #sorted_sp_a = np.lexsort((tmp_rho, tmp_is_p))
    
    tmp_rho = np.hstack((
                        rho[mask_c],
                        rho[mask_d])) * -1
                        
    tmp_is_p = np.hstack((
                        is_p[mask_c],
                        is_p[mask_d])) * -1
                        
    #sorted_sp_b = np.lexsort((tmp_rho, tmp_is_p))
    
    #sorted_sp = np.hstack((sorted_sp_a, sorted_sp_b))
    
    sorted_sp = np.hstack((mask_a, mask_b, mask_c, mask_d))
    
    
    
    
    
    
    
    

