# test_moea_config.py
from __future__ import print_function
from rocket.moea import AMOEAD, AMOEAD2, AMOEAD3
from rocket.helpers import get_n_genes, get_bounds
from rocket.labs import get_experiment_path
import numpy as np
import os

# define experiment output path
script_path = os.path.dirname(os.path.abspath(__file__))
output_path = get_experiment_path(script_path, name="sample")

    
if __name__ == "__main__":
    
    weights = np.genfromtxt("weights_m_3.txt")
    #weights = np.genfromtxt("weights_m_3_28.txt")
    #weights = np.genfromtxt("weights_m_3_36.txt")
    #weights = np.genfromtxt("weights_m_3_45.txt")
    pop_size, m_objs = weights.shape
    
    mop_name = "dtlz1-spin"
    #mop_name = "dtlz1"
    n_genes, wfg_k, wfg_l = get_n_genes(mop_name, m_objs)
    
    params = {
        "name"              : "amoead-pbi",                                             # name of moea
        "label"             : "AMOEA/D-PBI",                                            # label of moea
        "runs"              : 1,                                                        # number of independent runs
        "iters"             : 300,                                                      # number of generations
        "m_objs"            : m_objs,                                                   # number of objectives
        "pop_size"          : pop_size,                                                 # number of individuals
        "params_mop"        : {"name": mop_name, "k": wfg_k, "degrees":180},            # mop parameters
        "params_crossover"  : {"name": "sbx", "prob": 1.0, "eta": 20},                  # crossover parameters
        "params_mutation"   : {"name": "polymutation", "prob": 1./n_genes, "eta": 15},  # mutation parameters
        "params_stats"      : {"output_path": output_path},                             # stats parameters
        "verbose"           : True,                                                     # log verbosity
        
        
        # MOEAD params
        "neigh_size"        : 20,
        "pop_weights"       : weights,
        "params_form"       : {"name": "pbi", "pbi-theta": 5},
        
    }
    
    moea = AMOEAD3(params)
    chroms, objs, fig, ax = moea.evaluate(plot_output=True, plot_output_name="", plot_run=True)
    
    
