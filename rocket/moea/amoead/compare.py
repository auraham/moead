# test_moea_config.py
from __future__ import print_function
from rocket.moea import AMOEAD, AMOEAD2, AMOEAD3, AMOEAD5, AMOEAD6, AMOEAD7, AMOEAD8, MOEAD
from rocket.helpers import get_n_genes, get_bounds
from rocket.labs import get_experiment_path
import numpy as np
import os

# define experiment output path
script_path = os.path.dirname(os.path.abspath(__file__))
output_path = get_experiment_path(script_path, name="compare")

    
if __name__ == "__main__":
    
    weights = np.genfromtxt("weights_m_3.txt")
    pop_size, m_objs = weights.shape
    
    # mop_name, iters, params_form
    # uncomment for testing
    configs = (
        #("dtlz1",       400, {"name": "pbi", "pbi-theta": 5}), 
        ("dtlz1-spin",  30, {"name": "pbi", "pbi-theta": 5}), 
        #("inv-dtlz1",   500, {"name": "pbi", "pbi-theta": 5}), 
        #("min-dtlz1",   300, {"name": "asf", "asf-z_ref": np.array([0., 0., 0.])}), 
        #("min-dtlz1",   300, {"name": "ipbi", "ipbi-theta": 5, "ipbi-negative": True}), 
        )

    for config in configs:
    
        mop_name, iters, params_form = config
        n_genes, wfg_k, wfg_l = get_n_genes(mop_name, m_objs)
    
    
        params = {
            "runs"              : 1,                                                        # number of independent runs
            "iters"             : iters,                                                    # number of generations
            "m_objs"            : m_objs,                                                   # number of objectives
            "pop_size"          : pop_size,                                                 # number of individuals
            "params_mop"        : {"name": mop_name, "k": wfg_k, "degrees":180},            # mop parameters
            "params_crossover"  : {"name": "sbx", "prob": 1.0, "eta": 20},                  # crossover parameters
            "params_mutation"   : {"name": "polymutation", "prob": 1./n_genes, "eta": 15},  # mutation parameters
            "params_stats"      : {"output_path": output_path},                             # stats parameters
            "verbose"           : True,                                                     # log verbosity
            
            # MOEAD params
            "neigh_size"        : 20,
            "pop_weights"       : weights,
            "params_form"       : params_form,
            
        }
        
        #params["name"]  = "amoead3-%s"  % params_form["name"]
        #params["label"] = "AMOEA/D3-%s" % params_form["name"].upper()
        #amoead3         = AMOEAD3(params)
        #amoead3_chroms, amoead3_objs, amoead3_fig, amoead3_ax = amoead3.evaluate(plot_output=True, plot_output_name="", plot_run=True)
        
        #params["name"]  = "amoead5-%s"  % params_form["name"]
        #params["label"] = "AMOEA/D5-%s" % params_form["name"].upper()
        #amoead5         = AMOEAD5(params)
        #amoead5_chroms, amoead5_objs, amoead5_fig, amoead5_ax = amoead5.evaluate(plot_output=True, plot_output_name="", plot_run=True)
        
        #params["name"]       = "amoead6-hierarchy-%s"  % params_form["name"]
        #params["label"]      = "AMOEA/D6-HIERARCHY-%s" % params_form["name"].upper()
        #params["amoead_cat"] = "hierarchy"
        #amoead = AMOEAD6(params)
        #amoead.evaluate(plot_output=True, plot_output_name="", plot_run=True)
        
        #params["name"]       = "amoead6-nearest-%s"  % params_form["name"]
        #params["label"]      = "AMOEA/D6-NEAREST-%s" % params_form["name"].upper()
        #params["amoead_cat"] = "nearest"
        #amoead = AMOEAD6(params)
        #amoead.evaluate(plot_output=True, plot_output_name="", plot_run=True)
        
        params["name"]       = "amoead7-nearest-%s"  % params_form["name"]
        params["label"]      = "AMOEA/D7-NEAREST-%s" % params_form["name"].upper()
        params["amoead_cat"] = "nearest"
        amoead = AMOEAD7(params)
        amoead.evaluate(plot_output=True, plot_output_name="", plot_run=True)
        
        params["name"]       = "amoead8-%s"  % params_form["name"]
        params["label"]      = "AMOEA/D8-%s" % params_form["name"].upper()
        params["amoead_association_type"] = "nearest"
        params["amoead_sort_sp_a_type"] = "permutation"
        params["amoead_sort_sp_b_type"] = "half_first_a"
        amoead = AMOEAD8(params)
        amoead.evaluate(plot_output=True, plot_output_name="", plot_run=True)
        
        #params["name"]  = "moead-%s"  % params_form["name"]
        #params["label"] = "MOEA/D-%s" % params_form["name"].upper()
        #moead           = MOEAD(params)
        #moead_chroms, moead_objs, moead_fig, moead_ax = moead.evaluate(plot_output=True, plot_output_name="", plot_run=True)
        

