# test_association.py
from __future__ import print_function
import numpy as np
from numpy.random import RandomState

from rocket.moea.amoead.associate import associate, associate_ext, associate_frontend
from rocket.moea.amoead.sort_subproblems import sort_subproblems_frontend
from rocket.fronts import get_real_front
from rocket.plot import plot_pops, basecolors
        
        
from rocket.helpers import get_n_genes
import matplotlib.pyplot as plt


def debug_plot(objs, weights, rho, pie, to_keep, moea_name, mop_name):
        
    # ---- debug -----
    
    
    m_objs = objs.shape[1]
    n_genes = get_n_genes(mop_name, m_objs)
    
    real_front, p   = get_real_front(mop_name, m_objs, n_genes, {"degrees": 180})
    
    W = weights
    pops = (objs, objs[to_keep], W, W[to_keep])
    labels = ("objs", "objs (to_keep)", "weights", "weights (to_keep)")
    
    title = "%s - %s" % (mop_name.upper(), moea_name)
    fig, ax = plot_pops(pops, labels, real_front=real_front, p=p, title=title)
    
    plt.show()


if __name__ == "__main__":
    
    #mop_name  = "dtlz1-spin"
    mop_name  = "dtlz1"
    moea_name = "amoead8-pbi"
    
    label = "%s_%s" % (moea_name, mop_name)
    
    
    objs            = np.genfromtxt("objs_%s.txt"           % label)
    weights         = np.genfromtxt("weights_%s.txt"        % label)
    neighbors       = np.genfromtxt("neighbors_%s.txt"      % label, dtype=int)
    is_principal    = np.genfromtxt("is_principal_%s.txt"   % label, dtype=int)
    is_principal    = np.array(is_principal, dtype=bool)
    
    #base_rho        = np.genfromtxt("rho_%s.txt"   % label, dtype=int)
    #base_sorted_sp  = np.genfromtxt("sorted_sp_%s.txt"   % label, dtype=int)
    

    # ----
    
    association_type = "nearest"
    sort_sp_type = "hierarchy"
    budget = 91
    pop_size = objs.shape[0]
    rand = RandomState(100)
    
    # subsampling
    #import ipdb; ipdb.set_trace()
    rho, pie, _ = associate_frontend(association_type, objs, weights, is_principal, neighbors)
    sorted_sp = sort_subproblems_frontend(sort_sp_type, rho, budget, pop_size, rand)

    
    to_keep = sorted_sp[:budget]
    
    # debug
    debug_plot(objs, weights, rho, pie, to_keep, moea_name, mop_name)  
    
    
