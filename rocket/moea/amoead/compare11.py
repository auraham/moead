# compare11.py
from __future__ import print_function
from rocket.moea import AMOEAD11, MOEAD
from rocket.helpers import get_n_genes, get_bounds
from rocket.labs import get_experiment_path
import numpy as np
import os

# define experiment output path
script_path = os.path.dirname(os.path.abspath(__file__))
output_path = get_experiment_path(script_path, name="compare11")

    
if __name__ == "__main__":
    
    weights = np.genfromtxt("weights_m_3.txt")
    weights = np.genfromtxt("weights_m_3_36.txt")
    pop_size, m_objs = weights.shape
    
    # mop_name, iters, params_form
    # uncomment for testing
    configs = (
        #("dtlz1",       300, {"name": "pbi", "pbi-theta": 5}), 
        ("dtlz1-spin",  300, {"name": "pbi", "pbi-theta": 5}), 
        #("inv-dtlz1",   500, {"name": "pbi", "pbi-theta": 5}), 
        #("min-dtlz1",   300, {"name": "asf", "asf-z_ref": np.array([0., 0., 0.])}),    # no funciono
        #("min-dtlz1",   300, {"name": "asf", "asf-z_ref": np.array([0., 0., 0.])}),    # no funciono
        #("min-dtlz1",   300, {"name": "pbi", "pbi-theta": 5}), 
        )

    for config in configs:
    
        mop_name, iters, params_form = config
        n_genes, wfg_k, wfg_l = get_n_genes(mop_name, m_objs)
    
    
        params = {
            "runs"              : 1,                                                        # number of independent runs
            "iters"             : iters,                                                    # number of generations
            "m_objs"            : m_objs,                                                   # number of objectives
            "pop_size"          : pop_size,                                                 # number of individuals
            "params_mop"        : {"name": mop_name, "k": wfg_k, "degrees":180},            # mop parameters
            "params_crossover"  : {"name": "sbx", "prob": 1.0, "eta": 20},                  # crossover parameters
            "params_mutation"   : {"name": "polymutation", "prob": 1./n_genes, "eta": 15},  # mutation parameters
            "params_stats"      : {"output_path": output_path},                             # stats parameters
            "verbose"           : True,                                                     # log verbosity
            
            # MOEAD params
            "neigh_size"        : 20,
            "pop_weights"       : weights,
            "params_form"       : params_form,
            
        }
        
        params["name"]       = "amoead11-%s"  % params_form["name"]
        params["label"]      = "AMOEA/D11-%s" % params_form["name"].upper()
        params["amoead_association_a_type"] = "hierarchy"
        params["amoead_association_b_type"] = "hierarchy"
        params["amoead_association_c_type"] = "nearest"             # debe ser nearest en lugar de hierarchy
        params["amoead_association_a_norm"] = True
        params["amoead_association_b_norm"] = True
        params["amoead_association_c_norm"] = True            # debe ser nearest en lugar de hierarchy
        params["amoead_sort_sp_a_type"] = "hierarchy_rand"
        params["amoead_sort_sp_b_type"] = "hierarchy_rand"
        params["amoead_sort_sp_c_type"] = "hierarchy"            # no rand!
        params["amoead_expansion_threshold"] = 10
        params["amoead_use_adaptation"] = True
        params["amoead_expansion_p"] = 7
        params["amoead_budget"] = 91        # un maximo tamano de poblacion
        
        # agregar "hierarchy_rand"
        
        amoead = AMOEAD11(params)
        amoead.evaluate(plot_output=True, plot_output_name="", plot_run=True)
        

