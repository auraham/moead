"""
expansion.py
"""
from __future__ import print_function
from rocket.plot.colors import colors, basecolors
from rocket.plot import load_rcparams
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
from scipy.spatial.distance import cdist
from numpy.linalg import norm


def get_base_simplex(m_objs, p, scale=1):
    """
    Return an (m-1) simplex scaled and translated to one corner
    
    Input
    m_objs      Number of objectives
    p           Number of divisions of original simplex
    scale       Scale factor from [Jain12]
    
    Output
    base        (m_objs, m_objs) array, base simplex
    """
    
    base    = np.eye(m_objs, dtype=float)
    factor = (p-1 + (1-scale))*(1.0/p)
    
    for i in range(1, m_objs):
        
        d = base[0] - base[i]           # translation (direction) vector
        v = factor*d                    # scaled translation vector
        base[i] = base[i] + v           # move vector
    
    return base

def move_base_simplex(center_id, weights, base_simplex):
    """
    Move a base simplex (base_simplex, m-dimensional array) around m points of a vector (weights[center_id])
    Each movement is called a 'configuration'. If after this translation a configuration contains negative values,
    then this confugartion is skipped. Only valid configurations (those with positive-values only) are returned.
    
    Input
    center_id           int, index
    weights             (n_points, m_objs) array of weights (simplex)
    base_simplex        (m_objs, ) array with base simplex to move around weights[center_id] 
    
    Output
    configs             list of configurations
    """
    
    m_objs = weights.shape[1]
    configs = []
    
    a = weights[center_id]          # destiny
                                    # the base simplex will be translated (moved) around this vector
    
    for i in range(m_objs):
        
        b   = base_simplex[i]       # source
        d   = a - b                 # translation (direction) vector
        
        config = base_simplex + d   # configuration
        
        # check
        if (config < 0).sum() > 0:  # if config contains negative values, it is skipped
            continue
            
        configs.append(config)
    
    # return list of configurations
    return configs
    
def move_reduce_base_simplex(center_id, weights, base_simplex):
    """
    It is the same as move_base_simplex, but deletes duplicated points during translation.
    
    Input
    center_id           int, index
    weights             (n_points, m_objs) array of weights (simplex)
    base_simplex        (m_objs, ) array with base simplex to move around weights[center_id] 
    
    Output
    configs             list of configurations
    """
    
    m_objs = weights.shape[1]
    configs = []
    indices = np.arange(m_objs, dtype=int)
    
    a = weights[center_id]          # destiny
                                    # the base simplex will be translated (moved) around this vector
    
    for i in range(m_objs):
        
        b   = base_simplex[i]       # source
        d   = a - b                 # translation (direction) vector
        
        config = base_simplex + d   # configuration
        
        
        to_keep = indices != i      # remove the i-th vector from config    
        config  = config[to_keep]
        
        # check
        if (config < 0).sum() > 0:  # if config contains negative values, it is skipped
            continue
            
        configs.append(config)
    
    # return list of configurations
    return configs
    
def is_v_in_weights(v, weights):
                                        # shape of v         (m_objs, )
                                        # shape of weights   (n_points, m_objs)
    # @deprecated
    #is_equal = v == weights            # (n_points, m_objs) boolean array
    
    is_equal = np.isclose(v, weights, atol=0.00001)     # (n_points, m_objs) boolean array
    matches  = is_equal.all(axis=1)                     # (n_points, ) boolean array, matches[i]=True means "v is in weights"
    suma     = matches.sum()                            # number of copies of v in weights
    
    if suma > 0:
        return True
        
    return False
    
def reduce_configs(configs, weights):
    """
    Select from configs those vectors that are not in (1) weights nor (2) configs.
    That is, this function preserves new vectors only. 
    We assume that configs is the result of calling move_reduce_base_simplex()
    
    Input
    configs             list of configurations (result of move_reduce_base_simplex)
    weights             (n_points, m_objs) array of weights (simplex)

    Output
    new_weights         new set of weights after expansion
    """
    
    new_vectors = np.vstack(configs)                    # (n_points, m_objs) array, where n_points = m_objs * number of configs
    n_points    = new_vectors.shape[0]
    to_keep     = np.zeros((n_points, ), dtype=bool)
    
    
    # compare against weights
    for i, v in enumerate(new_vectors):                 # v is a (m_objs, ) array
        
        if not is_v_in_weights(v, weights):
            to_keep[i] = True
            
    
    expansion = new_vectors[to_keep]                    # (k_points, m_objs)
    
    new_weights = np.vstack((weights, expansion))       # merges (n_points, m_objs), (k_points, m_objs)
    
    
    # debug
    print("new vectors:", expansion.shape[0])
    
    return new_weights.copy()
    
def reduce_configs_ext(configs, weights):
    """
    Select from configs those vectors that are not in (1) weights nor (2) configs.
    That is, this function preserves new vectors only. 
    We assume that configs is the result of calling move_reduce_base_simplex()
    
    Input
    configs             list of configurations (result of move_reduce_base_simplex)
    weights             (n_points, m_objs) array of weights (simplex)

    Output
    new_weights         new set of weights after expansion
    """
    
    new_vectors = np.vstack(configs)                    # (n_points, m_objs) array, where n_points = m_objs * number of configs
    n_points    = new_vectors.shape[0]
    to_keep     = np.zeros((n_points, ), dtype=bool)
    
    
    # compare against weights
    for i, v in enumerate(new_vectors):                 # v is a (m_objs, ) array
        
        if not is_v_in_weights(v, weights):
            to_keep[i] = True
            
    
    expansion = new_vectors[to_keep]                    # (k_points, m_objs)
    
    new_weights = np.vstack((weights, expansion))       # merges (n_points, m_objs), (k_points, m_objs)
    
    
    # debug
    #print("new vectors:", expansion.shape[0])
    
    return new_weights.copy(), expansion.copy()
    
    
def expand(center_ids, weights, p, scale=0.5):
    
    m_objs  = weights.shape[1]
    
    # create base simplex
    base_simplex = get_base_simplex(m_objs, p, scale)
    
    new_weights = weights.copy()
    
    for center_id in center_ids:
    
        # get all valid configurations (removing duplicates)
        configs = move_reduce_base_simplex(center_id, new_weights, base_simplex)
       
        # get new weights from configs
        expansion = reduce_configs(configs, new_weights)

        # update reference
        new_weights = expansion.copy()
        
    return new_weights.copy()
    
    
def expand_ext(center_ids, weights, p, scale=0.5):
    
    m_objs  = weights.shape[1]
    counter = np.zeros(center_ids.shape, dtype=int)
    
    # create base simplex
    base_simplex = get_base_simplex(m_objs, p, scale)
    
    new_weights = weights.copy()
    
    for i, center_id in enumerate(center_ids):
    
        # get all valid configurations (removing duplicates)
        configs = move_reduce_base_simplex(center_id, new_weights, base_simplex)
       
        # get new weights from configs
        nw, expansion = reduce_configs_ext(configs, new_weights)

        # increment counter, number of vectors added around i-th weight vector
        counter[i] = expansion.shape[0]

        # update reference
        new_weights = nw.copy()
        
    return new_weights.copy(), counter.copy()
    
    
    
def plot_configs(center_id, scale, weights, base_simplex, configs, save=False, figname=""):
    """
    Plot a list of configurations over a simplex.
    
    Input
    center_id           int, index
    scale       Scale factor from [Jain12]
    weights             (n_points, m_objs) array of weights (simplex)
    base_simplex        (m_objs, ) array with base simplex to move around weights[center_id] 
    configs             list of configurations
    """
    
    fig = plt.figure()
    ax = fig.add_subplot(111, projection="3d")
    ax.set_xlabel("$x$")
    ax.set_ylabel("$y$")
    ax.set_zlabel("$z$")
    ax.set_title("Move around %d (scale: %.2f)" % (center_id, scale))
    
    # for callback
    lined = {}
    
    ax.plot(weights[:, 0], weights[:, 1], weights[:, 2], c=colors[0],
        mec=basecolors["almost_black"], ls="none", marker="o", markeredgewidth=0.15)
    
    label = "Base simplex"
    lined[label], = ax.plot(base_simplex[:, 0], base_simplex[:, 1], base_simplex[:, 2], c=colors[2], label=label,
        mec=basecolors["almost_black"], ls="none", marker="o", markeredgewidth=0.15)
    
    palete = ("#4163B3", "#77133A", "#652D7B")
    
    # translations
    for i, config in enumerate(configs):
        label = "Config %d" % (i+1, )
        lined[label], = ax.plot(config[:, 0], config[:, 1], config[:, 2], c=palete[i], label=label,
            mec=basecolors["almost_black"], ls="none", marker="o", markeredgewidth=0.15)
    
    # plot ids
    for i, w in enumerate(weights):
        x, y, z = w
        ax.text(x, y, z, str(i), color="#404040")
    
    # set view
    ax.view_init(15, 45)
    
    # legend
    leg = ax.legend(loc="lower right")
    
    # for callback
    for legtxt in leg.get_texts():
        legtxt.set_picker(5)
        
    # inner callback
    def on_pick(event):
        
        legtxt, line = None, None
        
        obj = event.artist
        is_callable = callable(getattr(obj, "get_text", None))
        
        if is_callable:
            
            legtxt = event.artist
            line = lined[legtxt.get_text()]
            
        else:
            
            legtxt = None
            line = event.artist
            
        is_visible = line.get_visible()
        
        # fade a little bit
        if is_visible:
            
            line.set_visible(False)
            
            if legtxt is not None:
                legtxt.set_alpha(0.2)
                
        else:
            
            line.set_visible(True)
            
            if legtxt is not None:
                legtxt.set_alpha(None)
                
        fig.canvas.draw()
    
    # register callback
    fig.canvas.mpl_connect("pick_event", on_pick)
    
    
    if save:
        output = figname if figname else "expansion_%d_%.2f.png" % (center_id, scale)
        fig.savefig(output, dpi=300)
    
    #plt.show()
    
def plot_weights(weights, save=False, figname="", title=""):
    
    fig = plt.figure()
    ax = fig.add_subplot(111, projection="3d")
    ax.set_xlabel("$x$")
    ax.set_ylabel("$y$")
    ax.set_zlabel("$z$")
    title = title if title else "After expansion"
    ax.set_title(title)
    
    ax.plot(weights[:, 0], weights[:, 1], weights[:, 2], c=colors[0],
        mec=basecolors["almost_black"], ls="none", marker="o", markeredgewidth=0.15)
    
    # plot ids
    for i, w in enumerate(weights):
        x, y, z = w
        ax.text(x, y, z, str(i), color="#404040")
    
    ax.view_init(15, 45)
    
    if save:
        output = figname if figname else "after_expansion.png" 
        fig.savefig(output, dpi=300)
    
    #plt.show()

if __name__ == "__main__":

    load_rcparams()
    
    weights = np.genfromtxt("weights_m_3_21.txt")
    m_objs  = weights.shape[1]
    p       = 5                                             # number of division of original simplex weights_m_3_21.txt
    scale   = 0.5
    
    # create base simplex
    base_simplex = get_base_simplex(m_objs, p, scale)
    
    # get all valid configurations
    center_id   = 8                 # this value can change
    configs     = move_base_simplex(center_id, weights, base_simplex)
   
    # check result
    plot_configs(center_id, scale, weights, base_simplex, configs, save=False)
   

    # get all valid configurations (removing duplicates)
    center_id   = 12                 # this value can change
    configs     = move_reduce_base_simplex(center_id, weights, base_simplex)
   
    # check result (removing duplicates)
    plot_configs(center_id, scale, weights, base_simplex, configs, save=False)
    
    # get new weights from configs
    new_weights = reduce_configs(configs, weights)

    # plot result
    plot_weights(new_weights, save=False)
    
    plt.show()
