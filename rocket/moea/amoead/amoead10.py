# amoead10.py
from __future__ import print_function
from rocket.moea import MOEAD
from rocket.moea.amoead.expansion import expand_ext
from rocket.moea.amoead.associate import associate, associate_ext, associate_frontend
from rocket.moea.amoead.normalize import norm_matrix
from rocket.moea.amoead.utility import utility_matrix
from rocket.moea.amoead.sort_subproblems import sort_subproblems_frontend
from rocket.moea.amoead.replacement import replacement

from rocket.plot import plot_pops, load_rcparams

import numpy as np
from numpy.random import RandomState
import matplotlib.pyplot as plt
import os

# debug
from rocket.dev.rand import Rand

class AMOEAD10(MOEAD):
    
    def __init__(self, params):
        
        MOEAD.__init__(self, params)
        
        #self.expansion_p = 12 # para los 91 vectores!
        #self.expansion_done = False
        #self.cat = params["amoead_cat"]
        
        # numero de divisiones en los vectores de referencia
        self.expansion_p        = params["amoead_expansion_p"]        # debe ser una tupla
        self.layer_indices      = params["amoead_layer_indices"]      # debe ser un array de (n_weights, )
        
        # esto es para usar o no la adaptacion
        self.use_adaptation     = params["amoead_use_adaptation"]
        
        self.association_a_type = params["amoead_association_a_type"]
        self.association_b_type = params["amoead_association_b_type"]
        self.association_c_type = params["amoead_association_c_type"]
        
        
        self.association_a_norm = params["amoead_association_a_norm"]
        self.association_b_norm = params["amoead_association_b_norm"]
        self.association_c_norm = params["amoead_association_c_norm"]
        
        
        self.sort_sp_a_type = params["amoead_sort_sp_a_type"]
        self.sort_sp_b_type = params["amoead_sort_sp_b_type"]
        self.sort_sp_c_type = params["amoead_sort_sp_c_type"]
        self.expansion_threshold = params["amoead_expansion_threshold"]
        
    
    def sort_subproblems(self, rho):
        """
        Return a sorted array of indices, where
        
        rho[i] = 1 is better than rho[i] == 2
        rho[i] = 2 is better than rho[i] == 3
        etc
        
        Input
        rho         (n_weights, ) association counter array
        
        Output
        sorted_rho  (n_weights, ) array of indices
        """
        
        tmp         = rho.copy()
        tmp[rho==0] = rho.max()+1
        sorted_rho  = tmp.argsort()
        
        return sorted_rho.copy() 
    
    
    def expand_weights(self, center_ids, weights):
        
        print("deprecated, use expand_weights_p instead")
        
        p = self.expansion_p
        n_weights = weights.shape[0]
        counter = np.zeros((n_weights, ))
        
        
        # create new weights
        weights, counter = expand_ext(center_ids, weights.copy(), p, scale=0.5)
        
        # como evitar que genere sobre los mismos center_ids una y otra vez?
        # counter debe ser zeros en ese caso
        
        return weights.copy(), counter.copy()
    
    
    def expand_weights_p(self, center_ids, weights, expansion_p, layer_indices):
        """
        Permite realizar la expansion en vectores distribuidos en una o dos capas
        
        Input
        center_ids          (k, ) array, contiene los k indices de los vectores que seran expandidos
        weights             (n_weights, ) array, contiene los vectores de referencia
        expansion_p         (p0, ) or (p0, p1) tuple, contiene el numero de divisiones de la capa externa 0 (p0) y capa interna 1 (p1)
        layer_indices       (n_weights, ) array de indices, layer_indices[i] = j indica que el i-th vector pertenece a la capa j (j=0,1 solamente)
        
        Output
        final_weights       (k_weights, ) array, contiene los vectores de refenecia previos y los nuevos
        final_counter       (k, ) array, final_counter[i]=j indica el numero j de vectores generados alrededor del vector center_ids[i]
        
        """
    
        final_weights = weights.copy()
        final_counter = None
        scales = (0.5, 0.25)
        
        final_counter = { ind: 0 for ind in center_ids }        # todos inician con cero
        
        for i, p in enumerate(expansion_p):
            
            to_keep = np.where(layer_indices == i)[0]
            
            w = weights[to_keep]                        # vectors from i-th layer
            
            # filtro, solo los que estan indices de center_ids que estan en to_keep
            to_expand = np.array([ ind for ind in center_ids if ind in to_keep ])
            
            if len(to_expand) > 0:
            
                # create new weights
                new_weights, new_counter = expand_ext(to_expand, final_weights.copy(), p, scale=scales[i])
                
                final_weights = new_weights.copy()
                
                for j, ind in enumerate(to_expand):
                    
                    final_counter[ind] = new_counter[j]
                
                
        final_counter = np.array([ final_counter[ind] for ind in center_ids ])  # conversion de dict a array en orden
                
        return final_weights.copy(), final_counter.copy()
 
    
    
    
    def expand_pop(self, chroms, objs, slots):
        
        # select random pool
        size = chroms.shape[0]
        rand_pool = self.rand.permutation(size)[:slots]     # 
        
        
        if slots > size:
            
            # si hay mas slots que llenar que individuos de donde copiar...
            
            rand_pool = self.rand.permutation(slots)     # indices aleatorios entre [0, slots-1], slots > pop_size 
            pool = np.zeros((slots, ), dtype=int)
            
            for i in range(slots):
                
                pool[i] = rand_pool[i] % size
            
            rand_pool = pool.copy()
        
        
        # create new blocks
        new_chroms_block = chroms[rand_pool].copy()     # seguro que aqui el copy si es necesario para no copiar referencias sino contenido!
        new_objs_block = objs[rand_pool].copy()
        
        # update pop
        new_chroms = np.vstack((chroms, new_chroms_block))
        new_objs = np.vstack((objs, new_objs_block))
        
        # update pop_size
        self.pop_size = new_chroms.shape[0]
        
        return new_chroms.copy(), new_objs.copy()
    
    
    def debug_plot(self, objs, weights, rho, pie, to_keep):
        
        # ---- debug -----
        from rocket.fronts import get_real_front
        from rocket.plot import plot_pops, basecolors
        
        mop_name        = self.params_mop["name"]
        real_front, p   = get_real_front(mop_name, self.m_objs, self.n_genes, self.params_mop)
        
        W = weights
        pops = (objs, objs[to_keep], W, W[to_keep])
        labels = ("objs", "objs (to_keep)", "weights", "weights (to_keep)")
        
        title = "%s - %s" % (self.params_mop["name"].upper(), self.label)
        fig, ax = plot_pops(pops, labels, real_front=real_front, p=p, title=title)
        
        """
        indices = np.where(rho > 0)[0]
        
        for ind in indices:
            w = W[ind]
            x, y, z = w
            #ax.text(x, y, z, str(ind), color="#404040", size=10)
            
            ax.plot(W[[ind], 0], W[[ind], 1], W[[ind], 2, ], c="#95234E", 
                mec=basecolors["almost_black"], ls="none", marker="o", markeredgewidth=0.15)
            
            to_plot = np.where(pie == ind)[0]
            
            if len(to_plot):
                for tp in to_plot:
                    
                    x, y, z = objs[tp]
                    #ax.text(x, y, z, str(tp), color="#92C3B8", size=10)
            
        """
        
        plt.show()
    
    
    def run(self, chroms, seed, run_id):
        """
        Independent run
        """
        
        self.rand   = RandomState(seed)
        self.pop_size = chroms.shape[0]        # @todo: [como remover este] reset necesario para multiples invocaciones de run
        
        pop_size    = chroms.shape[0]
        weights     = self.pop_weights.copy()
        neighbors   = self.create_neighborhood(weights)
        objs        = self.evaluate_pop_mop(chroms)
        
        # added to amoead
        budget      = pop_size
        indices     = np.arange(budget, dtype=int)      # not expandible
        to_ignore   = np.zeros((budget, ), dtype=bool)  # not expandible
        is_principal= np.ones((budget, ), dtype=bool)   # expandible, only first budget entries are True
        sorted_sp   = np.arange(budget, dtype=int)
        
        t = 1
        self.stats(chroms, objs, t, run_id)
        
        t = 2
        while t <= self.iters:
            
            for j in range(budget):
                
                # added to amoead
                i = sorted_sp[j]

                neigh = neighbors[i]
                
                neigh_objs = objs[neigh]
                
                neigh_weights = weights[neigh]
                
                child = self.genetic_operations(i, neigh, chroms)
                
                child = self.check_pop_bounds(child)            
                
                child_objs = self.evaluate_pop_mop(child)

                # moead                
                #scalar_child = self.evaluate_pop_scalar_method(child_objs, neigh_weights)
                #scalar_neigh = self.evaluate_pop_scalar_method(neigh_objs, neigh_weights)
                #to_replace = neigh[scalar_child <= scalar_neigh]
                #chroms[to_replace, :] = child.copy()
                #objs[to_replace, :] = child_objs.copy()
        
                # moead-gra
                scalar_child    = self.evaluate_pop_scalar_method(child_objs, weights)
                scalar_pop      = self.evaluate_pop_scalar_method(objs, weights)
                to_replace      = replacement(scalar_child, scalar_pop)
                chroms[to_replace, :] = child.copy()
                objs[to_replace, :] = child_objs.copy()
                
            
            if self.use_adaptation:
                
                
                # added to amoead
                rho, pie, _ = associate_frontend(self.association_a_type, objs, weights, is_principal, neighbors, normalize_objs=self.association_a_norm)
                
                # frontend sort_sp_a
                #import ipdb; ipdb.set_trace()
                sorted_sp = sort_subproblems_frontend(self.sort_sp_a_type, rho, budget, self.pop_size, self.rand)

                #sorted_sp = self.rand.permutation(self.pop_size)
                
                
                # indices a expandir
                threshold   = self.expansion_threshold
                ids         = indices[rho[is_principal] > threshold]            # (0, 1, ..., budget-1 a lo mas)
                center_ids  = ids[np.invert(to_ignore[ids])]                    # (0, 1, ..., budget-1 a lo mas)
                slots       = center_ids.shape[0]                               # numero de vectores a expandir, puede ser cero
                
                if slots > 0:
                    
                    # crear nuevos vectores (deprecated, solo expande en una capa)
                    #weights, counter = self.expand_weights(center_ids, weights.copy())
                    #new_vectors = counter.sum()
                    
                    # nuevo, permite expandir vectores en dos capas
                    weights, counter = self.expand_weights_p(center_ids, weights.copy(), tuple(self.expansion_p), self.layer_indices.copy())
                    new_vectors = counter.sum()
                    
                    # update
                    to_ignore[center_ids] = True
                    
                    # debug
                    print("t: %d, expansion, new vectors: %d, total vectors: %d, center_ids: %s" % (t, new_vectors, weights.shape[0], center_ids))
                    
                    if new_vectors > 0:
                        
                        # actualizar vecindario
                        # (n_weigths+new_vectors, neigh_size)
                        neighbors = self.create_neighborhood(weights)        
            
                        # actualizar pop
                        chroms, objs = self.expand_pop(chroms.copy(), objs.copy(), new_vectors)
                        
                        # actualizar rho
                        n_weights = weights.shape[0]
                        rho = np.zeros((n_weights, ))       # no importa si se sobreescribe
                        
                        # update
                        is_principal = np.zeros((n_weights, ), dtype=bool)
                        is_principal[:budget] = True
                    
                        rho, pie, _ = associate_frontend(self.association_b_type, objs, weights, is_principal, neighbors, normalize_objs=self.association_b_norm)
                        
                        # frontend sort_sp_b
                        sorted_sp = sort_subproblems_frontend(self.sort_sp_b_type, rho, budget, self.pop_size, self.rand)
                
                        #sorted_sp = self.sort_subproblems(rho) 
                        #sp = sorted_sp.copy()
                        #l = int(budget/2)
                        #sp[-l:] = self.rand.permutation(self.pop_size)[-l:]
                        #sorted_sp = sp.copy()
                        
                
                
            # @note: we output messages AFTER evaluating child on self.evaluate_pop_mop
            # this way, the value of self.fevals is the expected value
            if self.verbose:            # @todo: add log support here
                if t%100 == 0:
                    print ("t:", t, " fevals:", self.fevals)
            
            # compute stats
            if self.use_adaptation:
            
                # added to reduce subsamplings!
                if t % self.stats_step == 0:
                    
                    # subsampling before stats!
                    tmp_rho, _ , _ = associate_frontend(self.association_c_type, objs, weights, is_principal, neighbors, normalize_objs=self.association_c_norm)
                    tmp_sorted_sp = sort_subproblems_frontend(self.sort_sp_c_type, tmp_rho, budget, self.pop_size, self.rand)

                    tmp_to_keep = tmp_sorted_sp[:budget]
                    
                    input_chroms = chroms[tmp_to_keep]
                    input_objs = objs[tmp_to_keep]
                
                    self.stats(input_chroms, input_objs, t, run_id)
                    
                    # added to save rho
                    rho, _, _ = associate_frontend("nearest", objs, weights, is_principal, neighbors, normalize_objs=True)
                    
                    filename = "rho_%s_m_%d_run_%d_t_%d.txt" % (self.name, self.m_objs, run_id, t)
                    filepath = os.path.join(self.paths["path_pops"], filename)
                    np.savetxt(filepath, rho, fmt="%d")
                
            else:
                
                self.stats(chroms, objs, t, run_id)
            
            
            # next iter
            t+=1
        
        
        if self.use_adaptation:
            
            # subsampling
            rho, pie, _ = associate_frontend(self.association_c_type, objs, weights, is_principal, neighbors, normalize_objs=self.association_c_norm)
            sorted_sp = sort_subproblems_frontend(self.sort_sp_c_type, rho, budget, self.pop_size, self.rand)


            to_keep = sorted_sp[:budget]
            
            # esto es para evitar graficar desde chronos
            # debug
            ##self.debug_plot(objs, weights, rho, pie, to_keep)  
            
            # debug
            label = "%s_%s" % (self.name, self.params_mop["name"])
            #np.savetxt("objs_%s.txt"            % label, objs, fmt="%.10f")
            #np.savetxt("weights_%s.txt"         % label, weights, fmt="%.10f")
            #np.savetxt("neighbors_%s.txt"       % label, neighbors, fmt="%d")
            #np.savetxt("is_principal_%s.txt"    % label, is_principal, fmt="%d")
            #np.savetxt("rho_%s.txt"             % label, rho, fmt="%d")
            #np.savetxt("pie_%s.txt"             % label, pie, fmt="%d")
            #np.savetxt("sorted_sp_%s.txt"       % label, sorted_sp, fmt="%d")
            
            # ---- just for stats ----
            
            # objs
            filename = "debug_objs_%s_m_%d_run_%d_t_%d.txt" % (self.name, self.m_objs, run_id, t)
            filepath = os.path.join(self.paths["path_pops"], filename)
            np.savetxt(filepath, objs, fmt="%.10f")
            
            # weights
            filename = "debug_weights_%s_m_%d_run_%d_t_%d.txt" % (self.name, self.m_objs, run_id, t)
            filepath = os.path.join(self.paths["path_pops"], filename)
            np.savetxt(filepath, weights, fmt="%.10f")
            
            # neighbors
            filename = "debug_neighbors_%s_m_%d_run_%d_t_%d.txt" % (self.name, self.m_objs, run_id, t)
            filepath = os.path.join(self.paths["path_pops"], filename)
            np.savetxt(filepath, neighbors, fmt="%d")
            
            # is_principal
            filename = "debug_is_principal_%s_m_%d_run_%d_t_%d.txt" % (self.name, self.m_objs, run_id, t)
            filepath = os.path.join(self.paths["path_pops"], filename)
            np.savetxt(filepath, is_principal, fmt="%d")
            
            # rho
            filename = "debug_rho_%s_m_%d_run_%d_t_%d.txt" % (self.name, self.m_objs, run_id, t)
            filepath = os.path.join(self.paths["path_pops"], filename)
            np.savetxt(filepath, rho, fmt="%d")
            
            # pie
            filename = "debug_pie_%s_m_%d_run_%d_t_%d.txt" % (self.name, self.m_objs, run_id, t)
            filepath = os.path.join(self.paths["path_pops"], filename)
            np.savetxt(filepath, pie, fmt="%d")
            
            # sorted_sp
            filename = "debug_sorted_sp_%s_m_%d_run_%d_t_%d.txt" % (self.name, self.m_objs, run_id, t)
            filepath = os.path.join(self.paths["path_pops"], filename)
            np.savetxt(filepath, sorted_sp, fmt="%d")
            
            
            # debug
            pops = (objs, objs[to_keep], weights)
            labels = ("objs", "objs[to_keep]", "weights")
            plot_pops(pops, labels, title=self.name)
            
            # ----
            
            chroms = chroms[to_keep]
            objs = objs[to_keep]
            
        return chroms.copy(), objs.copy()

    
    
