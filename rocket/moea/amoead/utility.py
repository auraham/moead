# utility.py
from __future__ import print_function
from rocket.forms import evaluate_form
import numpy as np

def utility_matrix(objs, weights, z_ideal, z_nadir, params_form):
    
    pop_size    = objs.shape[0]
    n_weights   = weights.shape[0]
    m_objs      = weights.shape[1] 
    utility     = np.zeros((pop_size, n_weights))
    
    for row in range(pop_size):
        
        for col in range(n_weights):
            
            input_objs = objs[row, :].reshape((1, m_objs)).repeat(n_weights, axis=0)
            
            # check that input_objs.shape and weights.shape is the same
            scalar = evaluate_form(input_objs, weights, z_ideal, z_nadir, params_form)  # (n_weights, 1)
            utility[row, :] = scalar.flatten()                                          # (n_weights, )

    return utility.copy()
