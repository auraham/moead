# Archivos

- `run_experiment_a.py` permite evaluar AMOEA/D-{PBI,ASF} en DTLZ1, inv-DTLZ1 con `m_objs=3,4,5,6,7,8` objetivos. También se incluye DTLZ1-spin con `m_objs=3`.
- `run_experiment_a.sh` permite invocar al script anterior desde consola y automatizar la ejecución del experimento A.




# Parámetros








# Experimento A

Para ejecutar este experimento, ejecute 

```
./run_experiment_a.sh
```

El contenido del script `run_experiment_a.sh` es el siguiente:

```shell
#!/bin/bash

cd /home/acamacho/rocket/moea/amoead

nohup python3 run_experiment_a.py -m dtlz1 -f asf  > log_a_dtlz1_asf.log &
nohup python3 run_experiment_a.py -m dtlz1 -f pbi  > log_a_dtlz1_pbi.log &
nohup python3 run_experiment_a.py -m dtlz1 -f ipbi > log_a_dtlz1_ipbi.log &

nohup python3 run_experiment_a.py -m dtlz1-spin -f asf  > log_a_dtlz1_spin_asf.log &
nohup python3 run_experiment_a.py -m dtlz1-spin -f pbi  > log_a_dtlz1_spin_pbi.log &
nohup python3 run_experiment_a.py -m dtlz1-spin -f ipbi > log_a_dtlz1_spin_ipbi.log &

nohup python3 run_experiment_a.py -m inv-dtlz1 -f asf  > log_a_inv_dtlz1_asf.log &
nohup python3 run_experiment_a.py -m inv-dtlz1 -f pbi  > log_a_inv_dtlz1_pbi.log &
nohup python3 run_experiment_a.py -m inv-dtlz1 -f ipbi > log_a_inv_dtlz1_ipbi.log &

```

El script realiza diferentes invocaciones a `python3 run_experiment_a.py`. Cada una se envía a background con `nohup &`. De este modo, se pueden ejecutar las nueve invocaciones en paralelo en el servidor `chronos`.



# Resultados experimento A

Para calcular el hipervolumen a partir de los resultados del script `run_experiment_a.sh`, ejecute:

```
./get_results_experiment_a.sh
```

**Nota** Por ahora, debe editar el script anterior para obtener los resultados de `inv-dtlz1` como sigue:

```shell
# para inv-dtlz1
for mop_name in inv-dtlz1 ; do
	...
	# numero de generaciones: 600
	for step in {5..600..5} ; do
		...
	done
done
```

```shell
# para dtlz1, dtlz1-spin
for mop_name in dtlz1 dtlz1-spin ; do
	...
	# numero de generaciones: 300
    for step in {5..300..5} ; do
    	...
    done
done
```

El script `get_results_experiment_a.sh` crea un archivo con el formato:

```
hv_MOEA-NAME_m_M-OBJS_run_RUN-ID.txt
```

en el directorio `stats` de cada algoritmo evaluado en la experimentación. En este caso, se generarán 30 archivos por algoritmo, uno por cada ejecución independiente. Cada archivo contiene `iters/step` líneas. Cada línea representa el valor del hipervolumen en intervalos de `step` generaciones.

```
0.000000
0.000000
0.000000
0.000000
0.000000
0.000000
...
1.214490
1.215043
1.215833
1.217731
1.219765
1.223889
1.224422
```



