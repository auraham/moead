"""
run_experiment_a.py

this script allows us to compare AMOEA/D using different values for `expansion_threshold` on
    
    DTLZ1
    DTLZ1-spin
    inv-DTLZ1
    
Run
    
python3 run_experiment_a.py -m dtlz1 -f pbi

"""

from __future__ import print_function
from rocket.moea import AMOEAD, AMOEAD2, AMOEAD3, AMOEAD5, AMOEAD6, AMOEAD7, AMOEAD8, AMOEAD9, MOEAD
from rocket.helpers import get_n_genes, get_bounds
from rocket.labs import get_experiment_path
import numpy as np
import os

import argparse
import sys

# define experiment output path
script_path = os.path.dirname(os.path.abspath(__file__))
output_path = get_experiment_path(script_path, name="experiment_a_extended")

    
if __name__ == "__main__":
    
    mop_configs = {         # (iters, params_form)
        "dtlz1"         : 300,
        "dtlz1-spin"    : 300, 
        "inv-dtlz1"     : 600,
    }
    
    
    mop_configs = {     # {m_objs:iters}
        "dtlz1"         : {3: 300, 4: 500,  5:  600},
        "inv-dtlz1"     : {3: 600, 4: 1000, 5: 1200},
        
        #"dtlz1-spin"    : 300, 
    
    }
    
    
    expansion_p_configs = {
        3: 12,
        4: 7,
        5: 6,
    }
    
    form_configs = {
        "pbi"   : {"name": "pbi",  "pbi-theta": 5},
        "ipbi"  : {"name": "ipbi", "ipbi-theta": 5, "ipbi-negative": True},
        "asf"   : {"name": "asf",  "asf-z_ref": np.array([0., 0., 0.])},
    }
    # command line parameters
    ap = argparse.ArgumentParser()
    ap.add_argument("-m", "--mop_name",  required=True, help="mop name: dtlz1, dtlz1-spin, inv-dtlz1")
    ap.add_argument("-f", "--form_name", required=True, help="form name: pbi, ipbi, asf")
    
    args = vars(ap.parse_args())
    
    mop_name = args["mop_name"]
    form_name = args["form_name"]
    
    
    if not mop_name in mop_configs.keys():
        print("Incorrect mop name")
        sys.exit(0)
        
    if not form_name in form_configs.keys():
        print("Incorrect form name")
        sys.exit(0)
   
    # -- dont edit from here --
    
    for m_objs in (3, 4, 5):
    
        filename = "weights_m_%d.txt" % m_objs
        weights = np.genfromtxt(filename)
        pop_size, m_objs = weights.shape
        
        iters       = mop_configs[mop_name][m_objs]
        params_form = form_configs[form_name]
        n_genes, wfg_k, wfg_l = get_n_genes(mop_name, m_objs)
        
        expansion_p = expansion_p_configs[m_objs]

        # params_form
        params_form = None
        
        if form_name == "pbi":
            params_form = {"name": "pbi",  "pbi-theta": 5}
        
        if form_name == "asf":
            params_form = {"name": "asf",  "asf-z_ref": np.zeros((m_objs, ))}


        params = {
            "runs"              : 30,                                                       # number of independent runs
            "iters"             : iters,                                                    # number of generations
            "m_objs"            : m_objs,                                                   # number of objectives
            "pop_size"          : pop_size,                                                 # number of individuals
            "params_mop"        : {"name": mop_name, "k": wfg_k, "degrees":180},            # mop parameters
            "params_crossover"  : {"name": "sbx", "prob": 1.0, "eta": 20},                  # crossover parameters
            "params_mutation"   : {"name": "polymutation", "prob": 1./n_genes, "eta": 15},  # mutation parameters
            "params_stats"      : {"output_path": output_path, "step": 5},                  # stats parameters
            "verbose"           : True,                                                     # log verbosity
            
            # MOEAD params
            "neigh_size"        : 20,
            "pop_weights"       : weights,
            "params_form"       : params_form,
            
            # AMOEAD params
            "amoead_association_a_type" : "hierarchy",
            "amoead_association_b_type" : "hierarchy",
            "amoead_association_c_type" : "nearest",             # debe ser nearest en lugar de hierarchy
            "amoead_association_a_norm" : True,
            "amoead_association_b_norm" : True,
            "amoead_association_c_norm" : True,             
            "amoead_sort_sp_a_type" : "hierarchy_rand",
            "amoead_sort_sp_b_type" : "hierarchy_rand",
            "amoead_sort_sp_c_type" : "hierarchy",               # no rand!
            "amoead_expansion_p"    : expansion_p,
            
            
        }
        
        
        for expansion_threshold in (2, 4, 6, 8, 10, 12, 14, 16):

            # proposal
            params["name"]                          = "amoead-%s-%d"  % (params_form["name"], expansion_threshold)
            params["label"]                         = "AMOEA/D-%s-%d" % (params_form["name"].upper(), expansion_threshold)
            params["amoead_use_adaptation"]         = True
            params["amoead_expansion_threshold"]    = expansion_threshold 
            amoead = AMOEAD9(params)
            amoead.evaluate(plot_output=False, plot_output_name="", plot_run=False)
            


