#!/bin/bash

cd /home/acamacho/rocket/moea/amoead

nohup python3 run_experiment_a.py -m dtlz1 -f asf  > log_a_dtlz1_asf_ext.log &
nohup python3 run_experiment_a.py -m dtlz1 -f pbi  > log_a_dtlz1_pbi_ext.log &
#nohup python3 run_experiment_a.py -m dtlz1 -f ipbi > log_a_dtlz1_ipbi.log &

#nohup python3 run_experiment_a.py -m dtlz1-spin -f asf  >> log_a_dtlz1_spin_asf.log &
#nohup python3 run_experiment_a.py -m dtlz1-spin -f pbi  >> log_a_dtlz1_spin_pbi.log &
#nohup python3 run_experiment_a.py -m dtlz1-spin -f ipbi > log_a_dtlz1_spin_ipbi.log &

nohup python3 run_experiment_a.py -m inv-dtlz1 -f asf  > log_a_inv_dtlz1_asf_ext.log &
nohup python3 run_experiment_a.py -m inv-dtlz1 -f pbi  > log_a_inv_dtlz1_pbi_ext.log &
#nohup python3 run_experiment_a.py -m inv-dtlz1 -f ipbi > log_a_inv_dtlz1_ipbi.log &
