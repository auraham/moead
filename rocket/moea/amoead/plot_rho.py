# plot_rho.py
from __future__ import print_function
from rocket.plot import load_rcparams
import matplotlib.pyplot as plt
import numpy as np


def plot_rho(rho, title):
    
    
    fig = plt.figure()
    ax = fig.add_subplot(111)
    plt.subplot(1,1,1)
    
    vmin = rho.min()
    vmax = rho.max()
    plt.matshow(rho, fignum=False, cmap="Blues", vmin=vmin, vmax=vmax)
    
    plt.title(title)
    plt.colorbar()
    plt.grid(False)
    plt.xlabel("Generation $t$")
    plt.ylabel("Vector $w_i$")
    
    ax.xaxis.set_ticks_position("bottom")
    
    plt.show()

if __name__ == "__main__":
    
    load_rcparams()
    mop_name = "dtlz1-spin"
    #mop_name = "dtlz1"
    rho_record = np.genfromtxt("rho/rho_amoead3_%s.txt" % mop_name)
    
    plot_rho(rho_record, mop_name)
