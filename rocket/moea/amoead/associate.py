# associate.py
from __future__ import print_function
from rocket.helpers import rad2deg
from numpy.linalg import norm
import numpy as np
from rocket.moea.amoead.debug import debug_plot_objs_associate
from scipy.spatial.distance import cdist
from rocket.moea.amoead.normalize import norm_matrix


def get_angle(u, v):
    """
    Compute angle between vectors (degrees)
    
    Input
    u       (m, ) array
    v       (m, ) array
    
    Output
    deg     angle between u and v
    """
    
    rad = np.arccos(np.dot(u, v) / (norm(u) * norm(v))) 
    
    deg = rad2deg(rad)
    
    return deg
    
    
def get_pdist(u, v):
    """
    Compute perpendicular distance between objective vector u and weight vector v.
    In other words, compute the length of the orthogonal (perpendicular) vector between u and v.
    
    Input
    u       (m, ) array
    v       (m, ) array
    
    Output
    deg     perpendicular distance between u and v
    """
    
    # perpendicular vector
    #     (vector) - (          scalar          )  * (vector)
    wp =  u        - (np.dot(u, v) / (norm(v)**2)) * v

    # lenght
    d2 = norm(wp)

    return d2


def associate(norm_objs, weights, criteria="pdist"):
    """
    Associate weights to solutions in pie
    
    Input
    norm_objs       (pop_size*2, m_objs) normalized objs (parent+offspring)
    weights         (n_weights, m_objs) weights matrix
    
    Output
    pie             (pop_size*2, )  pie[i]=j  (association dict) returns the index (j) of the weight associated to the i-th individual in S set
    rho             (n_weights, )   rho[i]    (counter dict)     returns the number of individuals associated to the i-th weight vector
    """
    
    objs_size   = norm_objs.shape[0]
    n_weights   = weights.shape[0]
    pie         = np.ones((objs_size, ), dtype=int) * -1       # if pie[i] = -1, then the i-th individual was not assigned to any weight vector
    rho         = np.zeros((n_weights, ), dtype=int)
    distances   = np.ones((objs_size, n_weights)) * np.inf
    
    
    # for each obj vector s_i in s_indices
    for i, s in enumerate(norm_objs):

        # reset historial
        min_pdist       = np.inf        # min perpendicular dist (pdist)
        argmin_pdist    = 0
    
        
        # for each weight vector w_j in weights
        for j, w in enumerate(weights):


            # debug
            #if i in (69, 75) and j == (75):
            #    import ipdb; ipdb.set_trace()

            curr_pdist = np.inf

            if criteria == "pdist":
                curr_pdist = get_pdist(s, w)            
                
            elif criteria == "cosine":
                curr_pdist = get_angle(s, w)
            
            else:
                # default
                curr_pdist = get_pdist(s, w) 
                
            distances[i, j] = curr_pdist
            
            # update historial
            if curr_pdist < min_pdist:
                min_pdist = curr_pdist
                argmin_pdist = j
                
        # update pie
        pie[i] = argmin_pdist                           # i-th individual is associated to w_j (j = argmin_pdist, ie the nearest w_j)
        
        # added
        rho[argmin_pdist] += 1                          # increment the number of individuals associated to w_j
        
            
    return rho.copy(), pie.copy(), distances.copy()

def associate_from_cm(norm_objs, weights, cm):
    """
    Associate weights to solutions in pie
    """
    
    objs_size   = norm_objs.shape[0]
    n_weights   = weights.shape[0]
    pie         = np.ones((objs_size, ), dtype=int) * -1       # if pie[i] = -1, then the i-th individual was not assigned to any weight vector
    rho         = np.zeros((n_weights, ), dtype=int)
    
    # for each obj vector s_i in s_indices
    for i, s in enumerate(norm_objs):

        # reset historial
        min_pdist       = np.inf        # min perpendicular dist (pdist)
        argmin_pdist    = 0
        
        # for each weight vector w_j in weights
        for j, w in enumerate(weights):

            curr_pdist = cm[i, j]

            # update historial
            if curr_pdist < min_pdist:
                min_pdist = curr_pdist
                argmin_pdist = j
                
        # update pie
        pie[i] = argmin_pdist                           # i-th individual is associated to w_j (j = argmin_pdist, ie the nearest w_j)
        
        # added
        rho[argmin_pdist] += 1                          # increment the number of individuals associated to w_j
        
            
    return rho.copy(), pie.copy()


def get_closeness_matrix(norm_objs, weights, criteria):
    
    cm = None
    
    if criteria == "pdist":
        
        pdist = lambda u, v: norm(u - (np.dot(u, v) / (norm(v)**2)) * v)
    
        cm = cdist(norm_objs, weights, pdist)

    else:
        
        cm = cdist(norm_objs, weights, "cosine")

    return cm.copy()
        

"""
agrega esto
params["associate"] = "nearest"
params["associate"] = "hierarchy"
"""


def associate_nearest(norm_objs, weights, is_principal, neighbors, criteria="pdist"):
    """
    Associate weights to solutions in pie
    """
    
    objs_size   = norm_objs.shape[0]
    n_weights   = weights.shape[0]
    pie         = np.ones((objs_size, ), dtype=int) * -1       # if pie[i] = -1, then the i-th individual was not assigned to any weight vector
    rho         = np.zeros((n_weights, ), dtype=int)

    
    closeness   = get_closeness_matrix(norm_objs, weights, criteria)
    tmp_rho, _  = associate_from_cm(norm_objs, weights, closeness)

    for i in range(objs_size):

        q = closeness[i, :].argmin()            # q-th weight vector nearest to objs[i]
        
        # debug
        k = q
        
        # association
        rho[k] += 1
        pie[i] = k

    return rho.copy(), pie.copy(), None                 


def associate_ext(norm_objs, weights, is_principal, neighbors, criteria="pdist"):
    """
    Associate weights to solutions in pie
    """
    
    objs_size   = norm_objs.shape[0]
    n_weights   = weights.shape[0]
    pie         = np.ones((objs_size, ), dtype=int) * -1       # if pie[i] = -1, then the i-th individual was not assigned to any weight vector
    rho         = np.zeros((n_weights, ), dtype=int)

    
    closeness   = get_closeness_matrix(norm_objs, weights, criteria)
    tmp_rho, _  = associate_from_cm(norm_objs, weights, closeness)

    for i in range(objs_size):

        q = closeness[i, :].argmin()            # q-th weight vector nearest to objs[i]
        
        k = None
        
        if is_principal[q]:
            
            k = q                               # objs[i] is associated to weights[q]
            
            # aqui podriamos decir, si temp_rho[q] ya esta ocupado, asignalo al primer secundario libre
            
        else:
            
            neigh   = neighbors[q]                          # (neigh_size, ) index array
            to_keep = is_principal[neigh]                   # (>=neigh_size, ) boolean array
            
            if to_keep.sum() > 0:
            
                pweights        = neigh[to_keep]            # (>=neigh_size, ) index array
                pdists          = closeness[i, pweights]   
                sorted_pweights = pweights[pdists.argsort()]
                
                k = q                                       # association by default
                
                for pw in sorted_pweights:
                    
                    if tmp_rho[pw] == 0:
                        k = pw
                        break
                        
        # association
        rho[k] += 1
        pie[i] = k
        

    return rho.copy(), pie.copy(), None                 


def associate_frontend(cat, objs, weights, is_principal, neighbors, criteria="pdist", normalize_objs=False):
    """
    Associate weights to solutions in pie
    """
    
    rho, pie = None, None
    
    input_objs = objs.copy()
    
    if normalize_objs:
        input_objs = norm_matrix(objs)
        
    
    if cat == "nearest":
        
        rho, pie, _ = associate_nearest(input_objs, weights, is_principal, neighbors, criteria)
        
    
    elif cat == "hierarchy":
    
        rho, pie, _ = associate_ext(input_objs, weights, is_principal, neighbors, criteria)
        
    else:
        print("error on associate_frontend")
    
    
    return rho, pie, None

