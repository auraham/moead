# amoead5.py
from __future__ import print_function
from rocket.moea import MOEAD
from rocket.moea.amoead.expansion import expand_ext
from rocket.moea.amoead.associate import associate, associate_ext, associate_frontend
from rocket.moea.amoead.normalize import norm_matrix
from rocket.moea.amoead.utility import utility_matrix
from rocket.moea.amoead.sort_subproblems import sort_subproblems_frontend
from rocket.moea.amoead.replacement import replacement

import numpy as np
from numpy.random import RandomState
import matplotlib.pyplot as plt

# debug
from rocket.dev.rand import Rand

class AMOEAD8(MOEAD):
    
    def __init__(self, params):
        
        MOEAD.__init__(self, params)
        
        self.expansion_p = 12 # para los 91 vectores!
        self.expansion_done = False
        #self.cat = params["amoead_cat"]
        
        self.association_a_type = params["amoead_association_a_type"]
        self.association_b_type = params["amoead_association_b_type"]
        self.association_c_type = params["amoead_association_c_type"]
        
        
        self.sort_sp_a_type = params["amoead_sort_sp_a_type"]
        self.sort_sp_b_type = params["amoead_sort_sp_b_type"]
        self.sort_sp_c_type = params["amoead_sort_sp_c_type"]
        self.expansion_threshold = params["amoead_expansion_threshold"]
        
    
    def sort_subproblems(self, rho):
        """
        Return a sorted array of indices, where
        
        rho[i] = 1 is better than rho[i] == 2
        rho[i] = 2 is better than rho[i] == 3
        etc
        
        Input
        rho         (n_weights, ) association counter array
        
        Output
        sorted_rho  (n_weights, ) array of indices
        """
        
        tmp         = rho.copy()
        tmp[rho==0] = rho.max()+1
        sorted_rho  = tmp.argsort()
        
        return sorted_rho.copy() 
    
    
    def expand_weights(self, center_ids, weights):
        
        p = self.expansion_p
        n_weights = weights.shape[0]
        counter = np.zeros((n_weights, ))
        
        
        # create new weights
        weights, counter = expand_ext(center_ids, weights.copy(), p, scale=0.5)
        
        # como evitar que genere sobre los mismos center_ids una y otra vez?
        # counter debe ser zeros en ese caso
        
        return weights.copy(), counter.copy()
    
    
    def expand_pop(self, chroms, objs, slots):
        
        # select random pool
        size = chroms.shape[0]
        rand_pool = self.rand.permutation(size)[:slots]     # 
        
        
        if slots > size:
            
            # si hay mas slots que llenar que individuos de donde copiar...
            
            rand_pool = self.rand.permutation(slots)     # indices aleatorios entre [0, slots-1], slots > pop_size 
            pool = np.zeros((slots, ), dtype=int)
            
            for i in range(slots):
                
                pool[i] = rand_pool[i] % size
            
            rand_pool = pool.copy()
        
        
        # create new blocks
        new_chroms_block = chroms[rand_pool].copy()     # seguro que aqui el copy si es necesario para no copiar referencias sino contenido!
        new_objs_block = objs[rand_pool].copy()
        
        # update pop
        new_chroms = np.vstack((chroms, new_chroms_block))
        new_objs = np.vstack((objs, new_objs_block))
        
        # update pop_size
        self.pop_size = new_chroms.shape[0]
        
        return new_chroms.copy(), new_objs.copy()
    
    
    def debug_plot(self, objs, weights, rho, pie, to_keep):
        
        # ---- debug -----
        from rocket.fronts import get_real_front
        from rocket.plot import plot_pops, basecolors
        
        mop_name        = self.params_mop["name"]
        real_front, p   = get_real_front(mop_name, self.m_objs, self.n_genes, self.params_mop)
        
        W = weights
        pops = (objs, objs[to_keep], W, W[to_keep])
        labels = ("objs", "objs (to_keep)", "weights", "weights (to_keep)")
        
        title = "%s - %s" % (self.params_mop["name"].upper(), self.label)
        fig, ax = plot_pops(pops, labels, real_front=real_front, p=p, title=title)
        
        """
        indices = np.where(rho > 0)[0]
        
        for ind in indices:
            w = W[ind]
            x, y, z = w
            #ax.text(x, y, z, str(ind), color="#404040", size=10)
            
            ax.plot(W[[ind], 0], W[[ind], 1], W[[ind], 2, ], c="#95234E", 
                mec=basecolors["almost_black"], ls="none", marker="o", markeredgewidth=0.15)
            
            to_plot = np.where(pie == ind)[0]
            
            if len(to_plot):
                for tp in to_plot:
                    
                    x, y, z = objs[tp]
                    #ax.text(x, y, z, str(tp), color="#92C3B8", size=10)
            
        """
        
        plt.show()
    
    
    def run(self, chroms, seed, run_id):
        """
        Independent run
        """
        
        self.rand   = RandomState(seed)
        
        pop_size    = self.pop_size
        weights     = self.pop_weights
        neighbors   = self.create_neighborhood(weights)
        objs        = self.evaluate_pop_mop(chroms)
        
        # added to amoead
        budget      = self.pop_size
        indices     = np.arange(budget, dtype=int)      # not expandible
        to_ignore   = np.zeros((budget, ), dtype=bool)  # not expandible
        is_principal= np.ones((budget, ), dtype=bool)   # expandible, only first budget entries are True
        sorted_sp   = np.arange(budget, dtype=int)
        
        t = 1
        self.stats(chroms, objs, t, run_id)
        
        t = 2
        while t <= self.iters:
            
            for j in range(budget):
                
                # added to amoead
                i = sorted_sp[j]

                neigh = neighbors[i]
                
                neigh_objs = objs[neigh]
                
                neigh_weights = weights[neigh]
                
                child = self.genetic_operations(i, neigh, chroms)
                
                child = self.check_pop_bounds(child)            
                
                child_objs = self.evaluate_pop_mop(child)

                # moead                
                #scalar_child = self.evaluate_pop_scalar_method(child_objs, neigh_weights)
                #scalar_neigh = self.evaluate_pop_scalar_method(neigh_objs, neigh_weights)
                #to_replace = neigh[scalar_child <= scalar_neigh]
                #chroms[to_replace, :] = child.copy()
                #objs[to_replace, :] = child_objs.copy()
        
                # moead-gra
                scalar_child    = self.evaluate_pop_scalar_method(child_objs, weights)
                scalar_pop      = self.evaluate_pop_scalar_method(objs, weights)
                to_replace      = replacement(scalar_child, scalar_pop)
                chroms[to_replace, :] = child.copy()
                objs[to_replace, :] = child_objs.copy()
                
            
            # added to amoead
            rho, pie, _ = associate_frontend(self.association_a_type, objs, weights, is_principal, neighbors)
            
            # frontend sort_sp_a
            #import ipdb; ipdb.set_trace()
            sorted_sp = sort_subproblems_frontend(self.sort_sp_a_type, rho, budget, self.pop_size, self.rand)

            #sorted_sp = self.rand.permutation(self.pop_size)
            
            
            # indices a expandir
            threshold   = self.expansion_threshold
            ids         = indices[rho[is_principal] > threshold]            # (0, 1, ..., budget-1 a lo mas)
            center_ids  = ids[np.invert(to_ignore[ids])]                    # (0, 1, ..., budget-1 a lo mas)
            slots       = center_ids.shape[0]                               # numero de vectores a expandir, puede ser cero
            
            if slots > 0:
                
                # crear nuevos vectores
                weights, counter = self.expand_weights(center_ids, weights.copy())
                new_vectors = counter.sum()
                
                # update
                to_ignore[center_ids] = True
                
                # debug
                print("expansion, new vectors: %d, total vectors: %d, center_ids: %s" % (new_vectors, weights.shape[0], center_ids))
                
                if new_vectors > 0:
                    
                    # actualizar vecindario
                    # (n_weigths+new_vectors, neigh_size)
                    neighbors = self.create_neighborhood(weights)        
        
                    # actualizar pop
                    chroms, objs = self.expand_pop(chroms.copy(), objs.copy(), new_vectors)
                    
                    # actualizar rho
                    n_weights = weights.shape[0]
                    rho = np.zeros((n_weights, ))       # no importa si se sobreescribe
                    
                    # update
                    is_principal = np.zeros((n_weights, ), dtype=bool)
                    is_principal[:budget] = True
                
                    rho, pie, _ = associate_frontend(self.association_b_type, objs, weights, is_principal, neighbors)
                    
                    # frontend sort_sp_b
                    sorted_sp = sort_subproblems_frontend(self.sort_sp_b_type, rho, budget, self.pop_size, self.rand)
            
                    #sorted_sp = self.sort_subproblems(rho) 
                    #sp = sorted_sp.copy()
                    #l = int(budget/2)
                    #sp[-l:] = self.rand.permutation(self.pop_size)[-l:]
                    #sorted_sp = sp.copy()
                    
            
                
            # @note: we output messages AFTER evaluating child on self.evaluate_pop_mop
            # this way, the value of self.fevals is the expected value
            if self.verbose:            # @todo: add log support here
                if t%1 == 0:
                    print ("t:", t, " fevals:", self.fevals)
            
            # compute stats
            self.stats(chroms, objs, t, run_id)
            
            # next iter
            t+=1
        
        # subsampling
        rho, pie, _ = associate_frontend(self.association_c_type, objs, weights, is_principal, neighbors)
        sorted_sp = sort_subproblems_frontend(self.sort_sp_c_type, rho, budget, self.pop_size, self.rand)


        to_keep = sorted_sp[:budget]
        
        # debug
        self.debug_plot(objs, weights, rho, pie, to_keep)  
        
        # debug
        label = "%s_%s_2" % (self.name, self.params_mop["name"])
        #np.savetxt("objs_%s.txt"            % label, objs, fmt="%.10f")
        #np.savetxt("weights_%s.txt"         % label, weights, fmt="%.10f")
        #np.savetxt("neighbors_%s.txt"       % label, neighbors, fmt="%d")
        #np.savetxt("is_principal_%s.txt"    % label, is_principal, fmt="%d")
        #np.savetxt("rho_%s.txt"             % label, rho, fmt="%d")
        #np.savetxt("pie_%s.txt"             % label, pie, fmt="%d")
        #np.savetxt("sorted_sp_%s.txt"       % label, sorted_sp, fmt="%d")
        
        chroms = chroms[to_keep]
        objs = objs[to_keep]
        
        return chroms.copy(), objs.copy()

    
    
