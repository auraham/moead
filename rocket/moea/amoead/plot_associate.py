# plot_weights.py
from __future__ import print_function
from rocket.plot.colors import colors, basecolors
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from rocket.plot import load_rcparams
from rocket.fronts import get_real_front
import numpy as np
from rocket.moea.amoead.associate import associate
from rocket.moea.amoead.normalize import norm_matrix

#np.seterr(all='raise')

def plot_weights(objs, weights, real_front, p, rho, title, save=False):
    
    fig = plt.figure()
    ax = fig.add_subplot(111, projection="3d")
    ax.set_xlabel("$f_1$")
    ax.set_ylabel("$f_2$")
    ax.set_zlabel("$f_3$")
    ax.set_title(title)
    
    ax.set_xlim(0, 1.1)
    ax.set_ylim(0, 1.1)
    ax.set_zlim(0, 1.1)
    
    lined = {}
    
    # weights
    l, = ax.plot(weights[:, 0], weights[:, 1], weights[:, 2], mfc="none", label="weights",
                            mec=basecolors["almost_black"], ls="none", marker="o", markeredgewidth=0.15)
    lined["weights"] = l
             
    
    # objs
    l, = ax.plot(objs[:, 0], objs[:, 1], objs[:, 2], label="objs", c=colors[0], 
                        mec=basecolors["almost_black"], ls="none", marker="o", markeredgewidth=0.15)
    lined["objs"] = l

    # plot front
    X = real_front[:, 0].reshape(p, p)
    Y = real_front[:, 1].reshape(p, p)
    Z = real_front[:, 2].reshape(p, p)
    #ax.plot_wireframe(X, Y, Z, rstride=2, cstride=2, color=basecolors["real_front"])
    
    # weights
    vmin, vmax = rho.min(), rho.max()
    for i, w in enumerate(weights):
        x, y, z = w
        label = "%d (%d)" % (i, rho[i])
        #label = "%d" % rho[i]
        
        if rho[i] > 0:
            ax.text(x, y, z, label, color="#404040", size=10)
        
        alpha = (rho[i])/vmax
        
        ax.plot(weights[[i], 0], weights[[i], 1], weights[[i], 2], c="#9E0E42", 
                            mec=basecolors["almost_black"], ls="none", marker="o", markeredgewidth=0.15, alpha=alpha)
      
    
    # objs
    #for i, obj in enumerate(objs):
    #    x, y, z = obj
    #    label = "%d" % i
    #    ax.text(x, y, z, label, color="#4D8861", size=10)
        
        
                  
    ax.view_init(15, 45)
    
    leg = ax.legend(loc="lower left")
    
    # for callback
    for legtxt in leg.get_texts():
        legtxt.set_picker(5)
        
    # inner callback
    def on_pick(event):
        
        legtxt, line = None, None
        
        obj = event.artist
        is_callable = callable(getattr(obj, "get_text", None))
        
        if is_callable:
            
            legtxt = event.artist
            line = lined[legtxt.get_text()]
            
        else:
            
            legtxt = None
            line = event.artist
            
        is_visible = line.get_visible()
        
        # fade a little bit
        if is_visible:
            
            line.set_visible(False)
            
            if legtxt is not None:
                legtxt.set_alpha(0.2)
                
        else:
            
            line.set_visible(True)
            
            if legtxt is not None:
                legtxt.set_alpha(None)
                
        fig.canvas.draw()
    
    # register callback
    fig.canvas.mpl_connect("pick_event", on_pick)
    
    
    if save:
        output = "weights.png"
        fig.savefig(output, dpi=300)

    plt.show()

if __name__ == "__main__":
    
    load_rcparams()
    
    weights = np.genfromtxt("weights_m_3.txt")
    
    mop_name = "dtlz1-spin"
    criteria = "cosine"
    objs_path = "objs_issue.txt"
    #objs_path = "sample/dtlz1-spin/amoead-pbi/pops/objs_amoead-pbi_m_3_run_0_t_300.txt"
    objs = np.genfromtxt(objs_path)
        
    for mop_name in ("dtlz1-spin", ):
    
    
        # case a: no objs normalization, no weights scale
        objs = np.genfromtxt(objs_path)
        real_front, p = get_real_front(mop_name, 3, params={"degrees":180})
        rho_a, pie_a, dist_a = associate(objs, weights, criteria)
        plot_weights(objs, weights, real_front, p, rho_a, "case a: no objs normalization, no weights scale", save=False)
        print("case a, rho", rho_a[40:50])
        
        # case b: objs normalization, no weights scale
        objs = np.genfromtxt(objs_path)
        real_front, p = get_real_front(mop_name, 3, params={"degrees":180})
        norm_objs = norm_matrix(objs)
        rho_b, pie_b, dist_b = associate(norm_objs, weights, criteria)
        plot_weights(norm_objs, weights, real_front, p, rho_b, "case b: objs normalization, no weights scale", save=False)
        print("case b, rho", rho_b[40:50])
        
        
        # case c: no objs normalization, scaled weights
        objs = np.genfromtxt(objs_path)
        weights = np.genfromtxt("weights_m_3.txt")*0.5
        real_front, p = get_real_front(mop_name, 3, params={"degrees":180})
        rho_c, pie_c, dist_c = associate(objs, weights, criteria)
        plot_weights(objs, weights, real_front, p, rho_c, "case c: no objs normalization, scaled weights", save=False)
        print("case c, rho", rho_c[40:50])
        
        # case d: objs normalization, scaled weights
        objs = np.genfromtxt(objs_path)
        weights = np.genfromtxt("weights_m_3.txt")*0.5
        real_front, p = get_real_front(mop_name, 3, params={"degrees":180})
        norm_objs = norm_matrix(objs)
        rho_d, pie_d, dist_d = associate(norm_objs, weights, criteria)
        plot_weights(norm_objs, weights, real_front, p, rho_d, "case d: objs normalization, scaled weights", save=False)
        print("case d, rho", rho_d[40:50])
        
        """
        # case e. normalization, norm weigths
        weights = np.genfromtxt("weights_m_3.txt")
        objs = np.genfromtxt(objs_path)
        real_front, p = get_real_front(mop_name, 3, params={"degrees":180})
        norm_objs = norm_matrix(objs, np.array([0, 0, 0]), np.array([1,1,1]))
        rho_e, pie_e, dist_e = associate(norm_objs, weights, criteria)
        #plot_weights(norm_objs, weights, real_front, p, rho_e, "case e: normalization, scaled weights", save=False)
        #print("case e, rho", rho_e[40:50])
        """
    
