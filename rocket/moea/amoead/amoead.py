# amoead.py
from __future__ import print_function
from rocket.moea import MOEAD
from rocket.moea.amoead.expansion import expand_ext
from rocket.moea.amoead.associate import associate
from rocket.moea.amoead.normalize import norm_matrix
import numpy as np
from numpy.random import RandomState
import matplotlib.pyplot as plt

# debug
from rocket.dev.rand import Rand

class AMOEAD(MOEAD):
    
    def __init__(self, params):
        
        MOEAD.__init__(self, params)
        
        # @todo: add checks
        self.expansion_step = params.get("amoead_expansion_step", None)     # number of generations for expanding weight vectors
        self.expansion_p    = params.get("amoead_expansion_p", None)        # number of divisions
    
    
    def expand_pop(self, chroms, objs, weights):
        
        
        n_genes     = chroms.shape[1]
        m_objs      = objs.shape[1]
        pop_size    = chroms.shape[0]
        p           = self.expansion_p
        center_ids  = np.arange(pop_size, dtype=int)
        
        # create new weights
        new_weights, counter = expand_ext(center_ids, weights, p, scale=0.5)
        
        # preserve previous pop
        new_pop_size = new_weights.shape[0]
        
        new_chroms = np.zeros((new_pop_size, n_genes))
        new_objs   = np.zeros((new_pop_size, m_objs))
        
        new_chroms[:pop_size, :] = chroms.copy()            # copy previous chroms
        new_objs[:pop_size, :]   = objs.copy()              # copy previous objs
        
        
        # add and evaluate new chroms
        start = pop_size
        end = 0
        for i in range(pop_size):
            
            if counter[i] > 0:
                
                nc  = counter[i]         # number of copies of chroms[i] to be added to new_chroms
                end = start + nc
                
                block_chroms    = chroms[i].reshape((1, n_genes)).repeat(nc, axis=0)
                #block_weights  = new_weights[start:end]
                block_objs      = self.evaluate_pop_mop(block_chroms)
                
                new_chroms[start:end, :] = block_chroms
                new_objs[start:end, :]   = block_objs 
                
                start = end
            
            
        return new_chroms.copy(), new_objs.copy(), new_weights.copy()
    
    
    def associate(self, objs, offspring_objs, weights):
        
        
        merge = np.vstack((objs, offspring_objs))
        
        norm_objs = norm_matrix(merge)
        
        pie, rho = associate(merge, weights)
        
        return pie.copy(), rho.copy()
        
    
    
    def run(self, chroms, seed, run_id):
        """
        Independent run
        """
        
        self.rand   = RandomState(seed)
        
        pop_size    = self.pop_size
        weights     = self.pop_weights
        neighbors   = self.create_neighborhood(weights)
        objs        = self.evaluate_pop_mop(chroms)
        
        # added to amoead
        pie, rho = None, None
        
        t = 1
        self.stats(chroms, objs, t, run_id)
        
        t = 2
        while t <= self.iters:
            
            # creation of offspring
            offspring_chroms = np.zeros(chroms.shape)
            offspring_objs = np.zeros(objs.shape)
            for i in range(pop_size):
                
                neigh = neighbors[i]
                
                neigh_objs = objs[neigh]
                
                neigh_weights = weights[neigh]
                
                child = self.genetic_operations(i, neigh, chroms)  # neigh o neighbors?
                
                child = self.check_pop_bounds(child)            
                
                child_objs = self.evaluate_pop_mop(child)
                
                # save
                offspring_chroms[i] = child.copy()
                offspring_objs[i] = child_objs.copy()
                
                #scalar_child = self.evaluate_pop_scalar_method(child_objs, neigh_weights)
                #scalar_neigh = self.evaluate_pop_scalar_method(neigh_objs, neigh_weights)
                
                #to_replace = neigh[scalar_child <= scalar_neigh]
                
                #chroms[to_replace, :] = child.copy()
                #objs[to_replace, :] = child_objs.copy()
                
            # added to amoead
            # @todo: it is slow!
            #pie, rho = self.associate(objs, offspring_objs, weights)
                
            # survival selection
            for i in range(pop_size):
                
                child      = offspring_chroms[i]
                child_objs = offspring_objs[i]
                
                neigh = neighbors[i]
                
                neigh_objs = objs[neigh]
                
                neigh_weights = weights[neigh]
                
                scalar_child = self.evaluate_pop_scalar_method(child_objs, neigh_weights)
                scalar_neigh = self.evaluate_pop_scalar_method(neigh_objs, neigh_weights)
                
                to_replace = neigh[scalar_child <= scalar_neigh]
                
                chroms[to_replace, :] = child.copy()
                objs[to_replace, :] = child_objs.copy()
            
            
            # @note: we output messages AFTER evaluating child on self.evaluate_pop_mop
            # this way, the value of self.fevals is the expected value
            if self.verbose:            # @todo: add log support here
                if t%100 == 0:
                    print ("t:", t, " fevals:", self.fevals)
            
            
            
            
            self.stats(chroms, objs, t, run_id)
            
            t+=1
            
            
            
        # debug
        #np.savetxt("pie.txt", pie, fmt="%d")
        #np.savetxt("rho.txt", rho, fmt="%d")
            
        return chroms.copy(), objs.copy()
    
    
    
        
        
    
        
    """
    def run(self, chroms, seed, run_id):
        #
        #Independent run
        #
        
        self.rand   = RandomState(seed)
        
        # debug
        # chroms = np.genfromtxt("init_chroms.txt")
        # self.rand = Rand() 
        
        pop_size    = self.pop_size
        weights     = self.pop_weights
        neighbors   = self.create_neighborhood(weights)
        objs        = self.evaluate_pop_mop(chroms)
        
        t = 1
        self.stats(chroms, objs, t, run_id)
        
        t = 2
        while t <= self.iters:
            
            for k in range(pop_size):
                
                # added to moead_gra
                i = self.get_next_subproblem(poi, k)
                
                # added to moead_gra
                child = genetic_operations(i, neighbors, chroms, self.lbound, self.ubound, self.pop_size, self.params_crossover["prob"], self.params_mutation["prob"], self.rand)       # @todo: use crossover and mutation frontends instead
                
                child = self.check_pop_bounds(child)            
                
                child_objs = self.evaluate_pop_mop(child)
                
                # modified for moead_gra, use all the weights, not only a subset
                scalar_child = self.evaluate_pop_scalar_method(child_objs, weights)
                scalar_pop   = self.evaluate_pop_scalar_method(objs, weights)
                
                
                # added to moead_gra
                to_replace = replacement(scalar_child, scalar_pop)
                
                
                chroms[to_replace, :] = child.copy()
                objs[to_replace, :] = child_objs.copy()
                
                # stats
                poi_evals[i] += 1
                poi_record[i, t-1] += 1        # t-1, debe ser cero-basado        
            
            
            # @note: we output messages AFTER evaluating child on self.evaluate_pop_mop
            # this way, the value of self.fevals is the expected value
            if self.verbose:            # @todo: add log support here
                if t%100 == 0:
                    print ("t:", t, " fevals:", self.fevals)
            
            # compute stats
            self.stats(chroms, objs, t, run_id)
            
            # added to moead_gra
            if t % self.poi_delta == 0:
                
                fit_old_pop = fit_new_pop.copy()
                fit_new_pop = self.evaluate_pop_scalar_method(objs, weights) 
                
                poi = self.update_improvement(fit_old_pop, fit_new_pop)
                
            
            # added to amoead_gra
            if self.expansion_step:
                if t % self.expansion_step == 0:
                    print ("Expanding pop...")
                    
                    # expand pop 
                    new_chroms, new_objs, new_weights = self.expand_pop(chroms.copy(), objs.copy(), weights.copy())
                    
                    # update pop and weights
                    chroms = new_chroms.copy()
                    objs = new_objs.copy()
                    weights = new_weights.copy()
                    
                    # update pop_size
                    pop_size = chroms.shape[0]
                    self.pop_size = pop_size
                    
                    # update neighborhood
                    neighbors   = self.create_neighborhood(weights)
                    
                    # update poi
                    fit_old_pop = self.evaluate_pop_scalar_method(objs, weights) 
                    fit_new_pop = self.evaluate_pop_scalar_method(objs, weights) 
                    
                    # option 1: reset
                    #poi = np.ones((pop_size, ))
                    
                    # option 2: extend
                    slots     = pop_size - poi.shape[0]
                    poi       = np.hstack((poi, np.ones((slots, ))))
                    poi_evals = np.hstack((poi_evals, np.zeros((slots, ))))
                    poi_record= np.vstack((poi_record, np.zeros((slots, self.iters))))
                    
            # next generation
            t+=1
            
            
        # save stats
        self.poi_evals  = poi_evals.copy()
        self.poi_record = poi_record.copy() 
           
        return chroms.copy(), objs.copy()

    
    def plot_run(self, run_id):
        
        # @todo: put MOEAD_GRA.plot_run in plot_poi.py or something similar
        
        plot_poi_record(self.poi_record)
        #np.savetxt("poi_record.txt", self.poi_record, fmt="%d")
    """
