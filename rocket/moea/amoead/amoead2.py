# amoead2.py
from __future__ import print_function
from rocket.moea import MOEAD
from rocket.moea.amoead.expansion import expand_ext
from rocket.moea.amoead.associate import associate
from rocket.moea.amoead.normalize import norm_matrix
from rocket.moea.amoead.utility import utility_matrix
import numpy as np
from numpy.random import RandomState
import matplotlib.pyplot as plt

# debug
from rocket.dev.rand import Rand

class AMOEAD2(MOEAD):
    
    def __init__(self, params):
        
        MOEAD.__init__(self, params)
        
    
    def sort_subproblems(self, rho):
        """
        Return a sorted array of indices, where
        
        rho[i] = 1 is better than rho[i] == 2
        rho[i] = 2 is better than rho[i] == 3
        etc
        
        Input
        rho         (n_weights, ) association counter array
        
        Output
        sorted_rho  (n_weights, ) array of indices
        """
        
        tmp         = rho.copy()
        tmp[rho==0] = rho.max()+1
        sorted_rho  = tmp.argsort()
        
        return sorted_rho.copy() 
        
        
    def update_pop(self, merged_objs, weights, rho):
        
        
        to_keep = np.zeros((self.pop_size, ), dtype=int)
        
        z_ideal = merged_objs.min(axis=0)
        z_nadir = merged_objs.max(axis=0)
        utility = utility_matrix(merged_objs, weights, z_ideal, z_nadir, self.params_form)      # (pop_size+1, n_weights)
        
        if weights.shape[0] == self.pop_size:
            
            # si no ha habido expansion de vectores ...
            per_col = 0
            to_keep = utility.argmin(axis=per_col)
            
        else:
            
            # si ya hubo expansion de vectores ...
            sorted_sp = self.sort_subproblems(rho)
            
            for i in range(self.pop_size):
                
                k = sorted_sp[i]
                to_keep[i] = utility[:, k].argmin()     # el argmin de la k-th col 
        
        return to_keep.copy()
    
    def run(self, chroms, seed, run_id):
        """
        Independent run
        """
        
        self.rand   = RandomState(seed)
        
        pop_size    = self.pop_size
        weights     = self.pop_weights
        neighbors   = self.create_neighborhood(weights)
        objs        = self.evaluate_pop_mop(chroms)
        
        # added to amoead
        norm_objs   = norm_matrix(objs)
        rho, _, _   = associate(norm_objs, weights)
        sorted_sp   = self.sort_subproblems(rho)
        rho_record  = np.zeros((weights.shape[0], self.iters))      # stats, aunque el numero de rows crecera luego!
        
        t = 1
        self.stats(chroms, objs, t, run_id)
        
        t = 2
        while t <= self.iters:
            
            # creation of offspring
            offspring_chroms = np.zeros(chroms.shape)
            offspring_objs = np.zeros(objs.shape)
            for j in range(pop_size):
                
                # added to amoead
                i = sorted_sp[j]
                
                neigh = neighbors[i]
                
                neigh_objs = objs[neigh]
                
                neigh_weights = weights[neigh]
                
                child = self.genetic_operations(i, neigh, chroms)
                
                child = self.check_pop_bounds(child)            
                
                child_objs = self.evaluate_pop_mop(child) 
                
                # save
                offspring_chroms[i] = child.copy()
                offspring_objs[i] = child_objs.copy()
                
            # survival selection
                
            # ---- added to amoead ----
            #import ipdb; ipdb.set_trace()
            merged_chroms   = np.vstack((chroms, offspring_chroms))
            merged_objs     = np.vstack((objs, offspring_objs))
            merged_norm_objs= norm_matrix(merged_objs)
            
            # este paso se puede mejorar al asociar solo al nuevo individuo!
            # update: no lo creo porque la poblacion cambia cada vez!
            # update: se puede acelerar al usar el rho de la generacion anterior (rho_prev) y 
            # sumarlo al rho de offspring_objs (rho_next): rho = rho_prev + rho_next; rho = rho_next
            rho, _, _ = associate(merged_norm_objs, weights)
            to_keep = self.update_pop(merged_objs, weights, rho) 
            
            chroms = merged_chroms[to_keep, :].copy()
            objs   = merged_objs[to_keep, :].copy()
            
            # stats
            rho_record[:, t-1] = rho.copy()
            
            # volver a ordenar rho aqui
            sorted_sp   = self.sort_subproblems(rho)
            
            
            # expansion
            """
            center_ids = where(rho>=1)
            if (rho>=1).sum() > 0:
                # quiere decir que al menos un vector puede ser expandido, por lo tanto, lo expandemos!
            """
            
            # ---- added to amoead -----
            
                
            # @note: we output messages AFTER evaluating child on self.evaluate_pop_mop
            # this way, the value of self.fevals is the expected value
            if self.verbose:            # @todo: add log support here
                if t%1 == 0:
                    print ("t:", t, " fevals:", self.fevals)
            
            # compute stats
            self.stats(chroms, objs, t, run_id)
            
            # next iter
            t+=1
            
            # debug
            filepath = "rho/rho_%s.txt" % self.params_mop["name"]
            np.savetxt(filepath, rho_record, fmt="%d")
           
        return chroms.copy(), objs.copy()

    
    """
    while t <= self.iters:
            
        for j in range(pop_size):
            
            # nuevo
            i = sorted_sp[j]
            
            neigh = neighbors[i]
            
            neigh_objs = objs[neigh]
            
            neigh_weights = weights[neigh]
            
            child = self.genetic_operations(i, neigh, chroms)
            
            child = self.check_pop_bounds(child)            
            
            child_objs = self.evaluate_pop_mop(child)                   # no me convencen los nuevos nombres con pop
            
            scalar_child = self.evaluate_pop_scalar_method(child_objs, neigh_weights)
            scalar_neigh = self.evaluate_pop_scalar_method(neigh_objs, neigh_weights)
            
            to_replace = neigh[scalar_child <= scalar_neigh]
            
            chroms[to_replace, :] = child.copy()
            objs[to_replace, :] = child_objs.copy()
        
        # nuevo
        rho = associate(objs, weights)
        rho_record[:, t-1] = rho.copy()
        sorted_sp   = self.sort_subproblems(rho)
    """    
    
    
    """
    def run_slow(self, chroms, seed, run_id):
        
        self.rand   = RandomState(seed)
        
        pop_size    = self.pop_size
        weights     = self.pop_weights
        neighbors   = self.create_neighborhood(weights)
        objs        = self.evaluate_pop_mop(chroms)
        
        # added to amoead
        norm_objs   = norm_matrix(objs)
        rho         = associate(norm_objs, weights)
        sorted_sp   = self.sort_subproblems(rho)
        
        t = 1
        self.stats(chroms, objs, t, run_id)
        
        t = 2
        while t <= self.iters:
            
            for j in range(pop_size):
                
                # added to amoead
                i = sorted_sp[j]
                
                neigh = neighbors[i]
                
                neigh_objs = objs[neigh]
                
                neigh_weights = weights[neigh]
                
                child = self.genetic_operations(i, neigh, chroms)
                
                child = self.check_pop_bounds(child)            
                
                child_objs = self.evaluate_pop_mop(child)                   # no me convencen los nuevos nombres con pop
                
                # ---- added to amoead ----
                merged_chroms   = np.vstack((chroms, child))
                merged_objs     = np.vstack((objs, child_objs))
                merged_norm_objs= norm_matrix(merged_objs)
                
                # este paso se puede mejorar al asociar solo al nuevo individuo!
                # update: no lo creo porque la poblacion cambia cada vez!
                rho     = associate(norm_objs, weights)
                to_keep = self.update_pop(merged_objs, weights, rho) 
                
                chroms = merged_chroms[to_keep, :].copy()
                objs   = merged_objs[to_keep, :].copy()
                # ---- added to amoead -----
                
                
            # @note: we output messages AFTER evaluating child on self.evaluate_pop_mop
            # this way, the value of self.fevals is the expected value
            if self.verbose:            # @todo: add log support here
                if t%1 == 0:
                    print ("t:", t, " fevals:", self.fevals)
            
            
            self.stats(chroms, objs, t, run_id)
            
            t+=1
            
           
        return chroms.copy(), objs.copy()
    """
    
    
    
    
    
