# plot_weights.py
from __future__ import print_function
from rocket.plot.colors import colors, basecolors
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from rocket.plot import load_rcparams
from rocket.fronts import get_real_front
import numpy as np
from rocket.moea.amoead.associate import associate
from rocket.moea.amoead.normalize import norm_matrix
from numpy.random import RandomState

def plot_weights(objs, weights, rho, title, save=False):
    
    fig = plt.figure()
    ax = fig.add_subplot(111, projection="3d")
    ax.set_xlabel("$f_1$")
    ax.set_ylabel("$f_2$")
    ax.set_zlabel("$f_3$")
    
    ax.set_xlim(0, 1.1)
    ax.set_ylim(0, 1.1)
    ax.set_zlim(0, 1.1)
    
    lined = {}
    
    # weights
    l, = ax.plot(weights[:, 0], weights[:, 1], weights[:, 2], mfc="none", label="weights",
                            mec=basecolors["almost_black"], ls="none", marker="o", markeredgewidth=0.15)
    lined["weights"] = l
             
    
    # objs
    l, = ax.plot(objs[:, 0], objs[:, 1], objs[:, 2], label="objs", c=colors[0], 
                        mec=basecolors["almost_black"], ls="none", marker="o", markeredgewidth=0.15)
    lined["objs"] = l

    # plot front
    #X = real_front[:, 0].reshape(p, p)
    #Y = real_front[:, 1].reshape(p, p)
    #Z = real_front[:, 2].reshape(p, p)
    #ax.plot_wireframe(X, Y, Z, rstride=2, cstride=2, color=basecolors["real_front"])
    
    # weights
    vmin, vmax = rho.min(), rho.max()
    for i, w in enumerate(weights):
        x, y, z = w
        label = "%d (%d)" % (i, rho[i])
        #label = "%d" % rho[i]
        
        if rho[i] > 0:
            ax.text(x, y, z, label, color="#404040", size=10)
        
        alpha = (rho[i])/vmax
        
        ax.plot(weights[[i], 0], weights[[i], 1], weights[[i], 2], c="#9E0E42", 
                            mec=basecolors["almost_black"], ls="none", marker="o", markeredgewidth=0.15, alpha=alpha)
      
    
    # objs
    for i, obj in enumerate(objs):
        x, y, z = obj
        label = "%d" % i
        ax.text(x, y, z, label, color="#4D8861", size=10)
        
        
                  
    ax.view_init(15, 45)
    
    leg = ax.legend(loc="lower left")
    
    # for callback
    for legtxt in leg.get_texts():
        legtxt.set_picker(5)
        
    # inner callback
    def on_pick(event):
        
        legtxt, line = None, None
        
        obj = event.artist
        is_callable = callable(getattr(obj, "get_text", None))
        
        if is_callable:
            
            legtxt = event.artist
            line = lined[legtxt.get_text()]
            
        else:
            
            legtxt = None
            line = event.artist
            
        is_visible = line.get_visible()
        
        # fade a little bit
        if is_visible:
            
            line.set_visible(False)
            
            if legtxt is not None:
                legtxt.set_alpha(0.2)
                
        else:
            
            line.set_visible(True)
            
            if legtxt is not None:
                legtxt.set_alpha(None)
                
        fig.canvas.draw()
    
    # register callback
    fig.canvas.mpl_connect("pick_event", on_pick)
    
    
    if save:
        output = "weights.png"
        fig.savefig(output, dpi=300)

    plt.show()

if __name__ == "__main__":
    
    load_rcparams()
    
    # create sample objs
    rand        = RandomState(100)
    objs        = rand.random_sample((10, 3))
    objs[:, 2]  = 1.2 - objs[:, [0, 1]].sum(axis=1)
    to_keep = (objs > 0).all(axis=1)
    objs = objs[to_keep]
    
    objs = np.genfromtxt("sample/dtlz1-spin/amoead-pbi/pops/objs_amoead-pbi_m_3_run_0_t_100.txt")
    
    weights     = np.genfromtxt("weights_m_3.txt")
    
    rho, pie, _   = associate(objs, weights, "pdist")
    plot_weights(objs, weights, rho, "incorrect", save=False)
    
    
    merge       = np.vstack((weights, objs))
    z_ideal     = merge.min(axis=0)
    z_nadir     = merge.max(axis=0)
    norm_objs   = norm_matrix(objs, z_ideal, z_nadir)
    norm_weights= norm_matrix(weights, z_ideal, z_nadir)
    rho_n, pie_n, _   = associate(norm_objs, norm_weights, "pdist")
    plot_weights(norm_objs, norm_weights, rho_n, "correct", save=False)
     
   
   
