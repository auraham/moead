# crowded_comparison.py
from __future__ import print_function
import numpy as np

def crowded_comparison(fitness_a, fitness_b):
    """
    Crowded comparison operator
    
    Both fitness_a and fitness_b contains two elements: rank and crowding_dist
    
    Input
    fitness_a       (2, ) array
    fitness_b       (2, ) array
    
    Output
    result          1 if a is better than b, -1 otherwise
    """
    
    result = -1
    
    rank_a, rank_b = fitness_a[0], fitness_b[0]     # ranks
    dist_a, dist_b = fitness_a[1], fitness_b[1]     # crowding dist
    
    # compare ranks
    if rank_a < rank_b:
        result = 1
    elif (rank_a == rank_b) and (dist_a > dist_b):
        result = 1
        
    return result
