# nsga2.py
from __future__ import print_function
from rocket.moea.moea_base import MOEA
from rocket.genetics import crossover, mutation
from rocket.moea.nsga2 import nsga2_sorting, crowded_comparison
import numpy as np
from numpy.random import RandomState

class NSGA2(MOEA):
    
    def __init__(self, params):
        
        MOEA.__init__(self, params)
        
        
    def rank_population(self, objs):
        """
        Assign ranks and crowding distance (we assume minimization)
        
        Input
        objs        (pop_size, m_objs) objs matrix
        
        Output
        criteria    (pop_size, 2) matrix, col 0: ranks, col 1: crowding distance
        """
        
        indices, fronts, ranks, crowding_dist, criteria = nsga2_sorting(objs)
        
        return criteria.copy()
    
    
    def parent_selection(self, fitness):
        """
        Create a mating pool from fitness matrix
        
        Input
        fitness         (pop_size, 2) ranks and crowding dist matrix
        
        Output
        pool            (pop_size, ) int array
        """
        
        # mating pool
        pool = np.zeros((self.pop_size, ), dtype=int)
        
        # members to compete
        group_a = np.arange(0, self.pop_size)
        group_b = np.arange(0, self.pop_size)
        
        # only shuffle one group
        self.rand.shuffle(group_b)
        
        # binary tournaments
        for i in range(self.pop_size):
            
            # each entry (a and b) contains ranks and crowd dist
            a = fitness[group_a[i]]
            b = fitness[group_b[i]]
        
            winner, loser = None, None
            
            if crowded_comparison(a, b) == 1: # is a better than b according to crowding operator?
                
                # a is better than b
                winner = group_a[i]
                loser  = group_b[i]
                
            else:
                
                # b is better than a
                winner = group_b[i]
                loser  = group_a[i]
        
            # flip
            if self.rand.rand() < 0.9:
                
                # add the winner with a 0.9 chance
                pool[i] = winner
                
            else:
                
                # add the loser with a 0.1 chance
                pool[i] = loser
                
        return pool.copy()
        
    
    def create_child_pop(self, chroms, pool):
        """
        Apply crossover and mutation using chroms and the mating pool
        
        Input
        chroms          (pop_size, n_genes) matrix
        pool            (pop_size, ) int array, matring pool
        
        Output
        child_pop       (pop_size, n_genes) new chroms matrix
        """
        
        new_chroms = np.zeros((self.pop_size, self.n_genes))
        
        # crossover
        for i in range(0, self.pop_size, 2):
            
            # parents
            p1      = chroms[pool[i]] 
            p2      = chroms[pool[i+1]] 
            parents = (p1, p2)
            
            # apply operator
            c1, c2  = crossover(parents, self.lbound, self.ubound, self.params_crossover, self.rand) 

            # save
            new_chroms[i]   = c1.copy()
            new_chroms[i+1] = c2.copy()
            

        # mutation
        for i in range(self.pop_size):
            
            # individual to undergo mutation
            c1      = new_chroms[i].copy()
            parents = (c1, )
            
            # apply operator
            mut,    = mutation(parents, self.lbound, self.ubound, self.params_mutation, self.rand)

            # save
            new_chroms[i] = mut.copy()
            
    
        return new_chroms.copy()
        
    
    def genetic_operations(self, chroms, objs, fitness):
        """
        Create a new pop from parent pop (chroms)
        This function can be replaced for nsga3 if needed
        
        Input
        chroms          (pop_size, n_genes) matrix
        objs            (pop_size, m_objs) matrix
        fitness         (pop_size, 2) ranks and crowding dist matrix
        
        Output
        child_chroms    (pop_size, n_genes) matrix
        """
        
        # parent selection
        mating_pool = self.parent_selection(fitness)
        
        # create child pop
        child_chroms = self.create_child_pop(chroms, mating_pool)
        
        return child_chroms.copy()
        
    
    def update_selection(self, objs):
        """
        Rank (parent and child's) objs and return a vector (to_keep) of sorted indices
        
        Input
        objs        (pop_size*2, m_objs) objs matrix
        
        Output
        to_keep     (pop_size, ) int array with the best pop_size individuals (indices)
        """
        
        indices, fronts, ranks, crowding_dist, criteria = nsga2_sorting(objs)
        
        to_keep = indices[:self.pop_size]
        
        return to_keep.copy()
        
        
        
    def run(self, chroms, seed, run_id):
        """
        Independent run
        """
        
        self.rand   = RandomState(seed)
        
        pop_size    = self.pop_size
        objs        = self.evaluate_pop_mop(chroms)
        
        t = 1
        self.stats(chroms, objs, t, run_id)
        
        # assign ranks and crowding distance
        fitness = self.rank_population(objs)
        
        t = 2
        
        while t <= self.iters:
            
            # genetic operations (parent_selection + create_child_pop)
            child_chroms = self.genetic_operations(chroms, objs, fitness)
    
            # evaluate ranges
            child_chroms = self.check_pop_bounds(child_chroms)
            
            # evaluate child pop
            child_objs = self.evaluate_pop_mop(child_chroms)
            
            # merge pops
            merged_chroms = np.vstack((chroms, child_chroms))
            merged_objs   = np.vstack((objs,   child_objs))
            
            # update selection
            to_keep = self.update_selection(merged_objs)
            
            # new pop
            chroms = merged_chroms[to_keep].copy()
            objs   = merged_objs[to_keep].copy()
            
            # @todo: compute lightweight stats here
            
            # @note: we output messages AFTER evaluating child on self.evaluate_pop_mop
            # this way, the value of self.fevals is the expected value
            if self.verbose:            # @todo: add log support here
                if t%100 == 0:
                    print ("t:", t, " fevals:", self.fevals)
            
            # stats
            self.stats(chroms, objs, t, run_id)
            
            # next iter
            t += 1
            
        # @todo: save_stats?
            
        return chroms.copy(), objs.copy()

        
        
