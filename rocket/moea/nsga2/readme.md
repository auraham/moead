# About this package

This package contains the implementation of NSGA-II [Deb02]

# Example

This script allow us to run NSGA-II using a population with `pop_size=100` individuals for `iters=400` generations. The test problem is `dtlz1`. The MOEA will be executed `runs=2` times independently. After execution, a plot will appear with the final populations, one for each independent run.

```python
# test_moea_config.py
from __future__ import print_function
from rocket.moea import NSGA2
from rocket.helpers import get_n_genes, get_bounds
from rocket.labs import get_experiment_path
import numpy as np
import os

# define experiment output path
script_path = os.path.dirname(os.path.abspath(__file__))
#output_path = get_experiment_path(script_path, name=None)
output_path = get_experiment_path(script_path, name="sample")

    
if __name__ == "__main__":
    
    params = {
        "name"              : "nsga2",                                              # name of moea
        "label"             : "NSGA-II",                                            # label of moea
        "runs"              : 2,                                                    # number of independent runs
        "iters"             : 400,                                                  # number of generations
        "m_objs"            : 2,                                                    # number of objectives
        "pop_size"          : 100,                                                  # number of individuals
        "params_mop"        : {"name": "dtlz1"},                                    # mop parameters
        "params_crossover"  : {"name": "sbx", "prob": 1.0, "eta": 20},              # crossover parameters
        "params_mutation"   : {"name": "polymutation", "prob": 1./6, "eta": 15},    # mutation parameters
        "params_stats"      : {"output_path": output_path},                         # stats parameters
        "verbose"           : True,                                                 # log verbosity
    }    

    moea = NSGA2(params)
    chroms, objs = moea.evaluate()
    
    

```

![fronts.png](fronts.png)


# References

- [Deb02] *A fast and elitist multiobjective genetic algorithm: NSGA-II*.