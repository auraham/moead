# cda.py
from __future__ import print_function
import numpy as np
eps = 1.0e-20

def cda(objs):
    """
    Crowding distance assigment
    
    Input
    objs        (pop_size, m_objs) objs matrix
    
    Output
    dist        (pop_size, ) vector
    """
    
    pop_size, m_objs = objs.shape       # shapes
    dist = np.zeros((pop_size, ))       # init crowding distance
    
    for m in range(m_objs):
        
        # sort by m-th objective (lesser to greater)
        indices = np.argsort(objs[:, m])    

        id_first = indices[0]
        id_last  = indices[-1]

        # keep both extremes
        dist[id_first] = 10000              
        dist[id_last]  = 10000

        # min, max m-th objective values
        # @todo: maybe axis parameter can be ommited
        f_min = objs[:, m].min(axis=0)
        f_max = objs[:, m].max(axis=0)
        
        # added to avoid zero-division
        if f_min == f_max:
            f_max = f_max + eps
            
        # note that we skip 0 and pop_size-1 indices in this loop
        for i in range(1, pop_size-1):
            
            curr_i = indices[i]
            prev_i = indices[i-1]
            next_i = indices[i+1]
            
            prev_obj = objs[prev_i, m]
            next_obj = objs[next_i, m]
            
            # crowing distance
            # note that this distance is accumulative
            dist[curr_i] += (next_obj - prev_obj) / (f_max - f_min)
            
            
    return dist.copy()
    
    
if __name__ == "__main__":
    
    objs = np.array([
        [0.31, 6.10],
        [0.43, 6.79],
        [0.22, 7.09],
        [0.59, 7.85],
        [0.66, 3.65],
        [0.83, 4.23],
        [0.21, 5.90],
        [0.79, 3.97],
        [0.51, 6.51],
        [0.27, 6.93],
        [0.58, 4.52],
        [0.24, 8.54]])
        
    front_1 = np.array([4, 6, 10])
    front_2 = np.array([0, 2, 7, 9])
    
    dist0 = cda(objs)
    dist1 = cda(objs[front_1])
    dist2 = cda(objs[front_2])
        
