# __init__.py
from .nsga2_sorting import nsga2_sorting
from .crowded_comparison import crowded_comparison
from .cda import cda
