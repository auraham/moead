# nsga2_sorting.py
from __future__ import print_function
from rocket.moea.nsga2.cda import cda               # crowding distance assignment
from rocket.moea.nds import fnds                    # fast non dominanted sorting
from rocket.moea.dominance import compare_min       # pareto dominance comparison
import numpy as np

def nsga2_sorting(objs, compare=compare_min):
    """
    Fast non dominated sorting + crowding distance assigment shortcut
    
    Input
    objs        (pop_size, m_objs) objs matrix
    compare     dominance comparison function (we assume minimization)
    
    Output
    indices
    fronts
    ranks
    crowding_dist
    criteria
    """
    
    # get ranks
    # the rank of the individual x denotes which front the solution belongs to
    fronts, ranks = fnds(objs, compare)
    
    # get crowding distance
    # this matrix is useful for crowded_comparison.py
    crowding_dist = cda(objs)
    
    # matrix of criteria
    criteria = np.vstack((ranks, crowding_dist)).T
    
    # sorted indices
    # we sort solutions (rows) by ranks and then by crowding_dist, in this order
    # note that we multiply by -1 to sort both criteria (columns) assuming minimization (lower values are preferred)
    indices = np.lexsort((crowding_dist*-1, ranks))
    
    return indices, fronts, ranks, crowding_dist, criteria
    
if __name__ == "__main__":
    
    objs = np.array([
        [90, 5],
        [45, 65],
        [96, 30],
        [35, 90],
        [40, 30],
        [98, 40],
        [10, 90],
        [20, 20],
        [70, 15],
        [65, 35],
        [50, 60],
        [30, 50]])
        
    indices, fronts, ranks, crowding_dist, criteria = nsga2_sorting(objs)


