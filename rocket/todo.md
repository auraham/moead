# TODO list

- Copiar `problems` (solo falta descargar wfg y probarlo)
- Copiar `helpers` (listo)
- Copiar `fronts` (listo)
- Copiar `plot` (listo)
- Copiar `genetics`
- Copiar `performance`
- Copiar  `moea`
- Revisar el estado de los siguientes paquetes
```
examples  fronts  genetics  helpers  init.py  moea  performance  plot  problems  readme.md  weights
```
-  Pasar `nsga2/3`, `moea` de `mopy` o de `pymoea`

# Optimization
- Use `prospector` to sanitize our code
- `weights` (creo que mejor creamos un scripts `create_weights.py` según sea el caso por cada experimento)
- Continua con `performance` (mejor no)
- Comprobar que el frente de `"dtlz,zdt"` no cambia de forma a medida que aumenta el número de variables.
- Compara `moead.py` de `rocket` con `emo_research`, aun no está bien hecho.




# Features to add

- Add an option `normalization` before calling functions that need `objs` matrix as input, for instance  `evaluate_form` of `moead`:
```python
# moead.py

def evaluate_pop_scalar_method(self, objs, weights):

    z_ideal = self.z_ideal.flatten()
    z_nadir = self.z_nadir.flatten()

    input_objs   = objs.copy()            # objs to be evaluated
    flag_reshape = False                    

    # if objs contains a single obj vector
    # we need to reshape it before calling form
    if objs.ndim == 1:
        flag_reshape = True

        rows = weights.shape[0]
        cols = len(objs)

        input_objs = input_objs.reshape((1, cols)).repeat(rows, axis=0)

        # evaluate input_objs on form
        scalar = evaluate_form(input_objs, weights, z_ideal, z_nadir, self.params_form)
        
        return scalar.copy().flatten()
    
    
# ...
norm_objs = normalize(objs, params={"name": "basic_normalization"}) or
norm_objs = normalize(objs, z_ideal, z_nadir, name=self.params_normalization["name"]) or

self.evaluate_pop_scalar_method(norm_objs, weights)


def normalize(objs, z_ideal, z_nadir, name):
    """
    Returns a normalized objs matrix
    """
    
    name = params["name"]
    
    # default
    if name == "none":
    	return objs.copy()
    
    # basic
    if name == "basic":
        # la basica de moead
        
       
    if name == "adaptive":
        # la adaptativa tipo nsga-iii o delta-DEA

```



- If a MOEA requires a specific crossover/mutation operator:

  - Implement such operator on `rocket.genetic_operations`

  - Use a default value for `self.params_crossover` within `__init__`, such as:

    ```python

    class MOEAD_GRA(MOEAD):
        
        def __init__(self, params):
            
            MOEAD.__init__(self, params)
            
            #self.params_crossover   = params["params_crossover"]
            #self.params_mutation    = params["params_mutation"]
            
            self.params_crossover   = params.get("params_crossover", {"name":"moead-gra_crossover", "prob": 0.9})
            self.params_mutation    = params.get("params_mutation", {"name":"moead-gra_mutation", "prob": 0.1})
            
            

    ```

    

- Move `wfg.py` to `rocket.problems.wfg.frontend.py`

- Add a logger with highlighted output messages

- 

- Add `plot_nds` en `rocket.plot` para graficar los frentes de una matriz `objs` 2-dimensional

- Add `get_hv_ref_point(problem_name, m_objs)` in `helpers`.

- Add `get_iters(mop_name, m_objs)` in `helpers`.

- Add real fronts of `ZDT`, `WFG`
  
- Save `z_ideal` and `z_nadir` after finishing an independent run on `stats`
  
- Add  a frontend for formulations on `rocket.forms`, something like
```
# evaluate_form.py


def evaluate_form(objs, weights, z_ideal, z_nadir, params={}):

	name = params["name"]
	
	reshape z_ideal, z_nadir here
	check the shape of objs and weights here?
	
	if name == "pbi":
		theta = params.get("theta", 5)
		scalar = fast_pbi_matrix(objs, weights, z_ideal, z_nadir, theta)
		

continua con esta idea

```

- Check this idea:
  - Create a new fork of `rocket` for each new experiment (paper).
  - Create a `run_experiment_NAME.py` script for each experiment. This script must be able to add `ROCKET_PATH` to `PYTHON_PATH` automatically without adding it in `.local`. This way, the fork is an independent code that need no installation.
- `vads.py`

```
# todo: normalizar weights para que sean unitarios
# vads_matlab no lo hace porque asume que los weights son unitarios (eso creo)
```






# Frentes

Los frentes de `dtlz-spin` **no son correctos**, por eso debo quitarlos del framework.

# Thesis

- I was thinking about a modification to our current hypothesis, something like *we want to evaluate our proposal on ... and inverted problems. *
- Tendríamos que hablar sobre la relevancia de (y dificultades que presentan los) nuevos problemas de prueba. De ahi mencionar que *creemos que el moea propuesto es mejor que (1) los moeas actuales en ambos problemas y (2) es mejor en ambos tipos de problemas*, aunque el punto (2) me parece ahora redundante.