# experiments.py
from __future__ import print_function
import os
from distutils.dir_util import mkpath
import datetime

def get_experiment_path(main_path, name=None):
    """
    Return a default experiment name using the current date time.
    """

    now = datetime.datetime.now()
    
    date = now.strftime("%Y-%m-%d-%H:%M:%S")
    
    if name is None:
        name = "experiment_%s" % date
    
    experiment_path = os.path.join(main_path, name)
    
    return experiment_path

def prepare_experiment_dirs(experiment_path, mop_name, moea_name):
    """
    Create a structure of directories to store the results of a experiment
    """
    
    # @todo: check whether experiment_path is a valid path
    
    print("experiment path: %s" % experiment_path)              # @todo: add log support here
    
    paths = {
        "path_pops"  : os.path.join(experiment_path, mop_name, moea_name, "pops"),
        "path_stats" : os.path.join(experiment_path, mop_name, moea_name, "stats"),
        "path_debug" : os.path.join(experiment_path, mop_name, moea_name, "debug"),
        "path_temp"  : os.path.join(experiment_path, mop_name, moea_name, "temp"),
    }

    for v in paths.values():
        mkpath(v)
        
    return paths

def get_mop_configs(runs, step, budget):
    """
    Return a dict like this
    
    mop_configs = {
        
            "dtlz1" : {
                
                "runs": 31, 
            
                "budget" : (
                            (3, 300),                           # budget for m_objs=3 objectives
                            (4, 400),                           # budget for m_objs=4 objectives
                            ),
                            
                "methods": []
            }
    
    mop_configs is initialized using runs (integer) and budget (dict), where budget is like this
    
    budget = {          # m_objs: iters (generations)
        "dtlz1"         : {3: 300, 4: 500, 5: 600},
        "inv-dtlz1"     : {3: 400, 4: 600, 6: 700},
    }
    
    Input
    runs            int, number of independent runs
    budget          dict, number of generations for each mop instance
    
    Output
    mop_configs     dict, configuration for each mop
    """
    
    mop_configs = {}
    
    for mop_name, instances in budget.items():
        
        mop_configs[mop_name] = {
            "runs"      : runs,
            "step"      : step,
            "budget"    : { m_objs:iters for m_objs, iters in budget[mop_name].items() },
            "methods"   : [],
        }
        
    return mop_configs
    
def create_experiment_index(experiment_path, mop_configs, label=""):
    """
    Create an index.txt file containing the 
        (1) mop names of the problems evaluated during experimentation
        (2) their budget (number of objectives, number of iterations)
        (3) the list of algorithms evaluated
        
    mop_config should look like this example:
    
    mop_configs = {
        
            "dtlz1" : {
                
                "runs": 31, 
            
                "budget" : (
                            (3, 300),                           # budget for m_objs=3 objectives
                            (4, 400),                           # budget for m_objs=4 objectives
                            ),
                            
                "methods": ("amoead-asf-2", "amoead-asf-4", )
            }
            
            "inv-dtlz1" : {
            
                "runs": 31, 
            
                "budget" : (
                            (3, 600),                           # budget for m_objs=3 objectives
                            (4, 800),                           # budget for m_objs=4 objectives
                            ),
                            
                "methods": ("amoead-asf-2", "amoead-asf-4", )
            }
        }
    
    Input
    experiment_path     str, path of the experiment generated by get_experiment_path()
    mop_configs         dictionary of mop configurations
    """
    
    filename = "index%s.txt" % label
    filepath = os.path.join(experiment_path, filename)
    
    # if filepath exists, add a suffix
    if os.path.isfile(filepath):
    
        now = datetime.datetime.now()
        date = now.strftime("%Y-%m-%d-%H:%M:%S")
        
        filename = "index_%s%s.txt" % (label, date)
        filepath = os.path.join(experiment_path, filename)
    
    with open(filepath, "w") as log:
        
        # comment
        comment = "# mop_name ; runs ; m_objs ; iters ; step ; moea_name\n"
        log.write(comment)
        
        for mop_name in sorted(mop_configs.keys()):
            
            runs    = mop_configs[mop_name]["runs"]
            step    = mop_configs[mop_name]["step"]
            budget  = mop_configs[mop_name]["budget"]
            methods = sorted(mop_configs[mop_name]["methods"])
            
            # check for duplicates
            unique_methods = frozenset(methods)
            if len(unique_methods) != len(methods):
                print("There is an error of methods for %s, m_objs: %d. There may be a repeated element. Index file was not created")
                return
            
            # traverse in ascending order
            for m_objs in sorted(budget.keys()):
            
                iters = budget[m_objs]
                
                for moea_name in methods:
                
                    line = "%s;%d;%d;%d;%d;%s\n" % (mop_name, runs, m_objs, iters, step, moea_name)
                
                    log.write(line)
                
                
    print("index file (%s): %s" % (filename, filepath))

def filter_budget(budget, test_mop_name, test_m_objs):
    
    new_budget = {}
    ok = False
    
    for mop_name, instances in budget.items():
        
        for m_objs in sorted(instances.keys()):
            
            iters = budget[mop_name][m_objs]
            
            if test_m_objs == m_objs and test_mop_name == mop_name:
                
                ok = True
                new_budget[mop_name] = {m_objs:iters}
    
    if not ok:
        print("check budget config, instance not found (mop_name: %s, m_objs: %d)" % (test_mop_name, m_objs))
        sys.exit(0)
        
    return new_budget
