# default_form_params.py
from __future__ import print_function
import numpy as np

def parse_form_name(form, m_obj):
    """
    Return a dict with the configuration of form, where form has this pattern:
    
    form = name-value
    
    value is casted to float and used for configuration
    
    Input
    form            str, form name
    m_objs          int, number of objectives
    
    Output
    config          dict, configuration for form
    """
    
    parts = form.split("-")             # pbi-5
    form_name  = parts[0]               # pbi
    form_param = float(parts[1])        # 5
    
    params = {"name": form_name}
    
    if form_name == "pbi":
        params["pbi-theta"] = form_param
    
    elif form_name == "ipbi":
        params["ipbi-theta"] = form_param
        params["ipbi-negative"] = True
        
    elif form_name == "vads":
        params["vads-q"] = form_param
        
    else:
        print("there is no default config for form: %s" % form)
        return None
        
    return {"params_form": params}
    
    
    

def get_default_form_params(form, m_objs):
    """
    Return a dict with the default configuration of form
    
    Input
    form            str, form name
    m_objs          int, number of objectives
    
    Output
    config          dict, configuration for form
    """
    
    # check for slash
    if form.count("-") > 0:
        return parse_form_name(form, m_objs)
    
    defaults = {
        "ws"    : {"name": "ws"},
        "te"    : {"name": "te"},
        "pbi"   : {"name": "pbi",  "pbi-theta": 5},
        "ipbi"  : {"name": "ipbi",  "ipbi-theta": 5, "ipbi-negative": True},    # this option must be true when addressing minimization problems
        "asf"   : {"name": "asf", "asf-z_ref":np.zeros((m_objs, ))},
        "vads"  : {"name": "vads", "vads-q": 100},
        "wpo"   : {"name": "wpo", "wpo-p": 3},
        "wn"    : {"name": "wn", "wn-p": 0.5},
        "aasf"  : {"name": "aasf", "aasf-alpha": 0.0001},
        "d1"    : {"name": "d1"},
    }
    
    
    if not form in defaults:
        print("there is no default config for form: %s" % form)
        return None
        
    
    return {"params_form": defaults[form]}
