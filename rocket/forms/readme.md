# About this package

This package contains a frontend for evaluating formulations (or *form* for short): 
```
evaluate_form(objs, weights, z_ideal, z_nadir, params)
```

where:

- `objs` is a `(pop_size, m_objs)` objective matrix.
- `weights` is a `(pop_size, m_objs)` weight matrix.
- `z_ideal` is a `(m_objs, )` reference vector.
- `z_nadir` is a `(m_objs, )` reference vector.
- `params` is a dictionary with additional parameters.

Some of these arguments depends on the formulation. 

# Package structure

This package has the following structure:

```
forms
    scalar				 (1)
        pbi.py           (2)
        ipbi.py
        unit_test.py     (3)
    
    relaxed_dominance    (4)
        TODO
    
    obj_reduction        (4)
        TODO
        
    multiobjectivization (4)
        TODO
```

where:

1. `scalar` contains scalarizing functions, such as PBI[ref] and IPBI [ref].
2. `ipbi.py` and `ipbi.py` contains the implementation of PBI and IPBI, respectively. More scalarizing functions will be added.
3. `unit_test.py` allows us to test the implementation of the scalarizing functions.
4. The packages `relaxed_dominance`, `obj_reduction`, and `multiobjectivization` will contain implementations of other reformulation techniques.

# How to add new forms?

1. Add a new package in `rocket.forms`.
2. Add a new module in the recently created package.
3. Add a new entry into the `evaluate_form` function.

**Example** 



# TODO

- Add `test_evaluate_form` example in this readme
- Add a how-to-add-new-forms example



