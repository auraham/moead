# aasf.py
from __future__ import print_function
import numpy as np
from rocket.helpers import replace_zero_weigths

def fast_aasf_matrix(objs, weights, rho):
    """
    Augmented Achievement Scalarizing Function
    
    Input
    objs        (pop_size, m_objs) array, obj matrix
    weights     (pop_size, m_objs) array, weights matrix
    rho         float factor
    
    Output
    result      (pop_size, 1) array, scalars
    """
    
    # reshape z_ref
    pop_size, m_objs = objs.shape
    by_row  = 1 
    
    # replace zero weights
    Wt = replace_zero_weigths(weights)
    
    # compute aasf
    W    = (1.0 / Wt)           # reciprocal
    divs = (W*objs)             # objs / weights
    
    R = divs.max(axis=by_row) + (rho * divs.sum(axis=by_row))
    
    R = R.reshape((R.shape[0], 1))      # (pop_size, 1), reshape
    
    return R.copy()
    
    
    
def slow_aasf_matrix(objs, weights, rho):
    """
    Augmented Achievement Scalarizing Function
    
    Input
    objs        (pop_size, m_objs) array, obj matrix
    weights     (pop_size, m_objs) array, weights matrix
    rho         float factor
    
    Output
    result      (pop_size, 1) array, scalars
    """
    
    pop_size, m_objs = objs.shape
    
    R = np.zeros((pop_size, 1))
    
    for i in range(pop_size):
        
        R[i, 0] = aasf(objs[i], weights[i], rho)
        
    return R.copy()
    
    
def aasf(objs, weights, rho=0.0001):
    """
    Augmented Achievement Scalarizing Function
    
    Input
    objs        (m_objs, ) array, obj vector
    weights     (m_objs, ) array, weights vector
    rho         float factor
    
    Output
    result      scalar
    """
    
    wt = replace_zero_weigths(weights) 
    
    divs    = objs / wt                         # (m_objs, ) element-wise division
    
    result  = divs.max() + (rho * divs.sum())   # scalar 
    
    return result
    
    
if __name__ == "__main__":
    
    w = np.array([0.7, 0.3])
    
    objs = np.array([.2, .4])
    
    result = aasf(objs, w)
