# check_vads.py
from __future__ import print_function
import numpy as np
from vads import vads, vads_matlab
from numpy.linalg import norm

def pretty(a):
    
    for i, item in enumerate(a):
        print("%d: %.5f" % (i, item))

if __name__ == "__main__":
    
    weights = np.genfromtxt("weights_m_2.txt")
    norm_weights = np.genfromtxt("norm_weights_m_2.txt")
    objs = np.genfromtxt("objs.txt")
    
    pop_size = objs.shape[0]
    resa = np.zeros((pop_size, ))
    resb = np.zeros((pop_size, ))
    
    for i in range(pop_size):
        resa[i] = vads_matlab(objs[i], norm_weights[i], 100)
        resb[i] = vads(objs[i], weights[i], 100)

    print("matlab - norm_weights")
    pretty(resa)
    
    print("python - weights")
    pretty(resb)
    
    
    #unit_weights = weights / norm(weights,axis=1).reshape((pop_size, 1))
    #np.savetxt("norm_weights_m_2.txt", unit_weights, fmt="%.10f")
    
