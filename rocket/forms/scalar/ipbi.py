# ipbi.py
from __future__ import print_function
import numpy as np
from numpy.linalg import norm

def fast_ipbi_matrix(objs, weights, z_ideal, z_nadir, theta):
    
    pop_size, m_objs = objs.shape
    n_weights = weights.shape[0]
    
    if pop_size != n_weights:
        print("objs and weights do not have the same number of rows")       # @todo: add log support here
        return None
        
    # temp matrices
    Z_ideal = z_ideal.reshape((1, m_objs)).repeat(pop_size, 0)
    Z_nadir = z_nadir.reshape((1, m_objs)).repeat(pop_size, 0)

    per_row = 1
    norm_weights = norm(weights, axis=per_row)                                              # (pop_size, ), para no calcularlo dos veces en d1 y d2
    
    # d1
    dot = ((Z_nadir - objs) * weights).sum(axis=per_row)                                    # (pop_size, ) dot por cada row
    d1  = norm(dot.reshape(pop_size, 1), axis=per_row) / norm_weights                       # (pop_size, ) = (pop_size, ) / (pop_size, ), una d1 por cada row
    
    
    # d2
    unit_weights = weights / norm_weights.reshape(pop_size, 1)                              # (pop_size, m_objs), el reshape a (pop_size, 1) es necesario para hacer la division por fila
                                                                                            # norm(unit_weights, axis=1) es [1, 1, ..., 1]
    d2 = norm((Z_nadir - objs) - (d1.reshape(pop_size, 1) * unit_weights), axis=per_row)    # (pop_size, )
    
    r  = d1 - theta*d2
    
    # reshape solo para evitar algun problema
    r = r.reshape((r.shape[0], 1))
    
    return r.copy()
    
    
def ipbi(objs, weights, z_ideal, z_nadir, theta):
    
    d1 = norm(np.dot((z_nadir - objs), weights)) / norm(weights)
    
    d2 = norm((z_nadir - objs) - d1 * (weights/norm(weights)))
    
    r  = d1 - theta*d2
    
    return r, d1, d2
    
def slow_ipbi_matrix(objs, weights, z_ideal, z_nadir, theta):
    
    pop_size  = objs.shape[0]
    n_weights = weights.shape[0]
    
    if pop_size != n_weights:
        print("objs and weights do not have the same number of rows")       # @todo: add log support here
        return None
        
    result = np.zeros((pop_size, ))
    d1 = np.zeros((pop_size, ))
    d2 = np.zeros((pop_size, ))
    
    for i in range(pop_size):
        
        u = objs[i]
        w = weights[i]
        
        result[i], d1[i], d2[i] = ipbi(u, w, z_ideal, z_nadir, theta)
        
    return result.copy()
