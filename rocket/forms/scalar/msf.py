# msf.py
from __future__ import print_function
import numpy as np
from rocket.helpers import replace_zero_weigths

# uncomment to start ipdb after an exception is raised
#np.seterr(all='raise')

def fast_msf_matrix(objs, weights, z_ref, alpha):
    """
    Multiplicative Scalarizing Function
    
    Input
    objs        (pop_size, m_objs) array, obj matrix
    weights     (pop_size, m_objs) array, weights matrix
    z_ref       (pop_size, m_objs) array, reference vector
    alpha       int, scaling factor, controls the size of the improvement area.
                As the value of alpha grows, the size of the improvement area shrinks.
    
    Output
    result      (pop_size, 1) array, scalars
    """
    
    eps = 2.2204e-16                            # we use the same eps as vads does
    
    # reshape z_ref
    pop_size, m_objs = objs.shape
    by_row  = 1 
    Z_ref   = z_ref.reshape((1, m_objs)).repeat(pop_size, 0)    # creo que la resta seria la misma sin este reshape
    
    # replace zero weights
    Wt = replace_zero_weigths(weights, 0.0001)                  # replace zero-weigths by 0.0001 as recommended in [Jiang17]
    
    # compute msf
    T = np.abs(objs - z_ref)            # translate
    W = (1.0 / Wt)                      # reciprocal
    P = (W*T)                           # product
    T = P.max(axis=by_row)              # top, max element per row      (pop_size, )
    B = P.min(axis=by_row)              # bottom, min element per row   (pop_size, )
    
    B = replace_zero_weigths(B, 0.00001)                        # replace zero by 0.00001 before division as recommended in [Jiang17]
    
    R = (T**(1+alpha)) / ((B**alpha) + eps)                     # final division                (pop_size, )
    
    R = R.reshape((R.shape[0], 1))      # (pop_size, 1), reshape
    
    return R.copy()
    
    
def slow_msf_matrix(objs, weights, z_ref, alpha):
    """
    Multiplicative Scalarizing Function    
    
    Input
    objs        (pop_size, m_objs) array, obj matrix
    weights     (pop_size, m_objs) array, weights matrix
    z_ref       (pop_size, m_objs) array, reference vector
    alpha       int, scaling factor, controls the size of the improvement area.
                As the value of alpha grows, the size of the improvement area shrinks.
    
    Output
    result      (pop_size, 1) array, scalars
    """
    
    pop_size, m_objs = objs.shape
    
    R = np.zeros((pop_size, 1))
    
    for i in range(pop_size):
        
        R[i, 0] = msf(objs[i], weights[i], z_ref, alpha)
        
    return R.copy()
    
  
def msf(objs, weights, z_ref, alpha):
    """
    Multiplicative Scalarizing Function
    
    Input
    objs        (m_objs, ) array, obj vector
    weights     (m_objs, ) array, weights vector
    z_ref       (m_objs, ) array, reference vector
    alpha       int, scaling factor, controls the size of the improvement area.
                As the value of alpha grows, the size of the improvement area shrinks.
    
    Output
    result      scalar
    """
    
    eps = 2.2204e-16                            # we use the same eps as vads does
    
    wt = replace_zero_weigths(weights, 0.0001)  # replace zero-weigths by 0.0001 as recommended in [Jiang17] 
    
    trans   = (np.abs(objs - z_ref))            # (m_objs, ) absolute difference

    divs    = trans / wt                        # (m_objs, ) element-wise division
    
    num = divs.max(axis=0)                      # numerator
    div = divs.min(axis=0)                      # divisor
    
    if div == 0:
        div = 0.00001                           # replace zero by 0.00001 before division as recommended in [Jiang17]
                                                # if div == 0 and alpha is negative
                                                # then, div**a will raise an exception
    
    result  = (num**(1+alpha)) / ((div**alpha) + eps)   # scalar, we keep eps just for convention

    return result
    
