# ws.py
from __future__ import print_function
import numpy as np
from rocket.helpers import replace_zero_weigths

def fast_ws_matrix(objs, weights, z_ideal, z_nadir):
    
    pop_size, m_objs = objs.shape
    
    # convention
    W = weights
    R = z_ideal.reshape((1, m_objs)).repeat(pop_size, 0)
    F = objs
    
    r = (W * (F)).sum(axis=1)       # era max, lo cambie a sum
    
    # reshape solo para evitar algun problema
    r = r.reshape((r.shape[0], 1))
    
    return r.copy()
    
def ws(objs, weights):
    
    wt = replace_zero_weigths(weights) 
    
    r = (objs * wt).sum()
    
    return r
    
    
