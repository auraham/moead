# About this package

This package contains the following scalarizing methods:

- PBI (Penalty-based Boundary Intersection [Zhang07]) `pbi.py`
- IPBI (Inverted PBI [Zhang07]) `ipbi.py`
- ASF (Achievement Scalarazing Function [Hernandez15]) `asf.py`

These functions are intended to be used through `evaluate_form.py`.

# Contour plots

You can run the following scripts to visualize some contour plots in order to get a better insight of these scalarizing methods:

- `contours_pbi_ipbi.py`
- `contours_asf.py`


# ASF contours

The following figure shows the contour plot of ASF with three different weight vectors $\mathbf{w}$: `[0.2, 0.8]`,` [0.5, 0.5]`, and `[0.8, 0.2]` (these vectors are depicted with a black line). The reference vector $\mathbf{z}^{ref} = [0, 0]$ us the same in the three plots:

![ASF](asf.png)

These figures show the ... when the reference vector is incorrectly defined.

![ASF extended error](asf_extended_error.png)

These figures show the ... when the reference vector is correctly defined (is it true?).

![ASF extended](asf_extended.png)





# PBI and IPBI contours

The following figures show the contour plot of PBI and IPBI with three different weight vectors $\mathbf{w}$: `[0.2, 0.8]`,` [0.5, 0.5]`, and `[0.8, 0.2]` (these vectors are depicted with a black line). The ideal vector $\mathbf{z}^{*} = [0, 0]$ is used as reference vector:



![PBI](pbi.png)

![IPBI](ipbi.png)

These figures show the ... when the reference vectors are incorrectly defined.

![PBI extended error](pbi_extended_error.png)

![IPBI extended error](ipbi_extended_error.png)

These figures show the ... when the reference vectors are correctly defined.

![PBI extended](pbi_extended.png)

![IPBI extended](ipbi_extended.png)



# TODO

- Add documentation from `experiments_pymoea/ipbi`
- Add `geo_meaning.py`
- Remove `z_nadir` from `i/pbi.py`




# Weigths

We use this helper to replace zero-weights from a weights matrix as follows:

```python
def replace_zero_weigths(weights, eps=0.00001):
    
    W           = weights.copy()
    to_keep     = weights == 0
    W[to_keep]  = 0.00001        # recommended value [Ishibuchi16]
    
    return W.copy()
```

Note that this helper function works either `weights` is an one-dimensional array or a two-dimensional array.





# References

- [Zhang07] *MOEA/D: A Multiobjective Evolutionary Algorithm Based on Decomposition*.
- [Hernandez15] *Improved Metaheuristic Based on the R2 Indicator for Many-Objective Optimization*.
- [Ishibuchi16] *Performance of Decomposition-Based Many-Objective Algorithms Strongly Depends on Pareto Front Shapes*.