# unit_test.py
from __future__ import print_function
import numpy as np
from asf  import slow_asf_matrix,  fast_asf_matrix
from msf  import slow_msf_matrix,  fast_msf_matrix
from psf  import slow_psf_matrix,  fast_psf_matrix
from pbi  import slow_pbi_matrix,  fast_pbi_matrix
from ipbi import slow_ipbi_matrix, fast_ipbi_matrix
from vads import slow_vads_matrix, fast_vads_matrix
import unittest


objs = np.array([
    [0.1, 0.2],
    [0.2, 0.3],
    [0.5, 0.2],
    [0.4, 0.1],
    [0.3, 0.2],
    ])


weights = np.array([
    [0.00, 1.00],
    [0.25, 0.75],
    [0.50, 0.50],
    [0.75, 0.25],
    [0.00, 1.00],
    ])

z_ideal = np.array([0., 0.])
z_nadir = np.array([1., 1.])

objs = objs.repeat(10, axis=0)
pop_size = objs.shape[0]

span = np.linspace(0, 1, pop_size)
weights = np.zeros((pop_size, 2))

weights[:, 0] = span.copy()
weights[:, 1] = span[::-1].copy()

class Test(unittest.TestCase):
    
    def test_functions(self):
        
        # (slow_and_correct, fast)
        functions = (
            (slow_pbi_matrix, fast_pbi_matrix),
            (slow_ipbi_matrix, fast_ipbi_matrix),
            )
            
        for slow, fast in functions:
            
            a = slow(objs, weights, z_ideal, z_nadir, theta=1).flatten()
            b = fast(objs, weights, z_ideal, z_nadir, theta=1).flatten()
            
            for i in range(pop_size):
            
                self.assertAlmostEqual(a[i], b[i])
        
        # (slow_and_correct, fast)
        functions = (
            (slow_asf_matrix, fast_asf_matrix),
            )
            
        for slow, fast in functions:
            
            a = slow(objs, weights, z_ideal).flatten()
            b = fast(objs, weights, z_ideal).flatten()
            
            for i in range(pop_size):
            
                self.assertAlmostEqual(a[i], b[i])
        
        
        # (slow_and_correct, fast)
        functions = (
            (slow_msf_matrix, fast_msf_matrix),
            (slow_psf_matrix, fast_psf_matrix),
            )
            
        for slow, fast in functions:
            
            a = slow(objs, weights, z_ideal, alpha=0.5).flatten()
            b = fast(objs, weights, z_ideal, alpha=0.5).flatten()
            
            for i in range(pop_size):
            
                self.assertAlmostEqual(a[i], b[i])
        
        
        # (slow_and_correct, fast)
        functions = (
            (slow_vads_matrix, fast_vads_matrix),
            )
            
        for slow, fast in functions:
            
            a = slow(objs, weights).flatten()
            b = fast(objs, weights).flatten()
            
            for i in range(pop_size):
            
                self.assertAlmostEqual(a[i], b[i])
        
        
if __name__ == "__main__":
    
    unittest.main()
    
    
    
    
    
    
    
    


