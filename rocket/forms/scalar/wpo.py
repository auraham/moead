# wpo.py
from __future__ import print_function
import numpy as np
from rocket.helpers import replace_zero_weigths

def fast_wpo_matrix(objs, weights, p):
    """
    Weighted Power
    
    Input
    objs        (pop_size, m_objs) array, obj matrix
    weights     (pop_size, m_objs) array, weights matrix
    p           float, power
    
    Output
    result      (pop_size, 1) array, scalars
    """
    
    # reshape z_ref
    pop_size, m_objs = objs.shape
    by_row  = 1 
    
    # replace zero weights
    Wt = replace_zero_weigths(weights)
    
    # compute wpo
    W = (1.0 / Wt)                      # reciprocal
    divs = (objs**p) * W                # (objs**p) / wt
    R = divs.sum(axis=by_row)           # result, sum per row
    
    R = R.reshape((R.shape[0], 1))      # (pop_size, 1), reshape
    
    return R.copy()
    
    
    
def slow_wpo_matrix(objs, weights, p):
    """
    Weighted Power
    
    Input
    objs        (pop_size, m_objs) array, obj matrix
    weights     (pop_size, m_objs) array, weights matrix
    p           float, power
    
    Output
    result      (pop_size, 1) array, scalars
    """
    
    pop_size, m_objs = objs.shape
    
    R = np.zeros((pop_size, 1))
    
    for i in range(pop_size):
        
        R[i, 0] = wpo(objs[i], weights[i], p)
        
    return R.copy()
    
    
def wpo(objs, weights, p=3):
    """
    Weighted Power
    
    Input
    objs        (m_objs, ) array, obj vector
    weights     (m_objs, ) array, weights vector
    p           float, power
    
    Output
    result      scalar
    """
    
    wt = replace_zero_weigths(weights) 
    
    divs    = (objs**p) / wt    # (m_objs, ) element-wise division
    
    result  = divs.sum()        # scalar 
    
    return result
    
