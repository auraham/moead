# VADS



## Matlab

```matlab
% ~Dropbox/trixie/hughes/web-code/check_vads.m

weights = dlmread('weights_m_2.txt', ' ');
norm_weights = dlmread('norm_weights_m_2.txt', ' ');    % as weights, but each vector is a unit-lenght vector
objs    = dlmread('objs.txt', ' ');

% esta linea crea una lista de punteros a funciones
% note que @(obj,tv)vads_agg(obj,tv,V)
% es similar a un lambda de python
% lambda x, y : function(x, y, z=100)
% que permite cambiar la aridad de una funcion de tres a dos parametros
aggmthd={@wmm_agg, @(obj,tv)vads_agg(obj,tv,V)};  % array of aggregation function handles

pop_size=100;
resa = zeros(pop_size, 1);
resb = zeros(pop_size, 1);
fnh=aggmthd{2}; 

for row=1:pop_size
    resa(row)=fnh(objs(row, :), norm_weights(row, :));               % ambas invocaciones
    resb(row) = vads_agg(objs(row, :), norm_weights(row, :), 100);   % son equivalentes
end


for row=1:pop_size
    disp(sprintf('%.5f', resa(row)));
end

```

output from matlab

```
>> check_vads
2749699096204677.50000
44169.95509
0.12369
9129246211.23686
3.72593
4123242481366186.00000
966880984668922.37500
2.87597
3736612609234584.00000
3877647032680749.50000
180.95246
```

## Python

```python
# check_vads.py
from __future__ import print_function
import numpy as np
from vads import vads, vads_matlab
from numpy.linalg import norm

def pretty(a):
    
    for i, item in enumerate(a):
        print("%d: %.5f" % (i, item))

if __name__ == "__main__":
    
    weights = np.genfromtxt("weights_m_2.txt")
    norm_weights = np.genfromtxt("norm_weights_m_2.txt")
    objs = np.genfromtxt("objs.txt")
    
    pop_size = objs.shape[0]
    resa = np.zeros((pop_size, ))
    resb = np.zeros((pop_size, ))
    
    for i in range(pop_size):
        resa[i] = vads_matlab(objs[i], norm_weights[i], 100)
        resb[i] = vads(objs[i], weights[i], 100)

    print("matlab - norm_weights")
    pretty(resa)
    
    print("python - weights")
    pretty(resb)
```

output

```
matlab - norm_weights (function vads_matlab)
0: 2749756122678271.00000
1: 44169.95509
2: 0.12369
3: 9129246211.59759
4: 3.72593
5: 4123327993987842.50000
6: 966901036976828.37500
7: 2.87597
8: 3736690103474075.00000
9: 3877727451856949.00000
10: 180.95246

python - weights (function vads)
0: 2749756122678271.00000
1: 44169.95491
2: 0.12369
3: 9129246241.46377
4: 3.72593
5: 4123327993987842.50000
6: 966901036976828.37500
7: 2.87597
8: 3736690103474075.00000
9: 3877727451856949.00000
10: 180.95246
```



## Comparación

La salida de `vads_agg` (matlab), y sus equivalentes en python, `vads_matlab`, `vads`, es muy similar. Aún ignoro por qué la salida es tan diferente cuando el resultado es un número grande. Para fines prácticos, usaré `vads`.

La función `vads_matlab` es una copia lo más similar posible de `vads_agg`

```matlab
% vads_agg.m
t=sum(obj.*tv,2);                 % find projection of set onto target vector
lv=sqrt(sum(obj.^2,2));           % target vector length
Q=t./(lv+eps);                    % cos(angle)
tv=lv./(Q.^V+eps);                % VADS metric
```



```python
# vads.py
def vads_matlab(objs, weights, V):
    
    eps = 2.2204e-16
    t  = (objs*weights).sum()
    lv = np.sqrt((objs**2).sum())
    Q  = t / (lv + eps)
    tv = lv / (Q**V + eps)
    
    return tv
```

Luego de comparar ambas funciones, `vads_agg`, `vads_matlab`, y corroborar que la salida sea suficientemente similar, usé `vads_matlab` como base para implementar `vads`. Note que tanto `vads_agg` como `vads_matlab` asumen que el vector de pesos `weights` es unitario (ie, su longitud es 1). Sin embargo, los vectores de pesos que se usan en `rocket` no son unitarios. Por esta razón, implementé la función `vads` que, a diferencia de `vads_matlab`, realiza la normalización dentro de la función:

```python
def vads(objs, weights, q):
    """
    Vector Angle Distance Scaling
    
    Input
    objs        (m_objs, ) array, obj vector
    weights     (m_objs, ) array, weights vector
    
    Output
    result      scalar
    """
    
    eps = 2.2204e-16
    num = norm(objs)
    den = np.dot(objs, weights) / (norm(objs)*norm(weights) + eps)

    res = num / (den**q + eps)
    
    return res
```

## Lo que no funcionó

El autor sugiere usar una forma de VADS con logaritmos, pero no la implementa en matlab. El siguiente fragmento es mi implementación en python, pero no genera el mapa de contornos.

```python
def vads_log(objs, weights, q):
    """
    Vector Angle Distance Scaling
    
    Input
    objs        (m_objs, ) array, obj vector
    weights     (m_objs, ) array, weights vector
    
    Output
    result      scalar
    """
    
    eps = 2.2204e-16
    
    unit_weights = weights/norm(weights)
    unit_objs= objs/norm(objs)
    
    term_a = (q+1)*np.log(norm(objs)+eps)
    term_b = q*np.log(np.dot(unit_weights, unit_objs) + eps)
    res = np.exp(term_a - term_b + eps)
    
    return res
    
```





## Mapa de contornos

El mapa de contornos de VADS en [Hughes08] es el siguiente:

![](hughes08_vads.png)



Nuestro mapa de contornos es:



![](vads.png)

Compare el mapa de [Hughes08] con el nuestro para [0.5, 0.5]. A pesar de ser similares en el *tear-drop shape*, los valores escalares son diferentes a medida que se alejan del vector de pesos (compare la ubicación del valor 20 en ambos plots). Aún con estas diferencias, se evaluó VADS con MOEA/D y éste fue capaz de encontrar una buena aproximación de DTLZ1, con lo que considero que la implemetación de VADS es adecuada.

Para lograr el mapa de contornos, se tuvieron que hacer estos ajustes en `contour_vads.py`:

- Uso de `np.logspace`. Se generaron 501 puntos espaciados en una escala logarítmica entre $[10^0.1, 10^1]$, y luego se dividieron entre 10 para usar el rango $[\approx0, 1]$.

```python
n = 501
x = np.logspace(0.1, 1, n)/10
y = np.logspace(0.1, 1, n)/10
```

- Escala logarítmica de `vads`. Los valores escalares de `vads` fueron escalados de este modo.

```python
def eval_fn(...)
	...
	eps = 2.2204e-16
    Z[r, c] = np.log(fn(objs, weights, q=100)+eps)
```





## Manejo de exepcion

En la rev 2424 del experimento `vads_underflow` se presento la siguiente exepcion:

```
FloatingPointError: underflow encountered in power
```

causada por la funcion `fast_vads_matrix()`:

```python
def fast_vads_matrix(objs, weights, q=100):
    ...
    res = num / (den**q + eps)
```

Los detalles de la execpcion y como se abordo estan descritos en el experimento `vads_underflow` (ver `script.py` y `readme.md` en ese experimento).

El manejo de la exepcion se realiza en la funcion `compute_safe_vads_division()`:

```python
def fast_vads_matrix(objs, weights, q=100):
    res = None
    
    try:
        # fast way to compute res
        # this is needed for moead 
        res = num / (den**q + eps)
    
    except:
        # slow way to compute res
        # this is needed for amoead_v16
        res = compute_safe_vads_division(num, den, q, eps)
        
    return res.copy()
```

La funcion `compute_safe_vads_division` calcula cada entrada del arreglo `res` en un loop, por eso es un poco mas lento que hacerlo de manera vectorizada:

```python
def compute_safe_vads_division(num, den, q, eps):
    n_vals = den.shape[0]
    res = np.zeros((n_vals, ))
    
    for i in range(n_vals):
        
        try:
            res[i] = num[i] / (den[i]**q + eps)
            
        except:
            res[i] = num[i] / (0 + eps)     # den[i]**q is so close to zero
            								# that can ignore it
    
    return res.copy()
```





# Referencias

- [Hughes08] *Fitness Assigment Methods for Many-Objective Problems*