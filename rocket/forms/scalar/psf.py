# psf.py
from __future__ import print_function
import numpy as np
from rocket.helpers import replace_zero_weigths
from numpy.linalg import norm

# uncomment to start ipdb after an exception is raised
#np.seterr(all='raise')

def fast_psf_matrix(objs, weights, z_ref, alpha):
    """
    Penalty-based Scalarizing Function
    
    Input
    objs        (pop_size, m_objs) array, obj matrix
    weights     (pop_size, m_objs) array, weights matrix
    z_ref       (pop_size, m_objs) array, reference vector
    alpha       int, scaling factor, penalty value used to control diversity
    
    Output
    result      (pop_size, 1) array, scalars
    """
    
    eps = 2.2204e-16                            # we use the same eps as vads does
    
    # reshape z_ref
    pop_size, m_objs = objs.shape
    by_row  = 1 
    Z_ref   = z_ref.reshape((1, m_objs)).repeat(pop_size, 0)    # creo que la resta seria la misma sin este reshape
    
    # replace zero weights
    Wt = replace_zero_weigths(weights, 0.0001)                  # replace zero-weigths by 0.0001 as recommended in [Jiang17]
    
    # compute asf
    T = np.abs(objs - z_ref)            # translate
    W = (1.0 / Wt)                      # reciprocal
    P = (W*T)                           # product
    T = P.max(axis=by_row)              # top, max element per row      (pop_size, )
    
    # compute dist
    V = objs - z_ref
    A = (norm(V, axis=by_row)**2) * (norm(Wt, axis=by_row)**2)                  # use Wt, not W
    dot = (V*Wt).sum(axis=by_row)                                               # use Wt, not W
    B = dot**2
    
    num = np.sqrt(A - B)
    den = norm(Wt, axis=by_row)                                                 # use Wt, not W
    
    dist = num / den
    
    # final result
    R = T + alpha*dist
     
    R = R.reshape((R.shape[0], 1))      # (pop_size, 1), reshape
    
    return R.copy()
    
    
def slow_psf_matrix(objs, weights, z_ref, alpha):
    """
    Penalty-based Scalarizing Function    
    
    Input
    objs        (pop_size, m_objs) array, obj matrix
    weights     (pop_size, m_objs) array, weights matrix
    z_ref       (pop_size, m_objs) array, reference vector
    alpha       int, scaling factor, penalty value used to control diversity
    
    Output
    result      (pop_size, 1) array, scalars
    """
    
    pop_size, m_objs = objs.shape
    
    R = np.zeros((pop_size, 1))
    
    for i in range(pop_size):
        
        R[i, 0] = psf(objs[i], weights[i], z_ref, alpha)
        
    return R.copy()
    
  
def psf(objs, weights, z_ref, alpha):
    """
    Penalty-based Scalarizing Function
    
    Input
    objs        (m_objs, ) array, obj vector
    weights     (m_objs, ) array, weights vector
    z_ref       (m_objs, ) array, reference vector
    alpha       int, scaling factor, penalty value used to control diversity
    
    Output
    result      scalar
    """
    
    eps = 2.2204e-16                            # we use the same eps as vads does
    
    # compute asf
    
    wt = replace_zero_weigths(weights, 0.0001)  # replace zero-weigths by 0.0001 as recommended in [Jiang17] 
    
    trans   = (np.abs(objs - z_ref))            # (m_objs, ) absolute difference

    divs    = trans / wt                        # (m_objs, ) element-wise division
    
    asf     = divs.max(axis=0)                  # scalar, asf
    
    
    # compute distance
    
    v = objs - z_ref
    a = (norm(v)**2) * (norm(wt)**2)            # use wt, not weights
    b = norm(np.dot(v, wt))**2                  # use wt, not weights
    
    num = np.sqrt(a - b)
    den = norm(wt)                              # use wt, not weights
    
    dist = num / den
    
    
    # get final result
    
    result  = asf + alpha * dist 
    
    return result
    
