# asf.py
from __future__ import print_function
import numpy as np
from rocket.helpers import replace_zero_weigths

def fast_asf_matrix(objs, weights, z_ref):
    """
    Achievement Scalarizing Function
    
    Input
    objs        (pop_size, m_objs) array, obj matrix
    weights     (pop_size, m_objs) array, weights matrix
    z_ref       (pop_size, m_objs) array, reference vector
    
    Output
    result      (pop_size, 1) array, scalars
    """
    
    # reshape z_ref
    pop_size, m_objs = objs.shape
    by_row  = 1 
    Z_ref   = z_ref.reshape((1, m_objs)).repeat(pop_size, 0) # creo que la resta seria la misma sin este reshape
    
    # replace zero weights
    Wt = replace_zero_weigths(weights)
    
    # compute asf
    T = np.abs(objs - z_ref)            # translate
    W = (1.0 / Wt)                      # reciprocal
    R = (W*T).max(axis=by_row)          # result, max value per row
    
    R = R.reshape((R.shape[0], 1))      # (pop_size, 1), reshape
    
    return R.copy()
    
    
    
def slow_asf_matrix(objs, weights, z_ref):
    """
    Achievement Scalarizing Function
    
    Input
    objs        (pop_size, m_objs) array, obj matrix
    weights     (pop_size, m_objs) array, weights matrix
    z_ref       (pop_size, m_objs) array, reference vector
    
    Output
    result      (pop_size, 1) array, scalars
    """
    
    pop_size, m_objs = objs.shape
    
    R = np.zeros((pop_size, 1))
    
    for i in range(pop_size):
        
        R[i, 0] = asf(objs[i], weights[i], z_ref)
        
    return R.copy()
    
    
def asf(objs, weights, z_ref):
    """
    Achievement Scalarizing Function
    
    Input
    objs        (m_objs, ) array, obj vector
    weights     (m_objs, ) array, weights vector
    z_ref       (m_objs, ) array, reference vector
    
    Output
    result      scalar
    """
    
    wt = replace_zero_weigths(weights) 
    
    trans   = (np.abs(objs - z_ref))    # (m_objs, ) absolute difference

    divs    = trans / wt                # (m_objs, ) element-wise division
    
    result  = divs.max()                # scalar 

    return result
    
    
if __name__ == "__main__":
    
    w = np.array([0.7, 0.3])
    
    objs = np.array([.2, .4])
    
    z_ref = np.array([0, 0])

    result = asf(objs, w, z_ref)
