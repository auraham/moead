# contours_pbi_ipbi.py


# temporary
import os, sys
sys.path.insert(0, "/home/auraham/Dropbox/trixie/labs/forms_preliminar_study/")


import numpy as np
import matplotlib.pyplot as plt
from numpy.linalg import norm
from pbi import pbi
from ipbi import ipbi
from mpl_toolkits.axes_grid.anchored_artists import AnchoredText

from pylab import rcParams
rcParams["figure.figsize"] = 14, 4.8


def pretty(w):
    comps = ", ".join(["%.1f" % c for c in w])
    return comps

def plot_contour(X, Y, Z, w, z_ideal, z_nadir, figname, fn_label, subplot_id):
    
    plt.figure(figname)
    ax = plt.subplot(subplot_id)
    plt.cla()
    CS = plt.contour(X, Y, Z)
    plt.clabel(CS, inline=1, fontsize=10)
    #plt.title(title)
    plt.xlabel("$f_1$")
    plt.ylabel("$f_2$")
    plt.xlim(X.min(), X.max())
    plt.ylim(Y.min(), Y.max())
    
    
    # draw weigth vector
    x  = np.array([0, w[0]])
    y  = np.array([0, w[1]])
    plt.plot(x, y, "k")
    
    # draw weight vector direction
    sf = 8                          # scaling factor
    x  = np.array([0, w[0]])*sf
    y  = np.array([0, w[1]])*sf
    plt.plot(x, y, "k", ls=":")
    
    # title
    str_w   = "$w = [ %s ]$" % pretty(w) 
    str_z_ideal = "$z^{*} = [ %s ]$" % pretty(z_ideal) 
    plt.title(fn_label.upper())
    
    pos_a = (0.05, -0.1)
    
    if X.max() > 1:
        pos_a = (0.05, 3.8)
    
    # add anchored text
    label = "%s  %s" % (str_w, str_z_ideal)
    at = AnchoredText(label,
                      prop=dict(size=15), frameon=True,
                      loc=2,
                      )
    at.patch.set_boxstyle("round,pad=0.,rounding_size=0.2")
    ax.add_artist(at)
            
    
    plt.show()
    
def eval_fn(fn, X, Y, weights=np.array([0.5, 0.5]), z_ideal=np.array([0,0]), z_nadir=np.array([1,1]), theta=1):
    
    rows, cols = X.shape
    Z = np.zeros((rows, cols))
    
    m_objs = weights.shape[0]
    
    for r in range(rows):
        for c in range(cols):
            
            objs = np.zeros((m_objs, ))
            objs[0] = X[r, c]
            objs[1] = Y[r, c]
            
            Z[r, c], d1, d2 = fn(objs, weights, z_ideal, z_nadir, theta)
            
    return Z.copy()
    

def contours(figname, fn, weights, z_ideal, z_nadir):
    
    n = 101
    x = np.linspace(0, 1, n)
    y = np.linspace(0, 1, n)
    X, Y = np.meshgrid(x, y)
    subplot_ids = (131, 132, 133)
    
    for i, w in enumerate(weights):
        Z = eval_fn(fn, X, Y, w, z_ideal, z_nadir)
        #plot_contour(X, Y, Z, w, z_ideal, z_nadir, figname, figname+" "+str(w), subplot_ids[i])
        plot_contour(X, Y, Z, w, z_ideal, z_nadir, figname, figname, subplot_ids[i])
    
    plt.subplots_adjust(
        left  = 0.1,
        right = 0.95,
        wspace=0.24,
        bottom = 0.2, 
        )
    
    
    plt.savefig(figname+".eps")
    

def extended_contours(figname, fn, weights, z_ideal, z_nadir):
    
    n = 101
    x = np.linspace(-4, 4, n)
    y = np.linspace(-4, 4, n)
    X, Y = np.meshgrid(x, y)
    subplot_ids = (131, 132, 133)
    
    for i, w in enumerate(weights):
        Z = eval_fn(fn, X, Y, w, z_ideal, z_nadir)
        #plot_contour(X, Y, Z, w, figname, figname+" "+str(w), subplot_ids[i])
        plot_contour(X, Y, Z, w, z_ideal, z_nadir, figname, figname, subplot_ids[i])

    
    plt.subplots_adjust(
        left  = 0.1,
        right = 0.95,
        wspace=0.24,
        bottom = 0.2, 
        )
    
    
    plt.savefig(figname+".png")
    
if __name__ == "__main__":
    
    weights = (
        np.array([0.2, 0.8]),
        np.array([0.5, 0.5]),
        np.array([0.8, 0.2]),
    )
    
    z_ideal = np.array([0, 0])
    z_nadir = np.array([1, 1])
    
    
    # (figname, fn)
    params = (
        ("pbi", pbi),
        ("ipbi", ipbi),
    )
    
    for figname, fn in params:
        
        contours(figname, fn, weights, z_ideal=np.array([0, 0]), z_nadir=np.array([1, 1]))
        
        # uncomment these lines for more plots
        # these ones help understand the importance of defining the reference vectors correctly
        #extended_contours(figname+"_extended_error", fn, weights, z_ideal=np.array([0, 0]), z_nadir=np.array([1, 1]))       # this line causes a plot with curves because z_ideal and z_nadir are not defined correctly
        #extended_contours(figname+"_extended", fn, weights, z_ideal=np.array([-4, -4]), z_nadir=np.array([4, 4]))     # this line causes a plot with no curves because z_ideal and z_nadir are  defined correctly
    
