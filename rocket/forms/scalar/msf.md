# MSF

Proposed in [Jiang17]:

This is the contour plot of MSF shown in [Jiang17].

![](jiang17_msf.png)



This is the contour plot from our implementation:

![](msf.png)

To get this plot, run these commands from `ipython`:

```python
import os, sys; sys.path.insert(0, os.path.abspath("../../.."))
%run contours_msf.py
```

To plot more contour levels, change this snippet:

```python
def plot_contour(X, Y, Z, w, z_ref, figname, fn_label, subplot_id):
    
    # change levels here 
    levels_a = np.linspace(0.0, 10, 21)
    levels_b = np.array([0.4, 0.8, 1.2])
    levels = np.hstack((levels_a, levels_b))
```

If you change the value of `alpha` for `-0.5`, this is the contour plot:

![](msf_negative.png)

```python
def eval_fn(fn, X, Y, weights=np.array([0.5, 0.5]), z_ideal=np.array([0,0])):
    ...
    # change alpha here
    Z[r, c] = fn(objs, weights, z_ideal, alpha=-0.5)
```

From [Jiang17]:

> [...] $\alpha \geq 0$ is more desired compared with $\alpha < 0$ because just like the WS method, MSF with a $\alpha < 0$ is unable to approximate non-convex PFs and is very likely to lose diversity as mentioned earlier. For this reason, we only consider $\alpha \geq 0$ in this paper.

> However, $\alpha \leq 0$ may provide fast convergence in the early stage of the search and solve concave MOPs efficiently.

Finally, you can use `np.seterr(all='raise')` to start `ipdb` every time an exception is raised (such as division by zero).



## Notes about replacing zero values

In [Jiang17], they suggest to replace zero values under this circumstances:

- If a weight vector is zero, then replace its value for $10^{-4}$ (0.0001):

```python
# replace zero-weigths by 0.0001 as recommended in [Jiang17]
Wt = replace_zero_weigths(weights, 0.0001)                        
```

- If the denominator of Eq (7) is zero, then replace its value for $10^{-5}$ (0.00001):

```python
# denominator, min element per row   (pop_size, )
B = P.min(axis=by_row)              
    
# replace zero by 0.00001 before division as recommended in [Jiang17]
B = replace_zero_weigths(B, 0.00001)                        
```



# References

- [Jiang17] *Scalarizing Functions in Decomposition-based Multiobjective Evolutionary Algorithms*