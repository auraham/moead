# contours_asf.py


# temporary
import os, sys
sys.path.insert(0, "/home/auraham/Dropbox/trixie/labs/forms_preliminar_study/")


import numpy as np
import matplotlib.pyplot as plt
from numpy.linalg import norm
from vads import vads
from mpl_toolkits.axes_grid.anchored_artists import AnchoredText


from pylab import rcParams
rcParams["figure.figsize"] = 14, 4.8

def pretty(w):
    comps = ", ".join(["%.1f" % c for c in w])
    return comps


def get_custom_cmap(n_bin=100):
    
    from matplotlib.colors import LinearSegmentedColormap

        
    # (114, 19, 61) -> (150, 64, 102) -> (197, 116, 152)
    A = np.array([82, 20, 47]) / 255        # dark
    B = np.array([150, 64, 102]) / 255
    C = np.array([230, 196, 211]) / 255     # ligh
    
    
    """
    "cherry_dark"   : "#A91458",
    "cherry_light"  : "#E4AFC8",
    """
    
    A = np.array([82, 20, 47]) / 255
    
    B = np.array([169, 20, 88]) / 255       # cherry dark
    
    C = np.array([228, 175, 200]) / 255     # cherry light
    

    # from light (lower values) to dark (higher values)
    colors = [C, B, A]  # R -> G -> B


    cmap_name = "custom"

    # Create the colormap
    cm = LinearSegmentedColormap.from_list(
        cmap_name, colors, N=n_bin)
        
    return cm


def plot_contour(X, Y, Z, w, z_ref, figname, fn_label, subplot_id):
    
    cmap = get_custom_cmap()
    levels_a = np.arange(0, 4, 1)               # para darle mas lineas al centro
    levels_b = np.arange(4, 40, 4)
    levels = np.hstack((levels_a, levels_b))
    
    
    plt.figure(figname)
    ax = plt.subplot(subplot_id)
    CS = plt.contour(X, Y, Z, cmap=cmap, levels=levels)
    plt.clabel(CS, inline=1, fontsize=10)
    #plt.title(title)
    plt.xlabel("$f_1$")
    plt.ylabel("$f_2$")
    plt.xlim(X.min(), X.max())
    plt.ylim(Y.min(), Y.max())
    
    
    # draw weigth vector
    x  = np.array([0, w[0]])
    y  = np.array([0, w[1]])
    plt.plot(x, y, "k")
    
    # draw weight vector direction
    sf = 5000                          # scaling factor
    x  = np.array([0, w[0]])*sf
    y  = np.array([0, w[1]])*sf
    plt.plot(x, y, "k", ls=":")
    
    
    # title
    str_w   = "$w = [ %s ]$" % pretty(w) 
    str_z_ref = "$z^{ref} = [ %s ]$" % pretty(z_ref) 
    plt.title(fn_label.upper())
    
    
    pos_a = (0.05, -0.1)
    
    if X.max() > 1:
        pos_a = (0.05, 3.8)
    
    # add anchored text
    label = "%s  %s" % (str_w, str_z_ref)
    at = AnchoredText(label,
                      prop=dict(size=15), frameon=True,
                      loc=2,
                      )
    at.patch.set_boxstyle("round,pad=0.,rounding_size=0.2")
    ax.add_artist(at)
    
    plt.show()
    
def eval_fn(fn, X, Y, weights=np.array([0.5, 0.5]), z_ideal=np.array([0,0])):
    
    rows, cols = X.shape
    Z = np.zeros((rows, cols))
    
    m_objs = weights.shape[0]
    
    for r in range(rows):
        for c in range(cols):
            
            objs = np.zeros((m_objs, ))
            objs[0] = X[r, c]
            objs[1] = Y[r, c]
            
            eps = 2.2204e-16
            Z[r, c] = np.log(fn(objs, weights, q=100)+eps)
            #Z[r, c] = fn(objs, weights, q=100)
            
    return Z.copy()
    

def contours(figname, fn, weights, z_ideal):
    
    n = 501
    x = np.linspace(0, 100, n)
    y = np.linspace(0, 100, n)
    x = np.logspace(0.1, 1, n)/10
    y = np.logspace(0.1, 1, n)/10
    X, Y = np.meshgrid(x, y)
    subplot_ids = (131, 132, 133)
    
    for i, w in enumerate(weights):
        Z = eval_fn(fn, X, Y, w, z_ideal)
        #plot_contour(X, Y, Z, w, z_ideal, figname, figname+" "+str(w), subplot_ids[i])
        plot_contour(X, Y, Z, w, z_ideal, figname, figname, subplot_ids[i])
    
    plt.subplots_adjust(
        left  = 0.1,
        right = 0.95,
        wspace=0.24,
        bottom = 0.2, 
        )
    
    
    plt.savefig(figname+".eps")
    

def extended_contours(figname, fn, weights, z_ideal, z_nadir):
    
    n = 101
    x = np.linspace(-4, 4, n)
    y = np.linspace(-4, 4, n)
    X, Y = np.meshgrid(x, y)
    subplot_ids = (131, 132, 133)
    
    for i, w in enumerate(weights):
        Z = eval_fn(fn, X, Y, w, z_ideal)
        #plot_contour(X, Y, Z, w, z_ideal, figname, figname+" "+str(w), subplot_ids[i])
        plot_contour(X, Y, Z, w, z_ideal, figname, figname, subplot_ids[i])

    
    plt.subplots_adjust(
        left  = 0.1,
        right = 0.95,
        wspace=0.24,
        bottom = 0.2, 
        )
    
    plt.savefig(figname+".png")
    
if __name__ == "__main__":
    
    weights = (
        np.array([0.2, 0.8]),
        np.array([0.5, 0.5]),
        np.array([0.8, 0.2]),
    )
    
    z_ideal = np.array([0, 0])
    z_nadir = np.array([1, 1])
    
    
    # (figname, fn)
    params = (
        ("vads", vads),
    )
    
    for figname, fn in params:
        
        contours(figname, fn, weights, z_ideal=np.array([0, 0]))
        
        # uncomment these lines for more plots
        # these ones help understand the importance of defining the reference vectors correctly
        #extended_contours(figname+"_extended_error", fn, weights, z_ideal=np.array([0, 0]), z_nadir=np.array([1, 1]))       # this line causes a plot with curves because z_ideal and z_nadir are not defined correctly
        #extended_contours(figname+"_extended", fn, weights, z_ideal=np.array([-4, -4]), z_nadir=np.array([4, 4]))     # this line causes a plot with no curves because z_ideal and z_nadir are  defined correctly
    
