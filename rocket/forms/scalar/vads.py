# vads.py
from __future__ import print_function
import numpy as np
from numpy.linalg import norm

def vads(objs, weights, q):
    """
    Vector Angle Distance Scaling
    
    Input
    objs        (m_objs, ) array, obj vector
    weights     (m_objs, ) array, weights vector
    
    Output
    result      scalar
    """
    
    eps = 2.2204e-16
    num = norm(objs)
    den = np.dot(objs, weights) / (norm(objs)*norm(weights) + eps)

    try:
        res = num / (den**q + eps)
    except:
        res = num / (0 + eps)       # den**q is so close to zero that can ignore it
    
    return res
   
   
def vads_matlab(objs, weights, V):
    
    eps = 2.2204e-16
    t  = (objs*weights).sum()
    lv = np.sqrt((objs**2).sum())
    Q  = t / (lv + eps)
    tv = lv / (Q**V + eps)
    
    return tv
    

def compute_safe_vads_division(num, den, q, eps):
    """
    Compute this line from fast_vads_matrix():
    
        res = num / (den**q + eps)

    but using a loop
    
        for i in range(n_vals):
            res[i] = num[i] / (den[i]**q + eps)
    
    This function is needed to avoid this exception:
        FloatingPointError: underflow encountered in power
    
    Input
    num         (pop_size, ) array
    den         (pop_size, ) array
    q           int, power
    eps         float
    
    Output
    res         (pop_size, ) array, result
    """
    
    n_vals = den.shape[0]
    res = np.zeros((n_vals, ))
    
    for i in range(n_vals):
        
        try:
            res[i] = num[i] / (den[i]**q + eps)
            
        except:
            res[i] = num[i] / (0 + eps)     # den[i]**q is so close to zero that can ignore it
    
    return res.copy()
    
    

def fast_vads_matrix(objs, weights, q=100):
    """
    Vector Angle Distance Scaling
    
    Input
    objs        (pop_size, m_objs) array, objs matrix
    weights     (pop_size, m_objs) array, weights matrix
    
    Output
    result      (pop_size, 1) array, scalars
    """
    
    """
    eps = 2.2204e-16
    num = norm(objs)
    den = np.dot(objs, weights) / (norm(objs)*norm(weights) + eps)

    res = num / (den**q + eps)
    
    return res
    """
    
    eps = 2.2204e-16                                    # this eps was taken from matlab's eps
    per_row = 1
    
    # norms
    norm_objs    = norm(objs, axis=per_row)             # (pop_size, ), un escalar por cada row
    norm_weights = norm(weights, axis=per_row)          # (pop_size, ), un escalar por cada row
    
    # dot product
    dot = (objs*weights).sum(axis=per_row)              # (pop_size, ), un escalar por cada row
    
    
    # division
    num = norm_objs.copy()
    den = dot / (norm_objs * norm_weights + eps)        # (pop_size, ) = (pop_size, ) / (pop_size, )
    
    # this line can cause an underflow
    #res = num / (den**q + eps)                          # (pop_size, ), un resultado por cada row
    
    # underflow handling from here
    res = None
    
    try:
        
        res = num / (den**q + eps)                          # (pop_size, ), un resultado por cada row
    
    except:
        
        res = compute_safe_vads_division(num, den, q, eps)
        
    return res.copy()

def slow_vads_matrix(objs, weights, q=100):
    """
    Vector Angle Distance Scaling
    
    Input
    objs        (pop_size, m_objs) array, objs matrix
    weights     (pop_size, m_objs) array, weights matrix
    
    Output
    result      (pop_size, 1) array, scalars
    """
    
    pop_size, m_objs = objs.shape
    
    R = np.zeros((pop_size, 1))
    
    for i in range(pop_size):
        
        R[i, 0] = vads(objs[i], weights[i], q)
        
    return R.copy()
    
# ---- deprecated ----


def vads_error(objs, weights, q):
    """
    Vector Angle Distance Scaling
    
    Input
    objs        (m_objs, ) array, obj vector
    weights     (m_objs, ) array, weights vector
    
    Output
    result      scalar
    """
    
    V = weights/weights.sum()
    
    O = objs.copy()
    
    term_a = (q+1) * np.log(np.abs(O))
    term_b = q * np.log(np.dot(W, O))       # asumimos que objs esta normalizado en [0, 1]
                                            # para evitar que np.dot(weights, objs) sea negativo y afectar el np.log
    
    result = np.exp(term_a - term_b)

    return result
    
   
def vads_log(objs, weights, q):
    """
    Vector Angle Distance Scaling
    
    Input
    objs        (m_objs, ) array, obj vector
    weights     (m_objs, ) array, weights vector
    
    Output
    result      scalar
    """
    
    eps = 2.2204e-16
    
    unit_weights = weights/norm(weights)
    #unit_objs= objs/norm(objs)
    
    term_a = (q+1)*np.log(norm(objs)+eps)
    #term_b = q*np.log(np.dot(unit_weights, unit_objs) + eps)
    term_b = q*np.log(np.dot(unit_weights, objs) + eps)
    res = np.exp(term_a - term_b + eps)
    
    return res
   

def vads_matlab2(objs, weights, q):
    """
    Vector Angle Distance Scaling
    
    Input
    objs        (m_objs, ) array, obj vector
    weights     (m_objs, ) array, weights vector
    
    Output
    result      scalar
    """
    import ipdb; ipdb.set_trace()
    num = norm(objs)
    den = np.dot(objs, weights) / (norm(objs))  # omitimos norm(weights)

    res = num / (den**q)
    
    return res
    


if __name__ == "__main__":
    
    W = np.array([0.7071, 0.7071])
    
    objs = np.array([-0.5972, 0.9247])

    result_a = vads_norm(objs, W, q=100)
    result_b = vads_matlab(objs, W, q=100)
