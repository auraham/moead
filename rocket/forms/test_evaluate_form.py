# test_evaluate_form.py
from __future__ import print_function
from rocket.forms import evaluate_form
import numpy as np

if __name__ == "__main__":
    
    objs = np.array([
        [0.0, 0.1],
        [0.1, 0.2],
        [0.2, 0.3],
        [0.5, 0.2],
        [0.4, 0.1],
        [0.3, 0.2],
        [0.2, 0.0],
    ])
    
    objs = objs.repeat(10, axis=0)
    pop_size = objs.shape[0]

    span = np.linspace(0, 1, pop_size)
    weights = np.zeros((pop_size, 2))

    weights[:, 0] = span.copy()
    weights[:, 1] = span[::-1].copy()
    

    # pbi, ipbi    
    z_ideal = objs.min(axis=0)
    z_nadir = objs.max(axis=0)
    
    params = {
        "name"      : "pbi",
        "pbi-theta" : 5,
    }
    
    scalar = evaluate_form(objs, weights, z_ideal, z_nadir, params)


    # asf
    z_ref = np.zeros((2, ))
    
    params = {
        "name"      : "asf",
        "asf-z_ref"     : z_ref,       
    }
    
    scalar = evaluate_form(objs, weights, z_ideal, z_nadir, params)
    
    
    # aasf
    params = {
        "name": "aasf",
        "aasf-alpha": 0.0001,
    }
    
    scalar = evaluate_form(objs, weights, z_ideal, z_nadir, params)
    
    # wpo
    params = {
        "name": "wpo",
        "wpo-p": 3,
    }
    
    scalar = evaluate_form(objs, weights, z_ideal, z_nadir, params)
    
    # wn
    params = {
        "name": "wn",
        "wn-p": 0.5,
    }
    
    scalar = evaluate_form(objs, weights, z_ideal, z_nadir, params)
    
