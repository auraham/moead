# evaluate_form.py
from __future__ import print_function
from rocket.forms.scalar import fast_pbi_matrix
from rocket.forms.scalar import fast_ipbi_matrix
from rocket.forms.scalar import fast_asf_matrix
from rocket.forms.scalar import fast_te_matrix
from rocket.forms.scalar import fast_ws_matrix
from rocket.forms.scalar import fast_vads_matrix
from rocket.forms.scalar import fast_d1_matrix
from rocket.forms.scalar import fast_aasf_matrix
from rocket.forms.scalar import fast_wpo_matrix
from rocket.forms.scalar import fast_wn_matrix

def evaluate_form(objs, weights, z_ideal, z_nadir, params):
    """
    Evaluate a formulation using a matrix of objectives.
    The input and output depends on the formulation.
    
    Input
    objs        (pop_size, m_objs) objs matrix
    weights     (pop_size, m_objs) weights matrix
                or None
    z_ideal     (m_objs, ) reference vector 
                or None
    z_nadir     (m_objs, ) reference vector 
                or None
    params      dict with parameters
    
    Output
    result      (pop_size, ) scalar values vector
    """

    form_name   = params["name"].lower()
    result      = None
    
    # pbi
    if form_name == "pbi":
    
        # @todo: check objs and weights size here
        # @todo: check wheter z_ideal or z_nadir are Nones
        # @todo: check_scalar_params(objs, weights, z_ideal, z_nadir)
        
        theta   = params["pbi-theta"]
        result  = fast_pbi_matrix(objs, weights, z_ideal, z_nadir, theta) 
    
    # ipbi
    elif form_name == "ipbi":
    
        # @todo: check objs and weights size here
        # @todo: check wheter z_ideal or z_nadir are Nones
        # @todo: check_scalar_params(objs, weights, z_ideal, z_nadir)
        
        theta   = params["ipbi-theta"]
        negative= params["ipbi-negative"]                                       # this option must be True when addressing minimization problems
        result  = fast_ipbi_matrix(objs, weights, z_ideal, z_nadir, theta)
        
        if negative:
            result = result*(-1)
    
    # asf
    elif form_name == "asf":
    
        # @todo: check objs and weights size here
        # @todo: check wheter z_ideal or z_nadir are Nones
        # @todo: check_scalar_params(objs, weights, z_ideal, z_nadir)
        
        z_ref   = params["asf-z_ref"]
        result  = fast_asf_matrix(objs, weights, z_ref) 
    
    # te
    elif form_name == "te":
    
        # @todo: check objs and weights size here
        # @todo: check wheter z_ideal or z_nadir are Nones
        # @todo: check_scalar_params(objs, weights, z_ideal, z_nadir)
        
        result  = fast_te_matrix(objs, weights, z_ideal, z_nadir) 
    
    # ws
    elif form_name == "ws":
    
        # @todo: check objs and weights size here
        # @todo: check wheter z_ideal or z_nadir are Nones
        # @todo: check_scalar_params(objs, weights, z_ideal, z_nadir)
        
        result  = fast_ws_matrix(objs, weights, z_ideal, z_nadir) 
    
    # vads
    elif form_name == "vads":
    
        # @todo: check objs and weights size here
        # @todo: check wheter z_ideal or z_nadir are Nones
        # @todo: check_scalar_params(objs, weights, z_ideal, z_nadir)
        
        q = params.get("vads-q", 100) 
        result  = fast_vads_matrix(objs, weights, q) 
    
    # d1
    elif form_name == "d1":
    
        # @todo: check objs and weights size here
        # @todo: check wheter z_ideal or z_nadir are Nones
        # @todo: check_scalar_params(objs, weights, z_ideal, z_nadir)
        
        result  = fast_d1_matrix(objs, weights, z_ideal, z_nadir)
     
    # aasf
    elif form_name == "aasf":
    
        # @todo: check objs and weights size here
        # @todo: check wheter z_ideal or z_nadir are Nones
        # @todo: check_scalar_params(objs, weights, z_ideal, z_nadir)
        
        p = params["aasf-alpha"]
        result  = fast_aasf_matrix(objs, weights, p)
     
    # wpo
    elif form_name == "wpo":
    
        # @todo: check objs and weights size here
        # @todo: check wheter z_ideal or z_nadir are Nones
        # @todo: check_scalar_params(objs, weights, z_ideal, z_nadir)
        
        p = params["wpo-p"]
        result  = fast_wpo_matrix(objs, weights, p)
     
    # wn
    elif form_name == "wn":
    
        # @todo: check objs and weights size here
        # @todo: check wheter z_ideal or z_nadir are Nones
        # @todo: check_scalar_params(objs, weights, z_ideal, z_nadir)
        
        p = params["wn-p"]
        result  = fast_wn_matrix(objs, weights, p)
     
    else:
        
        print("%s is not implemented yet!" % form_name)      # @todo: add log support here
        return result
    
    return result.copy()
