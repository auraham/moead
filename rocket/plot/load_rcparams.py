# load_rcparams.py
from __future__ import print_function
from matplotlib.pyplot import rcParams


def load_rcparams(figsize=None, for_paper=False):
    """
    Load a custom rcParams dictionary
    """
    
    rcParams['axes.titlesize']  = 14            # title
    rcParams['axes.labelsize']  = 17            # $f_i$ labels
    rcParams['xtick.color']     = "#474747"     # ticks gray color
    rcParams['ytick.color']     = "#474747"     # ticks gray color
    rcParams['xtick.labelsize'] = 10            # ticks size
    rcParams['ytick.labelsize'] = 10            # ticks size
    rcParams['legend.fontsize'] = 12            # legend
    rcParams['legend.fontsize'] = 12            # legend

    if for_paper:    
        rcParams['font.family'] = 'serif'       # font face
                
    if isinstance(figsize, tuple):
        rcParams['figure.figsize'] = figsize    # figsize, common values: (12, 5), (8, 6)
    
    #print("(load_rcparams) figsize: (%d, %d), you can use (12, 5), (8, 6)" % (rcParams['figure.figsize'][0], rcParams['figure.figsize'][1]))
