# plot_pops.py
from __future__ import print_function
from rocket.plot.colors import colors, shadows, darks, basecolors
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np

def plot_series(series, labels,  title=None, fig=None, ax=None, figname=None,
        to_highlight=None,
        lower_lims=None, upper_lims=None, output="", show_plot=True,
        loc="lower left", ncol=2, xlabel="time", ylabel="value", step=None, custom_markers=None, custom_colors=None, custom_linestyles=None, 
        threshold=None):
    """
    Plot a set of arrays in 2D.
    
    series          list of l arrays of size (n_points, )
    labels          list of l string labels, one for each array
    
    title           string, plot title
    figname         string, figure name
    to_highlight    list of l boolean arrays of size (n_points, ) to highlight entries of series
    lower_lims      (m_objs, ) array, lower bounds of the plot
    upper_lims      (m_objs, ) array, upper bounds of the plot
    output          string, output path
    show_plot       bool, determines if the plot will be shown (useful when the plot will be saved but not shown)
    loc             string, legend location
    ncol            int, number of legend columns
    xlabel          str, label of x-axis 
    ylabel          str, label of y-axis 
    step            int, interval of xticks
    threshold       double, optional horizontal line
    """
    
    # create plot
    if fig is None and ax is None:
        fig = plt.figure(figname) if figname else plt.figure()
        ax  = fig.add_subplot(111)

    # set labels
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel, labelpad=14)

    # set title
    if title:
        ax.set_title(title)

    # set lims
    if not (lower_lims is None or upper_lims is None):
    
        ax.set_xlim(lower_lims[0], upper_lims[0])
        ax.set_ylim(lower_lims[1], upper_lims[1])
        
            
    # change the axis title to almostblack
    ax.title.set_color(basecolors["almost_black"])
    
    # callback
    lined = {}      # lines dictionary
    lines = []      # lines list
    
    n_colors = len(colors)
    
    # setup markers
    markers = ["o" for i in range(len(series))] 
    
    if not custom_markers is None:
        markers = custom_markers
        
    # setup colors
    line_colors = colors
    
    if not custom_colors is None:
        line_colors = custom_colors
        
    # setup linestyles
    line_styles = ["-" for i in range(len(series))]
    
    if not custom_linestyles is None:
        line_styles = custom_linestyles 
    
            
    # series
    max_x_val = 0
    for i, serie in enumerate(series):
        
        y_data = serie
        x_data = np.arange(0, len(y_data))
        
        if step:                                                    # eg, if step=10, len(y)=6
            x_data = np.arange(1, len(y_data)+1)*step               # [10, 20, 30, 40, 50, 60] for six points
            x_data = np.arange(step, len(y_data)*step+1, step)      # is the same as before, but using this sintax {start, stop+1, step}, where stop has +1 to include stop
        
        if x_data.max() > max_x_val:
            max_x_val = x_data.max()
        
        # highlights
        # @todo: add support to show/hide
        # @todo: add custom_shadows
        if not to_highlight is None:
            
            to_hl = to_highlight[i]
            
            l2, = ax.plot(x_data[to_hl], y_data[to_hl], c=shadows[i], ls="none", #c=colors[i%n_colors], 
                    mec=basecolors["almost_black"], marker=markers[i], markeredgewidth=0.3, ms=10)
                    
            
            for p in to_hl:
                
                xp, yp = x_data[p], y_data[p]
                ax.text(xp+10, yp, "$t: %d$" % xp)        
            
        
        # real series
        l1, = ax.plot(x_data, y_data, label=labels[i], c=line_colors[i], ls=line_styles[i], #c=colors[i%n_colors], 
                    mec=basecolors["almost_black"], marker=markers[i], markeredgewidth=0.3)
        
        
        
        
        
        lines.append(l1)
        lined[labels[i]] = l1
    
    # draw horizontal line
    if not threshold is None:
        threshold_line = np.array([
                [0,         threshold],
                [max_x_val, threshold]])
        ax.plot(threshold_line[:, 0], threshold_line[:, 1], c=basecolors["almost_black"], ls=":")
        
        # @to_remove
        # add label for threshold
        ax.text(400, 1.2*threshold, "Threshold $k = %.4f$" % threshold)

    
    # get legend
    leg = ax.legend(loc=loc, scatterpoints=1, ncol=ncol, numpoints=1)
    
    # for callback
    for legtxt, __ in zip(leg.get_texts(), lines):
        legtxt.set_picker(5)
        
        
    # inner callback
    def on_pick(event):
        
        legtxt, line = None, None
        
        obj = event.artist
        is_callable = callable(getattr(obj, "get_text", None))
        
        if is_callable:
            
            legtxt = event.artist
            line = lined[legtxt.get_text()]
            
        else:
            
            legtxt = None
            line = event.artist
            
        is_visible = line.get_visible()
        
        # fade a little bit
        if is_visible:
            
            line.set_visible(False)
            
            if legtxt is not None:
                legtxt.set_alpha(0.2)
                
        else:
            
            line.set_visible(True)
            
            if legtxt is not None:
                legtxt.set_alpha(None)
                
        fig.canvas.draw()
    
    
    # register callback
    #fig.canvas.callbacks.connect("pick_event", on_pick)
    fig.canvas.mpl_connect("pick_event", on_pick)
    
    
        
    if output:                                                  # @todo: check if output is a valid path
        #fig.set_size_inches(width, height)
        fig.savefig(output, dpi=300, bbox_inches="tight")
    
    
    if show_plot:
        plt.show()


    return fig, ax

   
def plot_shadow_series(M_list, labels, threshold=None, title=None, fig=None, ax=None, figname=None, 
        lower_lims=None, upper_lims=None, output="", show_plot=True,
        loc="upper right", ncol=2, xlabel="time", ylabel="value", step=None, debug=False):
    """
    Plot a set of arrays (medians) in 2D with a shaded region bounded by the min and max values.
    
    Input
    M_list          list of arrays (M1, M2, ...)
                    
                    each array has this shape: (runs, steps)
    
                    where each row is an independent run with steps points
                    each step represent a point where some performance indicator (such as hypervolume) was computed
    labels          list of strings, one for each M matrix
    threshold       float, draws a line 
    
    title           string, plot title
    fig             plt.figure, optional
    ax              subplot, optional
    figname         string, figure name
    lower_lims      (m_objs, ) array, lower bounds of the plot
    upper_lims      (m_objs, ) array, upper bounds of the plot
    output          string, output path
    show_plot       bool, determines if the plot will be shown (useful when the plot will be saved but not shown)
    loc             string, legend location
    ncol            int, number of legend columns
    xlabel          str, label of x-axis 
    ylabel          str, label of y-axis 
    step            int, interval of xticks
    """
    
    # create plot
    if fig is None and ax is None:
        fig = plt.figure(figname) if figname else plt.figure()
        ax  = fig.add_subplot(111)

    # set labels
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)

    # set title
    if title:
        ax.set_title(title)

    # set lims
    if not (lower_lims is None or upper_lims is None):
    
        ax.set_xlim(lower_lims[0], upper_lims[0])
        ax.set_ylim(lower_lims[1], upper_lims[1])
    
    # for threshold
    x_data = None
    
    # for callback
    lined = {}
    shadw = {}
    
    for ind, M in enumerate(M_list):
        
        runs, steps = M.shape
        
        upper = M.max(axis=0)           # max for each step (col)
        lower = M.min(axis=0)           # min for each step (col)
        median = np.median(M, axis=0)
        
        y_data = median
        x_data = np.arange(0, len(y_data))
            
        if step:                                                    # eg, if step=10, len(y)=6
            x_data = np.arange(1, len(y_data)+1)*step               # [10, 20, 30, 40, 50, 60] for six points
            x_data = np.arange(step, len(y_data)*step+1, step)      # is the same as before, but using this sintax {start, stop+1, step}, where stop has +1 to include stop
        
        # plot median
        l1, = ax.plot(x_data, y_data, label=labels[ind], lw=1, color=colors[ind])
        
        # plot shadow
        l2 = ax.fill_between(x_data, upper, lower, facecolor=shadows[ind], alpha=0.5, edgecolor="#252525", lw=0.15)
        
        # esto no tiene sentido, por eso no se usa
        # ie no tiene sentido porque yerr no puede ser upper+lower
        # plot error bars
        #if show_errorbars:
        #    ax.errorbar(x_data, y_data, yerr=sd)
        
        lined[labels[ind]] = l1
        shadw[labels[ind]] = l2
        
        if debug:
            for row_id, row in enumerate(M):
                label = "%s - %d" % (labels[ind], row_id)
                l3, = ax.plot(x_data, row, ls=":", label=label)
                
                lined[label] = l3
                shadw[label] = None
        
    
    # threshold
    if not threshold is None: 
        line = np.array([
        [0, threshold],
        [x_data.max(), threshold]])
        
        ax.plot(line[:, 0], line[:, 1], ls=":", c="#252525")
    

    # ----
    
    leg = ax.legend(loc=loc, scatterpoints=1, ncol=ncol, numpoints=1)

    # for callback
    for legtxt  in leg.get_texts():
        legtxt.set_picker(5)

    
    # inner callback
    def on_pick(event):
        
        legtxt, line, shadow = None, None, None
        
        obj = event.artist
        is_callable = callable(getattr(obj, "get_text", None))
        
        if is_callable:
            
            legtxt = event.artist
            line   = lined[legtxt.get_text()]
            shadow = shadw[legtxt.get_text()]
            
        else:
            
            legtxt = None
            line = event.artist
            
        is_visible = line.get_visible()
        
        # fade a little bit
        if is_visible:
            
            line.set_visible(False)
            if not shadow is None: shadow.set_alpha(0)
            
            if legtxt is not None:
                legtxt.set_alpha(0.2)
                
        else:
            
            line.set_visible(True)
            if not shadow is None: shadow.set_alpha(0.5)
            
            if legtxt is not None:
                legtxt.set_alpha(None)
                
        fig.canvas.draw()
    
    
    # register callback
    #fig.canvas.callbacks.connect("pick_event", on_pick)
    fig.canvas.mpl_connect("pick_event", on_pick)
    
    
    if output:                                                  # @todo: check if output is a valid path
        #fig.set_size_inches(width, height)
        fig.savefig(output, dpi=300, bbox_inches="tight")
    
    if show_plot:
        plt.show()

    return fig, ax


   
def plot_shadow_series_mean_sd(M_list, labels, threshold=None, title=None, fig=None, ax=None, figname=None, 
        lower_lims=None, upper_lims=None, output="", show_plot=True, show_shadow=True, show_errorbars=False,
        loc="upper right", ncol=2, xlabel="time", ylabel="value", step=None, custom_linestyles=None, debug=False, show_legend=True, custom_markers=None,
        custom_mean_lw=1.4, custom_bars_lw=1, custom_ms=4):
    """
    Plot a set of arrays (means) in 2D with a shaded region bounded by the standard deviation values.
    
    Input
    M_list          list of arrays (M1, M2, ...)
                    
                    each array has this shape: (runs, steps)
    
                    where each row is an independent run with steps points
                    each step represent a point where some performance indicator (such as hypervolume) was computed
    labels          list of strings, one for each M matrix
    threshold       float, draws a line 
    
    title           string, plot title
    fig             plt.figure, optional
    ax              subplot, optional
    figname         string, figure name
    lower_lims      (m_objs, ) array, lower bounds of the plot
    upper_lims      (m_objs, ) array, upper bounds of the plot
    output          string, output path
    show_plot       bool, determines if the plot will be shown (useful when the plot will be saved but not shown)
    show_shadow     bool, determines if the shadow wrapped by [min, max] is plotted
    show_errorbars  bool, determines if the errorbars defined by [min, max] are plotted
    loc             string, legend location
    ncol            int, number of legend columns
    xlabel          str, label of x-axis 
    ylabel          str, label of y-axis 
    step            int, interval of xticks
    """
    
    # create plot
    if fig is None and ax is None:
        fig = plt.figure(figname) if figname else plt.figure()
        ax  = fig.add_subplot(111)

    # set labels
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)

    # set title
    if title:
        ax.set_title(title)

    # set lims
    if not (lower_lims is None or upper_lims is None):
    
        ax.set_xlim(lower_lims[0], upper_lims[0])
        ax.set_ylim(lower_lims[1], upper_lims[1])
    
    # for threshold
    x_data = None
    
    # for callback
    lined = {}
    shadw = {}
    
    # setup linestyles
    line_styles = ["-" for i in range(len(M_list))]
    
    if not custom_linestyles is None:
        line_styles = custom_linestyles 
    
    # setup markers
    markers = ["o" for i in range(len(M_list))]
    
    if not custom_markers is None:
        markers = custom_markers
    
    for ind, M in enumerate(M_list):
        
        runs, steps = M.shape
        
        #upper = M.max(axis=0)           # max for each step (col)
        #lower = M.min(axis=0)           # min for each step (col)
        #median = np.median(M, axis=0)
        #y_data = median
        #x_data = np.arange(0, len(y_data))

        # added
        mean = np.mean(M, axis=0)
        sd = np.std(M, axis=0)
        upper = mean+sd
        lower = mean-sd
        
        y_data = mean
        x_data = np.arange(0, len(y_data))
        

            
        if step:                                                    # eg, if step=10, len(y)=6
            x_data = np.arange(1, len(y_data)+1)*step               # [10, 20, 30, 40, 50, 60] for six points
            x_data = np.arange(step, len(y_data)*step+1, step)      # is the same as before, but using this sintax {start, stop+1, step}, where stop has +1 to include stop
        
        # plot error bars
        if show_errorbars:
            ax.errorbar(x_data, y_data, yerr=sd, fmt="none", ecolor=darks[ind], lw=custom_bars_lw, capsize=2) # continua con este commit
        
        # plot mean
        l1, = ax.plot(x_data, y_data, label=labels[ind], ls=line_styles[ind], color=colors[ind], 
            mec=basecolors["almost_black"], marker=markers[ind], markeredgewidth=0.3, ms=custom_ms, lw=custom_mean_lw)
        
        # plot shadow
        l2 = None
        if show_shadow:
            #l2 = ax.fill_between(x_data, upper, lower, facecolor=shadows[ind], alpha=0.5, edgecolor="#252525", lw=0.15)
            #l2 = ax.fill_between(x_data, upper, lower, facecolor=shadows[ind], alpha=0.5, edgecolor=darks[ind], lw=0.15)
            l2 = ax.fill_between(x_data, upper, lower, facecolor=shadows[ind], alpha=0.5, edgecolor=shadows[ind], lw=0.15)
        
        
        lined[labels[ind]] = l1
        shadw[labels[ind]] = l2
        
        if debug:
            for row_id, row in enumerate(M):
                label = "%s - %d" % (labels[ind], row_id)
                l3, = ax.plot(x_data, row, ls=":", label=label)
                
                lined[label] = l3
                shadw[label] = None
        
    
    # threshold
    if not threshold is None: 
        line = np.array([
        [0, threshold],
        [x_data.max(), threshold]])
        
        ax.plot(line[:, 0], line[:, 1], ls=":", c="#252525")
    

    # ----
    
    if show_legend:
        
        leg = ax.legend(loc=loc, scatterpoints=1, ncol=ncol, numpoints=1)

        # for callback
        for legtxt  in leg.get_texts():
            legtxt.set_picker(5)

        
        # inner callback
        def on_pick(event):
            
            legtxt, line, shadow = None, None, None
            
            obj = event.artist
            is_callable = callable(getattr(obj, "get_text", None))
            
            if is_callable:
                
                legtxt = event.artist
                line   = lined[legtxt.get_text()]
                shadow = shadw[legtxt.get_text()]
                
            else:
                
                legtxt = None
                line = event.artist
                
            is_visible = line.get_visible()
            
            # fade a little bit
            if is_visible:
                
                line.set_visible(False)
                if not shadow is None: shadow.set_alpha(0)
                
                if legtxt is not None:
                    legtxt.set_alpha(0.2)
                    
            else:
                
                line.set_visible(True)
                if not shadow is None: shadow.set_alpha(0.5)
                
                if legtxt is not None:
                    legtxt.set_alpha(None)
                    
            fig.canvas.draw()
        
        
        # register callback
        #fig.canvas.callbacks.connect("pick_event", on_pick)
        fig.canvas.mpl_connect("pick_event", on_pick)
    
    
    if output:                                                  # @todo: check if output is a valid path
        #fig.set_size_inches(width, height)
        fig.savefig(output, dpi=300, bbox_inches="tight")
    
    if show_plot:
        plt.show()

    return fig, ax

def plot_shadow_series_mean_sd_colors(M_list, labels, threshold=None, title=None, fig=None, ax=None, figname=None, 
        lower_lims=None, upper_lims=None, output="", show_plot=True, show_shadow=True, show_errorbars=False,
        loc="upper right", ncol=2, xlabel="time", ylabel="value", step=None, custom_linestyles=None, debug=False, show_legend=True, custom_markers=None,
        custom_mean_lw=1.4, custom_bars_lw=1, custom_ms=4, custom_colors=None, custom_shadows=None):
    """
    Plot a set of arrays (means) in 2D with a shaded region bounded by the standard deviation values.
    
    Input
    M_list          list of arrays (M1, M2, ...)
                    
                    each array has this shape: (runs, steps)
    
                    where each row is an independent run with steps points
                    each step represent a point where some performance indicator (such as hypervolume) was computed
    labels          list of strings, one for each M matrix
    threshold       float, draws a line 
    
    title           string, plot title
    fig             plt.figure, optional
    ax              subplot, optional
    figname         string, figure name
    lower_lims      (m_objs, ) array, lower bounds of the plot
    upper_lims      (m_objs, ) array, upper bounds of the plot
    output          string, output path
    show_plot       bool, determines if the plot will be shown (useful when the plot will be saved but not shown)
    show_shadow     bool, determines if the shadow wrapped by [min, max] is plotted
    show_errorbars  bool, determines if the errorbars defined by [min, max] are plotted
    loc             string, legend location
    ncol            int, number of legend columns
    xlabel          str, label of x-axis 
    ylabel          str, label of y-axis 
    step            int, interval of xticks
    """
    
    # create plot
    if fig is None and ax is None:
        fig = plt.figure(figname) if figname else plt.figure()
        ax  = fig.add_subplot(111)

    # set labels
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)

    # set title
    if title:
        ax.set_title(title)

    # set lims
    if not (lower_lims is None or upper_lims is None):
    
        ax.set_xlim(lower_lims[0], upper_lims[0])
        ax.set_ylim(lower_lims[1], upper_lims[1])
    
    # for threshold
    x_data = None
    
    # for callback
    lined = {}
    shadw = {}
    
    # setup linestyles
    line_styles = ["-" for i in range(len(M_list))]
    
    if not custom_linestyles is None:
        line_styles = custom_linestyles
         
    
    # setup shadows
    shadow_colors = list(shadows)
    if not custom_shadows is None:
        shadow_colors = custom_shadows
    
    # setup colors
    bcolors = list(colors)
    
    if not custom_colors is None:
        bcolors = custom_colors 
    
    # setup markers
    markers = ["o" for i in range(len(M_list))]
    
    if not custom_markers is None:
        markers = custom_markers
    
    for ind, M in enumerate(M_list):
        
        runs, steps = M.shape
        
        #upper = M.max(axis=0)           # max for each step (col)
        #lower = M.min(axis=0)           # min for each step (col)
        #median = np.median(M, axis=0)
        #y_data = median
        #x_data = np.arange(0, len(y_data))

        # added
        mean = np.mean(M, axis=0)
        sd = np.std(M, axis=0)
        upper = mean+sd
        lower = mean-sd
        
        y_data = mean
        x_data = np.arange(0, len(y_data))
        

            
        if step:                                                    # eg, if step=10, len(y)=6
            x_data = np.arange(1, len(y_data)+1)*step               # [10, 20, 30, 40, 50, 60] for six points
            x_data = np.arange(step, len(y_data)*step+1, step)      # is the same as before, but using this sintax {start, stop+1, step}, where stop has +1 to include stop
        
        # plot error bars
        if show_errorbars:
            ax.errorbar(x_data, y_data, yerr=sd, fmt="none", ecolor=darks[ind], lw=custom_bars_lw, capsize=2) # continua con este commit
        
        # plot mean
        l1, = ax.plot(x_data, y_data, label=labels[ind], ls=line_styles[ind], color=bcolors[ind], 
            mec=basecolors["almost_black"], marker=markers[ind], markeredgewidth=0.3, ms=custom_ms, lw=custom_mean_lw)
        
        # plot shadow
        l2 = None
        if show_shadow:
            #l2 = ax.fill_between(x_data, upper, lower, facecolor=shadows[ind], alpha=0.5, edgecolor="#252525", lw=0.15)
            #l2 = ax.fill_between(x_data, upper, lower, facecolor=shadows[ind], alpha=0.5, edgecolor=darks[ind], lw=0.15)
            l2 = ax.fill_between(x_data, upper, lower, facecolor=shadow_colors[ind], alpha=0.5, edgecolor=shadow_colors[ind], lw=0.15)
        
        
        lined[labels[ind]] = l1
        shadw[labels[ind]] = l2
        
        if debug:
            for row_id, row in enumerate(M):
                label = "%s - %d" % (labels[ind], row_id)
                l3, = ax.plot(x_data, row, ls=":", label=label)
                
                lined[label] = l3
                shadw[label] = None
        
    
    # threshold
    if not threshold is None: 
        line = np.array([
        [0, threshold],
        [x_data.max(), threshold]])
        
        ax.plot(line[:, 0], line[:, 1], ls=":", c="#252525")
    

    # ----
    
    if show_legend:
        
        leg = ax.legend(loc=loc, scatterpoints=1, ncol=ncol, numpoints=1)

        # for callback
        for legtxt  in leg.get_texts():
            legtxt.set_picker(5)

        
        # inner callback
        def on_pick(event):
            
            legtxt, line, shadow = None, None, None
            
            obj = event.artist
            is_callable = callable(getattr(obj, "get_text", None))
            
            if is_callable:
                
                legtxt = event.artist
                line   = lined[legtxt.get_text()]
                shadow = shadw[legtxt.get_text()]
                
            else:
                
                legtxt = None
                line = event.artist
                
            is_visible = line.get_visible()
            
            # fade a little bit
            if is_visible:
                
                line.set_visible(False)
                if not shadow is None: shadow.set_alpha(0)
                
                if legtxt is not None:
                    legtxt.set_alpha(0.2)
                    
            else:
                
                line.set_visible(True)
                if not shadow is None: shadow.set_alpha(0.5)
                
                if legtxt is not None:
                    legtxt.set_alpha(None)
                    
            fig.canvas.draw()
        
        
        # register callback
        #fig.canvas.callbacks.connect("pick_event", on_pick)
        fig.canvas.mpl_connect("pick_event", on_pick)
    
    
    if output:                                                  # @todo: check if output is a valid path
        #fig.set_size_inches(width, height)
        fig.savefig(output, dpi=300, bbox_inches="tight")
    
    if show_plot:
        plt.show()

    return fig, ax


if __name__ == "__main__":
    
    """
    plot series
    """
    
    n_points = 100
    
    A = np.arange(n_points) * 0.5
    B = np.arange(n_points) * 0.8
    C = np.arange(n_points) * 0.9
    
    series  = (A, B, C)
    labels  = ("Serie A", "Serie B", "Serie C")
    plot_series(series, labels)


    
    """
    plot_shadow_series
    """
    
    from numpy.random import RandomState
    
    rand  = RandomState(100)
    runs  = 4
    steps = 15
    
    line  = np.arange(steps).reshape((1, steps))                    # (1, steps)
    M0_line  = line.repeat(runs, axis=0) * 1                        # (runs, steps)
    M1_line  = line.repeat(runs, axis=0) * 2                        # (runs, steps)
    M2_line  = line.repeat(runs, axis=0) * 4                        # (runs, steps)
    M3_line  = line.repeat(runs, axis=0) * 6                        # (runs, steps)
    M4_line  = line.repeat(runs, axis=0) * 8                        # (runs, steps)
    M5_line  = line.repeat(runs, axis=0) * 10                       # (runs, steps)
    M6_line  = line.repeat(runs, axis=0) * 12                       # (runs, steps)
    M7_line  = line.repeat(runs, axis=0) * 14                       # (runs, steps)
    M8_line  = line.repeat(runs, axis=0) * 16                       # (runs, steps)
    M9_line  = line.repeat(runs, axis=0) * 18                       # (runs, steps)
    M9_line  = line.repeat(runs, axis=0) * 20                       # (runs, steps)
    
    M_noise = rand.normal(loc=1, scale=5, size=(runs, steps))      # (runs, steps), loc=media
    
    M0 = M0_line + M_noise
    M1 = M1_line + M_noise
    M2 = M2_line + M_noise
    M3 = M3_line + M_noise
    M4 = M4_line + M_noise
    M5 = M5_line + M_noise
    M6 = M6_line + M_noise
    M7 = M7_line + M_noise
    M8 = M8_line + M_noise
    M9 = M9_line + M_noise
    
    
    # just for M0
    labels = ["run %d" % run_id for run_id in range(runs)]
    #plot_series(M0, labels)
    
    M_list = (M0, M1, M2, M3, M4, M5, M6, M7, M8, M9)
    labels = ("M0", "M1", "M2", "M3", "M4", "M5", "M6", "M7", "M8", "M9")
    plot_shadow_series(M_list, labels, threshold=10, loc="upper left")
    


