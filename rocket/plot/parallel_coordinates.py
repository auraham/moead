# parallel_coordinates.py
from __future__ import print_function
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.pyplot import rcParams
from rocket.plot.colors import colors, basecolors
    
    
def draw_vertical_lines(ax, ymin, ymax, pins, line_color="#c0c0c0"):
    
    for x in pins:
        
        points = np.array([[x, ymin], [x, ymax]])
        ax.plot(points[:, 0], points[:, 1], c=line_color)

def get_ylims_from_pops(pops, ylims):
    
    ymin, ymax = np.inf, 0
    
    # check for init values
    if ylims[0] is not None and ylims[1] is not None:
        ymin, ymax = ylims
        return ymin, ymax
    
    
    for pop in pops:
        
        pmin = pop.min()    # shorcut for pop.min(axis=by_col).min()
        pmax = pop.max()
        
        if pmin < ymin:
            ymin = pmin
        
        if pmax > ymax:
            ymax = pmax
            
    return ymin, ymax
        
def parallel_coordinates(pops, labels, pin_labels=None, title="", fig=None, ax=None, figname=None, ylabel="", ylims=(None, None), custom_colors=None, loc="upper right", ncol=1, output=None):
    """
    Make a parallel coordinates plot for a list of matrices
    
    
    Input
    pops            list of n (pop_size, m_objs) pop matrices
    labels          list of n labels, one for each pop
    pin_labels      list of m_objs labels, one of each objective (dimension)
    title           str, title
    ylabel          str, label for y axis
    ylims           tuple, where ymin, ymax = ylims; ymin and ymax are the lower and upper boundary for y axis
    custom_colors   tuple, list with colors for pops
    loc             str, position of legend
    ncol            int, number of columns in legend
    output          str, output path
    """
    
    # create plot
    if fig is None and ax is None:
        fig = plt.figure(figname) if figname else plt.figure()
        ax  = fig.add_subplot(111)
    
    # custom colors
    if custom_colors is None:
        custom_colors = tuple(colors)
    
    # define pins
    m_objs = pops[0].shape[1]
    pins = np.arange(0, m_objs, dtype=int)      # (0, 1, ..., m_objs-1)
    
    # draw vertical lines
    ymin, ymax = get_ylims_from_pops(pops, ylims)
    xmin, xmax = pins.min(), pins.max()
    draw_vertical_lines(ax, ymin, ymax, pins)
    
    # set lims for axes
    ax.set_ylim(ymin, ymax)
    ax.set_xlim(xmin, xmax)
    
    # draw x labels
    ax.set_xticks([ i for i in pins])                           # 0, ..., m_objs-1
    ax.set_xticklabels([ "$f_{%d}$" % (i+1,) for i in pins])    # f_1, ..., f_{m_objs} 

    # check for pin labels
    if pin_labels is not None:
        if len(pin_labels) == len(pins):
            rcParams['xtick.labelsize'] = 12
            ax.set_xticklabels(pin_labels)
        else:
            print("the length of pin_labels and pins is different")
    
    # title
    ax.set_title(title)

    # draw y label
    ax.set_ylabel(ylabel)
        
    # for callback
    lined = {}    
    
    # for legend
    legend_lines = []
    legend_labels = []    
    
    # plot pops
    for pid, pop in enumerate(pops):
        
        label = labels[pid]
        
        # a little trick
        # https://matplotlib.org/api/_as_gen/matplotlib.axes.Axes.plot.html?highlight=plot#matplotlib-axes-axes-plot
        # https://stackoverflow.com/questions/11481644/how-do-i-assign-multiple-labels-at-once-in-matplotlib
        x = pins
        y = pop.T       # transpose is needed
        lines = ax.plot(x, y, c=custom_colors[pid])
        
        # save lines and labels for later
        legend_lines.append(lines[0])
        legend_labels.append(label)
        
        # save lines for callback
        lined[label] = lines
        
    # setup legend
    leg = ax.legend(legend_lines, legend_labels, loc=loc, ncol=ncol)
    
    # for callback
    for legtext in leg.get_texts():
        legtext.set_picker(5)
    
    # inner callback
    def on_pick(event):
        
        legtext = event.artist              # artist is a Text instance
                                            # https://matplotlib.org/api/text_api.html?highlight=text#matplotlib.text.Text
        lines = lined[legtext.get_text()]
        
        for line in lines:
            
            is_visible = line.get_visible()
            
            # fade a little bit
            if is_visible:
                
                line.set_visible(False)
                
                if legtext is not None:
                    legtext.set_alpha(0.2)
                    
            else:
                
                line.set_visible(True)
                
                if legtext is not None:
                    legtext.set_alpha(None)

        fig.canvas.draw()
    
    # register callback
    fig.canvas.mpl_connect("pick_event", on_pick)
    
    # adjust
    plt.subplots_adjust(left=0.05, right=0.95)
    
    # save plot
    if output:
        fig.savefig(output, dpi=300)
        print(output)
    
    plt.show()
    
