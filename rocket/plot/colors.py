# colors.py
# @todo: use http://bl.ocks.org/mbostock/5577023 with brewer2mpl for managing color schemes
palette =  [
        "#74CE74",
        "#FFA500",
        "#F7523A",
        "#5A5A5A",
        "#0759AB",
        "#4DA1A9"]

colors = [
        "#74CE74",
        "#FFA500",
        "#F7523A",
        "#5A5A5A",
        "#0759AB",
        "y",
        "m",
        "g",
        "c",
        "#F9FF81",
        
        
        "#7BC4D4", 
        "#F8BD5A", 
        "#F86B5A", 
        "#4CBC9A", 
        "#45754A",
        "#85C68B", 
        "#6A007C",
        "#7BC4D4", 
        "#F8BD5A", 
        "#F86B5A", 
        "#4CBC9A", 
        "#45754A",
        "#85C68B", 
        "#6A007C",
        
        "#7BC4D4", 
        "#F8BD5A", 
        "#F86B5A", 
        "#4CBC9A", 
        "#45754A",
        "#85C68B", 
        "#6A007C",
        
        "#7BC4D4", 
        "#F8BD5A", 
        "#F86B5A", 
        "#4CBC9A", 
        "#45754A",
        "#85C68B", 
        "#6A007C",
        
        ]
        
shadows = [
    "#CBEACB",
    "#F8E1B8",
    "#F9CBC4",
    "#AFAFAF",
    "#95C6F8",
    "#F7EFB6",
    "#F38CA5",
    "#A1E0A1",
    "#A0E5E6",
    "#F0F2BC",
    
    "#B9E0E9",
    "#FBE3BA",
    "#FEC8C2",
    "#9FEAD3",
    "#98BD9B",
    "#C9E4CB",
    "#EAB0F4",
    
]

darks = [
        "#44A244",
        "#B57D17",
        "#943A2D",
        "#423939",
        "#0B3E71",
        "y",
        "m",
        "g",
        "c",
        "#F9FF81",
        
        # @todo: to change for darker colors
        "#7BC4D4", 
        "#F8BD5A", 
        "#F86B5A", 
        "#4CBC9A", 
        "#45754A",
        "#85C68B", 
        "#6A007C"
        
        ]
        
basecolors = {
    "real_front"    : "#7C8577",
    "z_ideal"       : "#ECF037",
    "z_nadir"       : "#F03785",
    "to_keep"       : "#8CDC6B",
    "to_replace"    : "#620F0E",
    "almost_black"  : "#262626",
    "weights"       : "#E4AFC8",
    "weights_hl"    : "#7D2950",
    "cherry_dark"   : "#A91458",
    "cherry_light"  : "#E4AFC8",
    "jitter"        : "#74CE74",
    "jitter_dark"   : "#539453",
}
