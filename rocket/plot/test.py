# test.py
from __future__ import print_function
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import os, sys, argparse

# add lab's path to sys.path
lab_path = os.path.abspath("../../")
sys.path.insert(0, lab_path)

from rocket.plot import plot_series, plot_shadow_series

 

if __name__ == "__main__":
    
    """
    plot series
    """
    
    n_points = 100
    
    A = np.arange(n_points) * 0.5
    B = np.arange(n_points) * 0.8
    C = np.arange(n_points) * 0.9
    
    series  = (A, B, C)
    labels  = ("Serie A", "Serie B", "Serie C")
    #plot_series(series, labels)


    """
    plot_shadow_series
    """
    
    from numpy.random import RandomState
    
    rand  = RandomState(100)
    runs  = 4
    steps = 15
    
    line  = np.arange(steps).reshape((1, steps))                    # (1, steps)
    M0_line  = line.repeat(runs, axis=0) * 1                        # (runs, steps)
    M1_line  = line.repeat(runs, axis=0) * 2                        # (runs, steps)
    M2_line  = line.repeat(runs, axis=0) * 4                        # (runs, steps)
    M3_line  = line.repeat(runs, axis=0) * 6                        # (runs, steps)
    M4_line  = line.repeat(runs, axis=0) * 8                        # (runs, steps)
    M5_line  = line.repeat(runs, axis=0) * 10                       # (runs, steps)
    M6_line  = line.repeat(runs, axis=0) * 12                       # (runs, steps)
    M7_line  = line.repeat(runs, axis=0) * 14                       # (runs, steps)
    M8_line  = line.repeat(runs, axis=0) * 16                       # (runs, steps)
    M9_line  = line.repeat(runs, axis=0) * 18                       # (runs, steps)
    M9_line  = line.repeat(runs, axis=0) * 20                       # (runs, steps)
    
    M_noise = rand.normal(loc=1, scale=5, size=(runs, steps))      # (runs, steps), loc=media
    
    M0 = M0_line + M_noise
    M1 = M1_line + M_noise
    M2 = M2_line + M_noise
    M3 = M3_line + M_noise
    M4 = M4_line + M_noise
    M5 = M5_line + M_noise
    M6 = M6_line + M_noise
    M7 = M7_line + M_noise
    M8 = M8_line + M_noise
    M9 = M9_line + M_noise
    
    
    # just for M0
    labels = ["run %d" % run_id for run_id in range(runs)]
    #plot_series(M0, labels)
    
    M_list = (M0, M1, M2, M3, M4, M5, M6, M7, M8, M9)
    labels = ("M0", "M1", "M2", "M3", "M4", "M5", "M6", "M7", "M8", "M9")
    plot_shadow_series(M_list, labels, threshold=10, loc="upper left")
    

