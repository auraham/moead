# plot_pops.py
from __future__ import print_function
from rocket.plot.colors import colors, basecolors
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

def plot_pops(pops, labels, title=None, figname=None, real_front=None, p=None, plot_surface=False, 
        lower_lims=None, upper_lims=None, output="", show_plot=True, z_ideal=None,
        z_nadir=None, loc="lower left", ncol=2, show_legend=True, custom_colors=None, custom_ms=None, elev=30, azim=45):
    """
    Plot a set of points in 2D and 3D.
    
    pops            list of l point matrices of size (n_points, m_objs)
    labels          list of l string labels, one for each point matrix
    
    title           string, plot title
    figname         string, figure name
    real_front      matrix, Pareto front
    p               int, spacing parameter needed for ploting real_front using a wireframe
    plot_surface    bool, determines if the wireframe will be filled
    lower_lims      (m_objs, ) array, lower bounds of the plot
    upper_lims      (m_objs, ) array, upper bounds of the plot
    output          string, output path
    show_plot       bool, determines if the plot will be shown (useful when the plot will be saved but not shown)
    z_ideal         (m_objs, ) array, z ideal vector
    z_nadir         (m_objs, ) array, z ideal nadir
    loc             string, legend location
    ncol            int, number of legend columns
    show_legend     bool, enable legend
    custom_colors   tuple, list with colors for pops
    """
    
    # check
    m_objs = pops[0].shape[1]
    if m_objs > 3:
        print("plot_pops.py, m_objs: %s > 3, it is not possible to plot" % m_objs)    # @todo: add log support here
        # return fig, ax
        return None, None

    # create plot
    fig = plt.figure(figname) if figname else plt.figure()
    ax  = fig.add_subplot(111) if m_objs == 2 else fig.add_subplot(111, projection="3d")

    # set labels
    ax.set_xlabel("$f_1$")
    ax.set_ylabel("$f_2$")
    if m_objs == 3:
        ax.set_zlabel("$f_3$")

    # set title
    if title:
        ax.set_title(title)

    # set lims
    if not (lower_lims is None or upper_lims is None):
    
        ax.set_xlim(lower_lims[0], upper_lims[0])
        ax.set_ylim(lower_lims[1], upper_lims[1])
        
        if m_objs == 3:
            ax.set_zlim(lower_lims[2], upper_lims[2])
    
    # change panes
    if m_objs == 3:
        ax.w_xaxis.set_pane_color((0.95, 0.95, 0.95, 1.0))
        ax.w_yaxis.set_pane_color((0.95, 0.95, 0.95, 1.0))
        ax.w_zaxis.set_pane_color((0.95, 0.95, 0.95, 1.0))
    
            
    # change the axis title to almostblack
    ax.title.set_color(basecolors["almost_black"])
    
    # callback
    lined = {}      # lines dictionary
    lines = []      # lines list
    
    # real front
    if real_front is not None:
        
        if m_objs == 2:
            
            l, = ax.plot(real_front[:, 0], real_front[:, 1], color=basecolors["real_front"])
            lines.append(l)
            lined["Real Front"] = l
            
        elif m_objs == 3:
            
            l = None
            
            if p:
            
                # plot meshgrid
                X = real_front[:, 0].reshape(p, p)
                Y = real_front[:, 1].reshape(p, p)
                Z = real_front[:, 2].reshape(p, p)
    
                if plot_surface:
                    l = ax.plot_surface(X, Y, Z, rstride=2, cstride=2, color=basecolors["real_front"], alpha=0.2)
                else:
                    l = ax.plot_wireframe(X, Y, Z, rstride=2, cstride=2, color=basecolors["real_front"])
                    
            else:
                
                # plot scatter
                l = ax.scatter(real_front[:, 0], real_front[:, 1], real_front[:, 2], label="Real Front", 
                        facecolor=basecolors["real_front"], c=basecolors["real_front"], 
                        edgecolor=basecolors["almost_black"], linewidth=0.15)
                    
            lines.append(l)
            lined["Real Front"] = l
    
    # custom colors
    if custom_colors is None:
        custom_colors = tuple(colors)       # @todo: maybe tuple() is not needed
    
    # custom marker sizes
    if custom_ms is None:
        custom_ms = tuple([6 for i in range(len(pops))])
    
    
    
    # pops
    for i, pop in enumerate(pops):
        
        if m_objs == 2:
            #l = ax.scatter(pop[:, 0], pop[:, 1], label=labels[i], facecolor=custom_colors[i], 
            #            edgecolor=basecolors["almost_black"], linewidth=0.15, zorder=5)
            
            l, = ax.plot(pop[:, 0], pop[:, 1], label=labels[i], c=custom_colors[i], 
                        mec=basecolors["almost_black"], ls="none", marker="o", markeredgewidth=0.15, ms=custom_ms[i])
            
            lines.append(l)
            lined[labels[i]] = l
            
        elif m_objs == 3:
            l, = ax.plot(pop[:, 0], pop[:, 1], pop[:, 2], label=labels[i], c=custom_colors[i], 
                        mec=basecolors["almost_black"], ls="none", marker="o", markeredgewidth=0.15, ms=custom_ms[i])
            lines.append(l)
            lined[labels[i]] = l
    
    
    # z_ideal
    if z_ideal is not None:
        
        l = None
        z = z_ideal.reshape((1, m_objs))
        
        if m_objs == 2:
            l, = ax.plot(z[:, 0], z[:, 1], label="Z ideal", mec=basecolors["almost_black"], 
                    c=basecolors["z_ideal"], ls="none", marker="*", markeredgewidth=0.15)
            
        elif m_objs == 3:
            l, = ax.plot(z[:, 0], z[:, 1], z[:, 2], label="Z ideal", mec=basecolors["almost_black"], 
                    c=basecolors["z_ideal"], ls="none", marker="*", markeredgewidth=0.15)

        lines.append(l)
        lined["Z ideal"] = l
        
        
    # z_nadir
    if z_nadir is not None:
        
        l = None
        z = z_nadir.reshape((1, m_objs))
        
        if m_objs == 2:
            l, = ax.plot(z[:, 0], z[:, 1], label="Z nadir", mec=basecolors["almost_black"], 
                    c=basecolors["z_nadir"], ls="none", marker="*", markeredgewidth=0.15)
            
        elif m_objs == 3:
            l, = ax.plot(z[:, 0], z[:, 1], z[:, 2], label="Z nadir", mec=basecolors["almost_black"], 
                    c=basecolors["z_nadir"], ls="none", marker="*", markeredgewidth=0.15)

        lines.append(l)
        lined["Z nadir"] = l
    
    
    # legend
    if show_legend:
        
        leg = ax.legend(loc=loc, scatterpoints=1, ncol=ncol, numpoints=1)
        
        # for callback
        for legtxt, __ in zip(leg.get_texts(), lines):
            legtxt.set_picker(5)
        
        
    # inner callback
    def on_pick(event):
        
        legtxt, line = None, None
        
        obj = event.artist
        is_callable = callable(getattr(obj, "get_text", None))
        
        if is_callable:
            
            legtxt = event.artist
            line = lined[legtxt.get_text()]
            
        else:
            
            legtxt = None
            line = event.artist
            
        is_visible = line.get_visible()
        
        # fade a little bit
        if is_visible:
            
            line.set_visible(False)
            
            if legtxt is not None:
                legtxt.set_alpha(0.2)
                
        else:
            
            line.set_visible(True)
            
            if legtxt is not None:
                legtxt.set_alpha(None)
                
        fig.canvas.draw()
    
    if m_objs == 3:
        ax.view_init(elev, azim)
        
    
    # register callback only if legend is visible
    if show_legend:
        #fig.canvas.callbacks.connect("pick_event", on_pick)
        fig.canvas.mpl_connect("pick_event", on_pick)

    # save before plt.show
    if output:                                                  # @todo: check if output is a valid path
        #fig.set_size_inches(width, height)
        fig.savefig(output, dpi=300, bbox_inches="tight")
    
    
    if show_plot:
        plt.show()


    return fig, ax
    
def plot_real_front(real_front, p=None, title=None, figname=None, plot_surface=False, 
        lower_lims=None, upper_lims=None, output="", show_plot=True, width=12, height=6, z_ideal=None,
        z_nadir=None, loc="lower left", ncol=2):
    """
    Plot a set of points in 2D and 3D.
    
    real_front      matrix, Pareto front
    p               int, spacing parameter needed for ploting real_front using a wireframe
    title           string, plot title
    figname         string, figure name
    plot_surface    bool, determines if the wireframe will be filled
    lower_lims      (m_objs, ) array, lower bounds of the plot
    upper_lims      (m_objs, ) array, upper bounds of the plot
    output          string, output path
    show_plot       bool, determines if the plot will be shown (useful when the plot will be saved but not shown)
    width           int, plot width
    height          int, plot height
    z_ideal         (m_objs, ) array, z ideal vector
    z_nadir         (m_objs, ) array, z ideal nadir
    loc             string, legend location
    ncol            int, number of legend columns
    """

    
    # check
    m_objs = real_front.shape[1]
    if m_objs > 3:
        print("m_objs: %s > 3, it is not possible to plot" % m_objs)    # @todo: add log support here

    # create plot
    fig = plt.figure(figname) if figname else plt.figure()
    ax  = fig.add_subplot(111) if m_objs == 2 else fig.add_subplot(111, projection="3d")

    # set labels
    ax.set_xlabel("$f_1$")
    ax.set_ylabel("$f_2$")
    if m_objs == 3:
        ax.set_zlabel("$f_3$")

    # set title
    if title:
        ax.set_title(title)

    # set lims
    if not (lower_lims is None or upper_lims is None):
    
        ax.set_xlim(lower_lims[0], upper_lims[0])
        ax.set_ylim(lower_lims[1], upper_lims[1])
        
        if m_objs == 3:
            ax.set_zlim(lower_lims[2], upper_lims[2])
            
    # change the axis title to almostblack
    ax.title.set_color(basecolors["almost_black"])
    
    # callback
    lined = {}      # lines dictionary
    lines = []      # lines list
    
    # real front
    if m_objs == 2:
        
        l, = ax.plot(real_front[:, 0], real_front[:, 1], color=basecolors["real_front"])
        lines.append(l)
        lined["Real Front"] = l
        
    elif m_objs == 3:
        
        l = None
        
        if p:
        
            # plot meshgrid
            X = real_front[:, 0].reshape(p, p)
            Y = real_front[:, 1].reshape(p, p)
            Z = real_front[:, 2].reshape(p, p)

            if plot_surface:
                l = ax.plot_surface(X, Y, Z, rstride=2, cstride=2, color=basecolors["real_front"], alpha=0.2)
            else:
                l = ax.plot_wireframe(X, Y, Z, rstride=2, cstride=2, color=basecolors["real_front"])
                
        else:
            
            # plot scatter
            l = ax.scatter(real_front[:, 0], real_front[:, 1], real_front[:, 2], label="Real Front", 
                    facecolor=basecolors["real_front"], c=basecolors["real_front"], 
                    edgecolor=basecolors["almost_black"], linewidth=0.15)
                
        lines.append(l)
        lined["Real Front"] = l
    
    
    # z_ideal
    if z_ideal is not None:
        
        l = None
        z = z_ideal.reshape((1, m_objs))
        
        if m_objs == 2:
            l, = ax.plot(z[:, 0], z[:, 1], label="Z ideal", mec=basecolors["almost_black"], 
                    c=basecolors["z_ideal"], ls="none", marker="*", markeredgewidth=0.15)
            
        elif m_objs == 3:
            l, = ax.plot(z[:, 0], z[:, 1], z[:, 2], label="Z ideal", mec=basecolors["almost_black"], 
                    c=basecolors["z_ideal"], ls="none", marker="*", markeredgewidth=0.15)

        lines.append(l)
        lined["Z ideal"] = l
        
        
    # z_nadir
    if z_nadir is not None:
        
        l = None
        z = z_nadir.reshape((1, m_objs))
        
        if m_objs == 2:
            l, = ax.plot(z[:, 0], z[:, 1], label="Z nadir", mec=basecolors["almost_black"], 
                    c=basecolors["z_nadir"], ls="none", marker="*", markeredgewidth=0.15)
            
        elif m_objs == 3:
            l, = ax.plot(z[:, 0], z[:, 1], z[:, 2], label="Z nadir", mec=basecolors["almost_black"], 
                    c=basecolors["z_nadir"], ls="none", marker="*", markeredgewidth=0.15)

        lines.append(l)
        lined["Z nadir"] = l
    
    #leg = ax.legend(loc=loc, scatterpoints=1, ncol=ncol, numpoints=1)
    
    # for callback
    """
    for legtxt, __ in zip(leg.get_texts(), lines):
        legtxt.set_picker(5)
    """
        
    # inner callback
    def on_pick(event):
        
        legtxt, line = None, None
        
        obj = event.artist
        is_callable = callable(getattr(obj, "get_text", None))
        
        if is_callable:
            
            legtxt = event.artist
            line = lined[legtxt.get_text()]
            
        else:
            
            legtxt = None
            line = event.artist
            
        is_visible = line.get_visible()
        
        # fade a little bit
        if is_visible:
            
            line.set_visible(False)
            
            if legtxt is not None:
                legtxt.set_alpha(0.2)
                
        else:
            
            line.set_visible(True)
            
            if legtxt is not None:
                legtxt.set_alpha(None)
                
        fig.canvas.draw()
    
    if m_objs == 3:
        ax.view_init(30, 45)
        
    
    # register callback
    #fig.canvas.mpl_connect("pick_event", on_pick)
    
    
        
    if output:                                                  # @todo: check if output is a valid path
        #fig.set_size_inches(width, height)
        fig.savefig(output, dpi=300, bbox_inches="tight")
    
    
    if show_plot:
        plt.show()


    return fig, ax

if __name__ == "__main__":
    
    from numpy.random import RandomState
    from rocket.fronts import get_real_front
    from rocket.helpers import get_n_genes
    from rocket.problems import evaluate_mop
    
    rand = RandomState(100)
    
    mop_name        = "min-dtlz1"
    pop_size        = 100
    m_objs          = 3
    mop_params      = {"name": mop_name}
    n_genes, __, __ = get_n_genes(mop_name, m_objs)
    
    scale = -100
    a = rand.rand(pop_size, n_genes) #* scale
    b = rand.rand(pop_size, n_genes) #* scale
    c = rand.rand(pop_size, n_genes) #* scale
    
    a[:, 2:] = 0.04977
    
    A = evaluate_mop(a, m_objs, mop_params)
    B = evaluate_mop(b, m_objs, mop_params)
    C = evaluate_mop(c, m_objs, mop_params)
    
    pops            = (A, B, C)
    labels          = ("Pop A", "Pop B", "Pop C")
    n_genes, __, __ = get_n_genes(mop_name, m_objs)
    real_front, p   = get_real_front(mop_name, m_objs, n_genes)
    plot_pops(pops, labels, real_front=real_front, p=p, output="%s.png" % mop_name)
    
            
        
