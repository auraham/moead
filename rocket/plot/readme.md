# About this module

This module contains functions to plot approximations (set of points) in 2D and 3D:

- `plot_pops(pops, labels, ...)` allows you to plot a list of populations.

**Example**

```python
from numpy.random import RandomState
from rocket.fronts import get_real_front
from rocket.helpers import get_n_genes
from rocket.problems import evaluate_mop
from rocket.plot import plot_pops

rand = RandomState(100)

mop_name        = "min-dtlz1"
pop_size        = 100
m_objs          = 3
mop_params      = {"name": mop_name}
n_genes, __, __ = get_n_genes(mop_name, m_objs)

scale = -100
a = rand.rand(pop_size, n_genes) #* scale
b = rand.rand(pop_size, n_genes) #* scale
c = rand.rand(pop_size, n_genes) #* scale

a[:, 2:] = 0.04977

A = evaluate_mop(a, m_objs, mop_params)
B = evaluate_mop(b, m_objs, mop_params)
C = evaluate_mop(c, m_objs, mop_params)

pops            = (A, B, C)
labels          = ("Pop A", "Pop B", "Pop C")
n_genes, __, __ = get_n_genes(mop_name, m_objs)
real_front, p   = get_real_front(mop_name, m_objs, n_genes)
plot_pops(pops, labels, real_front=real_front, p=p, output="%s.png" % mop_name)
```
**Result**

![Result](min-dtlz1.png)