# statistical_analysis.py
# Utilities for conducting a statistical analysis
import numpy as np
import pandas as pd
import scipy.stats as ss
import statsmodels.api as sa
import scikit_posthocs as sp
from scipy.stats import chi2
import matplotlib.pyplot as plt
from matplotlib.pyplot import rcParams
import matplotlib.patches as patches
from numpy.random import RandomState

# ---- plotting ----

def _load_rcparams(figsize=None, for_paper=False):
    """
    Load a custom rcParams dictionary
    """
    
    rcParams['axes.titlesize']  = 14            # title
    rcParams['axes.labelsize']  = 12            # $f_i$ labels
    rcParams['xtick.color']     = "#474747"     # ticks gray color
    rcParams['ytick.color']     = "#474747"     # ticks gray color
    rcParams['xtick.labelsize'] = 12            # ticks size
    rcParams['ytick.labelsize'] = 10            # ticks size
    rcParams['legend.fontsize'] = 12            # legend
    rcParams['legend.fontsize'] = 12            # legend

    if isinstance(figsize, tuple):
        rcParams['figure.figsize'] = figsize

def _boxplot(data, labels, tab_summary=None, control="", use_jitter=True, rotation="horizontal", ylabel="", save=True, ylims=(-0.2, 1.2)):
        
    rand = RandomState(42)
    
    # split columns
    n_rows, n_cols = data.shape
    cols = [data[:, i] for i in range(n_cols)]
    
    _load_rcparams((9, 5))
    
    fig = plt.figure()
    ax = fig.add_subplot(111)
    
    bp = ax.boxplot(cols, patch_artist=True)
    
    # customize boxplot
    # http://blog.bharatbhole.com/creating-boxplots-with-matplotlib/
    for bi, box in enumerate(bp["boxes"]):
        box.set(color="#262626", linewidth=1)
        box.set(facecolor="#ffffff")
        
    for whisker in bp["whiskers"]:
        whisker.set(color="#262626", linewidth=1, ls="-")
    
    for cap in bp["caps"]:
        cap.set(color="#262626", linewidth=1)
        
    for bmed in bp["medians"]:
        bmed.set(color="#A91458", linewidth=1)
    
    #import ipdb; ipdb.set_trace()
    for flier in bp["fliers"]:
        flier.set(marker="o", ms=4, markerfacecolor="#c0c0c0")

    # plot points around boxplots
    for row_id, row_points in enumerate(data):
        
        y = row_points                          # eg [0.90, 0.91, ..., 0.99] igd values
        x = np.ones((len(y), )) * (row_id+1)    # eg [   1,    1, ...,    1] position on x axis
        
        # add jitter mean:0, sd:0.05
        # https://stackoverflow.com/questions/29779079/adding-a-scatter-of-points-to-a-boxplot-using-matplotlib
        if use_jitter:
            jitter = rand.normal(0, 0.05, size=len(y))
            x = x + jitter
        
            ax.scatter(x, y, facecolor="#74CE74", edgecolor="#262626", linewidth=0.15, s=10)

    # setup xticks
    ax.set_xticks([ i for i in range(1, n_cols+1)])             # 1, 2, ...
    ax.set_xticklabels(labels, rotation=rotation)     # 1: "WS", 2: "TE", ...


    if not tab_summary is None:
        
        if control:
    
            col_labels = []
            
            for i, method in enumerate(labels):
                
                new_label = ""
                key = "%s-%s" % (control, method)
                
                if key in tab_summary:
                    new_label = "%s (%s)" % (method, tab_summary[key])
                else:
                    new_label = method
                    
                col_labels.append(new_label)
                
            # setup xticks
            ax.set_xticks([ i for i in range(1, n_cols+1)])
            ax.set_xticklabels(col_labels, rotation=rotation)
            
        else:
            print("Use control and tab_summary")



    # set y label
    ax.set_ylabel(ylabel)
    
    # set lims
    ax.set_ylim(ylims[0], ylims[1])

    # setup margins
    plt.subplots_adjust(left=0.1, 
                        right=0.92, 
                        top=0.9, 
                        bottom=0.1, 
                        hspace=0.45,
                        wspace=0.1)
    
    # save plot
    if save:
        output = "boxplot_py.png"
        fig.savefig(output, dpi=300)
        print(output)
            
    plt.show()

# ---- display information ---

def _print_posthoc_table(res):
    """
    Prints a table of the output of the post hoc procedure
    
    Example
    
            type 1  type 2  type 3  type 4  
    type 2  1.00000 -       -       -       
    type 3  0.00376 0.00188 -       -       
    type 4  0.00103 0.00052 1.00000 -       
    type 5  1.00000 1.00000 0.00488 0.00133
    """
    
    # it is a square matrix (dataframe)
    n, _ = res.shape
    data = res.values       # (n, n) np.array
    lbls = res.index        # Index(['type 1', 'type 2', 'type 3', 'type 4', 'type 5'], dtype='object')
    
    # print header
    title_len = np.max([7] + [len(lbl) for lbl in lbls]) + 1        # 7 is the length of a number, like 1.00000
    items = [" "] + [item for item in lbls[:n-1]]                   # last column is ignored
    fmt_items = [item.ljust(title_len) for item in items ]
    print("".join(fmt_items))
    
    for row in range(1, n):
        
        row_lbl = lbls[row].ljust(title_len)
        print(row_lbl, end="")
        
        for col in range(0, n-1):
            
            if col < row:
                value = data[row, col]
                str_value = "%.5f" % value
                print(str_value.ljust(title_len), end="")
            else:
                print("-".ljust(title_len), end="")
                
        print("")

def _print_posthoc_control(res, control):
    """
    Prints the row of the output of the post hoc procedure
    regarding a control method
    
    Example (type 4 is the control method)
    
            type 1  type 2  type 3  type 5  
    type 4  0.00103 0.00052 1.00000 0.00133
    """
    
    # it is a square matrix (dataframe)
    n, _ = res.shape
    data = res.values       # (n, n) np.array
    lbls = res.index        # Index(['type 1', 'type 2', 'type 3', 'type 4', 'type 5'], dtype='object')
    
    # is control in lbls?
    if not control in lbls:
        print("control method (%s) is not in the list (%s)" % (control, lbls))
        return
        
    # filter labels
    lbls_fil = [item for item in lbls if item != control]
    
    # print header
    title_len = np.max([7] + [len(lbl) for lbl in lbls_fil]) + 1        # 7 is the length of a number, like 1.00000
    items = [" "] + [item for item in lbls_fil[:n-1]]                   # last column is ignored
    fmt_items = [item.ljust(title_len) for item in items ]
    print("".join(fmt_items))
    
    
    
    # row of interest
    # eg:
    #   res.loc[control]                                                                                                                                     
    #   type 1    0.001031
    #   type 2    0.000521
    #   type 3    1.000000
    #   type 4   -1.000000
    #   type 5    0.001334
    #   Name: type 4, dtype: float64
    values = res.loc[control]
    
    # row of interest (without control)
    # eg, type 4 is control:
    #   res.loc[control]                                                                                                                                     
    #   type 1    0.001031
    #   type 2    0.000521
    #   type 3    1.000000
    #   type 5    0.001334
    #   Name: type 4, dtype: float64
    values = res.loc[control, lbls_fil]
    
    # print row (without control)
    print(control.ljust(title_len), end="")
    print(" ".join(["%.5f" % val for val in values]))
    
def _print_posthoc_result(res, control=None):
    
    if control:
        _print_posthoc_control(res, control)
    else:
        _print_posthoc_table(res)
    
# ---- store results as dictionaries ----

def _get_posthoc_summary(medians, posthoc_res, control, alpha=0.05, min_is_better=False):
    """
    Return the summary of the post hoc test
    
    Example (type 4 is the control method):
    
            type 1  type 2  type 3  type 5  
    type 4  0.00103 0.00052 1.00000 0.00133
    
    We return a dictionary like this:
    
    tab_pvalues = {
        'type 4-type 1': 0.00103,       type 4 and type 1 are different since 0.00103 < alpha = 0.05
        'type 4-type 2': 0.00052,       type 4 and type 2 are different since 0.00052 < alpha = 0.05
        'type 4-type 3': 1.00000,       type 4 and type 3 are similar   since 1.00000 > alpha = 0.05
        'type 4-type 5': 0.00133,       type 4 and type 5 are different since 0.00133 < alpha = 0.05
    }
    
    and this (min_is_better=False)
    
    tab_summary = {
        'type 4-type 1': '+',           type 4 is superior than type 1 since median['type 4'] > median['type 1']
        'type 4-type 2': '-',           type 4 is inferior than type 2 since median['type 4'] < median['type 2']
        'type 4-type 3': '~',           type 4 is similar  to   type 3 since tab_pvalue['type 4-type 3'] > alpha = 0.05
        'type 4-type 5': '+',           type 4 is superior than type 5 since median['type 4'] > median['type 5']
    }
    """
    
    # it is a square matrix (dataframe)
    n, _ = posthoc_res.shape
    data = posthoc_res.values       # (n, n) np.array
    lbls = posthoc_res.index        # Index(['type 1', 'type 2', 'type 3', 'type 4', 'type 5'], dtype='object')
    
    # is control in lbls?
    if not control in lbls:
        print("control method (%s) is not in the list (%s)" % (control, lbls))
        return
        
    # filter labels
    lbls_fil = [item for item in lbls if item != control]
    
    # output
    tab_pvalues = {}
    tab_summary = {}
    tab_tags = {key:"" for key in lbls}
    
    for method in lbls_fil:
        
        dict_key = "%s-%s" % (control, method)          # key like 'type 4-type 1'
        p_value  = posthoc_res.loc[control, method]     # p-value
        conclusion = "~"                                # control and method are similar (by default)
        
        if p_value < alpha:
            
            # control and method are different
            if min_is_better:
                
                # control is superior (+) than method if medians[control] < medians[method]
                conclusion = "+" if medians[control] < medians[method] else "-"
            
            else:
                
                # control is superior (+) than method if medians[control] > medians[method]
                conclusion = "+" if medians[control] > medians[method] else "-"
        
        # update tables
        tab_pvalues[dict_key] = p_value
        tab_summary[dict_key] = conclusion
        tab_tags.update({method: conclusion})
        
    return tab_summary, tab_pvalues, tab_tags
    
# ---- api ----

def statistical_analysis(df_pi, control, alpha=0.05, min_is_better=True, display_info=False, display_boxplot=False):
    """
    Conduct the following statistical analysis
    
    1. Read csv_filename, each column is a group (algorithm)
    2. Compare the groups using the non-parametric Kruskal Wallis test. This test will tell us
       whether the groups are different. 
       If there is a significant difference among the groups, then a posthoc test (Conover) is performed.
       Otherwise, the statistical analysis is done.
    3. Posthoc test: Use the Conover posthoc test with the Bonferroni correction.
       This test compares the control method against the rest of the groups. This test will tell us
       whether the control method is different to the compared method. If so, we assign a tag +, - to the methods
       regarding their medians. The following table is the outcome of the posthoc test:
       
       Example (type 4 is the control method):
    
            type 1  type 2  type 3  type 5  
        type 4  0.00103 0.00052 1.00000 0.00133

       We return a dictionary like this:

        tab_pvalues = {
            'type 4-type 1': 0.00103,       type 4 and type 1 are different since 0.00103 < alpha = 0.05
            'type 4-type 2': 0.00052,       type 4 and type 2 are different since 0.00052 < alpha = 0.05
            'type 4-type 3': 1.00000,       type 4 and type 3 are similar   since 1.00000 > alpha = 0.05
            'type 4-type 5': 0.00133,       type 4 and type 5 are different since 0.00133 < alpha = 0.05
        }

       We then assign the tags '+', '-', '~', to each pair (here, min_is_better=False):

        tab_summary = {
            'type 4-type 1': '+',           type 4 is superior than type 1 since median['type 4'] > median['type 1']
            'type 4-type 2': '-',           type 4 is inferior than type 2 since median['type 4'] < median['type 2']
            'type 4-type 3': '~',           type 4 is similar  to   type 3 since tab_pvalue['type 4-type 3'] > alpha = 0.05
            'type 4-type 5': '+',           type 4 is superior than type 5 since median['type 4'] > median['type 5']
        }     
    
    
    Input
    df_pi                   str, pd.DataFrame, each column represents the performance of an algorithm regarding an indicator.
    alpha                   float, Significance level
    min_is_better           bool, If True, lower values are preferred. It depends on the indicator employed in the csv_filename.
                            If you use IGD, then min_is_better=True. If you use hypervolume, then min_is_better=False.
    display_info            bool, Prints information, like p-values, throughout the process. It is useful for debugging.
    display_boxplot         bool, Shows a boxplot at the end of the test.
    
    Output
    tab_summary             dict, Summary of the posthoc test. It contains the symbols '+', '-', '~' to denote whether the control
                            method is superior, inferior or similar to the compared method.
                            Example:
                                tab_summary = {
                                    'type 4-type 1': '+',
                                    'type 4-type 2': '-',
                                    'type 4-type 3': '~',
                                    'type 4-type 5': '+',
                                }
                                
                            If the posthoc test is not performed, then tab_summary is an empty dictionary.
                                
    tab_pvalues             dict, p-values of the posthoc test. 
                            Example:
                                tab_pvalues = {
                                    'type 4-type 1': 0.00103,
                                    'type 4-type 2': 0.00052,
                                    'type 4-type 3': 1.00000,
                                    'type 4-type 5': 0.00133,
                                }
                                
                            If the posthoc test is not performed, then tab_values is an empty dictionary.
    
    tab_tags                dict, Similar to tab_summary. This dictionary contains tags ('+', '-', '~') for each method 
                            and an empty tag ("") for the control method. This dictionary is useful for creating tables or boxplots
                            Example:
                                tab_summary = {
                                    'type 1': '+',
                                    'type 2': '-',
                                    'type 3': '~',
                                    'type 4': '', 
                                    'type 5': '+',
                                }
    """
    
    # Output (default values)
    tab_summary = {}
    tab_pvalues = {}
    tab_tags = {}
    are_groups_different = False
    
    """
    # 1. Read CSV
    # read using pandas, this way, we can get the name of each column
    # df_pi: dataframe_performance_indicator
    df_pi = pd.read_csv(csv_filename, sep=";")
    
    # Optional: use short colum names
    # This replaces long column names with short ones
    if not short_colnames is None:
        df_pi = df_pi.rename(short_colnames, axis="columns", inplace=False)
    """
    
    # 2. Get data and column names
    X = df_pi.values
    cols = tuple(df_pi.columns)
    
    # 3. Prepare observations (obs) and labels (lbls)
    # Here, we reshape the data (df_pi) as a vector, and assing a label to each value
    
    obs = X.flatten()                   # flatten observations 
    n_rows, n_cols = X.shape            # n_rows: number of observations of each group (algorithm)
                                        # n_cols: number of groups (algorithms) in X

    lbls = cols * n_rows                # assign labels
                                        # we repeat [col_name_1, col_name_2, ..., col_name_l] n_rows times, 
                                        # one for each row in X

    data = {"obs": obs, "lbls": lbls}
    df = pd.DataFrame(data, columns=["obs", "lbls"])

    # 4. Kruskal Wallis test
    # This is the same as
    # H_value, p_value = ss.kruskal(X[:, 0], X[:, 1], X[:, 2], ..., X[:, n])
    H_value, p_value = ss.kruskal(*[X[:, col] for col in range(n_cols)])
    
    # compute the critical point of the chi-square distribution X_{0.95, 4}
    cp = chi2.isf(q=alpha, df=n_cols-1)     # inverse survival function

    # 5. Is the a difference among groups?
    if H_value > cp:
        # From [Ostertagova]:
        #   If the H_value is larger than the critical point of the chi-square distribution
        #   then the null hypothesis is rejected, meaning that there is a significant difference between the groups
        are_groups_different = True

    # 6. Display info
    if display_info:
        print("p-value:          %.4f" % p_value);
        print("H*-statistic:     %.4f" % H_value);
        print("critical point:   %.4f" % cp);
        print("Are groups diff?: %s"   % are_groups_different_pvalue)
    
    # 7. Continue with posthoc test?
    if not are_groups_different:
        
        tab_tags = {key:"" for key in cols}
        
        return tab_summary, tab_pvalues, tab_tags
    
    # 8. Post hoc test
    # We use the Conover test as recommended in [Ostertagova]
    # We also use the Bonferroni correction as recommended in [Pohlert19]
    posthoc_res = sp.posthoc_conover(df, val_col="obs", group_col="lbls", p_adjust="bonferroni")

    # 9. Display info
    if display_info:
        _print_posthoc_result(posthoc_res); print("")
        _print_posthoc_result(posthoc_res, control=control)

    # 10. Get summary of the posthoc test
    medians = df_pi.median(axis=0)     # median of the performance indicator value of each method
    tab_summary, tab_pvalues, tab_tags = _get_posthoc_summary(medians, posthoc_res, control, alpha, min_is_better)
    
    # 11. Optional: display a boxplot
    # Create a boxplot for each column in X
    if display_boxplot:
        _boxplot(X, cols, tab_summary, control, use_jitter=False, save=False)
    
    return tab_summary, tab_pvalues, tab_tags
    
def statistical_analysis_from_csv(csv_filename, control, alpha=0.05, min_is_better=True, display_info=False, delimiter=";", short_colnames=None, display_boxplot=False):
   
    # 1. Read CSV
    # read using pandas, this way, we can get the name of each column
    # df_pi: dataframe_performance_indicator
    df_pi = pd.read_csv(csv_filename, sep=";")
    
    # Optional: use short colum names
    # This replaces long column names with short ones
    if not short_colnames is None:
        df_pi = df_pi.rename(short_colnames, axis="columns", inplace=False)
    
    # 2. Call main function
    return statistical_analysis(df_pi, control, alpha, min_is_better, display_info, display_boxplot)
   
