# test_statistical_analysis.py
from __future__ import print_function
import numpy as np
import pandas as pd
from statistical_analysis import statistical_analysis
from statistical_analysis import statistical_analysis_from_csv

if __name__ == "__main__":
    
    # ---- approach 1: from csv ----
    
    short_colnames = {
        "# moead_norm_v1-ws-norm-true": "ws",
        "moead_norm_v1-te-norm-true": "te",
        "moead_norm_v1-asf-norm-true": "asf",
        "moead_norm_v1-pbi-norm-true": "pbi",
        "moead_norm_v1-ipbi-norm-true": "ipbi",
        "moead_norm_v1-vads-norm-true" : "vads",
        "mss-dci_norm_hyp_mean-divs-8-25-mean-10-ps-normal-ss-gra-norm-true": "mss",
    }

    csv_filename = "hv_dtlz1_m_objs_3_baselines_case_a.csv"
    control = "mss"
    
    tab_summary, tab_pvalues, tab_tags = statistical_analysis_from_csv(
                                                            csv_filename, 
                                                            control, 
                                                            alpha=0.05, 
                                                            min_is_better=False, 
                                                            display_info=False, 
                                                            delimiter=";", 
                                                            short_colnames=short_colnames,
                                                            display_boxplot=True)
    
    # ---- approach 2: from dataframe ----
    
    # read csv
    df_pi = pd.read_csv(csv_filename, sep=";")
    df_pi = df_pi.rename(short_colnames, axis="columns", inplace=False)
    
    tab_summary, tab_pvalues, tab_tags = statistical_analysis(
                                                            df_pi, 
                                                            control, 
                                                            alpha=0.05, 
                                                            min_is_better=False, 
                                                            display_info=False, 
                                                            display_boxplot=False)

    
