# polymutation.py
import numpy as np
import math

def polymutation(x, prob, eta, lbound, ubound, rand):
    
    mutated = x.copy()
    n_genes = len(x)
    
    for n_gene in range(n_genes):
        
        if rand.rand() <= prob:
            
            y  = x[n_gene]              # n-th gene
            
            yl = lbound[n_gene]         # n-th gene lower bound 
            yu = ubound[n_gene]         # n-th gene upper bound 
            
            delta1 = (y-yl) / (yu-yl)
            delta2 = (yu-y) / (yu-yl)
            deltaq = 0
            
            rnd = rand.rand()
            
            
            mut_pow = 1.0 / (eta+1.0)
            
            if rnd <= 0.5:
                
                xy      = 1.0 - delta1
                val     = 2.0 * rnd + (1.0 - 2.0*rnd) * (math.pow(xy, (eta+1.0)))
                deltaq  = math.pow(val, mut_pow) - 1.0
                
            else:
                
                xy      = 1.0 - delta2
                val     = 2.0 * (1.0 - rnd) + 2.0 * (rnd-0.5) * (math.pow(xy, (eta+1.0)))
                deltaq  = 1.0 - (math.pow(val, mut_pow))
                
            # n-th gene mutation
            y = y + deltaq * (yu-yl)
            
            # boundaries 
            if y < yl:
                y = yl
                
            if y > yu:
                y = yu
                
            # assigment
            mutated[n_gene] = y
            
    return mutated.copy()
