# m2m.py
from __future__ import print_function
import numpy as np

def annealrep(x1, x2, pmua, delta, lbound, ubound, rand):
    
    # crossover
    # always apply crossover
    dim_x   = len(x1)                           # chrom lenght
    r1      = rand.rand()                       # random number
    r2      = rand.rand()                       # random number
    rnd     = 2 * (1-r1**(-delta)) * (r2-0.5)   # step length
    x_cld   = x1 + rnd * (x1-x2)                # new individual

    # prior mutation
    # check boundaries; if one variable of the new individual x_cld
    # is out of boundaries, it is replaced by a random number
    for i in range(dim_x):
        
        # lower bound
        if x_cld[i] < lbound[i]:
            
            rnd = rand.rand()
            x_cld[i] = lbound[i] + 0.5*rnd*(x1[i] - lbound[i])
        
        # upper bound
        elif x_cld[i] > ubound[i]:
            
            rnd = rand.rand()
            x_cld[i] = ubound[i] - 0.5*rnd*(ubound[i]-x1[i])
            
    
    # selection of indices
    lst     = np.arange(dim_x, dtype=int)[rand.random_sample((dim_x, )) <= pmuta]   # indices (variables) to undergo mutation
    length  = len(lst)                                                              # note that lst could be an empty list
    
    if length == 0:
        length = 1
        ind = floor(rand.rand()*dim_x)
        lst = np.array([ind], dtype=int)
        
    # mutation (finally)
    # only the variables on lst will be mutated
    for j in range(length):
        
        yl = lbound[j]          # lower bound j-th variable
        yu = ubound[j]          # lower bound j-th variable
        y  = x_cld[lst[j]]      # j-th variable to undergo mutation
        
        rnd = 0.3 * (rand.rand() - 0.5) * (1.0-rand.rand()**(-delta))
        
        y   = y + rnd * (yu-yl) # mutated variable
        
        if y > yu:
            y = yu - 0.5 * rand.rand() * (yu - x_cld[lst[j]])

        elif y < yl:
            y = yl + 0.5 * rand.rand() * (x_cld[lst[j]] - yl)
    
        x_cld[lst[j]] = y
        
    return x_cld.copy()
    
    
