# test_genetics.py
from crossover import crossover
from mutation import mutation
import numpy as np
from numpy.random import RandomState


def pretty_string(x):
    
    return " ".join([ "%.4f" % i for i in x ])
    

if __name__ == "__main__":
    
    n_genes = 10
    rand    = RandomState(100)
    lbound  = np.zeros((n_genes, ))
    ubound  = np.ones((n_genes, ))
    
    x1 = rand.random_sample((n_genes, ))
    x2 = rand.random_sample((n_genes, ))
    
    # crossover
    parents = (x1, x2)
    params  = {"name": "sbx", "prob": 0.8, "eta": 20}
    c1, c2  = crossover(parents, lbound, ubound, params, rand)
    
    # mutation
    parents = (x1, )
    params  = {"name": "polymutation", "prob": 0.1, "eta": 20}
    y1,     = mutation(parents, lbound, ubound, params, rand)
    
    # output
    print("p1", pretty_string(x1))
    print("p2", pretty_string(x2))
    print("")
    print("c1", pretty_string(c1))
    print("c2", pretty_string(c2))
    print("")
    print("y1", pretty_string(y1))
    
    
