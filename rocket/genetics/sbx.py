# sbx.py
# @todo: add support for under/overflows
# @todo: this is a good candidate function to optimize with cython by predefining some data types
import numpy as np
import math
eps = 1.0e-14

def sbx(parent1, parent2, prob, eta, lbound, ubound, rand):
    
    child1 = parent1.copy()
    child2 = parent2.copy()
    
    n_genes = len(parent1)
    y1, y2 = None, None
    
    # apply crossover?
    if rand.rand() <= prob:
        
        # for each gene
        for n_gene in range(n_genes):
            
            # apply crossover on this gene (ie dimension, decision variable)?
            if rand.rand() < 0.5:
                
                if np.abs(parent1[n_gene] - parent2[n_gene]) > eps:
                    
                    # selection of y1, y2 genes, so that y1 < y2
                    if parent1[n_gene] < parent2[n_gene]:
                        y1 = parent1[n_gene]
                        y2 = parent2[n_gene]
                    else:
                        y1 = parent2[n_gene]
                        y2 = parent1[n_gene]
    
                    # set bounds
                    yl = lbound[n_gene]
                    yu = ubound[n_gene]
                    
                    rand_number = rand.rand()
                    
                    # child 1
                    # beta  = 1.0 + (2.0*(y1-yl) / (y2-y1))
                    # alpha = 2.0 - beta ** (-(eta+1.0))
                    
                    # child 1
                    alpha = None
                    beta  = 1.0 + (2.0*(y1-yl) / (y2-y1))
                    
                    try:
                        alpha = 2.0 - beta ** (-(eta+1.0))
                    except FloatingPointError:
                        
                        print("casting beta to np.float128 to avoid FloatingPointError exception")
                        
                        # casting added to avoid this error
                        # FloatingPointError: underflow encountered in double_scalars
                        beta  = np.float128(beta)                       
                        alpha = 2.0 - beta ** (-(eta+1.0))
                    
                    betaq = 0
                    if rand_number <= (1.0/alpha):
                        betaq = math.pow((rand_number*alpha), (1.0/(eta+1.0)))
                    else:
                        betaq = math.pow((1.0/(2.0 - rand_number*alpha)), (1.0/(eta+1.0)))
                        
                    # gene for child 1
                    c1 = 0.5 * ((y1 + y2) - betaq * (y2-y1))
                    
                    
                    # child 2
                    #beta  = 1.0 + (2.0*(yu-y2) / (y2-y1))
                    #beta  = np.float128(beta)                       # added to avoid this error
                    #                                                # FloatingPointError: underflow encountered in double_scalars
                    #alpha = 2.0 - math.pow(beta, -(eta+1.0))


                    # child 2
                    alpha = None
                    beta  = 1.0 + (2.0*(yu-y2) / (y2-y1))
                    
                    try:
                        alpha = 2.0 - math.pow(beta, -(eta+1.0))
                    except FloatingPointError:
                        
                        print("casting beta to np.float128 to avoid FloatingPointError exception")
                        
                        # casting added to avoid this error
                        # FloatingPointError: underflow encountered in double_scalars
                        beta  = np.float128(beta)
                        alpha = 2.0 - math.pow(beta, -(eta+1.0))


                    if rand_number <= (1.0/alpha):
                        betaq = math.pow((rand_number*alpha), (1.0/(eta+1.0)))
                    else:
                        betaq = math.pow((1.0/(2.0 - rand_number*alpha)), (1.0/(eta+1.0)))
                        
                    # gene for child 2
                    c2 = 0.5 * ((y1+y2) + betaq * (y2-y1))
                    
                    # boundaries check
                    if c1 < yl:
                        c1 = yl
                        
                    if c2 < yl:
                        c2 = yl
                        
                    if c1 > yu:
                        c1 = yu
                    
                    if c2 > yu:
                        c2 = yu
                        
                    
                    # copy the new gene
                    if rand.rand() <= 0.5:
                        child1[n_gene] = c2
                        child2[n_gene] = c1
                    else:
                        child1[n_gene] = c1
                        child2[n_gene] = c2
                        
                else:
                    
                    # if the difference among the genes of the parents in the n-th position
                    # is lesser than an eps threshold, the crossover is not performed
                    # therefore, the n-th genes are copied
                    child1[n_gene] = parent1[n_gene] 
                    child2[n_gene] = parent2[n_gene] 
            
            else:
                
                # if the flip is not lower than 0.5
                # the n-th genes are copied
                child1[n_gene] = parent1[n_gene] 
                child2[n_gene] = parent2[n_gene]
                

    return child1.copy(), child2.copy()

