# mutation.py
from __future__ import print_function
from rocket.genetics.polymutation import polymutation
import numpy as np

def mutation(parents, lbound, ubound, params, rand):
    
    name = params["name"]
    
    if name == "polymutation":
        
        prob    = params["prob"]
        eta     = params["eta"]
        p1      = parents[0]        # @todo: check if len(parents) > 1 to alert the user that the number of parents is only 1
        
        c1      = polymutation(p1, prob, eta, lbound, ubound, rand)
        
        return (c1.copy(), )
        
    else:
        
        print("%s mutation operator is not defined yet!" % name)       # @todo: add log support here
        return None
        
        
        
