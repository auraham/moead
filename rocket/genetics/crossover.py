# crossover.py
from __future__ import print_function
from rocket.genetics.sbx import sbx
from rocket.genetics.m2m import annealrep

def crossover(parents, lbound, ubound, params, rand):
    """
    Front-end to crossover operators
    
    Input
    parents     tuple of arrays, each array represents a parent
    lbound      (n_genes, ) array of lower bounds
    ubound      (n_genes, ) array of upper bounds
    params      dictionary of crossover params
    rand        generator of random numbers (numpy.random.RandomState instance)
    
    Output
    childs      tuple of arrays, each array represents a new child
                note: the length of childs (arity) depends on the crossover operator
    """
    
    name = params["name"]
    
    # simulated binary crossover
    if name == "sbx":
        
        prob    = params["prob"]                # @todo: it could be a good idea to use default parameters, such as params.get("prob", 1./n_genes)
        eta     = params["eta"] 
        p1, p2  = parents[0], parents[1]        # @todo: check if len(parents) > 2 to alert the user that the number of parents is only 2
        
        c1, c2  = sbx(p1, p2, prob, eta, lbound, ubound, rand)
        
        return (c1.copy(), c2.copy())
    
    # crossover+mutation of MOEA/D-M2M []
    elif name == "m2m":
        
        pmuta   = params["pmuta"]
        delta   = params["delta"]               # @todo: remember to update (decrement) delta each generation before using this crossover function
        p1, p2  = parents[0], parents[1]        # @todo: check if len(parents) > 2 to alert the user that the number of parents is only 2
        
        c1      = annealrep(p1, p2, pmuta, delta, lboundd, ubound, rand)
        
        return (c1.copy(), )
        
    else:
        
        print("%s crossover operator is not defined yet!" % name)       # @todo: add log support here
        return None
        
        
