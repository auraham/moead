# About this module

This module contains mutation and crossover operators. In the practice, you only will need these functions:

- `crossover(parents, lbound, ubound, params, rand)` 
- `mutation(parents, lbound, ubound, params, rand)`

where 

- `parents` is a tuple of arrays (parents)
- `lbound` and `ubound` contains the boundaries for each decision variable
- `params` is a dictionary with parameters, including
  - `name`: name of the operator (such as `"sbx"` and `"polymytation"`)
  - `prob`: probability of use
  - `eta`: distribution index of `"sbx"` and `polymutation`
  - `pmuta`,`delta`: parameters of `"m2m"`.
- `rand` is an instance of `numpy.random.RandomState`

**Example**

```python
# test_genetics.py
from crossover import crossover
from mutation import mutation
import numpy as np
from numpy.random import RandomState


def pretty_string(x):
    
    return " ".join([ "%.4f" % i for i in x ])
    

if __name__ == "__main__":
    
    n_genes = 10
    rand    = RandomState(100)
    lbound  = np.zeros((n_genes, ))
    ubound  = np.ones((n_genes, ))
    
    x1 = rand.random_sample((n_genes, ))
    x2 = rand.random_sample((n_genes, ))
    
    # crossover
    parents = (x1, x2)
    params  = {"name": "sbx", "prob": 0.8, "eta": 20}
    c1, c2  = crossover(parents, lbound, ubound, params, rand)
    
    # mutation
    parents = (x1, )
    params  = {"name": "polymutation", "prob": 0.1, "eta": 20}
    y1,     = mutation(parents, lbound, ubound, params, rand)
    
    # output
    print("p1", pretty_string(x1))
    print("p2", pretty_string(x2))
    print("")
    print("c1", pretty_string(c1))
    print("c2", pretty_string(c2))
    print("")
    print("y1", pretty_string(y1))
    
    # output
    # p1 0.5434 0.2784 0.4245 0.8448 0.0047 0.1216 0.6707 0.8259 0.1367 0.5751
    # p2 0.8913 0.2092 0.1853 0.1084 0.2197 0.9786 0.8117 0.1719 0.8162 0.2741

    # c1 0.5434 0.2784 0.4187 0.1202 0.0050 0.9692 0.6707 0.8259 0.1113 0.5751
    # c2 0.8913 0.2092 0.1911 0.8330 0.2208 0.1271 0.8117 0.1719 0.8417 0.2741

    # y1 0.5434 0.2784 0.3841 0.8448 0.0047 0.1216 0.6707 0.8259 0.1367 0.5751
```



# Available crossover operators

- `sbx` 
- `m2m`

# Available mutation operators

- `polymutation`

# TODO
- Add documentation and references

