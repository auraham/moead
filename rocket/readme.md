# About this repository #
This repository contains a framework for testing *Multiobjective Optimization Evoltionary Algorithms* (MOEAs) using python.



# Forking this repository

The aim of this project is to create a single toolkit for testing MOEAs. Although, and more interesting, you can develop experiments with this repository. We suggest the following structure:

```shell
EXPERIMENT_PATH
	rocket         			# (1)
	methods          		# (2)
	common.py				# (3)
	run_experiment_1.py		# (4)
	run_experiment_2.py 
	readme.md  				# (5)
```



where

1. `rocket` contains a copy or fork of this repository.
2. `methods` contains your algorithms or modified versions of our implementations. This directory is useful when you are interested on, for instance, compare the performance of MOEA/D (`rocket.moea.moead.moead.py`) against to a modified version. So, instead of modifying `rocket.moea.moead.moead.py`, you can grab a copy of `moead.py`, put it in `methods`, and change its behavior there. 
3. `common` contains utilities or helper functions for experimenting, if needed.
4. `run_experiment.py` allows us to replicate your experiment(s). 
5. `readme.md` just to remember what is the purpose of your work.

**Note** In order to use `rocket` in your experiments, you will need to add this snippet in `run_experiment.py`:

```python
import sys
sys.path.insert(0, 'EXPERIMENT_PATH/rocket/') 
```

That indicates the interpreter where to look for modules and packages.



# Setup

* Download this repository to some path, such as `~/rocket`.

* Open `ipython` and execute this:

  ```
  import site
  site.USER_SITE
  ```

  You will get something like this:

  ```
  '/home/auraham/.local/lib/python3.5/site-packages'
  ```

* Create a symbolic link to `~/rocket` within the previous path as follows:
        cd ~/.local/lib/python3.5/site-packages/
        ln -s ~/rocket




# Optional. Setup on virtual enviroments

* If you are using `virtualenv`, add the previous symbolic link as follows:

        cd virtualenvs/VIRTUAL_ENV_NAME/lib/pythonX.Y/site-packages/
        ln -s ~/rocket


where `pythonX.Y` is your python version.



# Best practices

### Comment lines to avoid overwriting files

Some scripts are employed to create new files. For instance, `fronts/create_fronts_dtlz.py` creates `npy` files with the real fronts of some test problems. However, before making a commit, it is suggested to comment lines such as:


```python
np.savez(filepath, objs=objs, p=np.array([p]))        
```

To avoid replacing this file (`filepath`), it is recommended to comment that line:

```
# good practice (comment before commit)
# np.savez(filepath, objs=objs, p=np.array([p]))   
```

```python
# bad practice (leaving this line uncommented before commit)
np.savez(filepath, objs=objs, p=np.array([p]))        
```

Although `npz` (and some `txt`) files are under control version (and we can revert accidental overwritings), it is recommended to comment that lines for future testing.



# TODO

- Add an option to avoid saving pops (chroms, objs matrices) every `steps` generations. Instead, save the final pop only. This option will be useful for early stages of development. 
- Parse params["name"], params["label"] (moea configuration) to avoid these characters: ";", "_"
 

# Contact

Auraham Camacho `auraham.cg@gmail.com`

