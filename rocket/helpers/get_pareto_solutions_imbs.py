# get_pareto_solutions_imbs.py
from __future__ import print_function
from rocket.helpers.mesh import mesh_2, mesh_3, mesh_4, mesh_7, mesh_9, mesh_14
import numpy as np

def get_pareto_solutions_imbs(n_genes, m_objs, p):
    """
    Returns pareto optimal solutions for imbs for m_objs in (2,3,5,8,10) objectives.
    @todo: add support for more objectives
    
    Input
    n_genes     int, number of decision variables
    m_objs      int, number of objectives
    p           int, spacing
    
    Output
    chroms      (rows, n_genes) matrix of pareto optimal solutions
    """
    
    matrix = None
    
    if m_objs == 2:
        span = np.linspace(0, 1, p)
        matrix = span.reshape((span.shape[0], 1))
        
    elif m_objs == 3:
        matrix = mesh_2(p)
    
    elif m_objs == 4:
        matrix = mesh_3(p)
    
    elif m_objs == 5:
        matrix = mesh_4(p)
        
    elif m_objs == 8:
        matrix = mesh_7(p)
        
    elif m_objs == 10:
        matrix = mesh_9(p)
        
    elif m_objs == 15:
        matrix = mesh_14(p)
        
    else:
        print("m_objs: %s is not defined for get_pareto_solutions_dtlz" % m_objs)       # @todo: add log support here
        return None
    
    rows, cols = matrix.shape
        
    
    # output matrix
    chroms = np.ones((rows, n_genes))
    
    # position (diversity) variables
    for col in range(cols):                     # rewrite the first cols of chroms
        chroms[:, col] = matrix[:, col]         # with the content of matrix
    
    
    # optimal value for convergence variables
    j = cols                                    # j position variables
    per_row = 1
    opt_x = chroms[:, :j].sum(axis=per_row) * 0.5   # (pop_size, ), suma de las primeras j variables 
    
    # distance (convergence) variables
    for k in range(j, n_genes):
        chroms[:, k] = opt_x.copy()                 # the lask variables of each decision vector contains the same fixed value (opt_x)
        
    return chroms.copy()
