# get_pareto_solutions_p.py
from __future__ import print_function
from rocket.helpers.mesh import mesh_2
import numpy as np

def get_pareto_solutions_p(n_genes, m_objs, p):
    """
    Returns pareto optimal solutions for p1-15 for m_objs=3 objectives.
    These test problems were defined for aligned_mop experiment only. 
    The number of objectives is not scalable.
    The number of decision variables is defined as in dtlz1.
    This function is similar to get_pareto_solutions_dtlz(), but with a single case (m_objs == 3).
    Notice that the optimal solutions for dtlz are the same for p1-15
    
    Input
    n_genes     int, number of decision variables
    m_objs      int, number of objectives
    p           int, spacing
    
    Output
    chroms      (rows, n_genes) matrix of pareto optimal solutions
    """
    
    matrix = None
        
    if m_objs == 3:
        matrix = mesh_2(p)
    
    else:
        print("m_objs: %s is not defined for get_pareto_solutions_p" % m_objs)       # @todo: add log support here
        return None
    
    rows, cols = matrix.shape
        
    # optimal value for distance parameters
    opt_x = 0.5
    
    chroms = np.ones((rows, n_genes))*opt_x
    
    for col in range(cols):                     # rewrite the first cols of chroms
        chroms[:, col] = matrix[:, col]         # with the content of matrix
        
    return chroms.copy()
    

