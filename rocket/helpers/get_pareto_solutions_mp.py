# get_pareto_solutions_mp.py
from __future__ import print_function
from rocket.helpers.mesh import mesh_2, mesh_3, mesh_4, mesh_7, mesh_9, mesh_14
import numpy as np

def get_pareto_solutions_mp(n_genes, m_objs, p):
    """
    Returns pareto optimal solutions for mp1-8 for m_objs in (2,3,5,8,10) objectives.
    @todo: add support for more objectives
    
    Input
    n_genes     int, number of decision variables
    m_objs      int, number of objectives
    p           int, spacing
    
    Output
    chroms      (rows, n_genes) matrix of pareto optimal solutions
    """
    
    matrix = None
    
    
    if m_objs == 2:
        span = np.linspace(0, 1, p)
        matrix = span.reshape((span.shape[0], 1))
        
    elif m_objs == 3:
        matrix = mesh_2(p)
    
    elif m_objs == 4:
        matrix = mesh_3(p)
    
    elif m_objs == 5:
        matrix = mesh_4(p)
        
    elif m_objs == 8:
        matrix = mesh_7(p)
        
    elif m_objs == 10:
        matrix = mesh_9(p)
        
    elif m_objs == 15:
        matrix = mesh_14(p)
        
    else:
        print("m_objs: %s is not defined for get_pareto_solutions_mp" % m_objs)       # @todo: add log support here
        return None
    
    rows, cols = matrix.shape
        
    # optimal value for distance parameters
    opt_x = 0.5
    
    chroms = np.ones((rows, n_genes))*opt_x
    
    for col in range(cols):                     # rewrite the first cols of chroms
        chroms[:, col] = matrix[:, col]         # with the content of matrix
        
    return chroms.copy()
    

def get_pareto_solutions_mp_split(n_genes, m_objs, p):
    """
    Returns the favored and unfavored Pareto Set for mp2, mp4, mp6 and mp8 for m_objs in (2,3,5,8,10) objectives.
    @todo: add support for more objectives
    
    Input
    n_genes     int, number of decision variables
    m_objs      int, number of objectives
    p           int, spacing
    
    Output
    chroms      (rows, n_genes) matrix of favored Pareto Set
    """
    
    chroms = get_pareto_solutions_mp(n_genes, m_objs, p)
    
    X1 = chroms[:, 0]                       # first variable
    
    # evaluate condition
    case_a = (2./3 <= X1) & (X1 <= 1.0)     # imbalance
    case_b = ~case_a
    
    favored   = chroms[case_a, :].copy()
    unfavored = chroms[case_b, :].copy()
    
    return favored, unfavored
    

