# filter_pop.py
from __future__ import print_function
import numpy as np
import os, sys, argparse
from rocket.moea.nds import vfns
from rocket.helpers import normalize, get_hv_ref_point

def save_filtered_objs(filtered_objs, filtered_filepath, add_separators=True):
    
    if filtered_objs.shape[0] > 0:
        
        # this line does not works as expected for wfg command
        # save filtered pop
        #np.savetxt(output_filepath, objs_b, header=" ", footer=" ", comments="#")

        with open(filtered_filepath, "w") as log:
        
            if add_separators:
                log.write("#\n")
        
            for point in filtered_objs:
                
                line = " ".join([ "%.18e" % val for val in point ])
                log.write(line+"\n")
                
            if add_separators:
                log.write("#\n")
            
    else:
        
        with open(filtered_filepath, "w") as log:
            
            log.write("#\n#\n")
            

def filter_by_reference_point(objs, ref_point):
    """
    Keep the points in pop inferior than ref_point
    """
    
    # check for empty pop
    if objs.shape[0] == 0:
        return objs.copy()
    
    pop_size, m_objs = objs.shape
    to_keep = np.zeros((pop_size, ), dtype=bool)
    
    for i, u in enumerate(objs):
        
        if (u < ref_point).all():        # estrictamente menor
            to_keep[i] = True
            
    return objs[to_keep].copy()


def filter_by_nds(objs, procs):
    
    # check for empty pop
    if objs.shape[0] == 0:
        return objs.copy()
    
    # very fast non dominanted sorting
    fronts, ranks = vfns(objs, procs)
    
    # keep first front only
    to_keep = ranks == 0
    
    first_front = objs[to_keep, :]
    
    return first_front.copy()


def filter_batch(moea_name, mop_name, m_objs, t, runs, experiment_path, z_ideal, z_nadir, hv_ref_point=None, suffix=""):
    """
    Filter a set of populations (objs) using (1) filter by reference point, and (2) filter by non-dominated sorting.
    
    Input
    moea_name           str, name of moea
    mop_name            str, name of mop
    m_objs              int, number of objectives
    t                   int, generation t
    runs                int, number of independent runs
    experiment_path     str, path of experiment ('results' directory)
    z_ideal             (m_objs, ) array, ideal vector
    z_nadir             (m_objs, ) array, Nadir vector
    hv_ref_point        (m_objs, ) array, reference point. If it is None, a default reference point will be used as suggested by 
                        [Ishibuchi18] How to Specify a Reference Point in Hypervolume Calculation for Fair Performance Comparison
    suffix              str, optional suffix for objs filename
    """

    # define ref point
    ref_p = None
    
    if hv_ref_point is None:
        ref_p = get_hv_ref_point(m_objs)
        print("using default reference point: %s" % ref_p)
    else:
        ref_p = np.array(ref_point)
        print("reference point: %s" % ref_p)

    
    for run_id in range(runs):
        
        # input
        filename = "objs_%s_m_%d_run_%d_t_%d%s.txt" % (moea_name, m_objs, run_id, t, suffix)      # no suffix eg: objs_amoead_v17-pbi_m_3_run_28_t_200.txt
        filepath = os.path.join(experiment_path, mop_name, moea_name, "pops", filename)           # suffix    eg: objs_amoead_fev_v11-step-1_m_8_run_9_t_800_fev_ws.txt
        
        # output
        filtered_filepath = filepath + ".normalized.filtered.twice"
        
        # load objs
        objs = np.genfromtxt(filepath)
        
        # normalize objs
        norm_objs = normalize(objs, z_ideal, z_nadir)
        
        # filter 1: reference point
        objs_a = filter_by_reference_point(norm_objs, ref_p)
        
        # filter 2: nds (first front only)
        first_front = filter_by_nds(objs_a, procs=12)
        
        # save filtered objs
        save_filtered_objs(first_front, filtered_filepath)
