# get_pareto_solutions_imb.py
from __future__ import print_function
import numpy as np

def get_pareto_solutions_imb1(pop_size, n_genes):
    """
    Returns pareto optimal solutions for IMB1.
    
    Input
    pop_size    int, number of points
    n_genes     int, number of decision variables
    
    Output
    X          (pop_size, n_genes) matrix of pareto optimal solutions
    """

    # optimal
    X = np.zeros((pop_size, n_genes))
    X[:, 0] = np.linspace(0, 1, pop_size)   # X1 puede contener cualquier valor entre 0 y 1
                                            # dato: si se usa np.linspace(0, k, pop_size), el valor del obj 1 estara en el rango [0, n]
                                            # ie, para que f1 abarque [0, 1], usa un linspace de [0, 1], no solo de [0, 0.2]
    
    for i in range(1, n_genes):
        X[:, i] = np.sin(0.5*np.pi*X[:, 0])
    
    return X.copy()
    
    
def get_pareto_solutions_imb4(p, n_genes):
    """
    Returns pareto optimal solutions for IMB4.
    
    Input
    p           int, mesh spacing
    n_genes     int, number of decision variables
    
    Output
    X          (pop_size, n_genes) matrix of pareto optimal solutions
    """

    x = np.linspace(0, 1, p)
    a, b = np.meshgrid(x, x)
    
    matrix = np.vstack((a.flatten(),
                        b.flatten()
                    )).T
    
    pop_size = matrix.shape[0]
    
    X = np.zeros((pop_size, n_genes))       # X[:, [0, 1]] ranges from 0 to 1
    X[:, 0] = matrix[:, 0]
    X[:, 1] = matrix[:, 1]
    
    
    for i in range(2, n_genes):
        X[:, i] = (X[:, 0] + X[:, 1])/2
        
    return X.copy()
    


 
    
    
