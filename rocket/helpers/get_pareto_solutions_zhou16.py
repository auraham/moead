# get_pareto_solutions_zhou16.py
from __future__ import print_function
import numpy as np

def get_pareto_solutions_zhou16(fn_id, pop_size, n_genes):
    """
    Returns pareto optimal solutions for t1,t2 for m_objs=2 objectives.
    
    Input
    fn_id       int, 1 or 2
    pop_size    int, number of points
    n_genes     int, number of decision variables
    
    Output
    chroms      (rows, n_genes) matrix of pareto optimal solutions
    """
    
    chroms = None
    
    if fn_id == 1:
        
        
        # optimal
        X = np.zeros((pop_size, n_genes))
        X[:, 0] = np.linspace(0, 1, pop_size)
        
        for i in range(1, n_genes):
            X[:, i] = np.sin(6.0*np.pi*X[:, 0] + (i+1)*np.pi/n_genes)
    
        chroms = X.copy()
    
    if fn_id == 2:
        
        # optimal
        X = np.zeros((pop_size, n_genes))
        X[:, 0] = np.linspace(0, 1, pop_size)
        
        factor = (4.0/5.0) * X[:, 0]
        for i in range(1, n_genes):
            X[:, i] = np.sin(6.0*np.pi*X[:, 0] + (i+1)*np.pi/n_genes) * factor
            
        chroms = X.copy()
        
        
    return chroms.copy()
     
    
    
