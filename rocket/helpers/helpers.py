# helpers.py
from __future__ import print_function
import numpy as np

def get_ref_points(mop_name, m_objs):
    """
    Auxiliary function to return z_ideal and z_nadir points from a given problem instance
    
    Input
    mop_name        string, name of problem
    m_objs          int, number of objectives
    
    Output
    z_ideal         (m_objs, ) array, ideal vector
    z_nadir         (m_objs, ) array, nadir vector
    """
    
    z_ideal = np.zeros((m_objs, ))
    z_nadir = np.ones((m_objs, ))
    
    if mop_name in ("dtlz1", "inv-dtlz1"):
        z_nadir = z_nadir * 0.5
        
    elif mop_name in ("dtlz2", "dtlz3"):
        z_ideal = np.zeros((m_objs, ))
        z_nadir = np.ones((m_objs, ))
        
    else:
        print("Reference points are not defined for this problem instance (mop: %s, m: %d)" % (mop_name, m_objs))
        return None, None
        
    return z_ideal.copy(), z_nadir.copy()
    
def get_hv_ref_point(m_objs, h=None):
    """
    Return the reference point for calculation of the hypervolume indicator
    as recommended in [Ishibuchi18] How to Specify a Reference Point in Hypervolume Calculation for Fair Performance Comparison
    
    Input
    m_objs          int, number of objectives
    h               int, spacing factor (parameter H of MOEA/D). If it is None, a default parameter will be used.
                    Notice that you should take care of this parameter. Review [Ishibuchi18] for more details.
    
    Output
    ref_point       (m_objs, ) array, reference point
    """
    
    H = None
    
    spacing = { 
        3: 12,
        4: 7,
        5: 6,
        6: 4, 
        7: 4,
        8: 3,
        10: 3,
        }
        
    if h is None:
        H = spacing[m_objs]
    else:
        H = h
        
    print("Defining hv_ref_point as [1+1/%d, ..., 1+1/%d]" % (H, H))
        
    ref_point = np.ones((m_objs, )) * (1 + (1/H))
    
    return ref_point.copy()


def get_n_genes(mop_name, m_objs):
    """
    Return the number of decision variables (n_genes), convergence (c), and diversity (d) variables.
    
    Input
    mop_name        string, name of problem
    m_objs          int, number of objectives
    
    Output
    n               int, number of variables
    c               int, number of convergence variables (if available)
    d               int, number of diversity variables (if available)
    """


    n, c, d = 0, 0, 0
    
    # dtlz, min-dtlz, dtlz-spin, imbs
    if mop_name in ("dtlz1", "dtlz2", "dtlz3", "dtlz4", "dtlz5", 
                    "inv-dtlz1", "inv-dtlz2",
                    "imb4", "imb5",
                    "imbs4", "imbs5",
                    
                    "sdtlz1", "sdtlz2", "sdtlz3", "sdtlz4", "sdtlz5",
                    "scaled_dtlz1", "scaled_dtlz2", "scaled_dtlz3", "scaled_dtlz4", "scaled_dtlz5",
                    
                    "min-dtlz1", "min-dtlz2", "min-dtlz3", "min-dtlz4", "min-dtlz5",
                    "dtlz1-spin", "dtlz2-spin", "dtlz3-spin", "dtlz4-spin", "dtlz5-spin",
                    "dtlz1-spin-cube", "dtlz2-spin-cube", "dtlz3-spin-cube", "dtlz4-spin-cube", "dtlz5-spin-cube"):
        
        if mop_name in ("dtlz1", "sdtlz1", "scaled_dtlz1", "min-dtlz1", "dtlz1-spin", "inv-dtlz1", "imb4", "imbs4", "mp1", "mp2", "mp3", "mp4"):
            c = 5               # number of convergence variables
        
        else:
            c = 10              # number of convergence variables
    
        n = m_objs + c - 1      # total number of variables
        d = n - c               # number of diversity variables
    
    # maf1-6
    elif mop_name in ("maf1", "maf2", "maf3", "maf4", "maf5", "maf6"):
        
        k = 10
        n = m_objs + k - 1
        
    elif mop_name == "maf7":
        
        k = 20
        n = m_objs + k - 1
        
    elif mop_name == "maf13":
        
        n = 5
    
    # mp 
    elif mop_name in ("mp1", "mp2", "mp3", "mp4", "mp5", "mp6", "mp7", "mp8"):
        
        c = 5                   # same as dtlz1
        n = m_objs + c - 1      # total number of variables
        d = n - c               # number of diversity variables
        
    # p - custom (m=3 objective) problems for aligned_mop experiment
    elif mop_name in ("p1", "p2",  "p3",  "p4",  "p5",
                    "p6",   "p7",  "p8",  "p9",  "p10",
                    "p11",  "p12", "p13", "p14", "p15"):
        
        c = 5                   # same as dtlz1
        n = m_objs + c - 1      # total number of variables
        d = n - c               # number of diversity variables
        
    # zdt
    elif mop_name in ("zdt1", "zdt2", "zdt3"):
        
        n = 30                  # from [Huband06]
        d = 1                   # 'all of the ZDT problems employ only one position (ie diversity) parameter'
        c = n - d
    
    elif mop_name in ("zdt4", "zdt6"):
        
        n = 10                  # from [Huband06]
        d = 1                   # 'all of the ZDT problems employ only one position (ie diversity) parameter'
        c = n - d
    
    # [Liu14]'s mops
    elif mop_name in ("mop1", "mop2", "mop3", "mop4", "mop5", "mop6", "mop7"):
            
        n = 10                  # @todo: add c and d values, although they are not needed for now
    
    # kursawe, [Coellobook] page 184
    elif mop_name == "kursawe":
            
        n = 3
    
    # [Liu16]'s IMB mops
    #elif mop_name in ("imb1", "imb2", "imb3", "imb4", "imb5", "imb6", "imb7", "imb8", "imb9", "imb10", "imb11", "imb12", "imb13", "imb14"):
    
    #    n = 10                  # @todo: add c and d values, although they are not needed for now
    #
    # @update: we use the same specification of dtlz for the number of variables, n
    
    
    
    # [Zhou16]'s t mops
    elif mop_name in ("t1", "t2"):
        
        n = 10                  # @todo: there is no default value for n, so we use n=10
    
    # wfg
    elif mop_name in ("wfg1", "wfg2", "wfg3", "wfg4", "wfg5", "wfg6", "wfg7", "wfg8", "wfg9",
                    "wfg1-spin", "wfg2-spin", "wfg3-spin", "wfg4-spin", "wfg5-spin", "wfg6-spin", "wfg7-spin", "wfg8-spin", "wfg9-spin"):
        
        d = 2*(m_objs-1)        # k position variables
        c = 20                  # l distance variables
        n = c + d               # according to [Li15] page 9
        
    else:
        
        print("%s is not defined for get_n_genes" % mop_name)       # @todo: add log support here

    # total, convergence, diversity
    return n, c, d
    
def get_bounds(mop_name, n_genes):
    """
    Return the lower and upper bounds for mop_name.
    
    Input
    mop_name        string, name of problem
    n_genes         int, number of decision variables
    
    Output
    lbound          (n_genes, ) array of lower bounds
    ubound          (n_genes, ) array of upper bounds
    """
    
    lbound = np.zeros((n_genes, ))
    ubound = np.ones((n_genes, ))
    
    # [Zhou16]'s t mops
    if mop_name in ("t1", "t2"):
        # (0, -1, ..., -1)
        # (1,  1, ...,  1)
        
        lbound = np.zeros((n_genes, ))
        ubound = np.ones((n_genes, ))
        
        lbound[1:] = -1
    
    # zdt4
    elif mop_name == "zdt4":
        # (0)               first variables
        # (1)
        # 
        # (-5, ..., -5)     remaining variables
        # ( 5, ...,  5)     remaining variables

        lbound = np.zeros((n_genes, ))
        ubound = np.ones((n_genes, ))

        lbound[1:] = -5
        ubound[1:] =  5
        
    # zdt
    elif mop_name in ("zdt1", "zdt2", "zdt3", "zdt6"):
        # (0, 0, ..., 0)
        # (1, 1, ..., 1)
        
        lbound = np.zeros((n_genes, ))
        ubound = np.ones((n_genes, ))
        
    # dtlz, min-dtlz, inv-dtlz, imbs
    elif mop_name in ("dtlz1", "dtlz2", "dtlz3", "dtlz4", "dtlz5", 
                    "inv-dtlz1", "inv-dtlz2",
                    "imbs4", "imbs5",
                    "sdtlz1", "sdtlz2", "sdtlz3", "sdtlz4", "sdtlz5",
                    "scaled_dtlz1", "scaled_dtlz2", "scaled_dtlz3", "scaled_dtlz4", "scaled_dtlz5",
                    "mp1", "mp2", "mp3", "mp4", "mp5", "mp6", "mp7", "mp8",
                    "min-dtlz1", "min-dtlz2", "min-dtlz3", "min-dtlz4", "min-dtlz5",
                    "dtlz1-spin", "dtlz2-spin", "dtlz3-spin", "dtlz4-spin", "dtlz5-spin",
                    "dtlz1-spin-cube", "dtlz2-spin-cube", "dtlz3-spin-cube", "dtlz4-spin-cube", "dtlz5-spin-cube"):
        # (0, 0, ..., 0)
        # (1, 1, ..., 1)
        
        lbound = np.zeros((n_genes, ))
        ubound = np.ones((n_genes, ))
        
    # maf1-7
    elif mop_name in ("maf1", "maf2", "maf3", "maf4", "maf5", "maf6", "maf7"):
        
        lbound = np.zeros((n_genes, ))
        ubound = np.ones((n_genes, ))
        
    elif mop_name == "maf13":
        # (0, 0, -2, -2, -2)
        # (1, 1,  2,  2,  2)
        
        lbound = np.zeros((n_genes, ))
        ubound = np.ones((n_genes, ))
        
        lbound[2:] = -2
        ubound[2:] = 2
    
    # p - custom (m=3 objective) problems for aligned_mop experiment
    elif mop_name in ("p1", "p2", "p3", "p4", "p5",
                    "p6", "p7", "p8", "p9", "p10",
                    "p11", "p12", "p13", "p14", "p15"):
    
        # (0, 0, ..., 0)
        # (1, 1, ..., 1)
        
        lbound = np.zeros((n_genes, ))
        ubound = np.ones((n_genes, ))
        
    # [Liu14]'s mops
    elif mop_name in ("mop1", "mop2", "mop3", "mop4", "mop5"):
        # (0, 0, ..., 0)
        # (1, 1, ..., 1)
        
        lbound = np.zeros((n_genes, ))
        ubound = np.ones((n_genes, ))
        
    # kursawe [Coellobook] page 184
    elif mop_name == "kursawe":
        # (-5, -5, ..., -5)
        # ( 5,  5, ...,  5)
        
        lbound = np.ones((n_genes, ))*-5
        ubound = np.ones((n_genes, ))*5
        
    # [Liu16]'s IMB mops
    elif mop_name in ("imb1", "imb2", "imb3", "imb4", "imb5", "imb6", "imb7", "imb8", "imb9", "imb10", "imb11", "imb12", "imb13", "imb14"):
        # (0, 0, ..., 0)
        # (1, 1, ..., 1)
        
        lbound = np.zeros((n_genes, ))
        ubound = np.ones((n_genes, ))
        
    # wfg
    elif mop_name in ("wfg1", "wfg2", "wfg3", "wfg4", "wfg5", "wfg6", "wfg7", "wfg8", "wfg9",
                    "wfg1-spin", "wfg2-spin", "wfg3-spin", "wfg4-spin", "wfg5-spin", "wfg6-spin", "wfg7-spin", "wfg8-spin", "wfg9-spin"):
        # (0, 0, 0, 0, ..., 0)
        # (2, 4, 6, 8, ..., n_genes*2)
        
        lbound = np.zeros((n_genes, ))
        ubound = np.arange(1, n_genes+1)*2      # int casting is not needed
        
    else:
        
        print("%s is not defined for get_bounds" % mop_name)        # @todo: add log support here

    return lbound.copy(), ubound.copy()

def check_n_genes(mop_name, m_objs, n_genes):
    """
    Check whether the value of n_genes is correct for a given mop_name
    This check only applies for "min-dtlz"
    
    Input
    mop_name    string, name of problem
    m_objs      int, number of objectives
    n_genes     int, number of decision variables
    
    Output
    result      bool, determines if the value of n_genes is correct or not
    """
    
    result = True
    
    # we only check the value of n_genes for "min-dtlz"
    # the other problems (as far as we know) are not sentitive to the value of n_genes
    # that is, unlike min-dtlz where the shape of the pareto front grows as the number of n_genes increases
    # the shape of pareto front of other problems remains the same
    if mop_name in ("min-dtlz1", "min-dtlz2", "min-dtlz3", "min-dtlz4", "min-dtlz5"):
    
        if n_genes:
        
            expected_n_genes, __, __ = get_n_genes(mop_name, m_objs)
        
            if n_genes != expected_n_genes:
                
                print("The number of n_genes is incorrect for this mop " \
                    "(mop_name: %s, n_genes: %d, expected: %d)" %                       # @todo: add log support here
                    (mop_name, n_genes, expected_n_genes))

        else:
            
            result = False
            print("The number of n_genes is requiered for this mop " \
                "(mop_name: %s, n_genes: %s)" %                                         # @todo: add log support here
                (mop_name, n_genes))
            
    return result
    
if __name__ == "__main__":
    
    m_objs = 8
    mop_names = ("dtlz1", "zdt1", "zdt4", "mop1", "t1", "wfg1", "maf1", "maf2", "maf3", "maf4", "maf5", "maf6",  "maf7", "maf13")
    
    for mop_name in mop_names:
        
        n, c, d = get_n_genes(mop_name, m_objs)
        lb, ub  = get_bounds(mop_name, n)
        
        print(mop_name)
        print("n: %d, convergence: %d, diversity: %d" % (n, c, d))
        print("lbound", lb)
        print("ubound", ub)
        print("")
    
