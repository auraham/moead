# About this package

`helpers.py` contains helper functions related to the problems available in `rocket.problems`, such as their bounds, number of variables, etc.

`rotation.py` contains functions for matrix rotation. For now, only 3D rotations are supported. This module is employed to rotate `"dtlz"` fronts.



# Notes about `imb` and `imbs` problems

The IMB [Liu17] benchmark suggest $n=10$ variables:

> All these test functions are to be minimized by default, and we recommend $n=10$ for all problems [Liu17].

Although, we proposed a generalization called `imbs`. Such a generalization is based on `dtlz`, where the number of variables is defined as $n=k+m-1$, where $k=5$ for `dtlz1`, and $k=10$ otherwise. In this implementation of `imbs`, we adopted the specification from `dtlz` for the number of variables $n$. This change is reflected in `helpers.py`:

```python
# helpers.py

# dtlz, min-dtlz, dtlz-spin, imbs
if mop_name in (...
                "imb4", "imb5",
                "imbs4", "imbs5",
                ...
                ):
        
	if mop_name in (..., "imb4", "imbs4"):
            c = 5               # number of convergence variables
        
    else:
        c = 10              	# number of convergence variables

        n = m_objs + c - 1      # total number of variables
        d = n - c               # number of diversity variables

```



# TODO

- Add `rotate_mesh.py` and `front_rotation_scale.py` to illustrate rotations (i.e., differences among `spin` and `spin_cube` functions).

# References

- [Huband06] *Review of Multiobjective Test Problems and a Scalable Test Problem Toolkit*
- [Liu14] *Decomposition of a Multiobjective Optimization Problem into a Number of Simple Multiobjective Subproblems*
- [Li15] *An Evolutionary Many-Objective Optimization Algorithm Based on Dominance and Decomposition*
- [Jia16] *Rotation in the Space*, Yan-Bin Jia. Sep 6, 2016.
- [Liu17] *Investigating the Effect of Imbalance Between Convergence and Diversity in Evolutionary Multi-objective Algorithms*