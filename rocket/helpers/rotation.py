# rotation.py
import numpy as np
from numpy.linalg import norm


def deg2rad(degrees):
    
    rad = degrees * (np.pi/180)
    return rad
    
def rad2deg(rad):
    
    deg = rad * (180.0/np.pi)
    
    return deg
    
def fast_rodrigues_rotation(V, theta):
    """
    Rotate a 3D matrix
    
    Input
    V       (pop_size, 3) matrix
    theta   rotation angle (radians)
    
    Output
    Vp      (pop_size, 3) rotated matrix
    """

    per_row = 1
    
    # normalized roation axis
    k = np.array([1,1,1])
    k = k / norm(k)
    
    # V_par
    rows, cols = V.shape
    K = k.reshape((1, cols)).repeat(rows, 0)            # (rows, cols), each row is a copy of k

    dot = (V*K).sum(axis=per_row)                       # (rows, ), each entry is equals to dot(V[i], k), for i in range(rows)
    
    V_par = dot.reshape((rows, 1)) * K                  # (rows, cols), each row represents dot[i] * K[i], for i in range(rows)
    
    
    # V_per
    V_per = V - V_par                                   # (rows, cols), each row represents V[i] - V_par[i], for i in range(rows)
    
    
    # W (orthogonal vector)
    W = np.cross(K, V_per)                              # (rows, cols)
    
    # Vp perpendicular
    Vp_per = V_per * np.cos(theta) + W * np.sin(theta)
    
    # final vector
    Vp = V_par + Vp_per
    
    return Vp.copy()
        
def rotate_matrix(matrix, theta):
    """
    Rotate a matrix
    
    Input
    matrix       (pop_size, m_objs) matrix
    theta   rotation angle (radians)
    
    Output
    matrix      (pop_size, m_objs) rotated matrix
    """
    
    objs = matrix.shape[1]
    
    if objs == 3:
        
        return fast_rodrigues_rotation(matrix, theta)
        
    else:
        
        print("Rotation is not supported for %d dimensions!" % objs)

    return None

def scale(objs):
    """
    Scale a objs matrix
    
    Input
    objs        (pop_size, m_objs) objs matrix

    Output
    new_objs    (pop_size, m_objs) scaled objs matrix
    """
    
    x = objs[:, 0]
    y = objs[:, 1]
    z = objs[:, 2]

    xp = (4*x +   y +   z)/6
    yp = (  x + 4*y +   z)/6
    zp = (  x +   y + 4*z)/6

    new_objs = np.zeros(objs.shape)
    
    new_objs[:, 0] = xp
    new_objs[:, 1] = yp
    new_objs[:, 2] = zp
    
    return new_objs
    
def spin(objs, degrees):
    """
    Rotate and scale a objs matrix (Rodrigues' method + Dr Landa's proposal)
    
    Input
    objs        (pop_size, m_objs) objs matrix
    degrees     rotation angle

    Output
    new_objs    (pop_size, m_objs) rotated-scaled objs matrix
    """
    
    # @note: comentar esta linea cuando se defina un metodo generalizado de rotacion 
    degrees = 180
    
    # rotate
    theta = deg2rad(degrees)
    rotated_objs = rotate_matrix(objs, theta)

    # scale
    new_objs = scale(rotated_objs)
    
    return new_objs.copy()

    
def spin_objs(objs, degrees):
    """
    Same as spin, but both rotated_objs and shrinked_objs are returned.
    
    Rotate and scale a objs matrix (Rodrigues' method + Dr Landa's proposal)
    
    Input
    objs        (pop_size, m_objs) objs matrix
    degrees     rotation angle

    Output
    new_objs    (pop_size, m_objs) rotated-scaled objs matrix
    """
    
    # rotate
    theta = deg2rad(degrees)
    rotated_objs = rotate_matrix(objs, theta)

    # shrink
    shrinked_objs = scale(rotated_objs)
    
    return rotated_objs.copy(), shrinked_objs.copy()


def scaling(objs, scaling_factor):
    """
    Scale a matrix using a scaling_factor for each dimension (column, objective)
    
    Input
    objs                (pop_size, m_objs) objs matrix
    scaling_factor      int, scaling factor for all the objectives
                        str, type of scaling: "irregular"
                        
    Output
    scaled_objs         (pop_size, m_objs) scaled objs matrix
    """
    
    # input
    pop_size, m_objs = objs.shape
    
    # scaling factor
    sf = np.ones((m_objs, ))
    
    if scaling_factor == "irregular":
        
        # irregular scaling factor
        # [1, 10, 100, ...]
        for i in range(m_objs):
            sf[i] = 10**((i+1)-1)
            
    elif isinstance(scaling_factor, (int, float)):
        
        sf = sf*scaling_factor
        
    else:
        
        print("scaling factor is not correct")      # @todo: add log support here
    
    # debug
    #print("sf", sf)
    
    # scaled objs
    scaled_objs = objs.copy()
    
    for i in range(m_objs):
        scaled_objs[:, i] = scaled_objs[:, i]*sf[i]
    
    
    return scaled_objs.copy()
    


# ----


def rotate_x(matrix, deg):
    
    theta = deg2rad(deg)
    
    rot = np.array([
                [1,  0,             0],
                [0,  np.cos(theta), -np.sin(theta)],
                [0,  np.sin(theta),  np.cos(theta)],
                ])
                
    
    return np.dot(matrix, rot)
    
def rotate_y(matrix, deg):
    
    theta = deg2rad(deg)
    
    rot = np.array([
                [np.cos(theta), 0,  np.sin(theta)],
                [0,             1,  0],
                [-np.sin(theta),0,  np.cos(theta)],
                ])
                
    
    return np.dot(matrix, rot)

def rotate_z(matrix, deg):
    
    theta = deg2rad(deg)
    
    rot = np.array([
                [np.cos(theta), -np.sin(theta), 0],
                [np.sin(theta),  np.cos(theta), 0],
                [0,              0,             1],
                ])
                
    
    return np.dot(matrix, rot)
    
    
def spin_cube(objs, rotation="positive"):
    """
    Rotate objs quadrant (ie the whole quadrant). 
    If rotation is "positive", the new_objs will be inside the positive quadrant. 
    If rotation is "negative", the new_objs will be inside the negative quadrant.
    
    Input
    objs        (pop_size, m_objs) objs matrix
    rotation    rotation option: "positive", "negative"
    
    Output
    new_objs    (pop_size, m_objs) rotated matrix
    """
    
    new_objs = objs.copy()
    
    if rotation == "positive":
        
        # rotate y
        objs_rot_y = rotate_y(objs, 180)
        
        # rotate yz
        objs_rot_yz = rotate_z(objs_rot_y, 270)
        
        # rotate yz, translate
        objs_rot_yzt = objs_rot_yz + 1
        
        new_objs = objs_rot_yzt.copy()
    
    
    elif rotation == "negative":
        
        # rotate y
        objs_rot_y = rotate_y(objs, 180)
        
        # rotate yz
        objs_rot_yz = rotate_z(objs_rot_y, 270)
        
        new_objs = objs_rot_yz.copy()
        
    else:
        
        print("not valid rotation option") # @todo: add log support here
        
        
    return new_objs
    

    
    
if __name__ == "__main__":
    
    
    a = np.array([[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12]])
    
    b = scaling(a, "irregular")
