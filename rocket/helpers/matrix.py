# matrix.py
from __future__ import print_function
import numpy as np

def replace_zero_weigths(weights, eps=0.00001):
    """
    Replace zero weights given an input matrix
    
    Input
    weights     (pop_size, m_objs) array, weights matrix
    eps         new weight value
    
    Output
    W           (pop_size, m_objs) array, new weights matrix
    """
    
    # replace zero weights
    W           = weights.copy()
    to_keep     = weights == 0
    W[to_keep]  = eps           #0.00001               # recommended value [Ishibuchi16]
    
    return W.copy()
