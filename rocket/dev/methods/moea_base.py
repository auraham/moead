"""
moea_base.py
"""
from __future__ import print_function

from pylab import *
from python_repository.moea.dominance import compare_max, compare_min
from python_repository.moea.sbx2 import sbx2
from python_repository.moea.polymutation2 import polymutation2


class MoeaBase(object):
    
    def __init__(self, params, name):
        
        self.name = name
        
        
        # params
        self.lbound             = params['lbound']                              # limite inferior de las variables de diseno
        self.ubound             = params['ubound']                              # limite superior de las variables de diseno
        
        
        self.chroms             = self.evaluate_bounds(params['initial_pop'])   # poblacion inicial
        self.n_genes            = self.chroms.shape[1]                          # numero de variables de diseno
        self.pop_size           = self.chroms.shape[0]                          # tamano poblacion
        
        
        
        self.minmax             = params['minmax']                              # tipo de problema: minimizacion o maximizacion 
        self.iters              = params['ea_iters']                               # numero de generaciones
        
        self.fun_crossover      = sbx2                                          # operador de cruza
        self.fun_mutation       = polymutation2                                 # operador de mutacion
    
        self.prob_mutation      = params['prob_mutation']                       # probabilidad de mutacion
        self.prob_crossover     = params['prob_crossover']                      # probabilidad de cruza
        
        self.eta_mutation       = params['eta_mutation'] 
        self.eta_crossover      = params['eta_crossover']
        
        
        self.randomizer         = params['randomizer']                          # generador de numeros aleatorios
        
        self.main_problem       = params['main_problem']                        # problema principal a optimizar (vector de funciones objetivo)
        
        self.m_main_objs        = len(self.main_problem) if self.main_problem else self.m_reference_objs   # numero de objetivos problema de referencia
        
        #self.reference_objs     = zeros((self.pop_size, self.m_reference_objs)) # objetivos problema de referencia
        #self.main_objs          = zeros((self.pop_size, self.m_main_objs))      # objetivos problema principal
        
        
        # tipo de problema
        # este parametro cambia la forma en que se evalua la dominancia
        # como se explica en dominance.py
        if params['minmax'] == 'min':
            self.dominance_comparison_operator = compare_min
        else:
            self.dominance_comparison_operator = compare_max
        
        
        
        """
        metodo de escalarizacion/multiobjetivizacion
        si es None, se tratara de un moea
        si es una funcion, se tratara de un alg evolutivo simple
        """
        self.weights_matrix        = params['weights_matrix']
        self.reformulation_method  = params['reformulation_method'] 
        
        """
        parametros opcionales (verbose)
        """
        self.verbose = params.get('verbose', False)
        
        
        """
        parametros opcionales (estadisticas)
        """
        self.statistics_window_size = params.get('statistics_window_size', 1)
        self.fronts_window_size     = params.get('fronts_window_size', 0)
        self.real_front             = params.get('real_front', None)
        self.extremes               = params.get('extremes', None)
        self.igd_limit              = params.get('igd_limit', None)
        
            
        """
        parametros opcionales (initial_main_objs y initial_reference_objs)
        en caso de pasar estos parametros, no se realizara la primea evaluacion de 
        la poblacion de padres en main()
        """
        self.initial_main_objs      = params.get('initial_main_objs', None)
        self.initial_reference_objs = params.get('initial_reference_objs', None)
        
        
        
        """
        contador de evaluaciones de la funcion objetivo
        aumenta en cada llamada a self.evaluate_pop
        """
        self.evaluations = 0
        
        
        
    def main(self):
        """
        Ciclo principal
        """
    
    
    
    def evaluate_pop(self, chroms):
        """
        Evalua la poblacion (chroms, objs) utilizando el problema de optimizacion principal (self.main_problem)
        
        Entrada
        chrom       Matriz de (pop_size, n_genes) con vectores de diseno
        
        Salida
        new_objs    Matriz de (pop_size, m_objectives) con vectores objetivo luego de la evaluacion
        """
        pass
        
    def evaluate_pop_using_reference_problem(self, chroms, new_main_objs):
        """
        Evalua a la poblacion (chroms) en alguna reformulacion del problema original
        Por defecto, solo se devuelven los objetivos previamente evaluados (new_main_objs)
        """
        
        if self.reformulation_method:
            
            
            #print "evaluacion por reformulacion"
            
            wm_rows, wm_cols = self.weights_matrix.shape
            
            if wm_rows != self.pop_size:
                print("la matriz de pesos no tiene el numero de filas adecuado, debe ser igual a pop_size")
                
            if wm_cols != self.m_main_objs:
                print("la matriz de pesos no tiene el numero de columnas adecuado, debe ser igual a m_main_objs")
            
            """
            aplicar metodo de escalarizacion/multiobjetivizacion
            """
            new_reference_objs = self.reformulation_method(chroms, new_main_objs, self.weights_matrix)
            self.m_reference_objs = new_reference_objs.shape[1]
            return new_reference_objs.copy()
            
        else:
        
            #print "evaluacion con multiples objetivos"
        
            """
            como no hay metodo de escalarizacion, solo devuelve los objetivos originales
            """
            self.m_reference_objs = new_main_objs.shape[1]  # el numero de objetivos de referencia es igual al numero de objetivos originales
            return new_main_objs.copy()
    
    def parent_selection(self, chroms):
        """
        Devuelve un mating pool (array de indices) a partir de torneos binarios entre padres
        
        
        Entrada
        chroms          Matriz de (pop_size, n_genes) con vectores de diseno
        
        Salida
        mating_pool     Array de indices
        """
        
        pass
        
    def update_selection(self, chroms):
        pass
        
    def mutation(self, chroms):
        """
        Aplica mutacion sobre la poblacion (chroms)
        
        Entrada:
        chroms          Matriz de (pop_size, n_genes) con vectores de diseno
        
        Salida:
        new_chroms      Matriz de (pop_size, n_genes) con vectores de diseno mutados
        """
        pass
        
    def crossover(self, chroms):
        """
        Realiza la cruza de los individuos en la poblacion (chroms)
        
        Entrada
        chrom       Matriz de (pop_size, n_genes) con vectores de diseno
        
        Salida
        childs      Matriz de (pop_size, n_genes) con vectores de diseno
        """
        pass
        
    def evaluate_bounds(self, chroms):
        """
        Evalua si algun cromosoma  contiene algun gen fuera 
        del intervalo permitido para cada variable de diseno.
        Si algun gen excede el limite, se le asiga el valor de limite
        
        Entrada:
        chroms          Matriz de (pop_size, n_genes) con vectores de diseno

        Salida:
        new_chroms      Matriz de (pop_size, n_genes) con vectores de diseno dentro del rango

        """
        
        pop_size, n_genes = chroms.shape
        new_chroms = chroms.copy()
        
        
        for c in range(pop_size):
        
            # para cada variable de diseno
            for n in range(n_genes):
                
                # verificacion de lbound
                if chroms[c, n] < self.lbound[n]:     # les asigna el valor minimo a todos
                    new_chroms[c, n] = self.lbound[n]     # los que excedan el limite inferior
                
                # verificacion de ubound
                if chroms[c, n] > self.ubound[n]:     # les asigna el valor maximo a todos
                    new_chroms[c, n] = self.ubound[n]     # los que excedan el limite superior
                
            
        return new_chroms
