"""
moead.py
"""
from __future__ import print_function

from pylab import *
from scipy.spatial.distance import cdist
from rocket.dev.methods.moea_base import MoeaBase
from rocket.dev.methods.moea_statistics import MoeaStatistics

class MOEAD(MoeaBase, MoeaStatistics):
    
    def __init__(self, params, name='MOEA/D'):
        
        MoeaBase.__init__(self, params, name)
        MoeaStatistics.__init__(self)
        
        """
        los parametros 'weights_matrix' y 'reformulation_method' se pasan en moea_base.py
        sin embargo, estos dos parametros son opcionales en moea_base.py y nsga2.py
        en moead.py son obligatorios, por lo tanto, se debe realizar la siguiente verificacion
        """
        
        
        if self.weights_matrix.shape[0] != self.chroms.shape[0] or self.pop_size != self.chroms.shape[0]:
            print("Error moead.py, no coindicen los valores de (weights_matrix: %d, chroms: %d, pop_size: %d)" % (self.weights_matrix.shape[0], self.chroms.shape[0], self.pop_size))
        
        
        
        """
        de aqui en adelante son parametros de moea/d
        """
        self.neighborhood_size      = params['neighborhood_size']       # tamano del vecindario (parametro T)
        
        
       
        """
        note que nsga2.py NO realiza la actualizacion de z_ideal ni z_nadir
        esto se debe a que nsga2 no requiere ninguna transformacion del problema original ni lo normaliza
        
        en cambio moead.py SI require z_ideal y z_nadir (dependiendo del metodo de escalarizacion)
        por este motivo, se colocan aqui
        """
        
        self.z_ideal                = ones((self.m_main_objs, ))*inf    #*1e+30  # inicializacion del vector ideal z
                                                                        # este es el mismo metodo que se usa en la implementacion de
                                                                        # moead en c++
                                                                        # no importa si el problema es min/max porque se usa abs(FX-Z_ideal) en tchebycheff
        
        
        self.z_nadir                = zeros((self.m_main_objs, ))
        
    
    
    def create_b_neighbors_matrix(self, chroms):
        """
        Crea la matriz B de (pop_size, neighborhood_size) (N=pop_size, T=neighborhood_size)
        donde B[i] = {i_1, ..., i_T} define los T vecinos mas cercanos al individuo (cromosoma) i
        en el espacio de decision, i=1, ..., N
        """
        
        T = self.neighborhood_size
        
        """
        distance matrix
        """
        distance_matrix = cdist(chroms, chroms)
        
        
        """
        sorting
        ordena cada una de las filas en distance_matrix
        """
        by_row = 1
        sorted_idxs = distance_matrix.argsort(axis=by_row)
        
        """
        T closest neighbors to each chrom
        contiene los T vecinos (indices) mas cercanos a cada individuo i, i=1,..., N
        note que entre estos T indices se encuentra el mismo indice i
        """
        b_neighbors_matrix = sorted_idxs[:, :T]
        
        return b_neighbors_matrix.copy()
    
    
    def update_z(self, objs):
        """
        Actualiza el vector de referencia z ideal con los valores MINIMOS de la matriz objs
        """
        
        for n in range(self.m_main_objs):
            
            if objs[n] < self.z_ideal[n]:
                self.z_ideal[n] = objs[n]
                
            if objs[n] > self.z_nadir[n]:
                self.z_nadir[n] = objs[n]
            
    
    def evaluate_chrom_using_main_problem(self, chrom):
        
        n_genes         = self.n_genes
        m_objs          = self.m_main_objs
        new_main_objs   = zeros((1, m_objs))
        
        chrom_tmp = chrom.reshape(1, n_genes).copy()
        
        
        for m in range(m_objs):
            new_main_objs[0, m] = self.main_problem[m](chrom_tmp, new_main_objs)
            
            
        # counter
        self.evaluations += 1
        
        # update z
        new_main_objs = new_main_objs.flatten()
        self.update_z(new_main_objs)
        
        return new_main_objs.copy()
        
    
    def evaluate_chroms_using_main_problem(self, chroms):
        """
        Entrada
        chroms      Matriz de (pop_size, n_genes)
        """
        
        pop_size    = self.pop_size
        m_objs      = self.m_main_objs
        
        new_main_objs = zeros((pop_size, m_objs))
        
        
        for n in range(self.pop_size):
            new_main_objs[n, :] = self.evaluate_chrom_using_main_problem(chroms[n].copy())
            
            
        return new_main_objs
    
    
    def evaluate_chroms_using_scalarization(self, new_main_objs, weights_matrix):
        
        pop_size            = new_main_objs.shape[0]       # se toma del tamano de new_main_objs, porque puede ser toda la poblacion o un vecindario!
        m_objs              = self.m_main_objs
        neighborhood_size   = weights_matrix.shape[0]
        
        if len(new_main_objs.shape) == 1:
            new_main_objs = new_main_objs.reshape(1, m_objs).repeat(neighborhood_size, axis=0)
        
        
        # es necesario que los vectores z_ideal, z_nadir tengan un shape de (1, m_objs)
        # independientemente de pop size 
        tmp_z_ideal = self.z_ideal.reshape(1, m_objs)
        tmp_z_nadir = self.z_nadir.reshape(1, m_objs)
        
        
        new_scalar_objs = self.reformulation_method(None, new_main_objs, weights_matrix, tmp_z_ideal, tmp_z_nadir)
        
        
        
        return new_scalar_objs.flatten().copy()
        
   
    def evaluate_initial_pop(self, chroms, weights_matrix):
        
        new_main_objs   = self.evaluate_chroms_using_main_problem(chroms)
        new_scalar_objs = self.evaluate_chroms_using_scalarization(new_main_objs, weights_matrix)
        
        return new_main_objs.copy(), new_scalar_objs.copy()
    
        
    def parent_selection(self, neighborhood, i):
        """
        Devuelve dos padres (indices diferentes a i) 
        a partir de un vecindario (conjunto de indices)
        
        
        Entrada
        neighborhood        Vector de (neighborhood_size, ) con indices
        i                   Indice
        
        Salida
        k, l                Indices 'padres' distintos a i
        
        @todo: hacer que selected[0] y selected[1] sean tambien diferentes
        """
        
        selected = array([i, i])
        
        for idx in range(2):
            while True:
                selected[idx] = self.randomizer.permutation(neighborhood)[0]
                if selected[idx] != i:
                    break
                    
                    
        return selected[0], selected[1]

    
    def crossover(self, p1, p2):
        """
        Realiza la cruza de los padres p1, p2 (arrays de n_genes elementos)
        Segun la definicion de MOEA/D, la cruza debe devolver solo un hijo por cada pareja de padres
        Por tal motivo, solo se devuelve c1
        
        Entrada
        p1, p2      Vectores de (n_genes, )
        
        Salida
        c1          Vector de (n_genes, )
        """
        
         
        c1, c2 = self.fun_crossover(p1, p2, self.prob_crossover, self.eta_crossover, self.lbound, self.ubound, self.randomizer)
        
        return c1.copy()
    
    
    def mutation(self, child):
        """
        Aplica mutacion sobre el individuo child
        
        Entrada:
        child           Vector de (n_genes, )
        
        Salida:
        new_child       Vector de (n_genes, ) mutados
        """
        
        
        # convencion
        prob_mutation = self.prob_mutation
        eta_mutation = self.eta_mutation
        randomizer = self.randomizer
        lbound = self.lbound
        ubound = self.ubound
        
            
        new_child = self.fun_mutation(child, prob_mutation, eta_mutation, lbound, ubound, randomizer)

            
        return new_child.copy()
        
        
    def genetic_operations(self, i, neighbors_idxs, chroms):
        """
        Aplica cruza y mutacion al i-esimo cromosoma (x_i) de la poblacion
        
        Entrada
        i               Indice del individo (x_i) al cual seran aplicados los operadores geneticos
        neighbors_idxs  Indices de los vecinos de x_i
        
        Salida
        child           Array de (n_genes, ) con el cromosoma modificado
        """
        
        """
        parents
        """
        p1_idx, p2_idx = self.parent_selection(neighbors_idxs, i)
        p1 = chroms[p1_idx]
        p2 = chroms[p2_idx]


        """
        operadores geneticos
        """
        child = self.crossover(p1.copy(), p2.copy())
        child = self.mutation(child.copy())
        
        
        return child.copy()
        

    def evaluate_child_bounds(self, child):
        
        n_genes = child.shape[0]
        new_chrom = child.copy()
        
        
        
        # para cada variable de diseno
        for n in range(n_genes):
            
            # verificacion de lbound
            if child[n] < self.lbound[n]:               # les asigna el valor minimo a todos
                new_chrom[n] = self.lbound[n]       # los que excedan el limite inferior
            
            # verificacion de ubound
            if child[n] > self.ubound[n]:           # les asigna el valor maximo a todos
                new_chrom[n] = self.ubound[n]       # los que excedan el limite superior
            
            
        return new_chrom.copy()

    def main(self):
        
        
        """
        pesos
        """
        weights_matrix = self.weights_matrix
        
    
        """
        intial pop
        """
        chroms = self.chroms
        
        """
        create b neighbors matrix
        """
        b_neighbors = self.create_b_neighbors_matrix(weights_matrix) 
        
        
        """
        evaluate pop
        """
        main_objs = self.evaluate_chroms_using_main_problem(chroms)
        
        
        """
        main loop
        """
        t = 1
        
        # added: eval stats
        if self.statistics_window_size:
            
            if t%self.statistics_window_size == 0:
            
                self.evaluate_statistics(chroms.copy(), main_objs.copy(), self.real_front, self.extremes)

                self.fronts_hist.append(main_objs.copy())
        
        
        t += 1
        while t<= self.iters:
            

            if self.verbose:
                if t%10 == 0: print("t:", t)
            
            
            for i in range(self.pop_size):
            
            
                """
                neighborhood of i
                """
                neighbors_idxs = b_neighbors[i]
                
                
                """
                objetivos del vecindario
                """
                main_objs_neighbors   = main_objs[neighbors_idxs]       # m objs
                #neighbors_scalar_objs = scalar_objs[neighbors_idxs]     # scalar
            
                """
                pesos del vecindario
                """
                neighbors_weights = weights_matrix[neighbors_idxs]
            
            
                """
                genetic operations
                """
                child = self.genetic_operations(i, neighbors_idxs, chroms)
                
                
                """
                evaluate bounds
                """
                child = self.evaluate_child_bounds(child)
                
                """
                evaluate (main_objs) child and update z_ideal
                """
                main_objs_child = self.evaluate_chrom_using_main_problem(child)
                
                
                """
                evaluate scalar_objs
                """
                scalar_objs_child       = self.evaluate_chroms_using_scalarization(main_objs_child.copy(), neighbors_weights.copy())
                scalar_objs_neighbors   = self.evaluate_chroms_using_scalarization(main_objs_neighbors.copy(), neighbors_weights.copy())
                
                
                """
                get indices to replace
                """
                to_replace_bool = scalar_objs_child <= scalar_objs_neighbors
                to_replace_idxs = neighbors_idxs[to_replace_bool]
                
                """
                neighborhood update
                """
                chroms[to_replace_idxs, :] = child.copy()                                   # child es de (n_genes, )
                main_objs[to_replace_idxs, :] = main_objs_child.copy()                      # childs_main_objs es de (m_objs, )
                
                
            """
            estadisticas
            """
            if self.statistics_window_size:
            
                if t%self.statistics_window_size == 0:
                
                    self.evaluate_statistics(chroms.copy(), main_objs.copy(), self.real_front, self.extremes)

                    self.fronts_hist.append(main_objs.copy())
                        
                    
                
                
            """
            siguiente generacion
            """
            t+=1
        
        
        #return chroms.copy(), main_objs.copy()

        # solo para que sea de la misma forma que nsga2.py
        return chroms.copy(), main_objs.copy(), main_objs.copy()


     
