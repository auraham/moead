"""
replacement.py
"""

from pylab import *

def replacement(scalar_child, scalar_pop):
    """
    Determina el individuo (indice k) que debe ser reemplazado por child.
    
    Primero se determina el k-th problema en donde child es el mejor.
    Luego se verifica si scalar_child[k] es mejor que scalar_pop[k]
    
    """
    
    to_replace = []
    
    # k is the problem which child is the best solution
    k = ((scalar_pop - scalar_child) / (scalar_pop)).argmax()
    
    if scalar_child[k] < scalar_pop[k]:
        to_replace = k
        
    return to_replace
