"""
moead_gra.py

Implementacion de [Zhou2016]. La diferencia entre esta implementacion y la de [Zhou2016] es que yo me baso en el MOEA/D original (moead.py), mientras que [Zhou2016] se basa en una version posterior de MOEA/D llamada MOEA/D-DE [Li2009]

[Zhou2016] Are all the subproblems equally important? Resource allocation in decomposition-based multiobjective evolutionary algorithms.
[Li2009] Multiobjetive optimization problems with complicated pareto sets, MOEA/D and NSGA-II.

"""
from __future__ import print_function

from pylab import *
from rocket.dev.methods.moead import MOEAD
from rocket.dev.methods.genetic_operations import genetic_operations
from rocket.dev.methods.replacement import replacement
from numpy.random import RandomState

# debug
from rocket.dev.rand import Rand

class MOEAD_GRA(MOEAD):

    def __init__(self, params, name="MOEAD_GRA"):
        
        MOEAD.__init__(self, params, name=name)
        
        
        self.delta = params['delta']
        
        
    def update_improvement(self, fit_old_pop, fit_new_pop):
        """
        Implementa la actualizacion de la mejora relativa, Eq (5) de [Zhou2016]
        """
        
        
        n   = self.pop_size         # number of subproblems
        u   = zeros((n, ))          # relative improvement
        poi = zeros((n, ))          # probability of improvement vector
        eps = 1e-50                 # to avoid zero division
        
        almost_zero = 0.00001       # reset poi threshold 
        
        for i in range(n):          # por cada subproblema
            
            u[i]    = (fit_old_pop[i] - fit_new_pop[i]) / (fit_old_pop[i] + eps)  # we need to add an eps to avoid zero division
            
        u_max = u.max()
        
        # check for reset
        if u_max < almost_zero:
            
            if self.verbose:
                print("reseting poi (u_max %.2)" % (u_max))
            
            poi = ones((n, ))       # now, each subproblem has a probability of 1
            return poi.copy()
        
        
        for i in range(n):          # se usan dos loops diferentes porque en este se necesita que el array u este completo
            
            poi[i]  = (u[i] + eps) / (u_max + eps)
            
        
        return poi.copy()
            
            
    
    def get_next_subproblem(self, poi, i):
        """
        Selecciona el siguiente subproblema (indice i) a evaluar:
            - Si rand() < poi[i], se selecciona el i-th problema
            - Sino, se prueba con otro problema j y se repite el proceso
        """
        
        next_sp = i
        
        if self.randomizer.rand() < poi[i]:
            
            # si rand() < poi[i], se selecciona el i-th problema
            next_sp = i
            
        else:
            
            # sino, seleccionamos otro problema con mayor poi
            
            while True:
                
                j = self.randomizer.randint(0, self.pop_size)   # elegimos un problema (indice) al azar, entre 0 y pop_size-1
                
                if self.randomizer.rand() < poi[j]:             # si rand() < poi[j], entonces seleccionamos el j-th problema         
                    next_sp = j
                    break
                    
        return next_sp
            
    
    def main(self):
        
        # weights
        weights_matrix = self.weights_matrix
        
        # initial pop
        chroms = self.chroms
        
        # added
        #savetxt("init_chroms.txt", chroms); print("chroms saved")
        chroms = genfromtxt("init_chroms.txt")
        self.randomizer = Rand()
        
        # create neighborhood
        b_neighbors = self.create_b_neighbors_matrix(weights_matrix) 
        
        # evaluate pop
        main_objs = self.evaluate_chroms_using_main_problem(chroms)
        
        
        # added to moea/d-gra
        poi = ones((self.pop_size, ))                                   # se inicializa como vector de unos
        fit_old_pop = self.evaluate_chroms_using_scalarization(main_objs.copy(), weights_matrix.copy())
        fit_new_pop = fit_old_pop.copy()

        # stats
        poi_evals = zeros((self.pop_size, ))                               # function evals counter
        
        # main loop
        t = 2
        while t <= self.iters:                                          # por cada iteracion
            
            for k in range(self.pop_size):                             # por cada subproblema
            
                # added to moea/d-gra
                #if self.randomizer.rand() < poi[i]:
                i = self.get_next_subproblem(poi, k)
            
                # genetic operations
                child = genetic_operations(i, b_neighbors, chroms, self.lbound, self.ubound, self.pop_size, self.prob_crossover, self.prob_mutation, self.randomizer)
                        
                # evaluate bounds
                child = self.evaluate_child_bounds(child)
                
                # evaluate (main_objs) child and update z_ideal
                main_objs_child = self.evaluate_chrom_using_main_problem(child)
                
                # evaluate scalar_objs
                scalar_objs_child   = self.evaluate_chroms_using_scalarization(main_objs_child.copy(), weights_matrix.copy())
                scalar_objs_pop     = self.evaluate_chroms_using_scalarization(main_objs.copy(), weights_matrix.copy())
                
                # get indices to replace
                to_replace = replacement(scalar_objs_child, scalar_objs_pop)
                
                
                # neighborhood update
                chroms[to_replace, :] = child.copy()                                   # child es de (n_genes, )
                main_objs[to_replace, :] = main_objs_child.copy()                      # childs_main_objs es de (m_objs, )
                
                # stats
                poi_evals[i] += 1
                    
            # stats
            if self.statistics_window_size:
            
                if t%self.statistics_window_size == 0:
                
                    self.evaluate_statistics(chroms.copy(), main_objs.copy(), self.real_front, self.extremes)

                    self.fronts_hist.append(main_objs.copy())
                    
                    
            # update PoI
            if t % self.delta == 0:
                
                fit_old_pop = fit_new_pop.copy()
                fit_new_pop = self.evaluate_chroms_using_scalarization(main_objs.copy(), weights_matrix.copy())
                
                poi = self.update_improvement(fit_old_pop, fit_new_pop)
                
                
            # next iter
            t+=1

        
        # stats
        self.poi_evals = poi_evals.copy()
        
        return chroms.copy(), main_objs.copy(), main_objs.copy()

        
        
