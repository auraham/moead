"""
genetic_operations.py
Implementa la cruza y mutacion de MOEA/D-GRA descrita en el Algoritmo 2 de [Zhou2016]
"""

from pylab import *

def parent_selection(pop_size, neighbors, i, randomizer, ps_delta=0.8):
    """
    Devuelve dos padres (indices diferentes a i)
    a partir de un mating pool.
    
    El mating pool se define usando un flip:
    
        - Si rand() < ps_delta:
            matin_pool = neighbors          # vecinos de i
            
        - Sino, se usa a toda la poblacion como mating pool
            matin_pool = (0, 1, ..., pop_size-1)
    
    ps_delta=0.8 es el valor usado en [Zhou2016]
    """
    
    # set mating pool
    mating_pool = arange(0, pop_size, dtype=int)
    
    if randomizer.rand() < ps_delta:
        mating_pool = neighbors.copy()
        
    
    # choose parents
    p1, p2 = 0, 0
    while True:
        
        p1 = randomizer.permutation(mating_pool)[0]
        p2 = randomizer.permutation(mating_pool)[0]
        
        if p1 != i and p2 != i and p1 != p2:
            break
            
    return p1, p2

def crossover(p0, p1, p2, prob_crossover, randomizer, F=0.5):
    """
    En la implementacion original siempre hay cruza.
    En esta implementacion, agregue prob_crossover
    """
    
    child = p0.copy()
    
    if randomizer.rand() < prob_crossover:
        child = p0 + F*(p1 - p2)
    
    return child.copy()
    

def repair(parent, child, lbound, ubound, randomizer):
    
    n_genes = child.shape[0]
    new_child = child.copy()
    
    for n in range(n_genes):
        
        if child[n] < lbound[n]:
            new_child[n] = parent[n] - randomizer.rand() * (parent[n] - lbound[n])
            
        if child[n] > ubound[n]:
            new_child[n] = parent[n] + randomizer.rand() * (ubound[n] - parent[n])
            
    return new_child.copy()
    
    
def mutation(child, randomizer, prob_mutation, lbound, ubound, eta=20):
    
    n_genes = child.shape[0]
    new_child = child.copy()
    
    for n in range(n_genes):
        
        # si flip() es True y child[n] esta dentro de los limites
        if randomizer.rand() < prob_mutation and child[n] >= lbound[n] and child[n] <= ubound[n]:
            
            r           = randomizer.rand()
            mut_delta   = 0
            
            if r < 0.5:
                frac  = ((ubound[n] - child[n]) / (ubound[n] - lbound[n])) ** (eta+1)
                power = (1.0/(eta+1))
                base  = 2*r + (1-2*r) * frac
                mut_delta = (base**power) - 1
            else:
                frac  = ((child[n] - lbound[n]) / (ubound[n] - lbound[n])) ** (eta+1)
                power = (1.0/(eta+1))
                base  = 2 - 2*r + (2*r - 1) * frac 
                mut_delta = 1 - (base**power)
            
            
            
            new_child[n] = child[n] + mut_delta * (ubound[n] - lbound[n])

    return new_child.copy()

def genetic_operations(i, neighborhood, chroms, lbound, ubound, pop_size, prob_crossover, prob_mutation, randomizer):
    """
    Devuelve un nuevo individuo
    
    i               indice del subproblema
    neighbors       (pop_size, neigh_size) vecindario
    chroms          (pop_size, n_genes) matriz de chroms
    """
    
    # parent selection
    p1, p2 = parent_selection(pop_size, neighborhood[i], i, randomizer)
    
    # crossover (siempre hay cruza en la implemetacion original!)
    child = crossover(chroms[i], chroms[p1], chroms[p2], prob_crossover, randomizer)
    
    # repair (checar los bounds)
    child = repair(chroms[i], child, lbound, ubound, randomizer)
    
    # mutation
    child = mutation(child, randomizer, prob_mutation, lbound, ubound)
    
    return child.copy()
    
    
    
    
    
