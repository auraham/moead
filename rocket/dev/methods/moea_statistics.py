"""
moea_statistics.py
"""

from pylab import *
from python_repository.performance_metrics.convergence.convergence import IGD, GD, delta_p
from python_repository.performance_metrics.diversity.delta_metric import get_delta_metric as delta_metric
from python_repository.performance_metrics.solow_polasky import solow_polasky
from python_repository.performance_metrics.r2_ind import r2_indicator

class MoeaStatistics(object):
    
    def __init__(self):
        
        # @todo: combinar moea_base.py y moea_stats.py
        # @todo: de este modo, podemos hacer algo como n=iters/stat_window_size, y fijar el tamano de las listas a n
        
        """
        historial de convergencia
        """
        self.convergence_igd = [] 
        self.convergence_gd  = []
        self.convergence_delta_p = []   # distancia hausdorff promedio
        
        
        
        """
        historial de diversidad
        """
        self.diversity_delta = []
        
        
        """
        historial de frentes encontrados
        """
        self.fronts_hist = []
        
        
        """
        historial de formulaciones
        """
        self.hist_reformulations = []
        
        
        # added
        self.hist_solow_polasky = []
        self.hist_r2 = []
        
        
    def evaluate_statistics(self, chroms, objs, real_front, extremes, reformulation=0):
        """
        Evalua diferentes metricas de convergencia y diversidad a partir
        de la poblacion (chroms, objs)
        
        Entrada
        
        chroms          Array de (pop_size, n_genes)
        objs            Array de (pop_size, m_objectives)
        real_front      Array con el frente de Pareto a comparar con objs
        extremes        Array con los puntos extremos (usando en delta metric)
        """
        
        """
        metricas de convergencia
        """
        
        """
        IGD(known_fronts, real_front)
        """
        igd = IGD(objs, real_front)
        self.convergence_igd.append(igd)
        
        
        """
        GD(known_fronts, real_front)
        """
        gd = GD(objs, real_front)
        self.convergence_gd.append(gd)
        
        
        """
        distancia Hausdorff promedio
        delta_p(known_fronts, real_front)
        """
        hausdorff = delta_p(objs, real_front)
        self.convergence_delta_p.append(hausdorff)
        
        
        """
        metricas de diversidad
        """
        
        """
        delta(objs, extremes)
        """
        #delta = delta_metric(objs, extremes)
        #self.diversity_delta.append(delta)
        
        
        """
        reformulaciones
        por defecto es 0
        """
        self.hist_reformulations.append(reformulation)
        
        # added
        #sp = solow_polasky(objs.copy())
        sp = -1                             # para evitar problemas con moead_ipbi
        self.hist_solow_polasky.append(sp)
        
        #value = r2_indicator(objs.copy())
        value = 0
        self.hist_r2.append(value)
        
    def get_statistics(self):
        """
        Devuelve una matriz de (ea_iters, n_metrics)
        donde cada columna representa una metrica
        """
        
        
        n = len(self.convergence_igd)
        
        array_igd               = array(self.convergence_igd).reshape(n, 1)
        array_gd                = array(self.convergence_gd).reshape(n, 1)
        array_delta_p           = array(self.convergence_delta_p).reshape(n, 1)
        array_reformulations    = array(self.hist_reformulations).reshape(n, 1)
        
        
        #array_delta = array(self.diversity_delta).reshape(n, 1)
        
        
        # added
        array_sp = array(self.hist_solow_polasky).reshape(n, 1)
        array_r2 = array(self.hist_r2).reshape(n, 1)
        
        
        #metrics = hstack((array_igd, array_gd, array_delta))
        
        # @todo: reorganizar esto, quiza como en pymoea
        metrics = hstack((array_igd, array_gd, array_delta_p, array_reformulations, array_sp, array_r2))
        
        return metrics
        
    
        
        
    def get_fronts_hist(self):
        return list(self.fronts_hist)
    
    def get_hist_fronts(self):
        return list([front.copy() for front in self.fronts_hist])
    
        
