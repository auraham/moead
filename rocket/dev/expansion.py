"""
expansion.py
"""
from __future__ import print_function
from rocket.plot.colors import colors, basecolors
from rocket.plot import load_rcparams
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
from scipy.spatial.distance import cdist
from numpy.linalg import norm

def plot_translation(weights, base, nb_a, nb_b, nb_c):
    
    fig = plt.figure()
    ax = fig.add_subplot(111, projection="3d")
    ax.set_xlabel("$x$")
    ax.set_ylabel("$y$")
    ax.set_zlabel("$z$")
    
    ax.plot(weights[:, 0], weights[:, 1], weights[:, 2], c=colors[0], 
        mec=basecolors["almost_black"], ls="none", marker="o", markeredgewidth=0.15)
    
    ax.plot(base[:, 0], base[:, 1], base[:, 2], c=colors[2], 
        mec=basecolors["almost_black"], ls="none", marker="o", markeredgewidth=0.15)
    
    # translations
    for i, nb in zip((3, 4, 5), (nb_a, nb_b, nb_c)):
        ax.plot(nb[:, 0], nb[:, 1], nb[:, 2], c=colors[3], 
            mec=basecolors["almost_black"], ls="none", marker="o", markeredgewidth=0.15)
    
    
    
    for i, w in enumerate(weights):
        x, y, z = w
        ax.text(x, y, z, str(i), color="#404040")
    
    ax.view_init(30, 45)
    plt.show()
    
    return fig, ax
    
def plot_base(weights, base):
    
    fig = plt.figure()
    ax = fig.add_subplot(111, projection="3d")
    ax.set_xlabel("$x$")
    ax.set_ylabel("$y$")
    ax.set_zlabel("$z$")
    
    ax.plot(weights[:, 0], weights[:, 1], weights[:, 2], c=colors[0], 
        mec=basecolors["almost_black"], ls="none", marker="o", markeredgewidth=0.15)
    
    ax.plot(base[:, 0], base[:, 1], base[:, 2], c=colors[2], 
        mec=basecolors["almost_black"], ls="none", marker="o", markeredgewidth=0.15)
    
    
    for i, w in enumerate(weights):
        x, y, z = w
        ax.text(x, y, z, str(i), color="#404040")
    
    ax.view_init(30, 45)
    plt.show()
    
    return fig, ax
    
def get_dist(weights, a, b):
    
    
    a = weights[[a]]
    b = weights[[b]]
    dist = cdist(a, b)[0,0]
    
    
    print ("dist", dist)
    return dist
    
def get_base_simplex(p=5):
    
    a = np.array([1, 0, 0])
    b = np.array([0, 1, 0])
    c = np.array([0, 0, 1])
    
    base = np.array([a, b, c], dtype=float)
    
    nbase = base.copy()
    
    # 0 es el origen de la traslacion
    for i in (1, 2):
        
        d       = base[0] - base[i]
       
        factor  = norm(d)/p     # falta corregir este factor
        factor  = 0.75
        factor  = (norm(d)/(p))     # ie, p-1 veces el factor de escala original (norm(d)/p)
        
        factor = 0.20
        
        factor = (1/p)
        
        factor = (p-1)*(1/p)        # este es el factor que ocupamos!
        
        v = factor*d
        nbase[i] = base[i] + v
        
    return base, nbase
    
    

# ---- new functions ----

def get_base_simplex(m_objs, p, scale=1):
    """
    Return an (m-1) simplex scaled and translated to one corner
    
    Input
    m_objs      Number of objectives
    p           Number of divisions of original simplex
    scale       Scale factor from [Jain12]
    
    Output
    base        (m_objs, m_objs) array, base simplex
    """
    
    base    = np.eye(m_objs, dtype=float)
    factor  = (p-1)*(1.0/p)                 # scale factor 
    
    factor  = factor*scale
    
    for i in range(1, m_objs):
        
        d = base[0] - base[i]           # translation (direction) vector
        v = factor*d                    # scaled translation vector
        base[i] = base[i] + v           # move vector
    
    return base

def move_base_simplex(center_id, weights, base_simplex):
    
    m_objs = weights.shape[1]
    list_tbs = []
    
    a = weights[center_id]          # destiny
                                    # the base simplex will be translated around this vector
    
    for i in range(m_objs):
        
        b   = base_simplex[i]       # source
        d   = a - b                 # translation (direction) vector
        
        tbs = base_simplex + d      # translated base simplex
        
        # check
        if (tbs < 0).sum() > 0:        # if tbs contains negative values, it is skipped
            continue
            
        list_tbs.append(tbs)
        
    return list_tbs

def plot_configs(weights, base, configs):
    
    fig = plt.figure()
    ax = fig.add_subplot(111, projection="3d")
    ax.set_xlabel("$x$")
    ax.set_ylabel("$y$")
    ax.set_zlabel("$z$")
    
    ax.plot(weights[:, 0], weights[:, 1], weights[:, 2], c=colors[0], 
        mec=basecolors["almost_black"], ls="none", marker="o", markeredgewidth=0.15)
    
    ax.plot(base[:, 0], base[:, 1], base[:, 2], c=colors[2], 
        mec=basecolors["almost_black"], ls="none", marker="o", markeredgewidth=0.15)
    
    # translations
    for i, config in enumerate(configs):
        ax.plot(config[:, 0], config[:, 1], config[:, 2], c=colors[3], 
            mec=basecolors["almost_black"], ls="none", marker="o", markeredgewidth=0.15)
    
    
    
    for i, w in enumerate(weights):
        x, y, z = w
        ax.text(x, y, z, str(i), color="#404040")
    
    ax.view_init(30, 45)
    plt.show()
    
    return fig, ax

if __name__ == "__main__":

    load_rcparams()
    
    weights = np.genfromtxt("weights_m_3_21.txt")
    m_objs  = weights.shape[1]
    p       = 5                                             # number of division of original simplex
    
    # create base simplex
    base_simplex = get_base_simplex(m_objs, p, scale=1)
    
    # get all valid configurations
    center_id   = 8                 # use other index
    configs     = move_base_simplex(center_id, weights, base_simplex)
    
    plot_configs(weights, base_simplex, configs)

    """
    
    load_rcparams()
    
    weights = np.genfromtxt("weights_m_3_21.txt")
    base    = np.genfromtxt("weights_m_3_3.txt")
    
    # asumimos que ya tenemos el base
    base = np.array([
                    weights[20],
                    weights[19],
                    weights[18],
                    ])
                    
    # asumimos que ya tenemos el base
    # puede ser incluso otro conjunto de indices
    base = np.array([
                    weights[7],
                    weights[12],
                    weights[8],
                    ])
                    
                    
    base = get_base_simplex(3, p=5)
    
    plot_base(weights, base)
    
    
    a = np.array([[0, 0, 1]])
    b = np.array([[0, 1, 0]])
    p = 5
    dist = cdist(a, b)[0,0]
    part = dist / p
    
    
    # traslation nb_a
    a = weights[[15]]        # destiny, pick an index from [0, ..., 20]
    b = base[[0]]           # source, use base[[0]], base[[1]], base[[2]]
    c = a - b
    
    nb   = b + c              
    nb_a = base + c           # translate base simplex using  direction c
    
    # traslation nb_b
    b       = base[[1]]           # source, use base[[0]], base[[1]], base[[2]]
    c       = a - b
    nb_b    = base + c           # translate base simplex using  direction c
    
    # traslation nb_c
    b       = base[[2]]           # source, use base[[0]], base[[1]], base[[2]]
    c       = a - b
    nb_c    = base + c           # translate base simplex using  direction c
    
    fig, ax = plot_translation(weights, base, nb_a, nb_b, nb_c)
    """
    
    
    
    
    
    
    
