"""
common.py
"""
from __future__ import print_function
from pylab import *

# moea stuff
from python_repository.weights.create_vectors import create_vectors
from python_repository.weights.weights import get_predefined_weights_matrix
from python_repository.moea.moea_plotting import *

# scalarization methods
from python_repository.scalarization_methods.ws  import ws,  nws
from python_repository.scalarization_methods.ls  import ls,  nls
from python_repository.scalarization_methods.te  import te,  nte
from python_repository.scalarization_methods.ate import ate, nate
from python_repository.scalarization_methods.mte import mte, nmte
from python_repository.scalarization_methods.pbi import pbi

# moea problems and real fronts
from python_repository.benchmark.liu2014.mop1 import mop1_f1, mop1_f2, get_mop1_pareto_front
from python_repository.benchmark.liu2014.mop2 import mop2_f1, mop2_f2, get_mop2_pareto_front
from python_repository.benchmark.liu2014.mop3 import mop3_f1, mop3_f2, get_mop3_pareto_front
from python_repository.benchmark.liu2014.mop4 import mop4_f1, mop4_f2, get_mop4_pareto_front
from python_repository.benchmark.liu2014.mop5 import mop5_f1, mop5_f2, get_mop5_pareto_front

# basic stuff
from python_repository.random.random_generator import RandomGenerator
from numpy.random import RandomState

import os
script_path = os.path.dirname(os.path.abspath(__file__))

def get_bounds(mop_name, n_genes):
    
    # debug
    #import ipdb; ipdb.set_trace()
    
    lbound = zeros((n_genes, ))
    ubound = ones((n_genes, ))
    
    if mop_name in ("t1", "t2"):
        lbound[0]   = 0
        lbound[1:]  = -1
        
        ubound[:]   = 1
        
    
    elif mop_name == "zdt4":
        lbound[0]   = 0
        lbound[1:]  = -5
        
        ubound[0]   = 1
        ubound[1:]  = 5
        
    elif mop_name in ("zdt1", "zdt2", "zdt3", "zdt6"):
        lbound = zeros((n_genes, ))
        ubound = ones((n_genes, ))
        
    elif mop_name in ("dtlz1", "dtlz2", "dtlz3", "dtlz4", "dtlz5", "mop1", "mop2", "mop3", "mop4", "mop5"):
        lbound = zeros((n_genes, ))
        ubound = ones((n_genes, ))
        
    else:
        print("error on bounds!")
    
    return lbound, ubound
        
def get_params(iters, seed, n_genes, mop, mop_name, real_front):
    """
    Devuelve un diccionario con los parametros comunes a varios moeas
    """
    
    params = {}
    randomizer = RandomState(seed)
    m_objs = len(mop)
    
    # weights
    #weights = get_predefined_weights_matrix(240-1, m_objs) if m_objs == 2 else get_predefined_weights_matrix(19, m_objs)
    weights = genfromtxt(os.path.join(script_path, "weights_m_2_240.txt")) if m_objs == 2 else genfromtxt(os.path.join(script_path, "weights_m_3_210.txt"))
    pop_size = weights.shape[0]
    
    # bounds
    lbound, ubound = get_bounds(mop_name, n_genes)
    
    
    # MOEA/D, MOEA/D-GRA params
    params['minmax']                = 'min'                 # minimizacion o maximizacion 
    params['lbound']                = lbound.copy()         # limite inferior de las variables de diseno
    params['ubound']                = ubound.copy()         # limite superior de las variables de diseno
    params['ea_iters']              = iters                 # numero de generaciones
    
    params['prob_mutation']         = 0.1
    params['prob_crossover']        = 0.8
    
    params['eta_mutation']          = 20
    params['eta_crossover']         = 10
    
    params['neighborhood_size']     = 20
    params['weights_matrix']        = weights.copy()
    params['reformulation_method']  = te
    
    params['statistics_window_size']= int(params['ea_iters']*0.1)
    params['verbose']               = False


    params['initial_pop']           = randomizer.rand(pop_size, n_genes)
    params['main_problem']          = mop                                   
    params['real_front']            = real_front.copy()
    params['randomizer']            = RandomState(seed)
    

    return params

def get_unitary_vectors():
    """
    Devuelve un conjunto de vectores unitarios (para graficacion)
    """
    
    k_subproblems   = 10
    subpop_size     = 24
    vectors         = create_vectors(k_subproblems-1, 2, add_eps=False)     # vectores uniformemente espaciados    
    unitary_vectors = array([v/norm(v) for v in vectors])                   # normalizacion
    
    return unitary_vectors.copy()
   
def get_step_labels(ea_iters, stats_window_size):
    """
    Devuelve una lista de steps
        (5, 10, 15, ... iters)
    """

    # labels
    k = ea_iters
    step = stats_window_size
    step_labels = ["t %d" % (t, ) for t in arange(step, k*step+1, step)]
    
    return step_labels

def plot_subproblems_budget(alg_name, poi, output=""):
    
    n = poi.shape[0]
    x = arange(n)
    y = poi
    
    fig = figure(alg_name+"-poi")
    ax = fig.add_subplot(111)
    ax.cla()
    ax.set_xlim(0, n-1)
    ax.set_title("%s - Budget, Probability of Improvement vector" % (alg_name, ))
    ax.set_xlabel("Subproblem ID")
    ax.set_ylabel("Evaluations")
    
    ax.plot(x, y, c="#907373")
    show()
    
    if output:
        
        width = 10
        height = 4
        
        fig.set_size_inches(width, height)
        fig.savefig(output, dpi=100, bbox_inches='tight')
    

def run_experiment(algorithms, iters, mop, mop_name, real_front):
    
    n_genes     = 10
    seed        = 100
    params      = None
    
    # plotting
    uv = None #get_unitary_vectors() if len(mop) == 2 else None
    
    # lims
    lims = (1.6, 3) if len(mop) == 2 else (1, 1, 1)
            
    # stats
    chroms  = [None]*len(algorithms)
    objs    = [None]*len(algorithms)
    igd     = [None]*len(algorithms)
    sp      = [None]*len(algorithms)
    r2      = [None]*len(algorithms)
    labels  = [label for _, label, _ in algorithms]

    for i, alg in enumerate(algorithms):
    
        # unpack
        algorithm, algorithm_name, delta = alg
        
        # debug
        print(algorithm_name)
        
        # set params
        params                  = get_params(iters, seed, n_genes, mop, mop_name, real_front)
        params["delta"]         = delta
        
        # set moea
        moea = algorithm(params)
        
        # run moea
        chroms[i], _, objs[i] = moea.main()
        
        # get stats
        metrics = moea.get_statistics()
        igd[i]  = metrics[:, 0]
        sp[i]   = metrics[:, 4]
        r2[i]   = metrics[:, 5]
        
        # poi
        if algorithm_name.count("GRA") > 0: 
            output = "moead_gra_delta_%d_%s.png" % (delta, mop_name)
            #plot_subproblems_budget(algorithm_name, moea.poi_evals, output)
        
        
        # plotting
        steps = get_step_labels(params["ea_iters"], params["statistics_window_size"])
        plot_pops(moea.fronts_hist, steps, real_front, algorithm_name, algorithm_name, weights=uv, lims=lims)
        
        print("")
    
    #plot_stats(igd, title='IGD', metric_name='IGD', labels=labels, stats_window_size=params['statistics_window_size'])
    #plot_stats(sp,  title='SP',  metric_name='SP',  labels=labels, stats_window_size=params['statistics_window_size'])
    #plot_stats(r2,  title='R2',  metric_name='R2',  labels=labels, stats_window_size=params['statistics_window_size'])
    
    #plot_pops(objs, labels, real_front, title=mop_name.upper(), figname="final", weights=uv, lims=lims, output=mop_name.upper()+".png")
    
    return chroms, objs
    
