"""
moead_comparative.py
Este script compara MOEA/D y MOEA/D-GRA usando diferentes problemas de prueba
"""

from pylab import *
from common import run_experiment

# methods
from methods.moead        import MOEAD
from methods.moead_gra    import MOEAD_GRA

# moea problems and real fronts
from python_repository.benchmark.zhou2016.t1 import t1_f1, t1_f2, get_t1_pareto_front
from python_repository.benchmark.zhou2016.t2 import t2_f1, t2_f2, get_t2_pareto_front
#from python_repository.benchmark.liu2014.mop1 import mop1_f1, mop1_f2, get_mop1_pareto_front
#from python_repository.benchmark.liu2014.mop2 import mop2_f1, mop2_f2, get_mop2_pareto_front
#from python_repository.benchmark.liu2014.mop3 import mop3_f1, mop3_f2, get_mop3_pareto_front
#from python_repository.benchmark.liu2014.mop4 import mop4_f1, mop4_f2, get_mop4_pareto_front
#from python_repository.benchmark.liu2014.mop5 import mop5_f1, mop5_f2, get_mop5_pareto_front
#from python_repository.benchmark.liu2014.mop6 import mop6_f1, mop6_f2, mop6_f3, get_mop6_pareto_front
#from python_repository.benchmark.liu2014.mop7 import mop7_f1, mop7_f2, mop7_f3, get_mop7_pareto_front
#from python_repository.benchmark.dtlz.problems_dtlz import dtlz1_m2_problem, dtlz3_m2_problem
#from python_repository.benchmark.dtlz.get_real_fronts import get_real_front as get_dtlz_front
#from python_repository.benchmark.zdt.zdt4 import zdt4_f1, zdt4_f2
#from python_repository.benchmark.zdt.get_real_fronts import get_real_front as get_zdt_front




if __name__ == "__main__":
    
    # (class, label, delta)
    algorithms = (
            #(MOEAD,     "MOEA/D",                None),
            (MOEAD_GRA, "MOEA/D-GRA (delta 10)", 10),
            #(MOEAD_GRA, "MOEA/D-GRA (delta 25)", 25),
            )
            
    iters = 200
    
    """
    # t1
    mop_f1          = lambda X, Z: t1_f1(X)
    mop_f2          = lambda X, Z: t1_f2(X)
    mop             = (mop_f1, mop_f2)
    real_front      = get_t1_pareto_front(100)
    problem_name    = "t1"
    chroms, objs    = run_experiment(algorithms, iters, mop, problem_name, real_front)
    """
    
    
    # t2
    mop_f1          = lambda X, Z: t2_f1(X)
    mop_f2          = lambda X, Z: t2_f2(X)
    mop             = (mop_f1, mop_f2)
    real_front      = get_t2_pareto_front(100)
    problem_name    = "t2"
    chroms, objs    = run_experiment(algorithms, iters, mop, problem_name, real_front)
    
    """
    # zdt4
    zdt4_m2_f1      = lambda X, Z: zdt4_f1(X)
    zdt4_m2_f2      = lambda X, Z: zdt4_f2(X)
    mop             = (zdt4_m2_f1, zdt4_m2_f2)
    problem_name    = "zdt4"
    real_front      = get_zdt_front(problem_name)
    run_experiment(algorithms, iters, mop, problem_name, real_front)
    
    
    # dtlz1
    problem_name    = "dtlz1"
    mop             = dtlz1_m2_problem
    real_front      = get_dtlz_front(problem_name, 2)
    run_experiment(algorithms, iters, mop, problem_name, real_front)
    
    
    # dtlz3
    problem_name    = "dtlz3"
    mop             = dtlz3_m2_problem
    real_front      = get_dtlz_front(problem_name, 2)
    run_experiment(algorithms, iters, mop, problem_name, real_front)
    
    # mop1
    mop_f1          = lambda X, Z: mop1_f1(X)
    mop_f2          = lambda X, Z: mop1_f2(X)
    mop             = (mop_f1, mop_f2)
    real_front      = get_mop1_pareto_front()
    problem_name    = "mop1"
    run_experiment(algorithms, iters, mop, problem_name, real_front)
    
    
    # mop2
    mop_f1          = lambda X, Z: mop2_f1(X)
    mop_f2          = lambda X, Z: mop2_f2(X)
    mop             = (mop_f1, mop_f2)
    real_front      = get_mop2_pareto_front()
    problem_name    = "mop2"
    run_experiment(algorithms, iters, mop, problem_name, real_front)
    
    
    # mop3
    mop_f1          = lambda X, Z: mop3_f1(X)
    mop_f2          = lambda X, Z: mop3_f2(X)
    mop             = (mop_f1, mop_f2)
    real_front      = get_mop3_pareto_front()
    problem_name    = "mop3"
    run_experiment(algorithms, iters, mop, problem_name, real_front)
    
    
    # mop4
    mop_f1          = lambda X, Z: mop4_f1(X)
    mop_f2          = lambda X, Z: mop4_f2(X)
    mop             = (mop_f1, mop_f2)
    real_front      = get_mop4_pareto_front()
    problem_name    = "mop4"
    run_experiment(algorithms, iters, mop, problem_name, real_front)
    
    
    # mop5
    mop_f1          = lambda X, Z: mop5_f1(X)
    mop_f2          = lambda X, Z: mop5_f2(X)
    mop             = (mop_f1, mop_f2)
    real_front      = get_mop5_pareto_front()
    problem_name    = "mop5"
    run_experiment(algorithms, iters, mop, problem_name, real_front)


    # mop6
    mop_f1          = lambda X, Z: mop6_f1(X)
    mop_f2          = lambda X, Z: mop6_f2(X)
    mop_f3          = lambda X, Z: mop6_f3(X)
    mop             = (mop_f1, mop_f2, mop_f3)
    real_front      = get_mop6_pareto_front()
    problem_name    = "mop6"
    run_experiment(algorithms, iters, mop, problem_name, real_front)
    """
