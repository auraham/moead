# rand.py
from __future__ import print_function
from numpy.random import RandomState

class Rand(RandomState):

    def __init__(self):
        RandomState.__init__(self, 100)
        self.inner_counter = 0
    
    def debug(self):
        
        
        stop = 57211
        stop = -1
        stop = 58159        # es antes de este punto
        stop = 58151        # es antes de este punto
        stop = -1
        
        if self.inner_counter == stop:
            import ipdb; ipdb.set_trace()
        
    
    def rand(self):
        
        r = RandomState.rand(self)
        self.inner_counter+=1
        
        #print("%d - rand, r: %.4f" % (self.inner_counter, r))
        self.debug()
        
        return r
        
        
    def randint(self, low, high):
        
        r = RandomState.randint(self, low, high)
        self.inner_counter+=1
        
        #print("%d - randint, r: %.4f" % (self.inner_counter, r))
        self.debug()
        
        return r
        
    def permutation(self, x):
        
        perm = RandomState.permutation(self, x.copy())
        self.inner_counter+=1
        
        #print("%d - permutation, perm: %s" % (self.inner_counter, perm))
        self.debug()
        
        return perm


if __name__ == "__main__":
    
    rand = Rand()
    rand.rand()
    rand.randint(0, 10)
    rand.permutation([1, 2, 3])
    
    rand = Rand()
    rand.rand()
    rand.randint(0, 10)
    rand.permutation([1, 2, 3])
    
    
    
