# parser.py

from __future__ import print_function
import numpy as np
import os, sys, argparse

if __name__ == "__main__":
    
    ap = argparse.ArgumentParser()
    ap.add_argument("-i", "--input",            required=True, help="Input file path")
    ap.add_argument("-o", "--output",           required=True, help="Output file path")
    ap.add_argument("-a", "--add_separators",   action='store_true', help="Wraps the output matrix with a '#' ")      # si es [], si se paso el parametro; si es None, no se paso el parametro
    ap.add_argument("-r", "--ref_point",        nargs="*", required=True, help="Reference point, eg. 1.1 1.1 1.1")
    #ap.add_argument("-p", "--mop_name", required=True, help="mop name: dtlz1, inv-dtlz1")
    #ap.add_argument("-m", "--m_objs",   required=True, help="m_objs: 6,7,8,10,15", type=int)
    #ap.add_argument("-l", "--label",    required=True, help="processor label, e.g. _proc1")
    
    #
    #args = ap.parse_args()

    #test_mop_name   = args.mop_name
    #test_m_objs     = args.m_objs
    #test_label      = args.label


    #ap.parse_args('a b --foo x y --bar 1 2'.split())
    #ap.parse_args('--foo x y'.split())
