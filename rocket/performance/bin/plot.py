# plot.py
import numpy as np
import matplotlib.pyplot as plt


def draw_lines(ax, ref_point):
    
    ax.set_xlim(0, 3.5)
    ax.set_ylim(0, 3.5)
    
    points = np.array([
            [0.0, ref_point[0]],
            [3.5, ref_point[1]]])
    
    ax.plot(points[:, 0], points[:, 1], c="#252525", ls=":")
    
    points = np.array([
            [ref_point[0], 0.0],
            [ref_point[1], 3.5]])
    
    ax.plot(points[:, 0], points[:, 1], c="#252525", ls=":")


if __name__ == "__main__":
    
    
    objs = np.genfromtxt("sample.txt")
    
    
    
    if objs.shape[0] == 0:
        print("no points to plot")
    
    else:
    
        fig = plt.figure()
        ax = fig.add_subplot(111)
    
        ax.plot(objs[:, 0], objs[:, 1], "bo")
        
        draw_lines(ax, np.array([1.1, 1.1]))
        plt.show()
