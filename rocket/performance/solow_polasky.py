# solow_polasky.py
from __future__ import print_function
import numpy as np
from scipy.spatial.distance import cdist

def solow_polasky(objs, theta=1):
    """
    Compute the Solow-Polasky diversity measure
    
    Input
    objs        (n_points, m_objs) array, population
    theta       int, parameter for SP
    
    Output
    d2          float, diversity measure of objs
    """
    
    n_points = objs.shape[0]
    e = np.ones((n_points, ))
    
    # distance
    dist = cdist(objs, objs, metric="euclidean")
    
    # M matrix
    M = np.exp((-theta)*dist)
    
    # pseudo inverse
    pM = np.linalg.pinv(M)
    
    # result
    d1 = np.dot(e, pM)
    d2 = np.dot(d1, e)
    
    return d2
    
    
def plot_objs(objs, title, output=""):
    
    import matplotlib.pyplot as plt
    from mpl_toolkits.mplot3d import Axes3D
    
    fig = plt.figure()
    ax = fig.add_subplot(111, projection="3d")
    
    ax.set_xlabel("$f_1$")
    ax.set_ylabel("$f_2$")
    ax.set_zlabel("$f_3$")
    ax.set_title(title)
    ax.view_init(30, 45)

    ax.plot(objs[:, 0], objs[:, 1], objs[:, 2], c="#74CE74", 
        mec="#252525", ls="none", marker="o", markeredgewidth=0.15)
    
    if output:
        fig.savefig(output, dpi=300, bbox_inches="tight")
        
    plt.show()
    

if __name__ == "__main__":
    
    A = np.genfromtxt("objs_moead-pbi-5_m_3_run_0_final.txt")
    B = np.genfromtxt("objs_amoead-pbi-5_m_3_run_0_final.txt")

    rA = solow_polasky(A)
    rB = solow_polasky(B)


    plot_objs(A, "SP(A) = %.4f" % rA)
    plot_objs(B, "SP(B) = %.4f" % rB)
