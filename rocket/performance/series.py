# series.py
from __future__ import print_function
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import os, sys, argparse
from matplotlib.pyplot import rcParams
from rocket.plot import plot_pops, plot_series, load_rcparams, colors, basecolors

def get_ref_point_single_track(ref_point, label, moea_name, mop_name, m_objs, iters, step, run_id, experiment_path):
    
    points = []
    
    for t in range(step, iters+1, step):
        
        filename = "%s_%s_m_%d_run_%d_t_%d.txt" % (ref_point, label, m_objs, run_id, t)     # z_nadir_sdci_estimate_m_3_run_9_t_75.txt
        filepath = os.path.join(experiment_path, mop_name, moea_name, "debug", filename)    # /home/auraham/local_improved_mss/experiments/test_amoead_mss_v7_case_c/results_local/dtlz1/amoead_mss_v7-no-form-contrib_step-25-contrib_window-10-smooth-mean-contrib-fast_no_parent_sdci_estimate-assignment-fast/debug

        pt = np.genfromtxt(filepath)
        
        points.append(pt.copy())
        
    return np.array(points)


def get_ref_point_track(ref_point, label, moea_name, mop_name, m_objs, iters, step, runs, experiment_path):
    """
    Return a list of m_objs series, one for each dimension of z_ideal/z_nadir
    for plotting witn plot_series_mean_sd()
    """
    read_from_files = True
    list_series = []
    
    # check files
    for m in range(m_objs):
            
        filename = "progress_ref_point_%s_%s_%s_m_%d_obj_%d_runs_%d.txt" % (ref_point, label, moea_name, m_objs, m, runs)
        filepath = os.path.join(experiment_path, mop_name, moea_name, "stats", filename)
        
        if os.path.isfile(filepath):
            serie = np.genfromtxt(filepath)
            list_series.append(serie.copy())
            
        else:
            read_from_files = False
            break
            
    
    if read_from_files:
        print("reading from files")
        return list_series
    
    else:

        # create series
        
        rows = runs
        cols = int(iters/step)
        
        series_base = np.zeros((rows, cols))
        list_series = [series_base.copy() for m in range(m_objs)]
        
        for run_id in range(runs):
            
            # (cols, m_objs)
            single_series = get_ref_point_single_track(ref_point, label, moea_name, mop_name, m_objs, iters, step, run_id, experiment_path)
        
        
            for m in range(m_objs):
                
                # the run_id-th row of list_series[m] is
                # the m-th col of single_series
                list_series[m][run_id, :] = single_series[:, m]
        
        # save matrices to ease future readings
        for m in range(m_objs):
            
            series = list_series[m].copy()
            
            filename = "progress_ref_point_%s_%s_%s_m_%d_obj_%d_runs_%d.txt" % (ref_point, label, moea_name, m_objs, m, runs)
            filepath = os.path.join(experiment_path, mop_name, moea_name, "stats", filename)
            np.savetxt(filepath, series)
            
            print("saving %s" % filepath)
        
    
    return list_series
    
    
    
"""
def get_ref_point_track(ref_point, label, moea_name, mop_name, m_objs, step, iters, run_id, experiment_path):
    
    rows = runs
    cols = int(iters/step)
    
    serie_base = np.zeros((rows, cols))
    list_series = [serie_base.copy() for i in range(m_objs)]
    
    for run_id in range(runs):
    
        points = []
        
        for t in range(step, iters+1, step):
            
            filename = "%s_%s_m_%d_run_%d_t_%d.txt" % (ref_point, label, m_objs, run_id, t)     # z_nadir_sdci_estimate_m_3_run_9_t_75.txt
            filepath = os.path.join(experiment_path, mop_name, moea_name, "debug", filename)    # /home/auraham/local_improved_mss/experiments/test_amoead_mss_v7_case_c/results_local/dtlz1/amoead_mss_v7-no-form-contrib_step-25-contrib_window-10-smooth-mean-contrib-fast_no_parent_sdci_estimate-assignment-fast/debug

            pt = np.genfromtxt(filepath)
            
            points.append(pt.copy())
            
        serie = np.array(points)
        
        for serie_id in range(m_objs):
            list_series[serie_id][run_id, :] = serie[:, m_objs].copy()
        
    return np.array(points)
    
"""    


def get_hv_last_generation(moea_name, mop_name, m_objs, iters, experiment_path, return_hv_raw=False):
    """
    Return the hypervolume of the last generation (it is not a matrix)
    (This is required for paper_test_mss_sensitivity/visualize and tables)
    """

    # hv final file
    filename_hv_final = "hv_%s_m_%d_t_%d_complete.txt" % (moea_name, m_objs, iters)
    filepath_hv_final = os.path.join(experiment_path, mop_name, moea_name, "stats", filename_hv_final)
    
    # read hv file
    hv_values = np.genfromtxt(filepath_hv_final)    # (runs, 3) array, hv_raw, hv_norm_1.1, hv_norm
                                                    # each line is an independent run
    
    # get hv_norm only (this is the series)
    series = hv_values[:, 2]
    
    if return_hv_raw:
        # this is needed for maf2 and maf4 when creating tables
        series = hv_values[:, 0]
    
    return series.copy()



def get_fev_hist(moea_name="amoead_fev_v11-no-form-contrib_step-1", mop_name="dtlz1", m_objs=3, fev_list=("ws", "te", "asf", "pbi", "ipbi", "vads"), runs=50, experiment_path="results"):
    """
    Histogram of fevs
    """
    
    rows = runs
    cols = len(fev_list)
    
    series = np.zeros((rows, cols))
    
    for run_id in range(runs):
        
        filename = "fev_hist_%s_m_%d_run_%d.txt" % (moea_name, m_objs, run_id)
        filepath = os.path.join(experiment_path, mop_name, moea_name, "debug", filename)
        
        # read csv
        # access as follows: df.loc['ws', 'count']
        df = pd.read_csv(filepath, sep=";", header=0, names=("fev", "count"), usecols=(0,1), index_col=0)
        
        # populate series
        for col, fev in enumerate(fev_list):
            series[run_id, col] = df.loc[fev, "count"]          # histogram of fev in this independent run
        
    return series.copy()
    
def get_fev_track(moea_name="amoead_fev_v11-no-form-contrib_step-1", mop_name="dtlz1", m_objs=3, fev="ws", runs=50, iters=300, experiment_path="results"):
    """
    Historial of fevs (for generation)
    """
    
    rows = runs
    cols = iters
    
    series = np.zeros((rows, cols))
    
    for run_id in range(runs):
        
        filename = "fev_track_%s_m_%d_run_%d_fev_%s.txt" % (moea_name, m_objs, run_id, fev)
        filepath = os.path.join(experiment_path, mop_name, moea_name, "debug", filename)
        
        # read csv
        # access as follows: df.loc['ws', 'count']
        df = pd.read_csv(filepath, sep=";", header=0, names=("generation_t", "was_fev_evaluated_at_t"), usecols=(0,1), index_col=0)
        
        series[run_id, :] = df["was_fev_evaluated_at_t"].values        # (iters, ) int array
        
    by_col = 0
    track = series.sum(axis=by_col)                             # (iters, ) int array
        
    return track.copy()
    
def get_contrib(moea_name="amoead_fev_v11-no-form-contrib_step-1", mop_name="dtlz1", fev="ws", m_objs=3, iters=300, step=1, runs=50, experiment_path="results"):
    """
    Contribution of each fev along the search process
    """
    
    rows = runs
    cols = int(iters/step)
    
    series = np.zeros((rows, cols))
    
    
    # added
    # ("ws", "te", "asf", "pbi", "ipbi", "vads")
    fevs = {
        "ws": 0,
        "te": 1,
        "asf": 2,
        "pbi": 3,
        "ipbi": 4,
        "vads": 5,
    }
    row_to_keep = fevs[fev]
    
    for run_id in range(runs):
        
        t = 0
        
        for col in range(cols):
            
            t += step
        
            filename = "contrib_int_%s_m_%d_run_%d_t_%d.txt" % (moea_name, m_objs, run_id, t)
            filepath = os.path.join(experiment_path, mop_name, moea_name, "debug", filename)
            serie = np.genfromtxt(filepath)             # 6 values
        
            series[run_id, col] = serie[row_to_keep]    # keep the value (contribution) corresponding to fev
        
        
    return series.copy()
      
def get_mdp_series(moea_name="amoead-pbi-5", mop_name="inv-dtlz1", m_objs=3, iters=600, step=10, runs=30, experiment_path="results", suffix=""):
    """
    Median of dispersion
    """
    
    rows = runs
    cols = int(iters/step)
    
    series = np.zeros((rows, cols))
    
    for run_id in range(runs):
        
        filename = "mdp_%s_m_%d_run_%d_t_%d%s.txt" % (moea_name, m_objs, run_id, iters, suffix)
        filepath = os.path.join(experiment_path, mop_name, moea_name, "debug", filename)
        serie = np.genfromtxt(filepath)
        
        series[run_id, :] = serie.copy()
        
    return series.copy()
      
def get_hv_series(moea_name="amoead-pbi-5", mop_name="inv-dtlz1", m_objs=3, iters=600, step=50, runs=30, experiment_path="results", suffix=""):
    """
    Hipervolume
    """
    
    rows = runs
    cols = int(iters/step)
    
    series = np.zeros((rows, cols))
    
    for run_id in range(runs):
        
        filename = "hv_%s_m_%d_run_%d_norm%s.txt" % (moea_name, m_objs, run_id, suffix)
        filepath = os.path.join(experiment_path, mop_name, moea_name, "stats", filename)
        serie = np.genfromtxt(filepath)     # two cols: [raw hv, norm hv]
        
        norm_serie = serie[:, 1]    # norm hv
        
        series[run_id, :] = norm_serie.copy()
        
    return series.copy()
    
def get_igd_series(moea_name="amoead-pbi-5", mop_name="inv-dtlz1", m_objs=3, iters=600, step=50, runs=30, experiment_path="results", suffix=""):
    """
    IGD
    """
    
    rows = runs
    cols = int(iters/step)
    
    series = np.zeros((rows, cols))
    
    for run_id in range(runs):
        
        filename = "igd_%s_m_%d_run_%d%s.txt" % (moea_name, m_objs, run_id, suffix)
        filepath = os.path.join(experiment_path, mop_name, moea_name, "stats", filename)
        serie = np.genfromtxt(filepath)     
        
        series[run_id, :] = serie[:cols].copy()
        
    return series.copy()
    
def get_sp_series(moea_name="amoead-pbi-5", mop_name="inv-dtlz1", m_objs=3, iters=600, step=50, runs=30, experiment_path="results", suffix=""):
    """
    Solow-Polasky
    """
    
    rows = runs
    cols = int(iters/step)
    
    series = np.zeros((rows, cols))
    
    for run_id in range(runs):
        
        filename = "sp_%s_m_%d_run_%d%s.txt" % (moea_name, m_objs, run_id, suffix)
        filepath = os.path.join(experiment_path, mop_name, moea_name, "stats", filename)
        serie = np.genfromtxt(filepath)
        
        series[run_id, :] = serie.copy()
        
    return series.copy()
    
def get_median_series(series):
    """
    Return an array with cols values which represent the median of series (by col)
    
    series = 
    
    [1, 2, 3, 4, 5]     # run 0
    [2, 3, 4, 5, 6]     # run 1
    [3, 4, 5, 6, 7]     # run 2
    
    rows: 3
    cols: 5
    
    median of series (by col), np.median(series, axis=0)
    array([ 2.,  3.,  4.,  5.,  6.])

    Input
    series          (rows, cols) array, where each row represent an independent run
    
    
    Output
    median          (cols, ) array, median by col of series
    """
    
    per_col=0
    series_median = np.median(series, axis=per_col)
    return series_median.copy()

def get_median_series_nan(series, def_value=0):
    """
    Similar to get_median_series(). If a col of series contains a nan,
    then its median is replaced by def_value
    
    series = 
    
    [nan,   2,   3, 4, 5]     # run 0
    [  2, nan,   4, 5, 6]     # run 1
    [  3,   4, nan, 6, 7]     # run 2
    
    rows: 3
    cols: 5
    
    median of series (by col) with nan-handling
    array([ nan,  nan,  nan,  5.,  6.])

    Input
    series          (rows, cols) array, where each row represent an independent run
    
    
    Output
    median          (cols, ) array, median by col of series
    """
    
    # get shape
    n_runs, n_steps = series.shape
    
    # new serie
    series_median = np.zeros((n_steps, ))
    
    # for each col of series
    for i in range(n_steps):
        
        # col contains all the hv values of the i-th step
        # for all the runs
        col = series[:, i]
    
        if np.isnan(col).any():
            
            # if col contains a nan, use def_value
            series_median[i] = def_value
            
        else:
            
            # otherwise, compute the median of col
            series_median[i] = np.median(col)
    
    return series_median.copy()
    
def get_summary_last_col_series(series, min_is_better):
    """
    Return the min, median, and max values of series[:, -1]
    
    series = 
    
    [1, 2, 3, 4, 5]     # run 0
    [2, 3, 4, 5, 6]     # run 1
    [3, 4, 5, 6, 7]     # run 2
    
    rows: 3
    cols: 5
    
    values from last column: [5, 6, 7]
    if min_is_better:
        best: 5
        median: 6
        worst: 7
    

    Input
    series          (rows, cols) array, where each row represent an independent run
    min_is_better   bool, if True, best=min_value and worst=max_value.
    
    Output
    best            float, best, median and worst values from last column of series, series[:, -1]
    median
    worst          
    """
    
    best, median, worst = 0, 0, 0
    last_col = series[:, -1]
    
    min_val = np.min(last_col)
    med_val = np.median(last_col)
    max_val = np.max(last_col)
    
    # added
    mean = np.mean(last_col)
    sd   = np.std(last_col)
    
    if min_is_better:
        
        best    = min_val
        median  = med_val
        worst   = max_val

    else:

        best    = max_val
        median  = med_val
        worst   = min_val

    return best, mean, median, worst, sd
    
def get_median_last_col_series(series):
    """
    Return the median (value and argument) of series[:, -1]
    This function is useful for getting the median approximation (ie objs matrix) of a series
    
    series = 
    
    [1, 2, 3, 4, 5]     # run 0
    [2, 3, 4, 5, 6]     # run 1
    [3, 4, 5, 6, 7]     # run 2
    
    rows: 3
    cols: 5
    
    values from last column: [5, 6, 7]
    
    median_val = 6
    median_arg = 1       ie the run 1 is the representative run of the three
    

    Input
    series          (rows, cols) array, where each row represent an independent run
    
    Output
    median_val      float, median value    from last column of series, series[:, -1]
    median_arg      float, median argmuent from last column of series, series[:, -1]
    """
    
    last_col = series[:, -1]
    median_val, median_arg = 0, 0
    
    sortd_vals = np.sort(last_col)
    sortd_args = np.argsort(last_col)
    n_vals = len(sortd_vals)
    
    
    if n_vals % 2 == 0:
        
        # even
        mid_a_arg = int(n_vals/2)
        mid_b_arg = mid_a_arg - 1
        
        mid_a_val = sortd_vals[mid_a_arg]
        mid_b_val = sortd_vals[mid_b_arg]
        
        
        # average of two middle values
        median_val = (mid_a_val + mid_b_val) / 2
        
        if np.abs(median_val - mid_a_val) < np.abs(median_val - mid_b_val):
            
            # ie si min_a_val esta mas cercano a la mediana que mid_b_val
            median_arg = sortd_args[mid_a_arg]
        
        else:
            
            median_arg = sortd_args[mid_b_arg]
        
    else:
        
        # odd
        mid_a       = int((n_vals-1)/2)
        median_val  = sortd_vals[mid_a]
        median_arg  = sortd_args[mid_a]
        
    return median_val, median_arg

