# About this package

This package contains performance indicators to measure the quality of an approximation to the Pareto front.

- IGD
- R2 [Hernandez15]
- Hypervolume []
- Solow-Polasky





# Inverted Generational Distance (IGD)

IGD measures ...

```python
import numpy as np
from rocket.performance import igd

real_front = np.array([
	[0, 10],
	[1,  6],
	[2,  2],
	[6,  1],
	[10, 0]])

set_a = np.array([
	[2, 4],
	[3, 3],
	[4, 2]])

set_b = np.array([
	[2, 8],
	[4, 4],
	[8, 2]])

igd_a = igd(set_a, real_front)
igd_b = igd(set_b, real_front)

print("IGD(A)", igd_a)		# 3.70709203161
print("IGD(B)", igd_b)		# 2.59148346585
```



# R2



# Hypervolume (Python)

The implementation in Python was obtained from [here](https://ls11-www.cs.uni-dortmund.de/rudolph/hypervolume/start). 

```

```



# Hypervolume (C, Fonseca06)

The implementation in C was obtained from [here](http://lopez-ibanez.eu/hypervolume). We use the version 1.3. The reader is referred to [Fonseca06] for more details.

## Compile

```
cd rocket/performance/hv-1.3-src/
make MARCH=native
```

## Usage

```
./hv-1.3-src/hv -q -r '1.1 1.1 1.1' objs_moead-pbi-5_m_3_run_0_final.txt
```

Output

```
0.975350 0.732795
```

The first value is the raw HV, and the second value is the normalized HV, obtained by dividing: `raw_hv/(1.1*1.1*1.1)`. We adopted this suggestion from [Cheng16].

## Modifications

We made three modifications to `main-hv.c` to avoid printing warning messages and normalize the HV. 

1. **Print zero hv**. In the original implementation, if `volume` is zero, the program prints a warning message and exits. In this version, it prints `0.0`, the message is printed only on verbose mode and then exits.  

```c
// if hv is zero, print zero and exit
if (volume == 0.0) {
            
  fprintf (outfile, "%f\n", volume);      

  // print warning message only on verbose mode
  if (verbose_flag >= 2)      
    errprintf ("none of the points strictly dominates the reference point\n");

  exit (EXIT_FAILURE);

}
```

2. **Do not print warning message**. In the original implementation, if some point does not strictly dominate the reference point, a warning message is displayed. In this version, such message is printed on verbose mode only. 

```c
if (reference != NULL) {
  for (n = 0; n < nobj; n++) {
    if (reference[n] <= maximum[n]) {

      // print warning message only on verbose mode 
      if (verbose_flag >= 2)
        warnprintf ("%s: some points do not strictly dominate "
                    "the reference point and they will be discarded",
                    filename);
      break;
    }
  }
}
```

3. **Normalize HV** The output of `main-hv.c` is a *raw HV*. According to [Cheng16], we can normalize this raw HV by dividing $\Pi_{i=1}^{m} r_i$, where $\mathbf{r} = [r_1, \dots, r_m]^T$ is the reference point, and $m$ is the number of objectives. To this end, we have added this block:

```c
// added for normalizing hv
double total_volume = 1;
for (n = 0; n < nobj; n++)
  total_volume *= reference[n];
```

and change the printing lines:

```c
if (volume == 0.0) {
    // print zero, zero
	fprintf (outfile, "%f %f\n", volume, volume/total_volume);  
	...
}
```

```c
time_elapsed = Timer_elapsed_virtual ();

// here, the raw and normalized HV are printed
fprintf (outfile, "%f %f\n", volume, volume/total_volume);
```





# Hypervolume (C, Bradstreet10)


The implementation in C was obtained from [here](http://www.wfg.csse.uwa.edu.au/hypervolume/). We use the version 1.15. The reader is referred to [Bradstreet10] for more details.

## Compile

In [Bradstreet10], the authors suggest to use `gcc -O3 -march=nocona -funroll-all-loops` for compilation, but such an option do not appear in `makefile`.  Instead, we compile `wfg.c` as follows:

```
cd rocket/performance/WFG_1.15
make march=nocona clean; make march=nocona
```

*Note*: We changed this line in `makefile` to use `getopt`:

```
CC = gcc  -Wall  $(OPT)    # needed to use getopt
#CC = gcc -std=c99 -Wall  $(OPT)
```

## Usage

The input file must follow this format:

```
# 
0.598 0.737 0.131 0.916 6.745
0.263 0.740 0.449 0.753 6.964
0.109 8.483 0.199 0.302 8.872
#
```
where 

- objective values are separated by spaces
- one point (objective vector) per line
- fronts are separated by `#` (otherwise, a `segfault` will arise)
- all fronts use the same reference point, therefore all points in all fronts must have the same number of objectives. 


To compute the hipervolume of a single front saved in `input_pop.txt` (`m_objs=15`), run:

```
./WFG_1.15/wfg -q input_pop.txt 1.1 1.1 1.1 1.1 1.1 1.1 1.1 1.1 1.1 1.1 1.1 1.1 1.1 1.1 1.1
```

Output (with `-q`): raw and normalized hypervolume values, respectively.

```
0.029785 0.007130
```

Output (without `-q`): this option prints additional information for debugging and the spent time.

```
verbose flag: 1, arg_index: 1, argc: 17, ind_filename: 1, ind_ref_vect: 2
filename: input_pop.txt
ref point:
0, 1.10 
1, 1.10 
2, 1.10 
3, 1.10 
4, 1.10 
5, 1.10 
6, 1.10 
7, 1.10 
8, 1.10 
9, 1.10 
10, 1.10 
11, 1.10 
12, 1.10 
13, 1.10 
14, 1.10 
0.029785 0.007130
Time: 7.988000 (s)
Total time = 7.988000 (s)
```



## Modifications

We made two modifications to `wfg.c` to avoid printing warning messages and normalize the HV. 

1. **Do not print times**. This is the output of the original implementation:

   ```
   hv(1) = 0.0297853976
   Time: 7.996000 (s)
   Total time = 7.996000 (s)
   ```

   We have added a parameter `-q` (for quiet) to reduce the amount of information as output to:

   ```
   0.0297853976
   ```

2. **Normalize HV** We performed the same procedure described in previous section. Now, the output contains two values: the raw and normalized hypervolume values, respectively:

   ```
   0.029785 0.007130
   ```



# Solow-Polasky

The Solow-Polasky diversity measure $D(P)$ of a population $P$ is determined as follows [Ulrich12]: Suppose $P$ contains the $n$ solutions $p_1, \dots, p_n$, where $|P| = n$. Furthermore, $d(p_i, p_j)$ denotes the distance between solutions $p_i$ and $p_j$. Then, we can define the $(n, n)$-matrix $M = (m_{ij})$ with elements:

$m_{ij} = \exp(-\theta \cdot d(p_i, p_j)), for all 1 \leq i, j \leq n$

Then, the Solow-Polasky measure can be given as:

$D(P) = eM^{-1}e^{T}$

where $e = (1, 1, \dots, 1)$, and $e^T$ denotes its transpose. In other words, $D(P)$ is the sum of all matrix elements of $M^{-1}$.

In this implementation, we have adopted Euclidean distance for $d(p_i, p_j)$, and $\theta=1$, as recommended by [Ishibuchi12].



## Example

```
# solow_polasky.py
A = np.genfromtxt("objs_moead-pbi-5_m_3_run_0_final.txt")
B = np.genfromtxt("objs_amoead-pbi-5_m_3_run_0_final.txt")

rA = solow_polasky(A)
rB = solow_polasky(B)
```

The script `solow_polasky.py` will produce these figures (the value of $D(P)$ appears in the title):



![](sp_a.png)



![](sp_b.png)

# TODO

- Colocar fórmulas, definición e interpretación geométrica de cada indicador.
- Add a check in Hypervolume to determine whether the `total_volume` is zero (i.e., some componente of the reference vector is zero).




# References

- [Hernandez15] *Improved Metaheuristic Based on the R2 Indicator for Many-Objective Optimization*.
- [Fonseca06] *An Improved Dimension Sweep Algorithm for the Hypervolume Indicator*.
- [Ulrich12] *Exploring Structural Diversity in Evolutionary Algorithms*.
- [Ishibuchi12] *Two-Objective Solution Set Optimization to Maximize Hypervolume and Decision Space Diversity in Multiobjective Optimization*.
- [Cheng16] *A Reference Vector Guided Evolutionary Algorithm for Many-Objective Optimization*.
- [Bradstreet10] *A Fast Many-objective Hypervolume Algorithm using Iterated Incremental Calculations*