# igd.py
from __future__ import print_function
import numpy as np
from scipy.spatial.distance import cdist

def igd(approximation, front):
    
    dist = cdist(front, approximation)
    
    by_row = 1
    min_dist = dist.min(axis=by_row)
    
    n_points = front.shape[0]
    value = min_dist.sum() / n_points
    
    return value
    

