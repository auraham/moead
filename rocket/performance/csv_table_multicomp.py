# csv_table_multicomp.py
from __future__ import print_function
import numpy as np
import os, sys, argparse
np.seterr(all='raise')

# add lab's path to sys.path
#lab_path = os.path.abspath("../../../")
#sys.path.insert(0, lab_path)

#from tables_core import table, table_frontend
#from rocket.labs import get_mop_configs

from rocket.performance import get_igd_series
from rocket.performance import get_sp_series
from rocket.performance import get_hv_last_generation
from distutils.dir_util import mkpath

def create_csv_table_multicomp(moea_names, mop_name, m_objs, iters, step, runs, indicator="hv", suffix="", output_dir="", delimiter=";"):
    """
    Create the csv file required for multicomp.m
    
    This file comprises n columns, one for each moea, and l rows.
    For instance, the i-th column represents the l=50 hv values obtained in the last generation
    by the i-th moea given in moea_names.
    The resulting csv file can be used as input for multicomp.m to conduct a statistical analysis.
    
    Input
    moea_names      tuple of tuples with this format: (experiment_path, moea_name, moea_label)
    mop_name        str, name of mop
    m_objs          int, number of objectives
    iters           int, number of generations
    step            int, interval for saving pops
    runs            int, number of independent runs
    indicator       str, name of the performance indicator: hv, igd, sp
    suffix          str, suffix of the output file
    output_dir      str, output directory
    delimiter       str, csv delimiter
    
    Output
    data            numpy array (runs, len(moea_names)), each colunm in data represents the (hv, igd, sp) values
                    of a given moea
    """
    
    cols = []
    lbls = []
    
    for col_id, (experiment_path, moea_name, moea_label) in enumerate(moea_names):
        
        # --- place your data reader here ----
        
        col = None
        min_is_better = None
        
        if indicator == "hv":
        
            col = get_hv_last_generation(moea_name, mop_name, m_objs, iters, experiment_path)
            min_is_better = False
            
            # corner case for maf2 and maf4
            if mop_name in ("maf2", "maf4"):
                col = get_hv_last_generation(moea_name, mop_name, m_objs, iters, experiment_path, return_hv_raw=True)
            
        elif indicator == "igd":
            
            # get igd series                                                                            # for m=3, step=100 and dtlz1, there are runs=50, t_steps=3, for t=100,200,300
            serie = get_igd_series(moea_name, mop_name, m_objs, iters, step, runs, experiment_path)     # shape: (50, 6) = (runs, t_steps)
            
            # shape (50, ) = (runs, )
            # each value represent the final hv of an independent run
            igd_last_generation = serie[:, -1]
            
            col = igd_last_generation.copy()
            min_is_better = True
        
        elif indicator == "sp":
            
            # get sp series                                                                             # for m=3, step=100 and dtlz1, there are runs=50, t_steps=3, for t=100,200,300
            serie = get_sp_series(moea_name, mop_name, m_objs, iters, step, runs, experiment_path)      # shape: (50, 6) = (runs, t_steps)
            
            # shape (50, ) = (runs, )
            # each value represent the final hv of an independent run
            sp_last_generation = serie[:, -1]
            
            col = igd_last_generation.copy()
            min_is_better = False
        
        # add to cols
        cols.append(col.copy())
        lbls.append(moea_name)
        
        # --- place your data reader here ----
    
    # make matrix and transpose cols
    data = np.array(cols).T
    
    # create output directory, if needed
    if not os.path.isdir(output_dir):
        mkpath(output_dir)

    # save matrix
    header = delimiter.join(lbls)
    csv_filename = "%s_%s_m_objs_%d%s.csv" % (indicator, mop_name, m_objs, suffix)
    csv_filepath = os.path.join(output_dir, csv_filename)
    np.savetxt(csv_filepath, data, delimiter=delimiter, header=header, comments="# ")
    print("output:", csv_filepath)
    
    return data.copy()


if __name__ == "__main__":
    
    #ap = argparse.ArgumentParser()
    #ap.add_argument("-l", "--case", required=True, help="case", choices=("a", "b"))
    #ap.add_argument("-s", "--stat", required=True, help="stat", choices=("mean", "median", "sd"))
    #ap.add_argument("-i", "--indicator", required=True, help="performance indicator", choices=("igd", "hv", "sp"))
    #args = vars(ap.parse_args())
    
    # debug
    args = {"case": "a"}
    
    runs = 50       # number of independent runs
    step = 100      # save pop every step generations for posterior hv calculation

    # results from gitlab
    baseline_path = "/media/data/git/moead_mss_dci_results/experiments/test_moead_norm_v1/results_case_%s_git" % args["case"]
    proposal_path = "/media/data/git/moead_mss_dci_results/experiments/paper_test_mss_contrib/results_case_%s_git" % args["case"]
    
    # define sorted moea_names
    moea_names = (
                
                # moead + normalization
                (baseline_path, "moead_norm_v1-ws-norm-true",    "WS"), 
                (baseline_path, "moead_norm_v1-te-norm-true",    "TE"),
                (baseline_path, "moead_norm_v1-asf-norm-true",   "MTE"),
                (baseline_path, "moead_norm_v1-pbi-norm-true",   "PBI"),
                (baseline_path, "moead_norm_v1-ipbi-norm-true",  "IPBI"), 
                (baseline_path, "moead_norm_v1-vads-norm-true",  "VADS"),
                
                # mss + normalization
                (proposal_path, "mss-dci_norm_hyp_mean-divs-8-25-mean-10-ps-normal-ss-gra-norm-true",  "MSS-DCI"),
                )
    
    mop_name = "dtlz1"
    m_objs = 3
    iters = 300
    
    cols = create_csv_table_multicomp(moea_names, mop_name, m_objs, iters, step, runs, indicator="hv", suffix="_baselines_case_a", output_dir="csv_baselines")

    
    
    
