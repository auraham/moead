# create_csv_table_for_stat_test.py

def create_csv_table_for_stat_test(moea_names, mop_name, m_objs, iters, experiment_path, output, indicator="hv", return_hv_raw=False):
    """
    Create a csv table for statistical significance analysis
    
    For each moea in moea_names:
        1. Read the performance (igd, hv) of the last generation (this is often a (50, ) array)
        2. Add this series as a column of the final table
        
    Save the final table
    """
    
    cols = []
    
    for moea_name in moea_names:
        
        col = None
        
        # --- place your data reader here ----
                
        if indicator == "hv":
        
            col = get_hv_last_generation(moea_name, mop_name, m_objs, iters, experiment_path)
            min_is_better = False
            
            # corner case for maf2 and maf4
            if mop_name in ("maf2", "maf4"):
                col = get_hv_last_generation(moea_name, mop_name, m_objs, iters, experiment_path, return_hv_raw=True)
            
        elif indicator == "igd":
            
            # get igd series                                                                            # for m=3, step=100 and dtlz1, there are runs=50, t_steps=3, for t=100,200,300
            serie = get_igd_series(moea_name, mop_name, m_objs, iters, step, runs, experiment_path)     # shape: (50, 6) = (runs, t_steps)
            
            # shape (50, ) = (runs, )
            # each value represent the final hv of an independent run
            igd_last_generation = serie[:, -1]
            
            col = igd_last_generation.copy()
        
        elif indicator == "sp":
            
            # get sp series                                                                             # for m=3, step=100 and dtlz1, there are runs=50, t_steps=3, for t=100,200,300
            serie = get_sp_series(moea_name, mop_name, m_objs, iters, step, runs, experiment_path)      # shape: (50, 6) = (runs, t_steps)
            
            # shape (50, ) = (runs, )
            # each value represent the final hv of an independent run
            sp_last_generation = serie[:, -1]
            
            col = sp_last_generation.copy()
        
        # --- place your data reader here ----
        
        # add col
        cols.append(col.copy())
        
    # create matrix, shape: (runs, n_moeas)
    data = np.array(cols).T
    
    # save matrix
    np.savetxt(output, data)
    print(output)

