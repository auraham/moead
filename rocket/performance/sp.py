# sp.py
from __future__ import print_function
import numpy as np
from scipy.spatial.distance import cdist

def sp(objs, theta=1):
    
    n_points = objs.shape[0]
    e = np.ones((n_points, ))
    
    # distance
    dist = cdist(objs, objs, metric="euclidean")
    
    # M matrix
    M = np.exp((-theta)*dist)
    
    # pseudo inverse
    pM = np.linalg.pinv(M)
    
    # result
    d1 = np.dot(e, pM)
    d2 = np.dot(d1, e)
    
    return d2

if __name__ == "__main__":
    
    A = np.genfromtxt("objs_moead-pbi-5_m_3_run_0_final.txt")
    B = np.genfromtxt("objs_amoead-pbi-5_m_3_run_0_final.txt")

    rA = sp(A)
    rB = sp(B)
