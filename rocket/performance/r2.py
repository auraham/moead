# r2.py
from __future__ import print_function
from rocket.forms import evaluate_form
import numpy as np

def r2(objs, weights, z_ref=None):
    """
    Computes R2 indicator (we assume minimization)
    
    Input
    objs        (pop_size, m_objs) array, objs matrix
    weights     (n_points, m_objs) array, weights matrix
    
    Output
    result      R2 indicator (scalar)
    """

    pop_size, m_objs = objs.shape
    n_points, _      = weights.shape
    per_col = 0
    
    utilities  = np.zeros((n_points, ))
    
    if z_ref is None:
        z_ref = np.zeros((m_objs, ))            # use the same vectors as [Hernandez14]
    
    for i in range(n_points):
        
        for j in range(pop_size):
            
            
            w_tmp = weights[i, :]
            W     = w_tmp.reshape((1, m_objs)).repeat(pop_size, 0)      # copy weights[i, :] pop_size times
                                                                        # W has this shape (pop_size, m_objs) 
            
            # each column of utilities contains the value of the utility fuction
            # of every obj vector against the same weight vector (weights[i])                                                            
            utilities[:, i] = evaluate_form(objs, W, None, None, params={"z_ref":z_ref})
    
    
    # we take the best utility value for each weight vector
    # ie, the best value per column
    best_utilities = utilities.min(axis=per_col)
    
    # finally, sum every best value
    result = (1./n_points) * best_utilities.sum()
    
    return result
