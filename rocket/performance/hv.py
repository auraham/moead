# hv.py
from __future__ import print_function
from rocket.performance.hypervolume import HyperVolume

def hv(objs, ref_point):
    """
    Given a set of objectives and a reference points, this function
    returns its hypervolume.
    
    Input
    objs        list of lists, objs matrix
    ref_point   list
    
    Output
    volume      int, hypervolume of objs
    """
    
    h = HyperVolume(ref_point)
    volume = h.compute(objs)
    
    return volume



if __name__ == "__main__":

    ref_point   = [2, 2, 2]
    objs        = [[1,0,1], [0,1,0]]
    volume      = hv(objs, ref_point)
