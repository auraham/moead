# frontend.py
from __future__ import print_function
import numpy as np
from rocket.helpers import spin_cube
from rocket.problems.dtlz.frontend import eval_dtlz_fn_matrix

def eval_dtlz_spin_cube_fn_matrix(fn_id, chroms, m_objs, rotation):
    
    # original problem
    objs = eval_dtlz_fn_matrix(fn_id, chroms, m_objs)
    
    # rotated objs
    rotated_objs = spin_cube(objs, rotation)

    return rotated_objs.copy()

if __name__ == "__main__":
    
    fn_id   = 1                     # dont change
    p       = 10                    # change this value to get more points
    m_objs  = 3                     # use 2 or 3
    span    = np.linspace(0, 1, p)
    rotation= "positive"
    
    
    if m_objs == 3:
        
        a, b = np.meshgrid(span, span)
        span = np.vstack((a.flatten(), b.flatten())).T
        
    
    pop_size        = span.shape[0]
    n_genes         = 30
    chroms          = np.ones((pop_size, n_genes)) * 0.5

    if m_objs == 2:
        chroms[:, 0] = span

    else:
        chroms[:, 0] = span[:, 0]
        chroms[:, 1] = span[:, 1]

    objs = eval_dtlz_spin_cube_fn_matrix(fn_id, chroms, m_objs, rotation)
    
    # plot
    title = "dltz%d-spin-cube-%s" % (fn_id, rotation)
    import matplotlib.pyplot as plt
    from mpl_toolkits.mplot3d import Axes3D
    
    fig = plt.figure(title)
    
    ax = fig.add_subplot(111, projection="3d") if m_objs == 3 else fig.add_subplot(111)
    
    if rotation == "positive":
        ax.set_xlim(0.5, 1.0)
        ax.set_ylim(0.5, 1.0)
        ax.set_zlim(0.5, 1.0)
    if rotation == "negative":
        ax.set_xlim(-0.5, 0)
        ax.set_ylim(-0.5, 0)
        ax.set_zlim(-0.5, 0)
    
    ax.set_title(title)
    ax.plot(objs[:, 0], objs[:, 1], objs[:, 2], "bo") if m_objs == 3 else ax.plot(objs[:, 0], objs[:, 1], "bo")
    
    
    # -- better example --
    
    from rocket.fronts import get_real_front
    from rocket.plot import plot_pops
    from rocket.helpers.rotation import rotate_x, rotate_y, rotate_z
    
    real_front, p = get_real_front("dtlz1", 3)
    
    objs_pos = eval_dtlz_spin_cube_fn_matrix(fn_id, chroms, m_objs, "positive")
    objs_neg = eval_dtlz_spin_cube_fn_matrix(fn_id, chroms, m_objs, "negative")
    
    
    pops = (objs_pos, objs_neg)
    labels = ("dtlz1-spin-cube-positive", "dtlz1-spin-cube-negative")
    
    fig, ax = plot_pops(pops, labels, real_front=real_front, p=p, lower_lims=(-1, -1, -1), upper_lims=(1, 1, 1))
    
    
    
    # --
    from itertools import product, combinations

    #draw cube
    x_params = []
    y_params = []
    z_params = []
    
    r = [0, 1]
    for s, e in combinations(np.array(list(product(r,r,r))), 2):
        if np.sum(np.abs(s-e)) == r[1]-r[0]:
            #ax.plot3D(*zip(s,e), color="g")
    
            xt, yt, zt = zip(s,e)
            
            x_params.append(xt)
            y_params.append(yt)
            z_params.append(zt)
            
            #ax.plot3D(xt, yt, zt, color="r")
    
    
    xflat = np.array(x_params).flatten().reshape(24, 1)
    yflat = np.array(y_params).flatten().reshape(24, 1)
    zflat = np.array(z_params).flatten().reshape(24, 1)
    
    cube_colors = ("#9DD19A", "#DCA9E0", "#A9D7E0", "#FF8FAF")

    
    #draw big reference cube
    r = [-1, 1]#BA58C2
    for s, e in combinations(np.array(list(product(r,r,r))), 2):
        if np.sum(np.abs(s-e)) == r[1]-r[0]:
            ax.plot3D(*zip(s,e), color="#c0c0c0")
    
    
    # original cube
    cube = np.hstack((xflat, yflat, zflat))
    ax.plot(cube[:, 0], cube[:, 1], cube[:, 2], c=cube_colors[0])

    # rotate y
    cube_rot_y = rotate_y(cube, 180)
    #ax.plot(cube_rot_y[:, 0], cube_rot_y[:, 1], cube_rot_y[:, 2], c=cube_colors[1])

    
    # rotate yz
    cube_rot_yz = rotate_z(cube_rot_y, 270)
    ax.plot(cube_rot_yz[:, 0], cube_rot_yz[:, 1], cube_rot_yz[:, 2], c=cube_colors[2])

    
    ax.view_init(30, 45)
    
    
    plt.show()
    
    
    
    
    
    
