# About this package

This package contains the implementation of `sdtlz` problems, as described in [Deb14].

Run `mop_design.py` to generate this figure:

![](mop_design.png)

## References
- [Deb14] *An Evolutionary Many-Objective Optimization Algorithm Using Reference-Point-Based Nondominated Sorting Approach, Part I: Solving Problems With Box Constrains*