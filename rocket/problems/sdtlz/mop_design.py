# mop_design.py
from __future__ import print_function
import numpy as np

import os, sys, argparse

# add lab's path to sys.path
lab_path = os.path.abspath("../../../")
sys.path.insert(0, lab_path)

from rocket.problems import evaluate_mop
from rocket.helpers import get_pareto_solutions_dtlz, get_n_genes
from rocket.plot import load_rcparams
from rocket.fronts import get_real_front
from rocket.plot.colors import colors, basecolors
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

def plot_pops(ax, mop_name, pop, real_front, p):
    
    ax.set_xlabel("$f_1$")
    ax.set_ylabel("$f_2$")
    ax.set_zlabel("$f_3$")
    
    ax.set_title(mop_name)
    
    ax.w_xaxis.set_pane_color((0.95, 0.95, 0.95, 1.0))
    ax.w_yaxis.set_pane_color((0.95, 0.95, 0.95, 1.0))
    ax.w_zaxis.set_pane_color((0.95, 0.95, 0.95, 1.0))

    ax.view_init(30, 45)

    X = real_front[:, 0].reshape(p, p)
    Y = real_front[:, 1].reshape(p, p)
    Z = real_front[:, 2].reshape(p, p)
    ax.plot_wireframe(X, Y, Z, rstride=2, cstride=2, color=basecolors["real_front"])


    ax.plot(pop[:, 0], pop[:, 1], pop[:, 2], c=colors[0], 
        mec=basecolors["almost_black"], ls="none", marker="o", markeredgewidth=0.15)
            
    
if __name__ == "__main__":
    
    mops = ("sdtlz1", "sdtlz2", "sdtlz3", "sdtlz4")
    m_objs = 3
    
    load_rcparams((18, 4))
    
    fig = plt.figure()
    axes = (fig.add_subplot(141, projection="3d"),
            fig.add_subplot(142, projection="3d"),
            fig.add_subplot(143, projection="3d"),
            fig.add_subplot(144, projection="3d"),
    )
    
    for fn_id, mop_name in enumerate(mops):
        
        n_genes, _, _ = get_n_genes(mop_name, m_objs)
        chroms = get_pareto_solutions_dtlz(n_genes, m_objs, p=10)
        objs = evaluate_mop(chroms, m_objs, {"name":mop_name})
        
        real_front, p = get_real_front(mop_name, m_objs)
        
        plot_pops(axes[fn_id], mop_name, objs, real_front, p)
        
    
    #output="mop_design.png"
    #fig.savefig(output, dpi=300)
    
    
    plt.subplots_adjust(left=0.05, right=0.95)
    plt.show()
        
    
