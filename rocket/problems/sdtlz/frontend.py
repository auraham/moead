# frontend.py
from __future__ import print_function
import numpy as np
from rocket.problems.dtlz.frontend import eval_dtlz_fn_matrix

def eval_sdtlz_fn_matrix(fn_id, chroms, m_objs):
    
    objs = None
    
    # original dtlz
    objs = eval_dtlz_fn_matrix(fn_id, chroms, m_objs)
    
    # get scaling factor
    sf = np.ones((m_objs, ))
    for i in range(m_objs):
        sf[i] = 10**(i)
        
    # scale objs
    sobjs = np.zeros(objs.shape)
    for i in range(m_objs):
        sobjs[:, i] = objs[:, i]*sf[i]
        
    return sobjs.copy()
