# maf3.py
import numpy as np
by_row = 1

def maf3(chroms, m_objs):
    """
    MaF3
    This problem is DTLZ2BZ
    """
    pop_size, _n_genes = chroms.shape
    objs = np.zeros((pop_size, m_objs))
    
    
    """
    # matlab
    PopDec = input;
    M      = Global.M;

    g      = 100*(Global.D-M+1+sum((PopDec(:,M:end)-0.5).^2-cos(20.*pi.*(PopDec(:,M:end)-0.5)),2));
    PopObj = repmat(1+g,1,M).*fliplr(cumprod([ones(size(g,1),1),cos(PopDec(:,1:M-1)*pi/2)],2)).*[ones(size(g,1),1),sin(PopDec(:,M-1:-1:1)*pi/2)];
    PopObj = [PopObj(:,1:M-1).^4,PopObj(:,M).^2];
    """
    
    # alias
    PopDec = chroms
    M = m_objs
    N, D = chroms.shape
    
    # matlab                        python
    # PopDec(:, 1:M-1)              PopDec[:, :M-1]      first m_objs-1 cols
    # PopDec(:, M:end)              PopDec[:, M-1:]      last cols
    

    # g      = 100*(Global.D-M+1+sum((PopDec(:,M:end)-0.5).^2-cos(20.*pi.*(PopDec(:,M:end)-0.5)),2));
    #                            ^    ^                       ^            ^
    #                            F    A                       C            B
    
    A = (PopDec[:, M-1:] - 0.5)**2
    B = (PopDec[:, M-1:] - 0.5)
    C = np.cos(20*np.pi*B)
    F = (A - C).sum(axis=by_row)
    g = 100 * (D - M + 1 + F)
    
    
    # PopObj = repmat(1+g,1,M).*fliplr(cumprod([ones(size(g,1),1),cos(PopDec(:,1:M-1)*pi/2)],2)).*[ones(size(g,1),1),sin(PopDec(:,M-1:-1:1)*pi/2)];
    #          ^                ^              ^                                                  ^                  ^
    #          A                C              B                                                  G                  F
    
    col = (1 + g).reshape((pop_size, 1))
    A   = np.tile(col, (1, M))
    
    col  = np.ones((pop_size, 1))
    othr = np.cos(PopDec[:, :M-1]*np.pi/2)  # cos(PopDec(:,1:M-1)*pi/2)
    B    = np.hstack((col, othr))
    
    C = np.fliplr(np.cumprod(B, axis=by_row))
    
    col = np.ones((pop_size, 1))
    F = np.sin(PopDec[:, M-1-1::-1]*np.pi/2) # sin(PopDec(:,M-1:-1:1)*pi/2)
    G = np.hstack((col, F))
    
    objs = A * C * G
    
    # added to increase precision
    # and avoid an underflow (power 4 next line)
    objs = np.array(objs, dtype=np.longdouble)
    
    # PopObj = [PopObj(:,1:M-1).^4,PopObj(:,M).^2];
    #           ^                  ^ 
    #           A                  B

    A = objs[:, :M-1]**4
    B = objs[:, [-1]]**2
    objs = np.hstack((A, B))
    
    
    return objs.copy()
