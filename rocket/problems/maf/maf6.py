# maf6.py
import numpy as np
by_row = 1

def maf6(chroms, m_objs):
    """
    MaF6
    This problem is DTLZ5(I,M) with I=2
    """
    pop_size, _n_genes = chroms.shape
    objs = np.zeros((pop_size, m_objs))
    I = 2
    
    """
    # matlab
    PopDec = input;
    M      = Global.M;

    g      = sum((PopDec(:,M:end)-0.5).^2,2);
    Temp   = repmat(g,1,M-I);
    PopDec(:,I:M-1) = (1+2*Temp.*PopDec(:,I:M-1))./(2+2*Temp);
    PopObj = repmat(1+100*g,1,M).*fliplr(cumprod([ones(size(g,1),1),cos(PopDec(:,1:M-1)*pi/2)],2)).*[ones(size(g,1),1),sin(PopDec(:,M-1:-1:1)*pi/2)];
    """
    
    # alias
    PopDec = chroms.copy()          # we need to copy before changing
    M = m_objs
    N, D = chroms.shape
    
    # matlab                        python
    # PopDec(:, 1:M-1)              PopDec[:, :M-1]      first m_objs-1 cols
    # PopDec(:, M:end)              PopDec[:, M-1:]      last cols
    
    
    # g = sum((PopDec(:,M:end)-0.5).^2,2);
    
    g = ((PopDec[:, M-1:] - 0.5)**2).sum(axis=by_row)
    
    # Temp = repmat(g,1,M-I);
    col = g.reshape((pop_size, 1))      # this is needed for Temp to get the correct shape: (pop_size, M-I)
    Temp = np.tile(col, (1, M-I))

    # PopDec(:,I:M-1) = (1+2*Temp.*PopDec(:,I:M-1))./(2+2*Temp);
    #                    ^                           ^
    #                    A                           B
    A = 1 + 2 * Temp * PopDec[:, I-1:M-1]
    B = 2 + 2 * Temp
    PopDec[:, I-1:M-1] = A / B
    
    # PopObj = repmat(1+100*g,1,M).*fliplr(cumprod([ones(size(g,1),1),cos(PopDec(:,1:M-1)*pi/2)],2)).*[ones(size(g,1),1),sin(PopDec(:,M-1:-1:1)*pi/2)];
    #          ^                    ^               ^                 ^                               ^^                 ^   
    #          A                    F               B                 C                               HB                 G
    col = (1 + 100*g).reshape((pop_size, 1))
    A = np.tile(col, (1, M))
    
    B = np.ones((pop_size, 1))
    C = np.cos(PopDec[:, :M-1]*np.pi/2)
    F = np.fliplr(np.cumprod(np.hstack((B, C)), axis=by_row))
    
    # sin(PopDec(:,M-1:-1:1)*pi/2)
    
    G = np.sin(PopDec[:, M-1-1::-1]*np.pi/2)
    H = np.hstack((B, G))
    
    objs = A * F * H
    
    
    return objs.copy()
