# maf4.py
import numpy as np
by_row = 1

def maf4(chroms, m_objs):
    """
    MaF4
    % This problem is inverted badly-scaled DTLZ3
    """
    pop_size, _n_genes = chroms.shape
    objs = np.zeros((pop_size, m_objs))
    
    
    """
    # matlab
    PopDec = input;
    [N,D]  = size(PopDec);
    M      = Global.M;

    g      = 100*(D-M+1+sum((PopDec(:,M:end)-0.5).^2-cos(20.*pi.*(PopDec(:,M:end)-0.5)),2));
    PopObj = repmat(1+g,1,M) - repmat(1+g,1,M).*fliplr(cumprod([ones(N,1),cos(PopDec(:,1:M-1)*pi/2)],2)).*[ones(N,1),sin(PopDec(:,M-1:-1:1)*pi/2)];
    PopObj = PopObj.*repmat(2.^(1:M),N,1);
    """
    
    # alias
    PopDec = chroms
    M = m_objs
    N, D = chroms.shape
    
    # matlab                        python
    # PopDec(:, 1:M-1)              PopDec[:, :M-1]      first m_objs-1 cols
    # PopDec(:, M:end)              PopDec[:, M-1:]      last cols
    
    
    # g      = 100*(D-M+1+sum((PopDec(:,M:end)-0.5).^2-cos(20.*pi.*(PopDec(:,M:end)-0.5)),2));
    #                     ^    ^                       ^
    #                     C    A                       B 
    
    A = (PopDec[:, M-1:] - 0.5)**2 
    B = np.cos(20*np.pi*(PopDec[:, M-1:] - 0.5))
    C = (A - B).sum(axis=by_row)
    g = 100 * (D - M + 1 + C)


    # PopObj = repmat(1+g,1,M) - repmat(1+g,1,M).*fliplr(cumprod([ones(N,1),cos(PopDec(:,1:M-1)*pi/2)],2)).*[ones(N,1),sin(PopDec(:,M-1:-1:1)*pi/2)];
    #          ^                 ^                ^               ^         ^                               ^^         ^
    #          A                 A                F               B         C                               HB         G
    
    col = (1 + g).reshape((pop_size, 1))
    A   = np.tile(col, (1, M))
    
    B = np.ones((pop_size, 1))
    C = np.cos(PopDec[:, :M-1]*np.pi/2)
    F = np.fliplr(np.cumprod(np.hstack((B, C)), axis=by_row)) 

    G = np.sin(PopDec[:, M-1-1::-1]*np.pi/2)
    H = np.hstack((B, G))
    
    objs = A - A * F * H

    # PopObj = PopObj.*repmat(2.^(1:M),N,1);
    #                  ^      ^ 
    #                  B       A
    
    pows = np.arange(1, M+1, dtype=int)     # [1, 2, ..., M]
    A = 2**pows 
    B = np.tile(A, (N, 1))
    objs = objs * B


    
    return objs.copy()
