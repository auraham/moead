# maf1.py
import numpy as np
by_row = 1

def maf1(chroms, m_objs):
    """
    MaF1
    This problem is inverted DTLZ1 with the g function of DTLZ2
    """
    pop_size, _n_genes = chroms.shape
    objs = np.zeros((pop_size, m_objs))
    
    
    # matlab
    # PopDec = input;
    # M      = Global.M;
    # g      = sum((PopDec(:,M:end)-0.5).^2,2);
    # PopObj = repmat(1+g,1,M) - repmat(1+g,1,M).*fliplr(cumprod([ones(size(g,1),1),PopDec(:,1:M-1)],2)).*[ones(size(g,1),1),1-PopDec(:,M-1:-1:1)];
    #          ^                 ^                ^      ^       ^                                        ^
    #          A                 A                D      C       B                                        E                                            
    
    
    # alias
    PopDec = chroms
    M = m_objs
    
    
    # matlab                        python
    # PopDec(:, 1:M-1)              PopDec[:, :M-1]      first m_objs-1 cols
    # PopDec(:, M:end)              PopDec[:, M-1:]      last cols
    
    g = ((PopDec[:, M-1:] - 0.5)**2).sum(axis=by_row)               # shape (pop_size, )
    
    col = (1 + g).reshape((pop_size, 1))                            # shape (pop_size, 1), reshape needed for repmat (np.tile)
    A   = np.tile(col, (1, M))                                      # shape (pop_size, M) 
    
    
    col  = np.ones((pop_size, 1))                                    # shape (pop_size, 1)
    othr = PopDec[:, :M-1] 
    B = np.hstack((col, othr))                                       # shape (pop_size, 1 + ...)
    
    C = np.cumprod(B, axis=by_row)
    
    D = np.fliplr(C)
    
    # [ones(size(g,1),1),1-PopDec(:,M-1:-1:1)]
    col = np.ones((pop_size, 1))
    start = M-1-1
    step = -1
    othr = 1 - PopDec[:, start::step]
    E = np.hstack((col, othr))
    
    
    objs = A - A * D * E
    
    
    return objs.copy()
