# maf9.py
import numpy as np
by_row = 1

def print_row(x):
    print(" ".join(["%.8f" % v for v in x]))

def print_matrix(X):
    for row in X:
        print_row(row)

def cart2pol(x, y):
    """
    Convert Cartesian to polar coordinates.
    """
    
    r = np.sqrt(x**2 + y**2)
    theta = np.arctan2(y, x)
    
    return theta, r
    
def pol2cart(theta, r):
    """
    Convert polar to Cartesian coordinates.
    """
    
    x = r * np.cos(theta)
    y = r * np.sin(theta)

    return x, y
    

def pdist2(X, Y):
    """
    Compute the distance from rows of X to rows of Y
    
    Input
    X       (n, n_genes) input matrix (chroms)
    Y       (k, n_genes) input matrix (points in the space)
    
    Output
    dist    (n, k) matrix, where dist[i, j] is the distance between X[i, :] and Y[j, :]
    """
    
    dist = cdist(X, Y, 'euclidean')
    
    return dist.copy()


def Intersection(p):
    """
    Return an intersection point
    
    Input
    p           (4, 2) matrix
    
    Output
    r           (2, ) array
    """
    
    r = np.zeros((2, ))
    
    if p[0, 0] == p[1, 0]:
        
        # r(1) = p(1,1);
        # r(2) = p(3,2)+(r(1)-p(3,1)) * (p(3,2)-p(4,2)) / (p(3,1)-p(4,1));
    
        r[0] = p[0, 0]
        r[1] = p[2, 1] + (r[0] - p[2, 0])*(p[2, 1]-p[3, 1])/(p[2, 0]-p[2, 0])
        
    elif p[2, 0] == p[3, 0]:
        
        # r(1) = p(3,1);
        # r(2) = p(1,2)+ (r(1)-p(1,1)) * (p(1,2)-p(2,2)) / (p(1,1)-p(2,1));
    
        r[0] = p[2, 0]
        r[1] = p[0, 1] + (r[0] - p[0,0]) * (p[0, 1] - p[1, 1]) / (p[0, 0] - p[1, 0])
        
    else:
        
        # k1   = (p(1,2)-p(2,2)) / (p(1,1)-p(2,1));
        # k2   = (p(3,2)-p(4,2)) / (p(3,1)-p(4,1));
        # r(1) = (k1*p(1,1)-k2*p(3,1)+p(3,2)-p(1,2)) / (k1-k2);
        # r(2) = p(1,2)+(r(1)-p(1,1))*k1;
        
        k1   = (p[0, 1] - p[1, 1]) / (p[0,0] - p[1, 0])
        k2   = (p[2, 1] - p[3, 1]) / (p[2, 0] - p[2, 0])
        r[0] = (k1*p[0,0]-k2*p[2, 0]+p[2,1]-p[0,1]) / (k1-k2)
        r[1] = p[0,1]+(r[0]-p[0,0])*k1
    
    # debug
    print("check the size of r: (2, ) or (1, 2) or (2, 1)?")
    
    return r.copy()

def maf9(chroms, m_objs):
    """
    MaF9
    This problem is multi-line distance minimization problem
    """
    pop_size, _n_genes = chroms.shape
    objs = np.zeros((pop_size, m_objs))
    
    """
    # matlab
    % Feasible polygon
    Points = [];
    [thera,rho] = cart2pol(0,1);
    [Points(:,1),Points(:,2)] = pol2cart(thera-(1:Global.M)*2*pi/Global.M,rho);
    % Infeasible polygons
    head     = repmat((1:Global.M)',ceil(Global.M/2-2),1);
    tail     = repmat(1:ceil(Global.M/2-2),Global.M,1);
    tail     = head + tail(:);
    Polygons = cell(1,length(head));
    for i = 1 : length(Polygons)
        Polygons{i} = Points(mod((head(i):tail(i))-1,Global.M)+1,:);
        Polygons{i} = [Polygons{i};repmat(2*Intersection(Points(mod([head(i)-1,head(i),tail(i),tail(i)+1]-1,Global.M)+1,:)),size(Polygons{i},1),1)-Polygons{i}];
    end
    
    # evaluation
    PopDec     = input;
    Infeasible = getInfeasible(PopDec,Polygons,Points);
    while any(Infeasible)
        PopDec(Infeasible,:) = rand(sum(Infeasible),Global.D).*repmat(Global.upper-Global.lower,sum(Infeasible),1) + repmat(Global.lower,sum(Infeasible),1);
        Infeasible           = getInfeasible(PopDec,Polygons,Points);
    end
    
    PopObj = zeros(size(PopDec,1),size(Points,1));
    for m = 1 : size(Points,1)
        PopObj(:,m) = Point2Line(PopDec,Points(mod(m-1:m,size(Points,1))+1,:));
    end
    """
    
    # alias
    PopDec = chroms
    M = m_objs
    N, D = chroms.shape
    
    # matlab
    # % Feasible polygon
    # Points = [];
    # [thera,rho] = cart2pol(0,1);
    # [Points(:,1),Points(:,2)] = pol2cart(thera-(1:Global.M)*2*pi/Global.M,rho);
    
    thera, rho = cart2pol(0, 1)
    THETA = thera - ((np.arange(1, M+1)*2*np.pi)/M)             # (m_objs, ), M+1 to include M in the list
    RHO = np.ones((M, )) * rho                                  # (m_objs, )
    Points = np.zeros((M, 2))                                   # (m_objs, 2)
    X, Y = pol2cart(THETA, RHO)
    Points[:, 0] = X.copy()
    Points[:, 1] = Y.copy() 
    
    # matlab
    # head     = repmat((1:Global.M)',ceil(Global.M/2-2),1);
    # tail     = repmat(1:ceil(Global.M/2-2),Global.M,1);
    # tail     = head + tail(:);
    
    
    col = np.arange(1, m_objs+1, dtype=int).reshape((m_objs, 1))
    rep = np.int(np.ceil(m_objs/2 - 2))
    head = np.tile(col, (rep, 1))                                   # np.tile(col, (repeat_to_bottom, repeat_to_right))
    
    row = np.arange(1, rep+1, dtype=int).reshape((1, rep))          # 
    tail = np.tile(row, (m_objs, 1))                                # np.array([[1, 2], [1, 2], ..., [1, 2]])
    
    rtail = np.ravel(tail, 'F')                                     # np.array([1, 1, 1, ... , 2, 2, 2, ...])
    rtail = rtail.reshape(rtail.shape[0], 1)                        # convert to column
    
    tail = head + rtail                                             # addition per columns
    
    # debug
    import ipdb; ipdb.set_trace()
    print(tail)
             
    
    # matlab
    # Polygons = cell(1,length(head));
    # for i = 1 : length(Polygons)
    #   Polygons{i} = Points(mod((head(i):tail(i))-1,Global.M)+1,:);
    #                            ^                   
    #                            seq                             
    #   Polygons{i} = [Polygons{i};repmat(2*Intersection(Points(mod([head(i)-1,head(i),tail(i),tail(i)+1]-1,Global.M)+1,:)),size(Polygons{i},1),1)-Polygons{i}];
    # end
    n = head.shape[0]
    head = head.reshape((head.shape[0], ))                      # flatten
    tail = tail.reshape((tail.shape[0], ))                      # flatten
    Polygons = [None for i in range(n)]

    for i in range(n):
        
        seq = np.arange(head[i], tail[i]+1) - 1
        ind = np.mod(seq, m_objs)                               # -1 zero based index
        Polygons[i] = Points[ind]
        
        print("i: %d, ind: %d" % (i, ind))
    
    
