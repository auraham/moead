% eval_maf.m

% set seed
rand('state', 100);     % for octave
%rng(100);              % for matlab: https://sachinashanbhag.blogspot.com/2012/09/setting-up-random-number-generator-seed.html



m_objs = 3;             % number of objectives
pop_size = 100;         % population size
glob = GLOBAL(m_objs);


% evaluate maf1
[PopDec]                = MaF1('init', glob, pop_size);             % create init pop
dlmwrite('input_maf1.txt', PopDec, ' ');                            % write input pop to file

[input, PopObj, PopCon] = MaF1('value', glob, PopDec);              % evaluate init pop
dlmwrite('output_maf1.txt', PopObj, ' ');                           % write output pop to file (expected result)

[Front] = MaF1('PF', glob, pop_size);                               % get real front


% -- more compact way --

mops = {{1,  @MaF1},  
        {2,  @MaF2},
        {3,  @MaF3},
        {4,  @MaF4},
        {5,  @MaF5},
        {6,  @MaF6},
        {7,  @MaF7},
        % {8,  @MaF8},      % warning: the 'pdist2' function belongs to the statistics package from Octave 
        % {9,  @MaF9},      % error: getInfeasible: A(I,J): column index out of bounds; value 1 out of bound 0
        % {10, @MaF10},     % error: 'roundn' undefined near line 98 column 14
        {11, @MaF11},
        {12, @MaF12},
        {13, @MaF13}};
        % {14, @MaF14},     % error: MaF14: A(I): index out of bounds; value 1 out of bound 0
        % {15, @MaF15}};    % error: MaF15: A(I): index out of bounds; value 1 out of bound 0

for k = 1:length(mops)

    mop = mops{k};

    i = mop{1};         # id
    MaF = mop{2};       # function
    
    % get mop_name
    mop_name = sprintf('maf%d', i);
    display(mop_name);
    
    % get filenames
    filename_input  = sprintf('input_%s.txt', mop_name) 
    filename_output = sprintf('output_%s.txt', mop_name) 
    
    
    [PopDec]                = MaF('init', glob, pop_size);          % create init pop 
    
    # falta hacer algo como esto en INDIVIDUAL.m
    # Decs  = max(min(Decs,Upper),Lower);
    
    
    dlmwrite(filename_input, PopDec, ' ');                          % write input pop to file

    [input, PopObj, PopCon] = MaF('value', glob, PopDec);           % evaluate init pop
    dlmwrite(filename_output, PopObj, ' ');                         % write output pop to file (expected result)

    %[Front]                 = MaF('PF', glob, pop_size);           % get real front
    
end


