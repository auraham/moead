# maf7.py
import numpy as np
by_row = 1

def maf7(chroms, m_objs):
    """
    MaF7
    This problem is DTLZ7
    """
    pop_size, _n_genes = chroms.shape
    objs = np.zeros((pop_size, m_objs))
    
    """
    # matlab
    PopDec = input;
    M      = Global.M;
    
    PopObj          = zeros(size(PopDec,1),M);
    g               = 1+9*mean(PopDec(:,M:end),2);
    PopObj(:,1:M-1) = PopDec(:,1:M-1);
    PopObj(:,M)     = (1+g).*(M-sum(PopObj(:,1:M-1)./(1+repmat(g,1,M-1)).*(1+sin(3*pi.*PopObj(:,1:M-1))),2));
    """
    
    # alias
    PopDec = chroms.copy()          # we need to copy before changing
    M = m_objs
    N, D = chroms.shape
    
    # matlab                        python
    # PopDec(:, 1:M-1)              PopDec[:, :M-1]      first m_objs-1 cols
    # PopDec(:, M:end)              PopDec[:, M-1:]      last cols
    
    
    # PopObj = zeros(size(PopDec,1),M);
    objs = np.zeros((pop_size, M))
    
    
    # g = 1+9*mean(PopDec(:,M:end),2);
    g = 1 + 9 * np.mean(PopDec[:, M-1:], axis=by_row)
    
    # PopObj(:,1:M-1) = PopDec(:,1:M-1);
    objs[:, :M-1] = PopDec[:, :M-1]
    
    # PopObj(:,M) = (1+g).*(M-sum(PopObj(:,1:M-1)./(1+repmat(g,1,M-1)).*(1+sin(3*pi.*PopObj(:,1:M-1))),2));
    #               ^      ^      ^                ^                    ^ 
    #               A      G      B                C                    F
    
    
    A   = (1 + g)
    B   = objs[:, :M-1]
    col = g.reshape((pop_size, 1))
    C   = 1 + np.tile(col, (1, M-1))
    F   = 1 + np.sin(3*np.pi*objs[:, :M-1])
    G   = M - (B / C * F ).sum(axis=by_row) 
    objs[:, M-1] = A * G
    
    
    
    return objs.copy()
