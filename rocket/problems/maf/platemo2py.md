# PlatEMO to Python



## Error on `maf6`

After evaluating `maf6` with `eval_maf.py`, we found this error:

```
%run eval_maf.py
maf6 is OK (m_objs=3)
maf6 is OK (m_objs=5)
Error on maf6 (m_objs=8)
```

This error is due to `objs` and `exp_objs` are different, but how much/large/big is such a difference? Well, we launch `ipdb` after the error is spoted:

```python
# eval_maf.py

if __name__ == "__main__":
	...

	if not objs is None:
        
    	# check results
        res = np.isclose(objs, exp_objs)
        
        if res.all():
        	print("%s is OK (m_objs=%d)" % (mop_name, m_objs))
        else:
           	print("Error on %s (m_objs=%d)" % (mop_name, m_objs))
            import ipdb; ipdb.set_trace()
```

Notice that `res` is a boolean matrix of shape`(pop_size, m_objs)`, where `res[i, j]` is `True` if `objs[i, j]` *is close* to `exp_objs[i, j]`. Otherwise, these values are different. Thus, in order to get the rows of `objs` that are different to `exp_objs`, we use this command:

```
to_keep = np.where(np.all(res, axis=1)==False)[0]
```

In this case

```python
ipdb> to_keep
array([26, 48, 69, 87])
```

Let's see which values are different:

```
ipdb> res[to_keep]
array([[ True,  True,  True,  True,  True,  True,  True, False],
       [ True,  True,  True,  True,  True,  True,  True, False],
       [ True,  True,  True,  True,  True,  True,  True, False],
       [ True,  True,  True,  True,  True,  True,  True, False]])
```

In turns out that only the last value of each row is distinct. Let's see the matrices:

```
ipdb> print_matrix(objs[to_keep])
0.19848974 0.66755329 2.33866331 8.20664887 28.69743996 39.72958402 165.43164501 0.00001069
0.13297596 0.56180528 2.42620553 10.53613966 45.74375882 29.48329539 232.39400936 0.00000462
0.66079326 0.16147926 2.79842163 11.85182383 49.89633345 18.00062909 223.96396073 0.00008233
1.28137138 0.30080696 0.30861319 5.76573329 25.25720747 49.06187244 235.56647035 0.00004790
```

```
ipdb> print_matrix(exp_objs[to_keep])
0.19848974 0.66755329 2.33866331 8.20664887 28.69743996 39.72958401 165.43164500 0.00001070
0.13297596 0.56180528 2.42620553 10.53613966 45.74375881 29.48329539 232.39400935 0.00000463
0.66079326 0.16147926 2.79842163 11.85182382 49.89633346 18.00062909 223.96396074 0.00008231
1.28137138 0.30080696 0.30861319 5.76573329 25.25720747 49.06187243 235.56647035 0.00004789
```

Well, there is a tiny difference in the last value. However, some other values are also distinct. Take the first row of each matrix as an example:

```
ipdb> print_row(objs[to_keep[0]])
0.19848974 0.66755329 2.33866331 8.20664887 
28.69743996 39.72958402 165.43164501 0.00001069
```

```
ipdb> print_row(exp_objs[to_keep[0]])
0.19848974 0.66755329 2.33866331 8.20664887 
28.69743996 39.72958401 165.43164500 0.00001070
```

Notice that the last two values of `objs[to_keep[0]]` are distinct`(165.43164501, 0.00001069)` regarding to the same last two values of `exp_objs[to_keep[0]]` `(165.43164500 0.00001070)`.

In order to understand why `np.isclose` consider such two pairs of values distinct, we create a script called `test_isclose.py`.

```
%run test_isclose.py
```

This script evaluates several values for `atol`:

```python
% test_isclose.py

if __name__ == "__main__":
    
    objs = np.genfromtxt("objs.txt")
    exp_objs = np.genfromtxt("exp_objs.txt")   

    atols = (
            1e-08,  # 0.00 00 00 01
            1e-07,  # 0.00 00 00 1
            1e-06,  # 0.00 00 01
            1e-05,  # 0.00 00 1
            1e-04,  # 0.00 0 1
            1e-03,  # 0.00 1
            1e-02,  # 0.0 1
            1e-01,  # 0.1
            )
            
    for atol in atols:
        
        res = np.isclose(objs, exp_objs, atol=atol)
        
        print("res: %s, atol: %.8f" % (res.all(), atol))
```

This is the output:

```
In [19]: %run test_isclose.py
res: False, atol: 0.00000001
res: True, atol: 0.00000010
res: True, atol: 0.00000100
res: True, atol: 0.00001000
res: True, atol: 0.00010000
res: True, atol: 0.00100000
res: True, atol: 0.01000000
res: True, atol: 0.10000000
```

That is, both matrices, `objs` and `exp_objs`, are equal according to `np.isclose` if `atol` is set to `0.00000010` (`1e-07`). Given this, we will use a slightly larger value, `atol=1e-06` in `eval_maf.py` for more flexibility.



## Error on `maf3`

We obtained this error when running `eval_maf.py` (`maf3.py`, input filename: `input_matrices/chroms_maf3_m_8_final.txt'`):

```
FloatingPointError: underflow encountered in power
> /home/auraham/Dropbox/trixie/local_improved_mss/rocket/problems/maf/maf3.py(70)maf3()
     68     #           A                  B
     69 
---> 70     A = objs[:, :M-1]**4        
     71     B = objs[:, [-1]]**2
     72     objs = np.hstack((A, B))
```

After debugging, the first row of `A` can be used to explain this error:

```
ipdb> A = objs[:, :M-1]**4
*** FloatingPointError: underflow encountered in power
ipdb> for idx in range(100): print("id: %d, r: %.4f" % (idx, objs[i, :M-1]**4))
*** NameError: name 'i' is not defined
ipdb> for idx in range(100): print("id: %d, r: %.4f" % (idx, objs[idx, :M-1]**4))
*** FloatingPointError: underflow encountered in power
ipdb> for idx in range(100): print("id:", idx); print("r: %.4f" %  objs[idx, :M-1]**4)
id: 0
*** FloatingPointError: underflow encountered in power
ipdb> ref = objs[0, :M-1]
ipdb> ref
array([5.62001589e-82, 4.18725337e-80, 6.83891962e-64, 1.33016130e-62,
       2.17518754e-46, 3.55235084e-30, 5.80142918e-14])
ipdb> print_row(ref)
0.00000000 0.00000000 0.00000000 0.00000000 0.00000000 0.00000000 0.00000000
ipdb> print(" ".join(["%.20f" % v for v in ref]))
0.00000000000000000000 0.00000000000000000000 0.00000000000000000000 0.00000000000000000000 0.00000000000000000000 0.00000000000000000000 0.00000000000005801429
ipdb> print(" ".join(["%.24f" % v for v in ref]))
0.000000000000000000000000 0.000000000000000000000000 0.000000000000000000000000 0.000000000000000000000000 0.000000000000000000000000 0.000000000000000000000000 0.000000000000058014291774
```

The first row of `A` is

```
ref = 0.000000000000000000000000 0.000000000000000000000000 0.000000000000000000000000 0.000000000000000000000000 0.000000000000000000000000 0.000000000000000000000000 0.000000000000058014291774
```

When we try to use power 2 to each element, we got this:

```
ipdb> for i in range(7): print("i", i); print("%.10f" % ref[i]**2)
i 0
0.0000000000
i 1
0.0000000000
i 2
0.0000000000
i 3
0.0000000000
i 4
0.0000000000
i 5
0.0000000000
i 6
0.0000000000
```

But, when we use power 4, we got this:

```
ipdb> for i in range(7): print("i", i); print("%.10f" % ref[i]**4)
i 0
*** FloatingPointError: underflow encountered in double_scalars
```

One way to solve this issue is to change the data type of `ref` from `float64` to either `np.longdouble` (recommended) or `np.float128`:

```
ipdb> ref2 = np.array(objs[0, :M-1], dtype=np.float128)
ipdb> for i in range(7): print("i", i); print("%.10f" % ref2[i]**4)
i 0
0.0000000000
i 1
0.0000000000
i 2
0.0000000000
i 3
0.0000000000
i 4
0.0000000000
i 5
0.0000000000
i 6
0.0000000000
ipdb> ref3 = np.array(objs[0, :M-1], dtype=np.longdouble)
ipdb> for i in range(7): print("i", i); print("%.10f" % ref3[i]**4)
i 0
0.0000000000
i 1
0.0000000000
i 2
0.0000000000
i 3
0.0000000000
i 4
0.0000000000
i 5
0.0000000000
i 6
0.0000000000
```

Given this, we fix `maf3.py` (and `maf13.py` in advance) by adding these lines:

```python
# maf3.py

def maf3(chroms, m_objs):
    objs = A * C * G
    
    # added to increase precision
    # and avoid an underflow (power 4 next line)
    objs = np.array(objs, dtype=np.longdouble)
    
    # PopObj = [PopObj(:,1:M-1).^4,PopObj(:,M).^2];
    #           ^                  ^ 
    #           A                  B

    A = objs[:, :M-1]**4
    B = objs[:, [-1]]**2
    objs = np.hstack((A, B))
    
    
    return objs.copy()
```

To evaluate this change, run `eval_maf.py` again, this is the output:

```
In [1]: %run eval_maf.py
maf1 is OK (m_objs=3, suffix=init)
maf1 is OK (m_objs=3, suffix=final)
maf1 is OK (m_objs=5, suffix=init)
maf1 is OK (m_objs=5, suffix=final)
maf1 is OK (m_objs=8, suffix=init)
maf1 is OK (m_objs=8, suffix=final)
maf1 is OK (m_objs=10, suffix=init)
maf1 is OK (m_objs=10, suffix=final)

maf2 is OK (m_objs=3, suffix=init)
maf2 is OK (m_objs=3, suffix=final)
maf2 is OK (m_objs=5, suffix=init)
maf2 is OK (m_objs=5, suffix=final)
maf2 is OK (m_objs=8, suffix=init)
maf2 is OK (m_objs=8, suffix=final)
maf2 is OK (m_objs=10, suffix=init)
maf2 is OK (m_objs=10, suffix=final)

maf4 is OK (m_objs=3, suffix=init)
maf4 is OK (m_objs=3, suffix=final)
maf4 is OK (m_objs=5, suffix=init)
maf4 is OK (m_objs=5, suffix=final)
maf4 is OK (m_objs=8, suffix=init)
maf4 is OK (m_objs=8, suffix=final)
maf4 is OK (m_objs=10, suffix=init)
maf4 is OK (m_objs=10, suffix=final)

maf6 is OK (m_objs=3, suffix=init)
maf6 is OK (m_objs=3, suffix=final)
maf6 is OK (m_objs=5, suffix=init)
maf6 is OK (m_objs=5, suffix=final)
maf6 is OK (m_objs=8, suffix=init)
maf6 is OK (m_objs=8, suffix=final)
maf6 is OK (m_objs=10, suffix=init)
maf6 is OK (m_objs=10, suffix=final)

maf7 is OK (m_objs=3, suffix=init)
maf7 is OK (m_objs=3, suffix=final)
maf7 is OK (m_objs=5, suffix=init)
maf7 is OK (m_objs=5, suffix=final)
maf7 is OK (m_objs=8, suffix=init)
maf7 is OK (m_objs=8, suffix=final)
maf7 is OK (m_objs=10, suffix=init)
maf7 is OK (m_objs=10, suffix=final)

maf13 is OK (m_objs=3, suffix=init)
maf13 is OK (m_objs=3, suffix=final)
maf13 is OK (m_objs=5, suffix=init)
maf13 is OK (m_objs=5, suffix=final)
maf13 is OK (m_objs=8, suffix=init)
maf13 is OK (m_objs=8, suffix=final)
maf13 is OK (m_objs=10, suffix=init)
maf13 is OK (m_objs=10, suffix=final)
```

The changes were made in `changeset:   164:275808b23338`.