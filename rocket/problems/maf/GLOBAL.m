classdef GLOBAL < handle

    properties
        N          = 100;               % Population size
        M;                              % Number of objectives
        D;                              % Number of variables
        lower;                          % Lower bound of each decision variable
        upper;                          % Upper bound of each decision variable
        evaluation = 10000;             % Maximum number of evaluations
        %operator   = @EAreal;          % Operator function
    end
    
    methods
        
        %% constructor
        function obj = GLOBAL(M)
            obj.M = M;
        end
    end
end
