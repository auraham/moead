# maf13.py
import numpy as np
by_row = 1

def maf13(chroms, m_objs):
    """
    MaF13
    This problem is P7
    """
    pop_size, _n_genes = chroms.shape
    objs = np.zeros((pop_size, m_objs))
    
    """
    # matlab
    X     = input;
    [N,D] = size(X);

    Y = X - 2*repmat(X(:,2),1,D).*sin(2*pi*repmat(X(:,1),1,D)+repmat(1:D,N,1)*pi/D);
    PopObj(:,1) = sin(X(:,1)*pi/2)                   + 2*mean(Y(:,4:3:D).^2,2);
    PopObj(:,2) = cos(X(:,1)*pi/2).*sin(X(:,2)*pi/2) + 2*mean(Y(:,5:3:D).^2,2);
    PopObj(:,3) = cos(X(:,1)*pi/2).*cos(X(:,2)*pi/2) + 2*mean(Y(:,3:3:D).^2,2);
    PopObj(:,4:Global.M) = repmat(PopObj(:,1).^2+PopObj(:,2).^10+PopObj(:,3).^10+2*mean(Y(:,4:D).^2,2),1,Global.M-3);
    """
    
    # alias
    X = chroms
    M = m_objs
    N, D = chroms.shape
    
    # matlab                        python
    # PopDec(:, 1:M-1)              PopDec[:, :M-1]      first m_objs-1 cols
    # PopDec(:, M:end)              PopDec[:, M-1:]      last cols
    
    # Y = X - 2*repmat(X(:,2),1,D).*sin(2*pi*repmat(X(:,1),1,D)+repmat(1:D,N,1)*pi/D);
    #           ^                   ^        ^                  ^
    #           A                   F        B                  C
    A   = np.tile(X[:, 1].reshape((pop_size, 1)), (1, D))
    B   = np.tile(X[:, 0].reshape((pop_size, 1)), (1, D))
    row = np.arange(1, D+1).reshape((1, D))
    C   = np.tile(row, (pop_size, 1))*np.pi/D              # repite row hacia abajo
    F   = np.sin(2*np.pi*B+C)
    Y   = X - 2 * A * F 
    
    # PopObj(:,1) = sin(X(:,1)*pi/2) + 2*mean(Y(:,4:3:D).^2,2);
    #               ^                  ^         
    #               A                  B
    A = np.sin(X[:,0]*np.pi/2)
    B = 2*np.mean(Y[:, 3:D:3]**2, axis=by_row)
    objs[:, 0] = A + B
    
     
    # PopObj(:,2) = cos(X(:,1)*pi/2).*sin(X(:,2)*pi/2) + 2*mean(Y(:,5:3:D).^2,2);
    #               ^                 ^                  ^   
    #               A                 B                  C
    A = np.cos(X[:, 0]*np.pi/2)
    B = np.sin(X[:, 1]*np.pi/2)
    C = 2*np.mean(Y[:, 4:D:3]**2, axis=by_row)
    objs[:, 1] = A * B + C
    
    
    # PopObj(:,3) = cos(X(:,1)*pi/2).*cos(X(:,2)*pi/2) + 2*mean(Y(:,3:3:D).^2,2);
    #               ^                 ^                  ^
    #               A                 B                  C
    A = np.cos(X[:, 0]*np.pi/2)
    B = np.cos(X[:, 1]*np.pi/2)
    C = 2*np.mean(Y[:, 2:D:3]**2, axis=by_row)
    objs[:, 2] = A * B + C
    
    # added to increase precision
    # and avoid an underflow (power 10 next line)
    objs = np.array(objs, dtype=np.longdouble)
    
    
    # PopObj(:,4:Global.M) = repmat(PopObj(:,1).^2+PopObj(:,2).^10+PopObj(:,3).^10+2*mean(Y(:,4:D).^2,2),1,Global.M-3);
    #                               ^              ^               ^               ^ 
    #                               A              B               C               F
    A = objs[:, 0]**2
    B = objs[:, 1]**10
    C = objs[:, 2]**10
    F = 2*np.mean(Y[:, 3:D]**2, axis=by_row)
    
    # reshape before tiling
    A = A.reshape((pop_size, 1))
    B = B.reshape((pop_size, 1))
    C = C.reshape((pop_size, 1))
    F = F.reshape((pop_size, 1))
    
    G = A + B + C + F
    objs[:, 3:] = np.tile(G, (1, M-3))
    
    
    return objs.copy()
