# maf2.py
import numpy as np
by_row = 1

def maf2(chroms, m_objs):
    """
    MaF2
    This problem is DTLZ2BZ
    """
    pop_size, _n_genes = chroms.shape
    objs = np.zeros((pop_size, m_objs))
    
    
    """
    # matlab
    PopDec = input;
    [N,D]  = size(PopDec);
    M      = Global.M;
    
    g = zeros(N,M);
    for m = 1 : M
        if m < M
            g(:,m) = sum(((PopDec(:,M+(m-1)*floor((D-M+1)/M):M+m*floor((D-M+1)/M)-1)/2+1/4)-0.5).^2,2);
        else
            g(:,m) = sum(((PopDec(:,M+(M-1)*floor((D-M+1)/M):D)/2+1/4)-0.5).^2,2);
        end
    end
    PopObj = (1+g).*fliplr(cumprod([ones(size(g,1),1),cos((PopDec(:,1:M-1)/2+1/4)*pi/2)],2)).*[ones(size(g,1),1),sin((PopDec(:,M-1:-1:1)/2+1/4)*pi/2)];
    """
    
    # alias
    PopDec = chroms
    M = m_objs
    N, D = chroms.shape
    
    # matlab                        python
    # PopDec(:, 1:M-1)              PopDec[:, :M-1]      first m_objs-1 cols
    # PopDec(:, M:end)              PopDec[:, M-1:]      last cols
    

    g = np.zeros((N, M))
    
    for m in range(M):
        
        # first M-1 objectives
        if m < M-1:
            
            # g(:,m) = sum(((  PopDec(:,M+(m-1)*floor((D-M+1)/M):M+m*floor((D-M+1)/M)-1)  /2+1/4  )-0.5).^2,2);
            #                  ^
            #                  A
            m_ind = m+1                             # one-based as in matlab
            start = M+(m_ind-1)*np.floor((D-M+1)/M)
            end   = M+m_ind*np.floor((D-M+1)/M)-1
            
            start = int(start-1)                    # -1 for python indexing
            end   = int(end)                      
            
            A = PopDec[:, start:end]
            B = A / 2+1 / 4
            C = ((B - 0.5)**2).sum(axis=by_row)
            
            g[:, m] = C.copy()
        
        # last objective
        else:
            
            # g(:,m) = sum(((  PopDec(:,M+(M-1)*floor((D-M+1)/M):D)/2+1/4)  -0.5).^2,2);
            #                  ^
            #                  A 
            
            start = M+(M-1)*np.floor((D-M+1)/M)
            end = D
            
            start = int(start-1)                # -1 for python indexing
            end = int(D)
            
            A = PopDec[:, start:end]
            B = A / 2+1 / 4
            C = ((B - 0.5)**2).sum(axis=by_row)
            
            g[:, m] = C.copy()
            
    
    
    # PopObj = (1+g).*fliplr(cumprod([ones(size(g,1),1),cos((PopDec(:,1:M-1)/2+1/4)*pi/2)],2)).*[ones(size(g,1),1),sin((PopDec(:,M-1:-1:1)/2+1/4)*pi/2)];
    #                 ^      ^       ^                  ^                                        ^                 ^
    #                 D      C       B                  A                                        F                 E
    
    # cos((PopDec(:,1:M-1)/2+1/4)*pi/2)
    A = np.cos((PopDec[:,:M-1]/2+1/4)*np.pi/2)
    B = np.ones((pop_size, 1))
    C = np.cumprod(np.hstack((B, A)), axis=by_row)
    D = np.fliplr(C)
    
    
    # sin((PopDec(:,M-1:-1:1)/2+1/4)*pi/2)
    start = M-1-1
    step = -1
    E = np.sin((PopDec[:, start::step]/2+1/4)*np.pi/2)
    F = np.ones((pop_size, 1))
    G = np.hstack((F, E))
    
    
    objs = (1+g) * D * G
    
    return objs.copy()
