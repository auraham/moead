# test_maf.py
from __future__ import print_function
import numpy as np
import os, sys, argparse

np.seterr(all='raise')

# add lab's path to sys.path
lab_path = os.path.abspath("../../..")
sys.path.insert(0, lab_path)

from rocket.problems.maf.frontend import eval_maf_fn_matrix


if __name__ == "__main__":
    
    fn_ids = (1, 2, 3, 4, 5, 6, 7, 11, 12, 13)

    for fn_id in fn_ids:
    
        # mop_name
        mop_name = "maf%d" % fn_id
        
        # filenames
        filename_input  = "input_%s.txt" % mop_name
        filename_output = "output_%s.txt" % mop_name
    
        # load input matrix
        chroms = np.genfromtxt(filename_input)
    
        # load expected result
        exp_objs = np.genfromtxt(filename_output)
        m_objs = exp_objs.shape[1]
    
        # evaluate python mop
        objs = eval_maf_fn_matrix(fn_id, chroms, m_objs)
        
        if not objs is None:
    
            # check results
            res = np.isclose(objs, exp_objs)
    
            if res.all():
                print("%s is OK" % mop_name)
            else:
                print("Error on %s" % mop_name)
