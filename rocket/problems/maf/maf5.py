# maf5.py
import numpy as np
by_row = 1

def maf5(chroms, m_objs):
    """
    MaF5
    This problem is scaled DTLZ4
    """
    pop_size, _n_genes = chroms.shape
    objs = np.zeros((pop_size, m_objs))
    
    
    """
    # matlab
    PopDec = input;
    M      = Global.M;

    PopDec(:,1:M-1) = PopDec(:,1:M-1).^100;
    g      = sum((PopDec(:,M:end)-0.5).^2,2);
    PopObj = repmat(1+g,1,M).*fliplr(cumprod([ones(size(g,1),1),cos(PopDec(:,1:M-1)*pi/2)],2)).*[ones(size(g,1),1),sin(PopDec(:,M-1:-1:1)*pi/2)];
    PopObj = PopObj.*repmat(2.^(M:-1:1),size(g,1),1);
    """
    
    # alias
    PopDec = chroms.copy()          # we need to copy before changing
    M = m_objs
    N, D = chroms.shape
    
    # added to increase precision
    # and avoid an underflow (power 100 next line)
    PopDec = np.array(PopDec, dtype=np.longdouble)
    
    # matlab                        python
    # PopDec(:, 1:M-1)              PopDec[:, :M-1]      first m_objs-1 cols
    # PopDec(:, M:end)              PopDec[:, M-1:]      last cols
    
    PopDec[:, :M-1] = PopDec[:, :M-1] ** 100
    
    # g      = sum((PopDec(:,M:end)-0.5).^2,2);
    g = ((PopDec[:, M-1:] - 0.5)**2).sum(axis=by_row)
    
    # PopObj = repmat(1+g,1,M).*fliplr(cumprod([ones(size(g,1),1),cos(PopDec(:,1:M-1)*pi/2)],2)).*[ones(size(g,1),1),sin(PopDec(:,M-1:-1:1)*pi/2)];
    #          ^                ^               ^                 ^                               ^^                 ^
    #          A                F               B                 C                               HB                 G
    
    col = (1 + g).reshape((pop_size, 1))
    A   = np.tile(col, (1, M))
    
    B = np.ones((pop_size, 1)) 
    C = np.cos(PopDec[:, :M-1]*np.pi/2) 
    F = np.fliplr(np.cumprod(np.hstack((B, C)), axis=by_row))
    
    
    G = np.sin(PopDec[:, M-1-1::-1]*np.pi/2)
    H = np.hstack((B, G))
    
    objs = A * F * H
    
    # added to increase precision
    # and avoid an underflow (power 4 next line)
    objs = np.array(objs, dtype=np.longdouble)
    
    # PopObj = PopObj.*repmat(2.^(M:-1:1),size(g,1),1);
    #                  ^      ^            
    #                  B      A           
    
    pows = np.arange(1, m_objs+1, dtype=int)[::-1]        # [m_objs, m_objs-1, ..., 1]
    A = 2**pows
    B = np.tile(A, (pop_size, 1))
    objs = objs * B
    
    
    return objs.copy()
