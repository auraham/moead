# create_input_matrix.py
from __future__ import print_function
import numpy as np
from numpy.random import RandomState

if __name__ == "__main__":
    
    rand = RandomState(100)
    chroms = rand.random_sample((100, 12))
    np.savetxt("chroms.txt", chroms)

