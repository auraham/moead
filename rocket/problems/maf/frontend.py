# frontend.py
from __future__ import print_function
import numpy as np
from rocket.problems.maf.maf1 import maf1
from rocket.problems.maf.maf2 import maf2
from rocket.problems.maf.maf3 import maf3
from rocket.problems.maf.maf4 import maf4
from rocket.problems.maf.maf5 import maf5
from rocket.problems.maf.maf6 import maf6
from rocket.problems.maf.maf7 import maf7
from rocket.problems.maf.maf8 import maf8
from rocket.problems.maf.maf9 import maf9
from rocket.problems.maf.maf13 import maf13

def eval_maf_fn_matrix(fn_id, chroms, m_objs):
    
    objs = None
    
    fns = {
        1:  maf1, 
        2:  maf2, 
        3:  maf3, 
        4:  maf4, 
        5:  maf5, 
        6:  maf6, 
        7:  maf7, 
        8:  maf8, 
        9:  maf9, 
        13: maf13, 
        }
    
    # fn_id check
    if fn_id not in fns:
        print("Error on fn_id value (fn_id: %d)" % fn_id)
        return objs
    
    objs = fns[fn_id](chroms, m_objs)
    
    return objs.copy()
    
    
if __name__ == "__main__":
    
    fn_id           = 1                     # use 1 to 15
    p               = 10                    # change this value to get more points
    m_objs          = 3                     # use 2 or 3
    span            = np.linspace(0, 1, p)
    
    
    if m_objs == 3:
        
        a, b = np.meshgrid(span, span)
        span = np.vstack((a.flatten(), b.flatten())).T
        
    
    pop_size        = span.shape[0]
    n_genes         = 30
    chroms          = np.ones((pop_size, n_genes)) * 0.5

    if m_objs == 2:
        chroms[:, 0] = span

    else:
        chroms[:, 0] = span[:, 0]
        chroms[:, 1] = span[:, 1]


    import ipdb; ipdb.set_trace()
    chroms = np.genfromtxt("pop.txt", delimiter=",")

    objs = eval_maf_fn_matrix(fn_id, chroms, m_objs)
    
    # plot
    title = "MaF"+str(fn_id)
    import matplotlib.pyplot as plt
    from mpl_toolkits.mplot3d import Axes3D
    
    fig = plt.figure(title)
    ax = fig.add_subplot(111, projection="3d") if m_objs == 3 else fig.add_subplot(111)
    ax.set_title(title)
    ax.plot(objs[:, 0], objs[:, 1], objs[:, 2], "bo") if m_objs == 3 else ax.plot(objs[:, 0], objs[:, 1], "bo")
    plt.show()
    
    
