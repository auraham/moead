# test_isclose.py
from __future__ import print_function
import numpy as np

def isclose_custom(a, b, rtol=1e-05, atol=1e-08):
    
    
    # absolute(`a` - `b`) <= (`atol` + `rtol` * absolute(`b`))
    if np.abs(a -b) <= (atol + rtol*np.abs(b)):
        return True
        
    return False
    
    
def print_row(x):
    print(" ".join(["%.12f" % v for v in x]))

def print_matrix(X):
    for row in X:
        print_row(row)
    

if __name__ == "__main__":
    
    objs = np.genfromtxt("objs.txt")
    exp_objs = np.genfromtxt("exp_objs.txt")
    res = np.genfromtxt("res.txt", dtype=int) == 1      # == 1 is needed to convert res as a boolean matrix

    atols = (
            1e-08,  # 0.00 00 00 01
            1e-07,  # 0.00 00 00 1
            1e-06,  # 0.00 00 01
            1e-05,  # 0.00 00 1
            1e-04,  # 0.00 0 1
            1e-03,  # 0.00 1
            1e-02,  # 0.0 1
            1e-01,  # 0.1
            )
            
    for atol in atols:
        
        res = np.isclose(objs, exp_objs, atol=atol)
        
        print("res: %s, atol: %.8f" % (res.all(), atol))

        #print_row(objs[res == False])
        #print_row(exp_objs[res == False])
