# maf8.py
import numpy as np
by_row = 1
from scipy.spatial.distance import cdist

def print_row(x):
    print(" ".join(["%.8f" % v for v in x]))

def print_matrix(X):
    for row in X:
        print_row(row)

def cart2pol(x, y):
    """
    Convert Cartesian to polar coordinates.
    """
    
    r = np.sqrt(x**2 + y**2)
    theta = np.arctan2(y, x)
    
    return theta, r
    
def pol2cart(theta, r):
    """
    Convert polar to Cartesian coordinates.
    """
    
    x = r * np.cos(theta)
    y = r * np.sin(theta)

    return x, y
    

def pdist2(X, Y):
    """
    Compute the distance from rows of X to rows of Y
    
    Input
    X       (n, n_genes) input matrix (chroms)
    Y       (k, n_genes) input matrix (points in the space)
    
    Output
    dist    (n, k) matrix, where dist[i, j] is the distance between X[i, :] and Y[j, :]
    """
    
    dist = cdist(X, Y, 'euclidean')
    
    return dist.copy()
    

def maf8(chroms, m_objs):
    """
    MaF8
    This problem is ParetoBox
    """
    pop_size, _n_genes = chroms.shape
    objs = np.zeros((pop_size, m_objs))

    """
    # matlab
    Points = [];
    [thera,rho] = cart2pol(0,1);
    [Points(:,1),Points(:,2)] = pol2cart(thera-(1:Global.M)*2*pi/Global.M,rho);
    
    # initial pop
    PopDec    = rand(input,Global.D).*repmat(Global.upper-Global.lower,input,1) + repmat(Global.lower,input,1);
    
    # evaluation
    PopDec = input;
    PopObj = pdist2(PopDec,Points);
    """
    
    # alias
    PopDec = chroms
    M = m_objs
    N, D = chroms.shape
    
    # matlab
    # Points = [];
    # [thera,rho] = cart2pol(0,1);
    # [Points(:,1),Points(:,2)] = pol2cart(thera-(1:Global.M)*2*pi/Global.M,rho);
    
    thera, rho = cart2pol(0, 1)
    THETA = thera - ((np.arange(1, M+1)*2*np.pi)/M)             # (m_objs, ), M+1 to include M in the list
    RHO = np.ones((M, )) * rho                                  # (m_objs, )
    Points = np.zeros((M, 2))                                   # (m_objs, 2)
    X, Y = pol2cart(THETA, RHO)
    Points[:, 0] = X.copy()
    Points[:, 1] = Y.copy()    
    
    # matlab
    # PopObj = pdist2(PopDec,Points);
    
    objs = pdist2(PopDec, Points)
    
    return objs.copy()
    
    
