# eval_maf.py
from __future__ import print_function
import numpy as np
import os, sys, argparse

np.seterr(all='raise')

# add lab's path to sys.path
lab_path = os.path.abspath("../../..")
sys.path.insert(0, lab_path)

from rocket.problems.maf.frontend import eval_maf_fn_matrix

def print_row(x):
    print(" ".join(["%.8f" % v for v in x]))

def print_matrix(X):
    for row in X:
        print_row(row)

if __name__ == "__main__":
    
    fn_ids = (1, 2, 3, 4, 5, 6, 7, 11, 12, 13)
    fn_ids = (1, 2, 3, 4, 5, 6, 7, 13)
 
    fn_ids = (9, 10, 11, 12, 14, 15)
    
    
    objs = None
    exp_objs = None

    for fn_id in fn_ids:
    
        for m_objs in (3, 5, 8, 10):
            
            for suffix in ("init", "final"):
            
                # mop_name
                mop_name = "maf%d" % fn_id
                
                # filenames
                filename_input  = "input_matrices/chroms_%s_m_%d_%s.txt" % (mop_name, m_objs, suffix)
                filename_output = "input_matrices/objs_%s_m_%d_%s.txt" % (mop_name, m_objs, suffix)
            
                # load input matrix
                chroms = np.genfromtxt(filename_input)
            
                # load expected result
                exp_objs = np.genfromtxt(filename_output)
                m_objs = exp_objs.shape[1]
            
                # evaluate python mop
                objs = eval_maf_fn_matrix(fn_id, chroms, m_objs)
                
                
                if not objs is None:
            
                    # check results
                    res = np.isclose(objs, exp_objs, atol=1e-06)
            
                    if res.all():
                        print("%s is OK (m_objs=%d, suffix=%s)" % (mop_name, m_objs, suffix))
                    else:
                        print("Error on %s (m_objs=%d, suffix=%s)" % (mop_name, m_objs, suffix))
                        
                        
        print("")
