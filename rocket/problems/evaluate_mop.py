# evaluate_mop.py
from __future__ import print_function
import numpy as np
#from rocket.problems.wfg        import eval_wfg_fn_matrix
#from rocket.problems.wfg_spin   import eval_wfg_spin_fn_matrix
from rocket.problems.zdt        import eval_zdt_fn_matrix
from rocket.problems.maf        import eval_maf_fn_matrix
from rocket.problems.dtlz       import eval_dtlz_fn_matrix
from rocket.problems.dtlz_spin  import eval_dtlz_spin_fn_matrix
from rocket.problems.dtlz_spin_cube  import eval_dtlz_spin_cube_fn_matrix
from rocket.problems.min_dtlz   import eval_min_dtlz_fn_matrix
from rocket.problems.inv_dtlz   import eval_inv_dtlz_fn_matrix
from rocket.problems.zhou16     import eval_zhou16_fn_matrix
from rocket.problems.imb        import eval_imb_fn_matrix
from rocket.problems.imbs       import eval_imbs_fn_matrix
from rocket.problems.mp         import eval_mp_fn_matrix
from rocket.problems.aligned_mop_p import eval_aligned_mop_p_fn_matrix
from rocket.problems.sdtlz      import eval_sdtlz_fn_matrix
from rocket.problems.scaled_dtlz      import eval_scaled_dtlz_fn_matrix
from rocket.problems.kursawe      import eval_kursawe_fn_matrix
from rocket.helpers import check_n_genes

def evaluate_mop(chroms, m_objs, mop_params):
    """
    Evaluate a matrix of chroms using a mop.
    
    Input
    chroms      (pop_size, n_genes) chroms matrix
    m_objs      number of objectives
    mop_params  dict with parameters
    
    Output
    objs        (pop_size, m_objs) objs matrix
    """

    pop_size, n_genes   = chroms.shape
    objs                = np.zeros((pop_size, m_objs))
    mop_name            = mop_params["name"].lower()
    
    # check n_genes
    check_n_genes(mop_name, m_objs, n_genes)
    

    # dtlz
    if mop_name in ("dtlz1", "dtlz2", "dtlz3", "dtlz4", "dtlz5"):
        
        fn_id = int(mop_name[-1])
        objs = eval_dtlz_fn_matrix(fn_id, chroms, m_objs)

    # kursawe
    elif mop_name in ("kursawe", ):
        
        objs = eval_kursawe_fn_matrix(chroms, m_objs)

    # sdtlz
    elif mop_name in ("sdtlz1", "sdtlz2", "sdtlz3", "sdtlz4", "sdtlz5"):
        
        fn_id = int(mop_name[-1])
        objs = eval_sdtlz_fn_matrix(fn_id, chroms, m_objs)

    # scaled_dtlz
    elif mop_name in ("scaled_dtlz1", "scaled_dtlz2", "scaled_dtlz3", "scaled_dtlz4", "scaled_dtlz5"):
        
        fn_id = int(mop_name[-1])
        objs = eval_scaled_dtlz_fn_matrix(fn_id, chroms, m_objs)

    # maf
    # (1, 2, 3, 4, 5, 6, 7, 13)
    elif mop_name in ("maf1", "maf2", "maf3", "maf4", "maf5", "maf6", "maf7", "maf13"):
        
        fn_id = int(mop_name.replace("maf", ""))
        objs = eval_maf_fn_matrix(fn_id, chroms, m_objs)
    
    # mp
    elif mop_name in ("mp1", "mp2", "mp3", "mp4", "mp5", "mp6", "mp7", "mp8"):
        
        fn_id = int(mop_name[-1])
        objs = eval_mp_fn_matrix(fn_id, chroms, m_objs)

    # p problems for aligned_mop experiment
    elif mop_name in ("p1", "p2", "p3", "p4", "p5", 
                    "p6", "p7", "p8", "p9", "p10",
                    "p11", "p12", "p13", "p14", "p15"):
        
        fn_id = int(mop_name[1:])
        objs = eval_aligned_mop_p_fn_matrix(fn_id, chroms, m_objs)

    # dtlz-spin
    elif mop_name in ("dtlz1-spin", "dtlz2-spin", "dtlz3-spin", "dtlz4-spin", "dtlz5-spin"):
        
        fn_id   = int((mop_name.split("-")[0])[-1])
        degrees = mop_params["degrees"]                 # @note: este parametro es ignorado en la funcion spin de helpers/rotation.py
        objs    = eval_dtlz_spin_fn_matrix(fn_id, chroms, m_objs, degrees)

    # inv-dtlz
    elif mop_name in ("inv-dtlz1", ):
        
        fn_id   = int((mop_name.split("-")[1])[-1])
        objs    = eval_inv_dtlz_fn_matrix(fn_id, chroms, m_objs)

    # [Zhou16]' t1 and t2
    elif mop_name in ("t1", "t2"):
        
        fn_id   = int(mop_name[-1])
        objs    = eval_zhou16_fn_matrix(fn_id, chroms)

    # [Liu16]' IMB
    elif mop_name in ("imb1", "imb4"):
        
        fn_id   = int(mop_name[-1])
        objs    = eval_imb_fn_matrix(fn_id, chroms)
        
    # [Liu16]' Scalable IMB (imbs) 
    elif mop_name in ("imbs4", ):
        
        fn_id   = int(mop_name[-1])
        objs    = eval_imbs_fn_matrix(fn_id, chroms, m_objs)

    # @todo: no he probado con los demas dtlz, por eso no los coloco en la tupla
    # @note: no creo que este problema sea bueno para la experimentacion porque la forma de las soluciones optimas cambio despues de rotar!
    elif mop_name in ("dtlz1-spin-cube", ):
        
        fn_id   = int((mop_name.split("-")[0])[-1])
        rotation = mop_params["rotation"] 
        objs    = eval_dtlz_spin_cube_fn_matrix(fn_id, chroms, m_objs, rotation)

    # min-dtlz
    elif mop_name in ("min-dtlz1", "min-dtlz2", "min-dtlz3", "min-dtlz4", "min-dtlz5"):
        
        fn_id = int(mop_name[-1])
        objs = eval_min_dtlz_fn_matrix(fn_id, chroms, m_objs)
    
    # zdt
    elif mop_name in ("zdt1", "zdt2", "zdt3", "zdt4", "zdt6"):
        
        fn_id = int(mop_name[-1])
        objs = eval_zdt_fn_matrix(fn_id, chroms)
        """    
        # wfg
        elif mop_name in ("wfg1", "wfg2", "wfg3", "wfg4", "wfg5", "wfg6", "wfg7", "wfg8", "wfg9"):
            
            k       = mop_params["k"]
            fn_id   = int(mop_name[-1])
            objs    = eval_wfg_fn_matrix(fn_id, chroms, m_objs, k)
        
        # wfg-spin
        elif mop_name in ("wfg1-spin", "wfg2-spin", "wfg3-spin", "wfg4-spin", "wfg5-spin", "wfg6-spin", "wfg7-spin", "wfg8-spin", "wfg9-spin"):
            
            k       = mop_params["k"]
            fn_id   = int((mop_name.split("-")[0])[-1])
            degrees = mop_params["degrees"]                 # @note: este parametro es ignorado en la funcion spin de helpers/rotation.py
            objs    = eval_wfg_spin_fn_matrix(fn_id, chroms, m_objs, k, degrees)
        """
        
    else:
        print("%s is not implemented yet!" % mop_name)
    
    return objs.copy()
