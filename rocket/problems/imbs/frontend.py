# frontend.py
from __future__ import print_function
import numpy as np
from rocket.problems.imbs.imbs4 import imbs4

def eval_imbs_fn_matrix(fn_id, chroms, m_objs):
    
    objs = None
    
    fns = {
        4: imbs4, 
        }
    
    # fn_id check
    if fn_id not in fns:
        print("Error on fn_id value (fn_id: %d)" % fn_id)
    
    pop_size = chroms.shape[0]
    objs = np.zeros((pop_size, m_objs))
    
    for m in range(m_objs):
        objs[:, m] = fns[fn_id](chroms, m, m_objs)

    return objs.copy()
