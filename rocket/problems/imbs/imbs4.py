# imbs4.py : based on dtlz1.py
import numpy as np
by_row = 1

def imbs4(X, m, M):
    
    r = None
    
    # separation of X
    n = X.shape[1]
    j = M - 1           # j position (diversity)   variables (first j variables of x)
    k = n - j           # k distance (convergence) variables (last  k variables of x)
    
    Y = X[:, :-k]
    Z = X[:, -k:]
    
    ##g = dtlz1_g(Z, k)
    g = imb4_g(X, j)
    
    # objective 0
    if m == 0:
        r = (1.0 + g) * (Y.prod(axis=by_row))
    
    # objectives [1, .., m-2]
    if m in range(1, M-1):
        r = (1.0 + g) * (Y[:, :M-m-1].prod(axis=by_row)) * (1 - Y[:, M-m-1])
    
    # objective M-1
    if m == M-1:
        r = (1.0 + g) * (1 - Y[:, 0])

    return r

def dtlz1_g(Z, k):
    r = 100 * (k+(((Z - 0.5)**2) - np.cos(20*np.pi*(Z-0.5))).sum(axis=by_row))
    return r
    

def imb4_g(X, j):
    
    pop_size, n_genes = X.shape
    
    X1       = X[:, 0]                  # first variable
    Xn       = X[:, j:]                 # j+1 to last variables
    g        = np.zeros((pop_size, ))   # return value
    
    # evaluate condition
    #case_a = (0 <= X1) & (X1 <= 2./3)      # error
    case_a = (2./3 <= X1) & (X1 <= 1.0)     # correct!
    case_b = ~case_a
    
    # case b
    per_row = 1
    coss    = 2*np.cos((np.pi*X1)/2)                    # (pop_size, )
    sumTi   = (X[:, :j]).sum(axis=per_row) / 2          # (pop_size, ), suma las primeras j columnas (ie. las primeras j variables)
    sumTi   = sumTi.reshape((pop_size, 1))              # (pop_size, 1) para hacer la suma por columna
    Ti      = Xn - sumTi                                # (pop_size, n-2)
    prod1   = -0.9 * (Ti**2)
    prod2   = np.abs(Ti)**0.6
    suma    = coss * (prod1 + prod2).sum(axis=per_row)
    
    # ---
    #sine    = np.sin(0.5*np.pi*X1).reshape((pop_size, 1)).repeat(n_genes-1, axis=1)     # (pop_size, n-1)
    #Ti      = Xn - sine                                                                 # (pop_size, n-1)
    #prod1   = -0.9 * (Ti**2)
    #prod2   = np.abs(Ti)**0.6
    #suma    = (0.5 * (prod1 + prod2)).sum(axis=per_row)
    # ---
    
    # save
    g[case_a]   = 0
    g[case_b]   = suma[case_b]
    
    return g.copy()
