# About this package

This package contains a generalization of IMB [Liu17]. 



# Notes about construction

To scale IMB to more objectives, we need to make some changes. Consider IMB4 as an example:

```python
# imbs4.py
def imbs4(X, m, M):
    
    n = X.shape[1]      # num of variables
    j = M - 1           # j position (diversity) vars
    k = n - j           # k distance (convergence) vars
  
    g = imb4_g(X, j) 
```

First, the function `imb4_g` must be changed to handle more than $j=2$ position variables:

```python
# imb_v1/imb4.py
def imb4_g(X):
    
    X1       = X[:, 0]   # first variable
    Xn       = X[:, 2:]  # third to last variables

    sumTi   = (X[:, [0, 1]]).sum(axis=per_row) / 2      # (pop_size, )
```

That snippet was replaced in `imbs4.py` as follows:

```python
# imbs4.py
def imb4_g(X, j):
        
    X1       = X[:, 0]    # first variable
    Xn       = X[:, j:]   # j+1 to last variables
    
    sumTi   = (X[:, :j]).sum(axis=per_row) / 2  # (pop_size, ), 
    										    # suma las primeras j columnas 
                                                # (ie. las primeras j variables)
```

As it can be seen, the change is pretty simple, just define the number of position variables as `j`, where:

- `X[:, 2:] ` is replaced by `X[:, j:]`
- `X[:, [0, 1]]` is replaced by `X[:, :j]`

Also, do not forget to change the definition of the objectives (just remove the factor `0.5`):

```python
# imbs4.py
def imbs4(X, m, M):

    # objective 0
    if m == 0:
        r = (1.0 + g) * (Y.prod(axis=by_row))
    
    # objectives [1, .., m-2]
    if m in range(1, M-1):
        r = (1.0 + g) * (Y[:, :M-m-1].prod(axis=by_row)) * (1 - Y[:, M-m-1])
    
    # objective M-1
    if m == M-1:
        r = (1.0 + g) * (1 - Y[:, 0])

```



```python
# dtlz1.py
def dtlz1(X, m, M):

    # objective 0
    if m == 0:
        r = (1.0 + g) * 0.5 * (Y.prod(axis=by_row))
    
    # objectives [1, .., m-2]
    if m in range(1, M-1):
        r = (1.0 + g) * 0.5 * (Y[:, :M-m-1].prod(axis=by_row)) * (1 - Y[:, M-m-1])
    
    # objective M-1
    if m == M-1:
        r = (1.0 + g) * 0.5 * (1 - Y[:, 0])
```



# Coverage issue

![](coverage_issue_imbs4.png)



The previous figure shows how the Pareto front is not completely covered if the $j$ position variables only cover the range [0, 1] partially. In this case, the range covered is $x_i \in [0, 0.3]$, for $i=1, \dots, j$. You can get this figure by using `coverage_issue.py`.  The function `fake_mesh_2` covers the previous range as follows:

```python
def fake_mesh_2(p):
    
    print("using fake mesh to cover [0, 0.3]")
    x = np.linspace(0, 0.3, p)
    
    a, b = np.meshgrid(x, x)
    
    matrix = np.vstack((a.flatten(),
                        b.flatten()
                        )).T
                        
    return matrix.copy()
```

**TL;DR** It is important to cover the range [0,1] using a meshgrid for the $j=m-1$ decision (diversity) variables to get the whole Pareto front.



# References

- [Liu17] *Investigating the Effect of Imbalance Between Convergence and Diversity in Evolutionary Multi-objective Algorithms*