# test_evaluate_mop_min_dtlz.py
from __future__ import print_function
from rocket.problems.evaluate_mop import evaluate_mop
from rocket.problems.dtlz.dtlz1 import dtlz1
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

def create_sample_chroms():
    
    p = 10           # change this value to get more points
    m_objs = 3       # dont change
    x = np.linspace(0, 1, p)
    a, b, c = np.meshgrid(x, x, x)
    span = np.vstack((
                a.flatten(),
                b.flatten(),
                c.flatten(),
                )).T
    pop_size = span.shape[0]
    n_genes  = 12
    chroms   = np.ones((pop_size, n_genes)) * 0.04977       # optimal value for min-dtlz1

    for i in range(m_objs-1):
        chroms[:, i] = span[:, i]
        
    return chroms.copy()
    
    
# our proposal
def test_evaluate_problem(chroms, m_objs):
    
    objs = evaluate_mop(chroms, m_objs, {"name":"min-dtlz1"})    
    return objs
    
# our competitor
def problem(chroms, m_objs):
    
    pop_size = chroms.shape[0]
    objs = np.zeros((pop_size, m_objs))
    
    for m in range(m_objs):
        objs[:, m] = dtlz1(chroms, m, m_objs) * (-1)        # this is the single change needed, -1
        
    return objs
    
if __name__ == "__main__":
    
    m_objs = 3
    chroms = create_sample_chroms()
    objs   = test_evaluate_problem(chroms, m_objs)


    fig = plt.figure("test_evaluate_mop_min_dtlz")
    ax = fig.add_subplot(111, projection="3d")
    ax.set_title("min-dtlz")
    ax.plot(objs[:, 0], objs[:, 1], objs[:, 2], "bo")
    ax.view_init(30, 45)
    plt.show()
