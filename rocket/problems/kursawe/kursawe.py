# kursawe.py

import numpy as np

def kursawe(x):
   obj = np.zeros((len(x),2))
   obj[:,0] = -10.0 * np.exp(-0.2 * np.sqrt((x[:,0]*x[:,0]) + (x[:,1]*x[:,1]))) + -10.0 * np.exp(-0.2 * np.sqrt((x[:,1]*x[:,1]) + (x[:,2]*x[:,2])))
   obj[:,1] = np.sum(np.abs(x)**0.8 + 5.0 * np.sin(x*x*x), axis=1)
   return obj

