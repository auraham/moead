# frontend.py
from __future__ import print_function
import numpy as np
from rocket.problems.kursawe.kursawe import kursawe

def eval_kursawe_fn_matrix(chroms, m_objs):
    
    if m_objs != 2:
        print("Error, kusawe is a two-objective problem")
        return None
    
    pop_size = chroms.shape[0]
    objs = kursawe(chroms)

    return objs.copy()
    
    
