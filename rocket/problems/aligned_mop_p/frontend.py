# frontend.py
from __future__ import print_function
import numpy as np
from rocket.problems.aligned_mop_p.common import linear_shape, multimodal_linear_shape, unimodal_linear_shape
from rocket.helpers import scaling, spin_objs

def eval_aligned_mop_p_fn_matrix(fn_id, chroms, m_objs, debug=False):
    
    objs = None
    
    
    # fn_id: (scaling_factor, rotation_degrees)
    params = {
    
        # no scaling factor
        1: (1, 0),
        2: (1, 45),
        3: (1, 90),
        4: (1, 135),
        5: (1, 180),
        
        # regular scaling factor
        6:  (100, 0),
        7:  (100, 45),
        8:  (100, 90),
        9:  (100, 135),
        10: (100, 180),
        
        
        # irregular scaling factor
        11: ("irregular", 0),
        12: ("irregular", 45),
        13: ("irregular", 90),
        14: ("irregular", 135),
        15: ("irregular", 180),
    }
    
    
    # m_objs check
    if m_objs != 3:
        print("Rotation is not supported for m_objs!=3")
        return None
        
    # fn_id check
    if fn_id not in params:
        print("Error on fn_id value (fn_id: %d)" % fn_id)
        return None
    
    
    # get params
    scaling_factor, degrees = params[fn_id]
    
    # debug
    #print("fn_id: %d, scaling factor: %s, degrees: %d" % (fn_id, scaling_factor, degrees))
    
    # shape
    #objs = linear_shape(chroms, m_objs)                # no distance-variable function here, therefore, all the solutions lie in the same plane
    
    #objs = multimodal_linear_shape(chroms, m_objs)      # multimodal distance-variable function (dtlz1_g), but some 'random' solutions can be optimal (possibly to numerical precision)
    
    objs = unimodal_linear_shape(chroms, m_objs)        # unimodal distance-variable function (dtlz2_g)
    
    # scale
    scaled_objs = scaling(objs, scaling_factor)
    
    # rotate and shrink
    rotated_objs, shrinked_objs = spin_objs(scaled_objs, degrees)
    
    # return all?
    if debug:
        return objs.copy(), scaled_objs.copy(), rotated_objs.copy(), shrinked_objs.copy()
    
    return shrinked_objs.copy()
