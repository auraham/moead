# common.py
import numpy as np
by_row = 1

def linear_shape_col(X, m, M):
    """
    Evaluate a chroms matrix into a linear shape function (one objective at a time)
    This function is similar to DTLZ1, but without the multimodal function g nor scaling factor 0.5
    
    Input
    chroms          (pop_size, n_genes) chroms matrix
    m               int, m-th objective to be evaluated
    m_objs          int, total number of objectives
    
    Output
    r               (pop_size, ) m-th objective column
    """
    
    r = None
    
    # separation of X
    n = X.shape[1]
    j = M - 1
    k = n - j
    
    Y = X[:, :-k]
    Z = X[:, -k:]
    
    # added: no multimodality
    #g = dtlz1_g(Z, k)
    
    # objective 0
    if m == 0:
        
        # no multimodality, no scaling factor 0.5
        #r = (1.0 + g) * 0.5 * (Y.prod(axis=by_row))
        
        r = (Y.prod(axis=by_row))
    
    # objectives [1, .., m-2]
    if m in range(1, M-1):
        
        # no multimodality, no scaling factor 0.5
        #r = (1.0 + g) * 0.5 * (Y[:, :M-m-1].prod(axis=by_row)) * (1 - Y[:, M-m-1])
        
        r = (Y[:, :M-m-1].prod(axis=by_row)) * (1 - Y[:, M-m-1])
    
    # objective M-1
    if m == M-1:
        
        # no multimodality, no scaling factor 0.5
        #r = (1.0 + g) * 0.5 * (1 - Y[:, 0])
        r = (1 - Y[:, 0])

    return r.copy()
    

def linear_shape(chroms, m_objs):
    """
    Evaluate a chroms matrix into a linear shape function
    This function is similar to DTLZ1, but without the multimodal function g nor scaling factor 0.5
    
    Input
    chroms          (pop_size, n_genes) chroms matrix
    m_objs          int, number of objectives
    
    Output
    objs            (pop_size, m_objs) objs matrix
    """
    
    pop_size = chroms.shape[0]
    objs = np.zeros((pop_size, m_objs))
    
    for m in range(m_objs):
        objs[:, m] = linear_shape_col(chroms, m, m_objs)

    return objs.copy()
    
# -- multimodal --

def dtlz1_g(Z, k):
    r = 100 * (k+(((Z - 0.5)**2) - np.cos(20*np.pi*(Z-0.5))).sum(axis=by_row))
    return r


def multimodal_linear_shape_col(X, m, M):
    """
    Evaluate a chroms matrix into a linear shape function (one objective at a time)
    This function is similar to DTLZ1, but without the scaling factor 0.5
    
    Input
    chroms          (pop_size, n_genes) chroms matrix
    m               int, m-th objective to be evaluated
    m_objs          int, total number of objectives
    
    Output
    r               (pop_size, ) m-th objective column
    """
    
    r = None
    
    # separation of X
    n = X.shape[1]
    j = M - 1
    k = n - j
    
    Y = X[:, :-k]
    Z = X[:, -k:]
    
    # unlike linear_shape(), multimodality is enabled
    g = dtlz1_g(Z, k)
    
    # scaling factor (change to sf=0.5 to behave like dtlz1)
    sf = 1.0
    
    # objective 0
    if m == 0:
        r = (1.0 + g) * sf * (Y.prod(axis=by_row))
        
    
    # objectives [1, .., m-2]
    if m in range(1, M-1):
        r = (1.0 + g) * sf * (Y[:, :M-m-1].prod(axis=by_row)) * (1 - Y[:, M-m-1])
        
    
    # objective M-1
    if m == M-1:
        r = (1.0 + g) * sf * (1 - Y[:, 0])


    return r
    

def multimodal_linear_shape(chroms, m_objs):
    """
    Evaluate a chroms matrix into a linear shape function
    This function is similar to DTLZ1, but without the scaling factor 0.5
    
    Input
    chroms          (pop_size, n_genes) chroms matrix
    m_objs          int, number of objectives
    
    Output
    objs            (pop_size, m_objs) objs matrix
    """
    
    pop_size = chroms.shape[0]
    objs = np.zeros((pop_size, m_objs))
    
    for m in range(m_objs):
        objs[:, m] = multimodal_linear_shape_col(chroms, m, m_objs)

    return objs.copy()
    

# -- unimodal --

def dtlz2_g(Z, k):
    
    r = ((Z - 0.5)**2).sum(axis=by_row)
    
    return r
    

def unimodal_linear_shape_col(X, m, M):
    """
    Evaluate a chroms matrix into a linear shape function (one objective at a time)
    This function is similar to DTLZ1, but without the scaling factor 0.5 (and dtlz2_g instead of dtlz1_g)
    
    Input
    chroms          (pop_size, n_genes) chroms matrix
    m               int, m-th objective to be evaluated
    m_objs          int, total number of objectives
    
    Output
    r               (pop_size, ) m-th objective column
    """
    
    r = None
    
    # separation of X
    n = X.shape[1]
    j = M - 1
    k = n - j
    
    Y = X[:, :-k]
    Z = X[:, -k:]
    
    # unlike multimodal_linear_shape(), multimodality is disabled
    g = dtlz2_g(Z, k)
    
    # scaling factor (change to sf=0.5 to behave like dtlz1)
    sf = 1.0
    
    # objective 0
    if m == 0:
        r = (1.0 + g) * sf * (Y.prod(axis=by_row))
        
    
    # objectives [1, .., m-2]
    if m in range(1, M-1):
        r = (1.0 + g) * sf * (Y[:, :M-m-1].prod(axis=by_row)) * (1 - Y[:, M-m-1])
        
    
    # objective M-1
    if m == M-1:
        r = (1.0 + g) * sf * (1 - Y[:, 0])


    return r
    

def unimodal_linear_shape(chroms, m_objs):
    """
    Evaluate a chroms matrix into a linear shape function
    This function is similar to DTLZ1, but without the scaling factor 0.5 (and dtlz2_g instead of dtlz1_g)
    
    Input
    chroms          (pop_size, n_genes) chroms matrix
    m_objs          int, number of objectives
    
    Output
    objs            (pop_size, m_objs) objs matrix
    """
    
    pop_size = chroms.shape[0]
    objs = np.zeros((pop_size, m_objs))
    
    for m in range(m_objs):
        objs[:, m] = unimodal_linear_shape_col(chroms, m, m_objs)

    return objs.copy()
    


if __name__ == "__main__":
    
    import os, sys, argparse

    # add lab's path to sys.path
    lab_path = os.path.abspath("../../../")
    sys.path.insert(0, lab_path)
    
    from rocket.helpers import get_pareto_solutions_dtlz, get_n_genes
    from rocket.plot import plot_pops, load_rcparams
    
    # create pareto optimal pop
    m_objs = 3
    n_genes, _, _ = get_n_genes("dtlz1", m_objs)
    
    chroms = get_pareto_solutions_dtlz(n_genes, m_objs, p=12)
    
    # evaluate pop
    objs = linear_shape(chroms, m_objs)
    

    # plot pop
    load_rcparams()
    
    pops = (objs, )
    labels = ("pop", )
    
    plot_pops(pops, labels, title="linear shape", lower_lims=(0,0,0), upper_lims=(1.05, 1.05, 1.05))
