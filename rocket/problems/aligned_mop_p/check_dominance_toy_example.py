# check_dominance_toy_example.py
# este script muestra que
# 1. existe una solucion en el gap que es no dominada (quiza por precision numerica)
# 2. el inconveniente (1) se podria remover usando otra funcion de convergencia, quiza como la de dtlz2, que no involucre funciones trigonometricas
# 3. falta comparar la relacion de dominancia con el problema similar a P10 (ie no rotado, P6)
# 4. el valor de las variables de convergencia es muy sensible! ie hay un gran cambio de 0.5 a 0.6, por lo que creo que podria dificultar un poco la busqueda 

# otra forma de hacer prueba de preservacion de dominancia seria
# 1. evaluar P1 y P5 (original y rotado; ie P1 vs alguno de {P2, P3, P4, P5})
# 2. generar los conjuntos de puntos A, B, C
# 3. for mop in (P1, P5):
# 4.     obtener nds de (A_objs, B_objs, C_objs)
# 5. si el nds_indices de P1 y P5 es el mismo, entonces la relacion de dominancia no cambio luego de la transformacion del problema
# (ojo, compara nds_indices [ie todos los indices], no solo nds_best)

from __future__ import print_function
import numpy as np

import os, sys, argparse

# add lab's path to sys.path
lab_path = os.path.abspath("../../../")
sys.path.insert(0, lab_path)

from rocket.problems.aligned_mop_p import eval_aligned_mop_p_fn_matrix
from rocket.helpers import get_pareto_solutions_dtlz, get_pareto_solutions_p, get_n_genes
from rocket.plot import load_rcparams, plot_pops
from rocket.fronts import get_real_front
from rocket.plot.colors import colors, basecolors
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

from rocket.moea.nds import fnds
from rocket.helpers.mesh import mesh_2



def get_suboptimal_solutions_p(n_genes, m_objs, p):
    """
    Return a set of suboptimal solutions for p1-15 problems
    This function is similar to get_pareto_solutions_p(), but opt_x is modified.
    
    Input
    n_genes     int, number of decision variables
    m_objs      int, number of objectives
    p           int, spacing
    
    Output
    chroms      (rows, n_genes) matrix of pareto optimal solutions
    """
    
    matrix = None
        
    if m_objs == 3:
        matrix = mesh_2(p)
    
    else:
        print("m_objs: %s is not defined for get_pareto_solutions_p" % m_objs)       # @todo: add log support here
        return None
    
    rows, cols = matrix.shape
        
    # optimal value for distance parameters
    opt_x = 0.601                                # @note: only modify this line (something slightly greater than 0.5)!
    
    chroms = np.ones((rows, n_genes))*opt_x
    
    for col in range(cols):                     # rewrite the first cols of chroms
        chroms[:, col] = matrix[:, col]         # with the content of matrix
        
    return chroms.copy()
    
    
    
def get_random_solutions(n_genes, m_objs, p):
    """
    Return a set of random suboptimal solutions for p1-15 problems
    This function is similar to get_pareto_solutions_p(), but opt_x is modified randomly.
    
    Input
    n_genes     int, number of decision variables
    m_objs      int, number of objectives
    p           int, spacing
    
    Output
    chroms      (rows, n_genes) matrix of pareto optimal solutions
    """
    
    matrix = None
        
    if m_objs == 3:
        matrix = mesh_2(p)
    
    else:
        print("m_objs: %s is not defined for get_pareto_solutions_p" % m_objs)       # @todo: add log support here
        return None
    
    rows, cols = matrix.shape
    
    # optimal value for distance parameters
    rand = np.random.RandomState(100)
    lbound = 0.5                        # optimal value
    ubound = 0.601                        # a user-defined top value
    
    opt_x = (ubound - lbound) * rand.random_sample((rows, n_genes)) + lbound   # rand values between [lbound, ubound]
    
    chroms = np.ones((rows, n_genes))*opt_x
    
    for col in range(cols):                     # rewrite the first cols of chroms
        chroms[:, col] = matrix[:, col]         # with the content of matrix
        
    return chroms.copy()
    

def diff(a_list, b_list):

    to_keep = []

    for b in b_list:
        
        if b in a_list:
            continue
        else:
            to_keep.append(b)
            
    return np.array(to_keep, dtype=int)

def pretty_print(mat, indices):
    
    for i, row in enumerate(mat):
        
        print("%d: " % indices[i], end="")
        
        for col in row:
            print(" %.15f " % col, end="")
        print("")

def analyze_solutions(objs, set_ref, set_diff, mop_name):
    
    print("-- %s --" % mop_name)
    
    print("ref")
    pretty_print(objs[set_ref], set_ref)

    print("diff")
    pretty_print(objs[set_diff], set_diff)
    



if __name__ == "__main__":

    p_vals = {
        8: 64,
        7: 49,
        6: 36,
        5: 25,
        4: 16,
        3: 9,
    }
    
    
    fn_ids = (10, 6)                    # mops to be compared, P6 and P10
    p = 5
    n_points = p_vals[p]*3                    # three sets of p_vals[p] points each
    indices = np.zeros((2, n_points))
    
    set_refs = []
    set_diff = []
    
    for fn_count, fn_id in enumerate(fn_ids):
    
        # -- dont edit from here --
        
        m_objs  = 3             # number of objectives
        mop_name= "p"+str(fn_id)
        
        n_genes, c_vars, d_vars  = get_n_genes(mop_name, m_objs)
        
        # get solutions
        A = get_pareto_solutions_p(n_genes, m_objs, p)      # they are the same as dtlz's optimal solutions
        B = get_suboptimal_solutions_p(n_genes, m_objs, p)  # similar to A
        C = get_random_solutions(n_genes, m_objs, p)        # random solutions near to the pareto front
        
        # evaluate
        A_objs = eval_aligned_mop_p_fn_matrix(fn_id, A, m_objs)
        B_objs = eval_aligned_mop_p_fn_matrix(fn_id, B, m_objs)
        C_objs = eval_aligned_mop_p_fn_matrix(fn_id, C, m_objs)
        
        # merge sets
        merged_chroms= np.vstack((A, B, C))
        merged_objs  = np.vstack((A_objs, B_objs, C_objs))
        
        # non dominating sorting
        nds_fronts, nds_ranks = fnds(merged_objs)
        
        # save indices
        indices[fn_count] = nds_ranks.copy()
        
        # get nds indices
        nds_indices = np.arange(len(nds_ranks), dtype=int)
        nds_best = nds_indices[nds_ranks == nds_ranks.min()]    # indices of those solutions with the lowest (better) rank
        
        # check indices
        A_indices = np.arange(len(A), dtype=int)
    
        
        if (len(A_indices) == len(nds_best)):
            if (A_indices == nds_best).all():
                # every solution in A is pareto optimal
                # ie, better than solutions in B and C
                print("A is optimal for %s" % mop_name)
        
        else:
            
            print("A_indices")
            print(A_indices)
            
            print("nds_best")
            print(nds_best)
            
            set_diff = diff(A_indices, nds_best)
            
            print("diff")
            print(set_diff)
            
            set_refs = set_diff - 50 # @note: this is a hard-coded line!
            
            # for debug
            import ipdb; ipdb.set_trace()
            
        # print for both P10, P6
        analyze_solutions(merged_objs, set_refs, set_diff, mop_name)
            
        
        # real front
        real_front = eval_aligned_mop_p_fn_matrix(fn_id, A, m_objs)
        
        # plot
        pops = (A_objs, B_objs, C_objs, merged_objs[nds_best], merged_objs[set_diff])
        labels = ["A (0.5)", "B (suboptimal)", "C (suboptimal random)", "NDS", "NDS - diff"]
        
        fig, ax = plot_pops(pops, labels, title=mop_name.upper(), real_front=real_front, p=p, elev=9.3, azim=-45, output="check_dominance_%s.png" % mop_name)
        
    
    # final comparison
    if (indices[0] == indices[1]).all():
        print("dominance relation did not changed for P%s and P%s!" % fn_ids)
    else:
        print("dominance relation changed for P%s and P%s!" % fn_ids)
