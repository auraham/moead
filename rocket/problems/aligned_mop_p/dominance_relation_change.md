# Change in dominance relationship

This document explains how the dominance relation is changed/altered by our transformation. This change is slight, but it is worth to know it.

Given a objective vector $\mathbf{f} = (f_1(\mathbf{x}), \dots, f_m(\mathbf{x}))$, our transformation returns a new objective vector $\mathbf{f}'$  as follows:

1. Scaling
2. Rotation
3. Shrinking

When this transformation is applied to a simplex-like Pareto front of a given problem, its size and orientation changes preserving its simplex-like shape. Run `mop_design.py` to generate the following figure. This figure shows how the original front (a) is transformed. This procedure returns a new front with a similar shape to the original front, but with different scaling and orientation in the objective space. The aim of such transformation is to generate new test problems to evaluate the shape-invariance of decomposition-based moeas. If a decomposition-based moea works well in (a) but no in (d), then such moea is not shape-invariance, that is, its performance depends/is subjected to/is related to/ iit is sensible /on the geometric nature of the Pareto front. However, if a decomposition-based moea works well on both original (a) and shrinked (d) fronts, then such a moea is rotation-invariant. That is, it is considered robust within this context.

![](fig_mop_design.png)

The implementation of such a transformation is in `frontend.py`:

```
code
```



## What changes when the problem is scaled, rotated, and shrinked? 

We develop our transformation method to change the shape/orientation of the Pareto front in the objecitve space, without altering/degradaring significantly the dominance relationship of the original problem. 





Our transformation takes a set of objective vectors $P$ and returns a 

Our transformation changes the shape/.. of the Pareto front $A$



On of the side effects of modifying the definition of a mop is the possible alteration of the dominance relationship. Consider two sets of objective vectors, denoted by $A$ and $B$, and the original and transformed problem denoted by $\mathbf{f}$ and $\mathbf{f}'$. Also assume that every solution in $A$ belongs to the Pareto front, thus $B$ is inferior to $A$:
$$
\text{ for } \forall b \in B, \exists a\in A \text{ such that } a \text{ dominantes } b
$$
We are interested in preserving the dominance relation defined in (1). If such a relationship changes when the problem if transformed, then some solutions in $B$ can be part of the Pareto front.

then, the Pareto optimal set and front can be modified. This is a side-effect of changing the problem definition. If $A$ is Pareto optimal according to the original problem $\mathbf{f}$, then it must be also Pareto optimal according to $\mathbf{f}'$.

We developed our transformation in order t



Run `check_dominance_toy_example.py` to illustrate this change. Three sets are defined

- `A` contains Pareto optimal solutions to the original problem (`P6`)
- `B` contains suboptimal solutions
- `C` contains suboptimal random solutions

This scripts performs the following tasks

1. Merge the three sets (`merged_chroms`)
2. Evaluate the merged set on the original problem `P6`
3. Perform Non-dominated sorting to get the best solutions from the merged set
4. Repeat the first three steps by using the rotated problem `P10`
5. If the set `A` is optimal in both `P6` and `P10`, then the dominante relationship is not altered.

This is the output of `check_dominance_toy_example.py` for `P10`:

```
A_indices
[ 0  1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24]

nds_best
[ 0  1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24
 50 74]
```

As can be seen, two solutions, `50, 74`, were considered non-dominated. Thus, the optimal solutions `A` on the original problem (dtlz1) are different in the new one (`P10`). Such a change is caused by the rotation, because the arrays `A_indices` and `nds_best` are the same on `P6`. The *new solutions*, `50, 74` belongs to `C` (`A` has solutions in the range `[0,24]`, `B` in `[25, 49]`, and `C` in `[50, 74]`, inclusive). Their corresponding solutions in `A` are `0` and `24` (there is an offset of `50`, so `24 = 74 - 50`).

Consider the following solutions:

```
set_refs = array([ 0, 24])
set_diff = array([50, 74])
```

The dominance relation of `set_refs` and `set_diff` in `P6` is

```
-- p6 --
ref
0:   16.666666666666668  16.666666666666668  66.666666666666671 
24:  66.666666666666671  16.666666666666668  16.666666666666668 
diff
50:  16.897645793296885  16.897645793296885  67.590583173187540 
74:  67.475150219694740  16.868787554923685  16.868787554923685 
```

that is, `0` dominates to `50` and `24` dominates `74`. `P6` is not rotated. Now, this is the dominance relation in `P10`:

```
-- p10 --
ref
0:   50.000000000000021  50.000000000000021  0.000000000000019 
24:  0.000000000000019  50.000000000000021  50.000000000000021 
diff
50:  50.692937379890658  50.692937379890651  0.000000000000009 
74:  0.000000000000009  50.606362664771076  50.606362664771062
```

`0` and `50` are non-dominated, and `24` and `74` are non-dominated too. Note how the first and last objectives are almost zero. I think such a difference is the cause of the alteration of the dominance relation.

 