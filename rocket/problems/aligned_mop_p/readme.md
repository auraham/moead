# About this package

This package contains our new benchmark based on:

- DTLZ [Deb05]
- inv-DTLZ [Jain13]


The proposed benchmark, called P, is detailed below.



[INSERT TABLE HERE]




| Problem | Base            | Properties                            | Geometry |
| ------- | --------------- | ------------------------------------- | -------- |
| MP1     | DTLZ1           | Multimodal                            | Linear   |
| MP2     | DTLZ1, IMB4     | Multimodal, Imbalance                 | Linear   |
| MP3     | inv-DTLZ1       | Multimodal, Inverted shape            | Linear   |
| MP4     | IMB4, inv-DTLZ1 | Multimodal, Imbalance, Inverted shape | Linear   |

| Problem | Base               | Properties                            | Geometry |
| ------- | ------------------ | ------------------------------------- | -------- |
| MP5     | DTLZ3              | Multimodal                            | Concave  |
| MP6     | DTLZ1, DTLZ3, IMB4 | Multimodal, Imbalance                 | Concave  |
| MP7     | inv-DTLZ2          | Multimodal, Inverted shape            | Concave  |
| MP8     | IMB4, inv-DTLZ2    | Multimodal, Imbalance, Inverted shape | Concave  |



# *Pareto fronts

![](fig_benchmark_regions.png)

This figure shows the Pareto fronts of the proposed benchmark for $m=3$ objectives. To generate this figure, run `labs/aligned_mop/experiments/notebooks/visualize.py`.





# Script `common.py`

This script contains the `linear_shape` function. This function takes as input a set of solutions (`chroms`) and the number of objectives (`m_objs`). As output, it returns a matrix of objectives (`objs`). This function is similar to DTLZ1. The only purspose of `linear_shape` is to provide to the Pareto front a linear (simplex-like) shape. As a result, `linear_shape` **do not include** multimodality nor a scaling factor `0.5` as DTLZ1. Also, `linear_function_col` is a helper function to evaluate one objective at a time. The differences between both `linear_shape_col` and `dtlz` are shown below:

```python
# dtlz1.py
def dtlz1(X, m, M):
    ...
    
    # multimodality
    g = dtlz1_g(Z, k)
    
    # objective 0
    if m == 0:
        r = (1.0 + g) * 0.5 * (Y.prod(axis=by_row))
    
    # objectives [1, .., m-2]
    if m in range(1, M-1):
        r = (1.0 + g) * 0.5 * (Y[:, :M-m-1].prod(axis=by_row)) * (1 - Y[:, M-m-1])
    
    # objective M-1
    if m == M-1:
        r = (1.0 + g) * 0.5 * (1 - Y[:, 0])

    return r
```

```python
# common.py
def linear_shape_col(X, m, M):
    ...
    
    # objective 0
    if m == 0:
        r = (Y.prod(axis=by_row))
    
    # objectives [1, .., m-2]
    if m in range(1, M-1):
        r = (Y[:, :M-m-1].prod(axis=by_row)) * (1 - Y[:, M-m-1])
    
    # objective M-1
    if m == M-1:
        r = (1 - Y[:, 0])

    return r.copy()
```

This script also includes an example, just execute using `ipython` as follows:

```
% run common.py
```

This example evaluates a Pareto optimal set of solutions `chroms`. The resulting `objs` matrix is a simplex-like front. Unlike DTLZ1, the extremes of this front are located at `[1, 0, 0]`, `[0, 1, 0]`, and `[0, 0, 1]`.

![](linear_shape.png)

**Update (changeset 106)** It seems that multimodality is needed. The following figure shows two objective sets: `pop` (evenly distributed) and `rand` (check `linear_shape_issue.py`).  `pop` was created by using `chroms`, whereas `rand` by using `rand_chroms`:


```python
# linear_shape_issue.py

# fixed pop
chroms = get_pareto_solutions_dtlz(n_genes, m_objs, p=12)
objs = linear_shape(chroms, m_objs)

# random pop
rand = np.random.RandomState(100)
rand_chroms = rand.random_sample(chroms.shape)
rand_objs = linear_shape(rand_chroms, m_objs)
```

At first, I thought that `rand` would be far away from the Pareto front. However, it is not the case, as can be seen in the following figure. Instead, both the fixed (`pop`) and random (`rand`) populations lie in the sample plane. That is because we are using `linear_shape()` only. This function employs *position (diversity)* variables only, that is, *distance (convergence)* variables are ignored. For this reason, it is required an additional (maybe multimodal) function to involve convergence variables. 

![](linear_shape_issue.png)

Given this issue, two new function were added in `common.py`:

- `multimodal_linear_shape`
- `multimodal_linear_shape_col`

These functions takes into account both diversity and convergence variables. `multimodal_linear_shape()` is similar to `dtlz1` but without the scaling factor `0.5`.

**Note** if you want the scaling factor `0.5` back, just modify `sf` as follows:

```python
def multimodal_linear_shape_col(X, m, M):
	...
    # scaling factor (change to sf=0.5 to behave like dtlz1)
    sf = 0.5
```

**Update (changeset 1111)** This revision contains `check_dominance.py`. This script determines whether the dominance relation changes after problem transformation. Two problems are compared: `P6` and `P10`. The following figures are generated by this script:

![](check_dominance_p6.png)

![](check_dominance_p10.png)

*tl;dr* dominance relation can change after transformation. In `P16`, the first set (`A`) is Pareto optimal as expected. However, after transforming this problem as `P10`, the set of non-dominated solutions changes. In that case, a solution of `C` is non-dominated as depicted above (gray point in `C`). As a result, the dominance relation has changed. My initial thought are:

-  This issue is due to numerical precision. Remember that now `multimodal_linear_shape()` is employed in `frontend.py`, thus, trigonometric functions are used. If `dtlz1_g` was replaced for other non-trigonometric-based function, such as `dtlz2_g`, maybe this issue can be solved.
- It is not shown above but, the variable `x_opt` (for convergence variables) is very sensible to small changes. The optimal value is `x_opt=0.5`. However, another value slightly different, such as `x_opt=0.6` will result in an approximation far far away from the real front. This issue is alleviated by using `unimodal_linear_shape()` instead of `unimodal_linear_shape()`, see changeset 1114.



**Para mañana** Tiene sentido que la relación de dominancia cambie si se transforma el problema (creo). Lo que ocurre es que estamos empleando el `fnds` en dos conjuntos distintos: el orignal (`merged_objs` de `P6`) y el rotado (`merged_objs` de `P10`). En ambos casos, lo que queremos es que las soluciones del conjutno `A`, que son óptimas de Pareto en `P6`, sigan siendo óptimas de Pareto en `P10`. Si esto ocurre, podemos decir que la relación de dominancia no se degrada (ie que los óptimos no cambian tras la transformación).  *Sin embargo, esto no ocurre necesariamente para `B` y `C`*. Creo que para entenderlo mejor, deberíamos hacer un ejemplo (como `check_dominance.py`) con menos soluciones, quizá 10 por cada set A,B,C, y mostrar que

- A es óptimo en P6 y P10
- cómo cambia la relación de dominancia entre las soluciones de B y C. Es decir, encontrar dos soluciones $b \in B$ y $c \in C$ tal que (1)  $b$ domine a $c$ en `P6` y (2) $c$ domine a $b$ en `P10`. 

Procede como sigue:

- Crea `check_dominance_toy_example.py` con conjuntos `A`, `B`, `C` más pequeños.
- Encuentra los puntos $b$ y $c$ como se describe arriba.
- Describe el ejemplo en estas notas.

**Nota**: De acuerdo a lo que comenté arriba, la prueba de dominancia se debe realizar así:

```python
# check indices
A_indices = np.arange(len(A), dtype=int)

if (len(A_indices) == len(nds_best)):
  if (A_indices == nds_best).all():
  # every solution in A is pareto optimal
  # ie, better than solutions in B and C
  print("A is optimal for %s" % mop_name)

```

mientras que la otra prueba no debe ser tomada en cuenta:

```python
# final comparison
if (indices[0] == indices[1]).all():
    print("dominance relation did not changed for P%s and P%s!" % fn_ids)
else:
    print("dominance relation changed for P%s and P%s!" % fn_ids)
```

Como se mencionó arriba, queremos que `A` sea óptimo en ambos problemas. No importa (para propósitos de este script) si la relación de domincia de los conjuntos `B` y `C` cambia.




# References

- [Deb05] *Scalable Test Problems for Evolutionary Multiobjective Optimization*
- [Liu17] *Investigating the Effect of Imbalance Between Convergence and Diversity in Evolutionary Multi-objective Algorithms*
- [Jain13] *An Improved Adaptive Approach for Elitist Nondominated Sortng Genetic Algorithm for Many-Objective Optimization*



