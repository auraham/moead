# imb1.py
from __future__ import print_function
import numpy as np

def IMB1(X):
    # @todo: generalize for more objetives
    
    pop_size = X.shape[0]
    objs = np.zeros((pop_size, 2))
    
    objs[:, 0] = imb1_f1(X)
    objs[:, 1] = imb1_f2(X)
    
    return objs.copy()
    
    
def imb1_g(X):
    
    pop_size, n_genes = X.shape
    
    X1       = X[:, 0]                  # first variable
    Xn       = X[:, 1:]                 # second to last variables
    g        = np.zeros((pop_size, ))   # return value
    
    # evaluate condition
    case_a = (0 <= X1) & (X1 <= 0.2)
    case_b = ~case_a
    
    # case b
    per_row = 1
    sine    = np.sin(0.5*np.pi*X1).reshape((pop_size, 1)).repeat(n_genes-1, axis=1)     # (pop_size, n-1)
    Ti      = Xn - sine                                                                 # (pop_size, n-1)
    prod1   = -0.9 * (Ti**2)
    prod2   = np.abs(Ti)**0.6
    suma    = (0.5 * (prod1 + prod2)).sum(axis=per_row)
    
    # save
    g[case_a]   = 0
    g[case_b]   = suma[case_b]
    
    return g.copy()
    
    
def imb1_f1(X):
    
    X1  = X[:, 0]
    g   = imb1_g(X)
    r   = (1 + g)*X1
    
    return r.copy()
    
def imb1_f2(X):
    
    X1  = X[:, 0]
    g   = imb1_g(X)
    r   = (1 + g)*(1 - np.sqrt(X1))
    
    return r.copy()
    
    
    
    
