# frontend.py
from __future__ import print_function
import numpy as np
from rocket.problems.imb.imb1 import IMB1
from rocket.problems.imb.imb4 import IMB4

def eval_imb_fn_matrix(fn_id, chroms):
    
    objs = None
    
    # fn_id check
    if fn_id not in (1, 4):
        print("Error on fn_id value (fn_id: %d)" % fn_id)
        return
        
    if fn_id == 1:
        objs = IMB1(chroms)
    
    if fn_id == 4:
        objs = IMB4(chroms)
    

    return objs.copy()
    
    
if __name__ == "__main__":
    
    fn_id           = 1         # use 1, 4 
    pop_size        = 100
    n_genes         = 10
    X               = None
    
    if fn_id == 1:
        
        # optimal
        X = np.zeros((pop_size, n_genes))
        X[:, 0] = np.linspace(0, 1, pop_size)   # X1 puede contener cualquier valor entre 0 y 1
                                                # dato: si se usa np.linspace(0, k, pop_size), el valor del obj 1 estara en el rango [0, n]
                                                # ie, para que f1 abarque [0, 1], usa un linspace de [0, 1], no solo de [0, 0.2]
        
        for i in range(1, n_genes):
            X[:, i] = np.sin(0.5*np.pi*X[:, 0])
        
    if fn_id == 4:
        
        p = 10
        x = np.linspace(0, 1, p)
        a, b = np.meshgrid(x, x)
        
        matrix = np.vstack((a.flatten(),
                            b.flatten()
                        )).T
        
        pop_size = matrix.shape[0]
        X = np.zeros((pop_size, n_genes))
        X[:, 0] = matrix[:, 0]
        X[:, 1] = matrix[:, 1]
        
        
        for i in range(2, n_genes):
            X[:, i] = (X[:, 0] + X[:, 1])/2
        
    
    chroms = X
    objs   = eval_imb_fn_matrix(fn_id, chroms)
    m_objs = objs.shape[1]
    
    # plot
    title = "IMB"+str(fn_id)
    import matplotlib.pyplot as plt
    from mpl_toolkits.mplot3d import Axes3D

    fig = plt.figure(title)
    ax = fig.add_subplot(111) if m_objs == 2 else fig.add_subplot(111, projection="3d")
    ax.set_title(title)
    ax.plot(objs[:, 0], objs[:, 1], "bo") if m_objs == 2 else ax.plot(objs[:, 0], objs[:, 1], objs[:, 2], "bo")
    plt.show()
    
    
