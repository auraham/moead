# imb4.py
from __future__ import print_function
import numpy as np

def IMB4(X):
    
    pop_size = X.shape[0]
    objs = np.zeros((pop_size, 3))
    
    objs[:, 0] = imb4_f1(X)
    objs[:, 1] = imb4_f2(X)
    objs[:, 2] = imb4_f3(X)
    
    return objs.copy()
    
    
def imb4_g(X):
    
    pop_size, n_genes = X.shape
    
    X1       = X[:, 0]                  # first variable
    Xn       = X[:, 2:]                 # third to last variables
    g        = np.zeros((pop_size, ))   # return value
    
    # evaluate condition
    #case_a = (0 <= X1) & (X1 <= 2./3)      # error
    case_a = (2./3 <= X1) & (X1 <= 1.0)     # correct!
    case_b = ~case_a
    
    # case b
    per_row = 1
    coss    = 2*np.cos((np.pi*X1)/2)                    # (pop_size, )
    sumTi   = (X[:, [0, 1]]).sum(axis=per_row) / 2      # (pop_size, )
    sumTi   = sumTi.reshape((pop_size, 1))              # (pop_size, 1) para hacer la suma por columna
    Ti      = Xn - sumTi                                # (pop_size, n-2)
    prod1   = -0.9 * (Ti**2)
    prod2   = np.abs(Ti)**0.6
    suma    = coss * (prod1 + prod2).sum(axis=per_row)
    
    # ---
    #sine    = np.sin(0.5*np.pi*X1).reshape((pop_size, 1)).repeat(n_genes-1, axis=1)     # (pop_size, n-1)
    #Ti      = Xn - sine                                                                 # (pop_size, n-1)
    #prod1   = -0.9 * (Ti**2)
    #prod2   = np.abs(Ti)**0.6
    #suma    = (0.5 * (prod1 + prod2)).sum(axis=per_row)
    # ---
    
    # save
    g[case_a]   = 0
    g[case_b]   = suma[case_b]
    
    return g.copy()
    
    
def imb4_f1(X):
    
    X1  = X[:, 0]
    X2  = X[:, 1]
    g   = imb4_g(X)
    r   = (1 + g)*X1*X2
    
    return r.copy()
    
def imb4_f2(X):
    
    X1  = X[:, 0]
    X2  = X[:, 1]
    g   = imb4_g(X)
    r   = (1 + g)*X1*(1-X2)
    
    return r.copy()
    
def imb4_f3(X):
    
    X1  = X[:, 0]
    g   = imb4_g(X)
    r   = (1 + g)*(1-X1)
    
    return r.copy()
    
    
    
    
