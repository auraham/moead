# About this package #

This package contains the implementation of the imbalanced problems IMB described in [Liu16].

The input variables are in the range `[0, 1]`. The authors recommend $n=10$ variables.



# About input range

This snippet creates an input matrix `X` with optimal values for `IMB1`:

```python
X = np.zeros((pop_size, n_genes))

# X1 should cover [0, 1], not only [0, 0.2]
X[:, 0] = np.linspace(0, 1, pop_size)   

for i in range(1, n_genes):
    X[:, i] = np.sin(0.5*np.pi*X[:, 0])
```

The first decision variable `X[:, 0]` should cover the whole range `[0, 1]`, not only `[0, 0.2]`. If `X[:, 0]` covers only `[0, n]`, then the first objective $f_1$ will cover `[0, n]`.

This figure shows the evaluation of `X` on `IMB1` , where `X[:, 0]` spans `[0, 1]`. The Pareto front is completly covered:

![](imb1_full_cover.png)

This figure shows the evaluation of `X` on `IMB1` , where `X[:, 0]` spans `[0, 0.5]`. The Pareto front is partially covered:

![](imb1_partial_cover.png)

This snippet plots the previous figures:

```python
# frontend.py

fn_id           = 1         
pop_size        = 100
n_genes         = 10
X = np.zeros((pop_size, n_genes))

# full front
X[:, 0] = np.linspace(0, 1, pop_size)

# partial front
# X[:, 0] = np.linspace(0, 0.5, pop_size)

for i in range(1, n_genes):
    X[:, i] = np.sin(0.5*np.pi*X[:, 0])

    chroms = X
    objs   = eval_imb_fn_matrix(fn_id, chroms)

# plot
title = "IMB"+str(fn_id)
import matplotlib.pyplot as plt
fig = plt.figure(title)
ax = fig.add_subplot(111)
ax.set_title(title)
ax.plot(objs[:, 0], objs[:, 1], "bo")
plt.show()
```



# TODO

- Generalize for `m_objs`.



# References

-  [Liu16] *Investigating the Effect of Imbalance Between Convergence and Diversity in Evolutionary Multi-objective Algorithms*.



