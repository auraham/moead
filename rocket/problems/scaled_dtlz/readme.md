# About this package

This package contains the implementation of `scaled_dtlz` problems, as described in [Deb14].

**Update** Unlike `sdtlz`, `scaled_dtlz` uses the following scaling factors:

| $m$ objectives | scaling factor $s_i$ |
| -------------- | -------------------- |
| 3              | $10^i$               |
| 5              | $10^i$               |
| 8              | $3^i$                |
| 10             | $2^i$                |
| 15             | $1.2^i$              |

In our implementation:

```python
# base for scaling factor
base = 10

if m_objs >= 8:
	base = 3

if m_objs >= 10:
	base = 2

if m_objs >= 15:
	base = 1.2

# get scaling factor
sf = np.ones((m_objs, ))
for i in range(m_objs):
	sf[i] = base**(i)
```

**Note** In [Deb14], the authors use a different scale factor for `dtlz1` and `dtlz2`. In our implementation, we use the same scaling factor for both problems for simplicity.

Run `mop_design.py` to generate this figure:

![](mop_design.png)

## References
- [Deb14] *An Evolutionary Many-Objective Optimization Algorithm Using Reference-Point-Based Nondominated Sorting Approach, Part I: Solving Problems With Box Constrains*