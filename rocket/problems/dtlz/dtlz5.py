# dtlz5.py
import numpy as np
by_row = 1

def dtlz5(X, m, M):
    
    r = None
    
    # separation of X
    pop_size, n = X.shape
    j = M - 1
    k = n - j
    
    Y = X[:, :-k]
    Z = X[:, -k:]
    
    g = ((Z - 0.5)**2).sum(axis=by_row)

    theta = Y.copy()                # y = {x_1, ..., x_M-1}
    theta[:, 0] *= (np.pi * 0.5)    # segun raquel

    for i in range(1, M-1):         # solo modifica las variables {x_2, ..., x_M-1}
        
        # g es un vector de (pop_size, )
        # theta es una matriz de (pop_size, M-2)
        # por lo tanto, no es posible multiplicar g*theta directamente
        # por eso se usara mul
    
        mul = np.array([ theta[j]*g[j] for j in range(pop_size, ) ])
    
        num = (np.pi * (1+2*mul[:, i]))     # mul[:, i] = x[:, i]*g[i] mas o menos
                                            # num es un array de (pop_size, )
        den = 4 * (1 + g)                   # den es un array de (pop_size, )
        
        theta[:, i] = num/den               # esto es posible porque num y den son del mismo shape
        
    # objective 0
    if m == 0:
        r = (1 + g) * np.cos(theta).prod(axis=by_row)
    
    # objectives [1, ..., M-2]
    if m in range(1, M-1):
        r = (1 + g) * np.cos(theta[:, :M-m-1]).prod(axis=by_row) * np.sin(theta[:, M-m-1])

    # objective M-1
    if m == M-1:
        r = (1 + g) * np.sin(theta[:, 0])
        
    return r
    

