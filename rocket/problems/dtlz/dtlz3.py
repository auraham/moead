# dtlz3.py
import numpy as np
by_row = 1

def dtlz3(X, m, M):
    
    r = None
    
    # separation of X
    n = X.shape[1]
    j = M - 1
    k = n - j
    
    Y = X[:, :-k]
    Z = X[:, -k:]
    
    g = dtlz3_g(Z, k)

    # objective 0
    if m == 0:
        r = (1 + g) * np.cos(Y*np.pi/2).prod(axis=by_row)
    
    # objectives [1, ..., M-2]
    if m in range(1, M-1):
        r = (1 + g) * np.cos(Y[:, :M-m-1]*np.pi/2).prod(axis=by_row) * np.sin(Y[:, M-m-1]*np.pi/2)

    # objective M-1
    if m == M-1:
        r = (1 + g) * np.sin((Y[:, 0] * np.pi) / 2.0)
        
    return r
    
def dtlz3_g(Z, k):
    
    r = 100 * (k+(((Z - 0.5)**2) - np.cos(20*np.pi*(Z - 0.5))).sum(axis=by_row)) 
    
    return r
