# frontend.py
from __future__ import print_function
import numpy as np
from rocket.problems.dtlz.dtlz1 import dtlz1
from rocket.problems.dtlz.dtlz2 import dtlz2
from rocket.problems.dtlz.dtlz3 import dtlz3
from rocket.problems.dtlz.dtlz4 import dtlz4
from rocket.problems.dtlz.dtlz5 import dtlz5

def eval_dtlz_fn_matrix(fn_id, chroms, m_objs):
    
    objs = None
    
    # fn_id check
    if fn_id not in (1, 2, 3, 4, 5):
        print("Error on fn_id value (fn_id: %d)" % fn_id)
        
    fns = {
        1: dtlz1, 
        2: dtlz2, 
        3: dtlz3, 
        4: dtlz4, 
        5: dtlz5, 
        }
    
    pop_size = chroms.shape[0]
    objs = np.zeros((pop_size, m_objs))
    
    for m in range(m_objs):
        objs[:, m] = fns[fn_id](chroms, m, m_objs)

    return objs.copy()
    
    
if __name__ == "__main__":
    
    fn_id           = 4                     # use 1 to 5
    p               = 10                    # change this value to get more points
    m_objs          = 3                     # use 2 or 3
    span            = np.linspace(0, 1, p)
    
    
    if m_objs == 3:
        
        a, b = np.meshgrid(span, span)
        span = np.vstack((a.flatten(), b.flatten())).T
        
    
    pop_size        = span.shape[0]
    n_genes         = 30
    chroms          = np.ones((pop_size, n_genes)) * 0.5

    if m_objs == 2:
        chroms[:, 0] = span

    else:
        chroms[:, 0] = span[:, 0]
        chroms[:, 1] = span[:, 1]

    objs = eval_dtlz_fn_matrix(fn_id, chroms, m_objs)
    
    # plot
    title = "DTLZ"+str(fn_id)
    import matplotlib.pyplot as plt
    from mpl_toolkits.mplot3d import Axes3D
    
    fig = plt.figure(title)
    ax = fig.add_subplot(111, projection="3d") if m_objs == 3 else fig.add_subplot(111)
    ax.set_title(title)
    ax.plot(objs[:, 0], objs[:, 1], objs[:, 2], "bo") if m_objs == 3 else ax.plot(objs[:, 0], objs[:, 1], "bo")
    plt.show()
    
    
