# wfg.py
from __future__ import print_function
import numpy as np
import numpy.ctypeslib as npct
from ctypes import c_int

import os
script_path = os.path.dirname(os.path.abspath(__file__))
libwfg_path = os.path.join(script_path, "WFG_v2006.03.28")

# a double array with single dimension that is contiguous
array_1d_double = npct.ndpointer(
                        dtype=np.double,
                        ndim=1,
                        flags='CONTIGUOUS')
                        
# load the library, using numpy mechanisms
# use abs path to libwfg!
libcd = npct.load_library("libwfg", libwfg_path)

# setup the return and argument types
# ex: void wfg1(double *chrom, double * objs, int n_genes, int m_objs, int k);
#libcd.wfg.restype = None
#libcd.wfg.argtypes = [
#                array_1d_double,
#                array_1d_double,
#                c_int,
#                c_int,
#                c_int
#                    ]

# ex: void eval_wfg_fn (int fn_id, double *chrom, double *objs, int n_genes, int m_objs, int k);
libcd.eval_wfg_fn.restype = None
libcd.eval_wfg_fn.argtypes = [
                c_int,
                array_1d_double,
                array_1d_double,
                c_int,
                c_int,
                c_int]


def eval_wfg_fn_vector(fn_id, chrom, m_objs, k):
    """
    Evaluate a WFG function according to fn_id
    
    Input
    fn_id       WFG ID (1, ..., 9)
    chrom       (n_genes, ) array
    m_objs      Number of objectives
    k           WFG parameter
    
    Output
    objs        (m_objs, ) array
    
    Note: chrom and objs should be initialized before calling eval_wfg_fn
    """
    
    # resource allocation
    objs = np.zeros((m_objs, ))
    
    # fn_id check
    if fn_id < 1 or fn_id > 9:
        print("error on fn_id value (fn_id: %d)" % fn_id)
        return objs.copy()
        
    # casting
    chrom_array = np.ascontiguousarray(chrom)
    objs_array  = np.ascontiguousarray(objs)
    
    # compute wfg function
    libcd.eval_wfg_fn(fn_id, chrom_array, objs_array, chrom_array.shape[0], objs_array.shape[0], k)
    
    return objs_array.copy()
    
    
def eval_wfg_fn_matrix(fn_id, chroms, m_objs, k):
    """
    Evaluate a WFG function according to fn_id
    
    Input
    fn_id       WFG ID (1, ..., 9)
    chrom       (pop_size, n_genes) matrix
    m_objs      Number of objectives
    k           WFG parameter
    
    Output
    objs        (pop_size, m_objs) matrix
    """
    
    pop_size = chroms.shape[0]
    objs = np.zeros((pop_size, m_objs))
    
    for i, chrom in enumerate(chroms):
        objs[i, :] = eval_wfg_fn_vector(fn_id, chrom.copy(), m_objs, k).copy()
    
    return objs.copy()
    
