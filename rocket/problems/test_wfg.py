# test_wfg.py
import numpy as np
from wfg import eval_wfg_fn_vector, eval_wfg_fn_matrix
from rocket.helpers import get_n_genes, get_bounds

if __name__ == "__main__":
    
    chrom = np.array([
            0.00033101403395380404,
            2.5001454534358956e-20,
            2.9443601025398259e-05,
            0.00010356313678985132,
            3.5,
            4.1999999999999993,
            4.8999999999999995,
            5.9999999999999996
            ])
    m_objs = 3
    k = 4
    
    # expected output
    # wfg1 (authors) 0.863964 0.471846 2.95507
    z = eval_wfg_fn_vector(1, chrom, m_objs, k)

    
    # evaluate a chrom matrix
    chroms = np.array([
            chrom.copy(),
            chrom.copy(),
            chrom.copy(),
            chrom.copy(),
            ])
            
    # expected output
    # wfg1 (authors) 0.863964 0.471846 2.95507
    z = eval_wfg_fn_matrix(1, chroms, m_objs, k)
    
    
    # final test
    problems = {
        1: "wfg1", 
        2: "wfg2", 
        3: "wfg3", 
    }

    fnc             = 1
    problem_name    = problems[fnc]
    pop_size        = 80
    m_objs          = 3
    n_genes, c, d   = get_n_genes(problem_name, m_objs)
    lb, ub          = get_bounds(problem_name, n_genes)
    
    chroms = lb + np.random.random((pop_size, n_genes)) * (ub-lb)
    z      = eval_wfg_fn_matrix(fnc, chroms, m_objs, k)
