# test_evaluate_mop_dtlz.py
from __future__ import print_function
from rocket.problems.evaluate_mop import evaluate_mop
from rocket.problems.dtlz.dtlz1 import dtlz1
import numpy as np
import matplotlib.pyplot as plt

def create_sample_chroms():
    
    p = 5           # change this value to get more points
    m_objs = 5      # dont change
    x = np.linspace(0, 1, p)
    a, b, c, d, e = np.meshgrid(x, x, x, x, x)
    span = np.vstack((
                a.flatten(),
                b.flatten(),
                c.flatten(),
                d.flatten(),
                e.flatten(),
                )).T
    pop_size = span.shape[0]
    n_genes  = 30
    chroms   = np.ones((pop_size, n_genes)) * 0.5

    for i in range(m_objs-1):
        chroms[:, i] = span[:, i]
        
    return chroms.copy()
    
    
# our proposal
def test_evaluate_problem(chroms, m_objs):
    
    objs = evaluate_mop(chroms, m_objs, {"name":"dtlz1"})    
    return objs
    
# our competitor
def problem(chroms, m_objs):
    
    pop_size = chroms.shape[0]
    objs = np.zeros((pop_size, m_objs))
    
    for m in range(m_objs):
        objs[:, m] = dtlz1(chroms, m, m_objs)
        
    return objs
    
if __name__ == "__main__":
    
    chroms = create_sample_chroms()
    objs = test_evaluate_problem(chroms, 5)

