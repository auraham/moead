# About this package

This package contains our proposed benchmark `"dtlz-spin"`, a modification of the original `"dtlz"` testbed. 

The script `geo_meaning.py` generates these figures. From these, we can get a better insight of the proposed rotation method and how the original front (gray color) is rotated (pink color) and scaled (red color). The source code of the rotation method is on `rocket.helpers.rotation`, function `spin`.





![dtlz1-spin](dtlz1-spin.png)

![dtlz2-spin](dtlz2-spin.png)

![dtlz3-spin](dtlz3-spin.png)

![dtlz4-spin](dtlz4-spin.png)