# geo_meaning.py
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from rocket.fronts import get_real_front
from numpy.linalg import norm

def rotate(v, k, theta):

    # v parallel
    v_par = np.dot(v, k) * k

    # v perpendicular
    v_per = v - v_par

    # w (orthogonal vector)
    w = np.cross(k, v_per)

    # vp perpendicular
    vp_per = v_per * np.cos(theta) + w * np.sin(theta)

    # final vector
    vp = v_par + vp_per

    return vp.copy()
      
def draw_line(v, ax):
      
    vf = v.flatten()

    x = np.array([0, vf[0]])
    y = np.array([0, vf[1]])
    z = np.array([0, vf[2]])

    ax.plot(x, y, z, c="#505050")

def plot_front(real_front, p, ax, color):
      
    # plot meshgrid
    X = real_front[:, 0].reshape(p, p)
    Y = real_front[:, 1].reshape(p, p)
    Z = real_front[:, 2].reshape(p, p)

    l = ax.plot_wireframe(X, Y, Z, rstride=2, cstride=2, color=color)

    plt.show()

def slow_rotation(matrix, k, theta):

    rows, cols = matrix.shape
    rmatrix = np.zeros((rows, cols))
    
    for r in range(rows):
        
        rmatrix[r] = rotate(matrix[r], k, theta)
        
    return rmatrix.copy()
    
    
    
def fast_rotation(V, k, theta):

    per_row = 1
    
    
    # V_par
    rows, cols = V.shape
    K = k.reshape((1, cols)).repeat(rows, 0)            # (rows, cols), each row is a copy of k

    dot = (V*K).sum(axis=per_row)                       # (rows, ), each entry is equals to dot(V[i], k), for i in range(rows)
    
    V_par = dot.reshape((rows, 1)) * K                  # (rows, cols), each row represents dot[i] * K[i], for i in range(rows)
    
    
    # V_per
    V_per = V - V_par                                   # (rows, cols), each row represents V[i] - V_par[i], for i in range(rows)
    
    
    # W (orthogonal vector)
    W = np.cross(K, V_per)                              # (rows, cols)
    
    # Vp perpendicular
    Vp_per = V_per * np.cos(theta) + W * np.sin(theta)
    
    # final vector
    Vp = V_par + Vp_per
    
    return Vp.copy()
    
    
def scale_naive(front):
    
    return (front*0.5)+0.091

def scale(front):
    
    x = front[:, 0]
    y = front[:, 1]
    z = front[:, 2]

    xp = (4*x +   y +   z)/6
    yp = (  x + 4*y +   z)/6
    zp = (  x +   y + 4*z)/6

    pfront = np.zeros(front.shape)
    
    pfront[:, 0] = xp
    pfront[:, 1] = yp
    pfront[:, 2] = zp
    
    return pfront

    
if __name__ == "__main__":
      
    fig = plt.figure("rotation")
    ax = fig.add_subplot(111, projection="3d")
    ax.cla()

    #ax.set_xlim(0, 1)
    #ax.set_ylim(0, 1)
    #ax.set_zlim(0, 1)
    ax.set_xlabel("$x$")
    ax.set_ylabel("$y$")
    ax.set_zlabel("$z$")

    # get original front
    mop_name = "dtlz4"                          # change this line for "dtlz[1,2,3,4]"
    front, p = get_real_front(mop_name, 3)

    # rotate it!
    k = np.array([1,1,1])
    k = k / norm(k)
    theta = np.pi

    outer_front = fast_rotation(front, k, theta)
    inner_front = scale(outer_front)

    # plot fronts
    plot_front(front, p, ax, "#B0B8AC")           # original front
    plot_front(outer_front, p, ax, "#FFC1C1")     # rotated front (fast)
    plot_front(inner_front, p, ax, "#FA4B4B")     # rotated front (fast)
    draw_line(k, ax)                              # rotation axis

    

    title = "%s-spin" % mop_name.upper()
    figname = "%s-spin.png" % mop_name
    
    ax.set_title(title)
    ax.view_init(30, 45)
    plt.savefig(figname)
    plt.show()
    
    
    
    

