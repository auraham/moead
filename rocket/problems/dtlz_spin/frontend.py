# frontend.py
from __future__ import print_function
import numpy as np
from rocket.helpers import spin
from rocket.problems.dtlz.frontend import eval_dtlz_fn_matrix

def eval_dtlz_spin_fn_matrix(fn_id, chroms, m_objs, degrees):
    
    # original problem
    objs = eval_dtlz_fn_matrix(fn_id, chroms, m_objs)
    
    # rotated objs
    rotated_objs = spin(objs, degrees)

    return rotated_objs.copy()

if __name__ == "__main__":
    
    fn_id   = 1                     # dont change
    p       = 10                    # change this value to get more points
    m_objs  = 3                     # use 2 or 3
    span    = np.linspace(0, 1, p)
    degrees = 20
    
    
    if m_objs == 3:
        
        a, b = np.meshgrid(span, span)
        span = np.vstack((a.flatten(), b.flatten())).T
        
    
    pop_size        = span.shape[0]
    n_genes         = 30
    chroms          = np.ones((pop_size, n_genes)) * 0.5

    if m_objs == 2:
        chroms[:, 0] = span

    else:
        chroms[:, 0] = span[:, 0]
        chroms[:, 1] = span[:, 1]

    objs = eval_dtlz_spin_fn_matrix(fn_id, chroms, m_objs, degrees)
    
    # plot
    title = "dltz%d-spin-%d" % (fn_id, degrees)
    import matplotlib.pyplot as plt
    from mpl_toolkits.mplot3d import Axes3D
    
    fig = plt.figure(title)
    ax = fig.add_subplot(111, projection="3d") if m_objs == 3 else fig.add_subplot(111)
    ax.set_title(title)
    ax.plot(objs[:, 0], objs[:, 1], objs[:, 2], "bo") if m_objs == 3 else ax.plot(objs[:, 0], objs[:, 1], "bo")
    plt.show()
    
    
