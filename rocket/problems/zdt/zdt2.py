# zdt2.py
import numpy as np

def ZDT2(X):
    
    pop_size = X.shape[0]
    m_objs = 2

    objs = np.zeros((pop_size, m_objs))
    objs[:, 0] = zdt2_f1(X)
    objs[:, 1] = zdt2_f2(X)

    return objs.copy()
    
def zdt2_f1(X):
    return X[:, 0]
    
def zdt2_f2(X):
    
    n = X.shape[1]
    k = n - 1
    
    Z = X[:, -k:] 
    
    f1 = zdt2_f1(X)
    g  = zdt2_g(Z)
    h  = zdt2_h(f1, g)
    
    result = g * h
    
    return result
    
def zdt2_g(Z):
    
    k = Z.shape[1]
    
    result = 1.0 + 9.0*(Z/k).sum(axis=1) 
    
    return result
    

def zdt2_h(f1, g):
        
    result = 1.0 - (f1/g)**2
    
    return result
    
