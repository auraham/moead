# zdt1.py
import numpy as np

def ZDT1(X):
    
    pop_size = X.shape[0]
    m_objs = 2

    objs = np.zeros((pop_size, m_objs))
    objs[:, 0] = zdt1_f1(X)
    objs[:, 1] = zdt1_f2(X)

    return objs.copy()
    
def zdt1_f1(X):
    return X[:, 0]
    
def zdt1_f2(X):
    
    n = X.shape[1]
    k = n - 1
    
    Z = X[:, -k:]
    
    f1 = zdt1_f1(X)
    g  = zdt1_g(Z)
    h  = zdt1_h(f1, g)
    
    result = g * h

    return result
    
def zdt1_g(Z):
    
    k = Z.shape[1]
    
    result = 1.0 + 9.0 * (Z/k).sum(axis=1)
    
    if any(result < 0):
        import ipdb; ipdb.set_trace()
        
    return result
    

def zdt1_h(f1, g):
    
    if any((f1/g) < 0):
        import ipdb; ipdb.set_trace()
        
    result = 1.0 - np.sqrt(f1/g)
    
    return result
