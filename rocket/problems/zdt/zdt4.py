# zdt4.py
import numpy as np

def ZDT4(X):
    
    pop_size = X.shape[0]
    m_objs = 2

    objs = np.zeros((pop_size, m_objs))
    objs[:, 0] = zdt4_f1(X)
    objs[:, 1] = zdt4_f2(X)

    return objs.copy()
    
def zdt4_f1(X):
    return X[:, 0]
    
def zdt4_f2(X):
    
    n = X.shape[1]
    k = n - 1
    
    Z = X[:, -k:]
    
    f1 = zdt4_f1(X)
    g  = zdt4_g(Z)
    h  = zdt4_h(f1, g)
    
    result = g * h
    
    return result

    
def zdt4_g(Z):
    
    k = Z.shape[1]
    
    result = 1 + 10*k + ((Z**2) - (10*np.cos(4*np.pi*Z))).sum(axis=1)

    return result

def zdt4_h(f1, g):
    
    result = 1 - np.sqrt(f1/g)
    
    return result
