# zdt3.py
import numpy as np

def ZDT3(X):
    
    pop_size = X.shape[0]
    m_objs = 2

    objs = np.zeros((pop_size, m_objs))
    objs[:, 0] = zdt3_f1(X)
    objs[:, 1] = zdt3_f2(X)

    return objs.copy()
    
def zdt3_f1(X):
    return X[:, 0]
    
def zdt3_f2(X):
    
    n = X.shape[1]
    k = n - 1
    
    Z = X[:, -k:]
    
    f1 = zdt3_f1(X)
    g  = zdt3_g(Z)
    h  = zdt3_h(f1, g)
    
    result = g * h
    
    return result
    
def zdt3_g(Z):
    
    k = Z.shape[1]
    
    result = 1.0 + 9.0*(Z/k).sum(axis=1)
    
    return result

def zdt3_h(f1, g):
        
    result = 1.0 - np.sqrt(f1/g) - (f1/g)*np.sin(10.0*np.pi*f1)
    
    return result
