# zdt6.py
import numpy as np

def ZDT6(X):
    
    pop_size = X.shape[0]
    m_objs = 2

    objs = np.zeros((pop_size, m_objs))
    objs[:, 0] = zdt6_f1(X)
    objs[:, 1] = zdt6_f2(X)

    return objs.copy()
    
def zdt6_f1(X):
    return 1.0 - np.exp(-4.0*X[:, 0]) * np.sin(6.0*np.pi*X[:, 0])**6
    
def zdt6_f2(X):
    
    n = X.shape[1]
    k = n - 1
    
    Z = X[:, -k:]
    
    f1 = zdt6_f1(X)
    g  = zdt6_g(Z)
    h  = zdt6_h(f1, g)
    
    result = g * h
    
    return result

    
def zdt6_g(Z):
    
    k = Z.shape[1]
    
    result = 1.0 + 9.0*(Z/k).sum(axis=1)**0.25

    return result

def zdt6_h(f1, g):
    
    result = 1.0 - (f1/g)**2
    
    return result
