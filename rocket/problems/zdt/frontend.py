# frontend.py
from __future__ import print_function
import numpy as np
from rocket.problems.zdt.zdt1 import ZDT1
from rocket.problems.zdt.zdt2 import ZDT2
from rocket.problems.zdt.zdt3 import ZDT3
from rocket.problems.zdt.zdt4 import ZDT4
from rocket.problems.zdt.zdt6 import ZDT6

def eval_zdt_fn_matrix(fn_id, chroms):
    
    objs = None
    
    # fn_id check
    if fn_id not in (1, 2, 3, 4, 6):
        print("Error on fn_id value (fn_id: %d)" % fn_id)
        
    if fn_id == 1:
        objs = ZDT1(chroms)
    
    if fn_id == 2:
        objs = ZDT2(chroms)
    
    if fn_id == 3:
        objs = ZDT3(chroms)
    
    if fn_id == 4:
        objs = ZDT4(chroms)
    
    if fn_id == 6:
        objs = ZDT6(chroms)
    

    return objs.copy()
    
    
if __name__ == "__main__":
    
    fn_id           = 6
    pop_size        = 100
    n_genes         = 30
    chroms          = np.zeros((pop_size, n_genes))
    chroms[:, 0]    = np.linspace(0, 1, pop_size)
    objs            = eval_zdt_fn_matrix(fn_id, chroms)
    
    # plot
    title = "ZDT"+str(fn_id)
    import matplotlib.pyplot as plt
    fig = plt.figure(title)
    ax = fig.add_subplot(111)
    ax.set_title(title)
    ax.plot(objs[:, 0], objs[:, 1], "bo")
    plt.show()
    
    
