# test_evaluate_mop_zdt.py
from __future__ import print_function
from rocket.problems.evaluate_mop import evaluate_mop
from rocket.problems.zdt.zdt1 import ZDT1
import numpy as np
import matplotlib.pyplot as plt

# sample chroms
pop_size            = 500
n_genes             = 30
sample_chroms       = np.zeros((pop_size, n_genes))
sample_chroms[:, 0] = np.linspace(0, 1, pop_size)


# our proposal
def test_evaluate_problem(chroms):
    
    fn_id           = 1
    m_objs          = 2
    problem_name    = "zdt1"
    objs            = evaluate_mop(chroms, m_objs, {"name":problem_name})    
    return objs
    
# our competitor
problem = lambda chroms : ZDT1(chroms)

if __name__ == "__main__":
    
    objs = test_evaluate_problem(sample_chroms)
    
    fig = plt.figure("test_evaluate_mop_zdt")
    ax = fig.add_subplot(111)
    ax.plot(objs[:, 0], objs[:, 1], "bo")
    plt.show()
