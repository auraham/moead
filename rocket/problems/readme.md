# About this package

- This package contains some benchmark problems for testing the performance of MOEAs.
- Currently, the following problems are available:
  - `ZDT`(1-4,6) [Zitzler00]
  - `DTLZ` (1-4) [Deb09]
  - `DTLZ1-spin` 
  - `WFG` (1-9) [Huband09]
  - `T` (1,2) [Zhou16]
  - `inv-DTLZ` [Jain12]

# How to use this package
You can evaluate any of the available problems using the `evaluate_mop` function like this:
```python
from rocket.problems import evaluate_mop

# zdt
mop_params = {"name":"zdt1"}

# dtlz
mop_params = {"name":"dtlz1"}

# dtlz redundant (not available yet)
mop_params = {"name":"dtlz1_redundant", "M_objs":3, "M_list":(0,2,3)}

# wfg
mop_params = {"name":"wfg1", "k":10}

objs = evaluate_mop(chroms, m_objs, mop_params)
```
Before calling `evaluate_mop`, you will need three parameters:
- `chroms`: An input matrix of size `(pop_size, n_genes)`, where `pop_size` is the number of design vectors (rows) and `n_genes` is the number of decision variables.
- `m_objs`: The number of objectives. This parameter is fixed for `ZDT` (`m_objs=2`). Although, this parameter is needed for `DTLZ` and `WFG`.
- `problem_params`: A dictionary with some attributes dependent of the problem. The attribute `name` is needed for all the problems.

The variable `objs` is a matrix of size `(pop_size, m_objs)`. To ease computation, `evaluate_mop` assumes a matrix as input (`chroms`) and returns a matrix as output (`objs`). If you need to evaluate a single decision vector, its shape should be `(1, n_genes)`, not `(n_genes, )`.

# How to add more problems
Assume that you need to add three similar problems called `mop1`, `mop2`, and `mop3`. Also, these three problems require an integer parameter of *dificulty*, called `t`, in the range [0, 1]. You will need to add the following snippets:
- Create a new module for each problem. In this example, we need three modules, `mop1.py`, `mop2.py`, and `mop3.py`, all of them inside `rocket/problems/mop`. Each module will look like this:
```python
# rocket/problems/mop/mop1.py

def MOP1(chroms, m_objs, t):

    # implement your problem here...
    
    # save the result in objs, a (pop_size, m_objs) matrix
    
    # return the result
    return objs.copy()
```
- Create a new module `frontend.py` with a single function called `eval_mop_fn_matrix`. We follow this naming convention `eval_MOPNAME_fn_matrix`. This function acts as a frontend for the previous functions `MOP1`, `MOP2`, and `MOP3`.
```python
# rocket/problems/mop/frontend.py

def eval_mop_fn_matrix(fn_id, chroms, m_objs, t):
    
    objs = None
    
    # check t values, if needed
    if t < 0 or t > 1:
        print("error on t value")
        return None
        
    # check fn_id
    # we only have three possible values of fn_id in this example, so
    if not fn_id in (1, 2, 3):
        print("error on fn_id value")
        return None
        
    if fn_id == 1:
        objs = MOP1(chroms, m_objs, t)
        
    if fn_id == 2:
        objs = MOP2(chroms, m_objs, t)
    
    if fn_id == 3:
        objs = MOP3(chroms, m_objs, t)
        
    return objs.copy()
```
- Finally, add an entry into `evaluate_mop.py` like this:
```python
# rocket/problems/evaluate_mop.py

def evaluate_mop(chroms, m_objs, mop_params):
    
    # ...
    
    elif problem_name in ("mop1", "mop2", "mop3"):
        
        t     = problem_params["t"]    # difficulty parameter
        fn_id = int(problem_name[-1])
        objs  = eval_mop_fn_matrix(fn_id, chroms, m_objs, t)
```
After these changes, you will be able to test your problems as follows:
```python
from rocket.problems imort evaluate_mop

# mop
mop_params = {"name":"mop1", "t":0.5}

objs = evaluate_mop(chroms, m_objs, mop_params)
```
**Additional files** To ease experimentation, add support for new problems in these files inside `rocket/helpers` directory:

- `helpers.py`: update `get_n_genes()`, and `get_bounds()`. These functions are used for `moea_base.py`.
- `get_pareto_solutions_[mop_name].py`(*optional*): This script contains a single function called `get_pareto_solutions_[mop_name]()`. Such a function returns a set of Pareto optimal solutions for `[mop_name]`. This function is needed for generating the Pareto front of `[mop_name]`. This function can be employed for plotting purposes.

Also, update these files inside `rocket/fronts`:

- `create_fronts_[mop_name].py` (*optional*). This script creates the Pareto fronts of `[mop_name]`, if they are known. This is a *run-only-once* script. The resulting fronts are stored in `rocket/fronts/files` as `npz` files. These fronts are needed for `get_real_front.py`. If the fronts are not available, `get_real_front()` will return `None`.
- `get_real_front.py` (*optional*): update `get_real_front` function to return the Pareto front of `[mop_name]`. If `[mop_name]` does not requires additional parameters (such as rotation angle for `mp` problems), then `get_real_front()` does need any modification. Otherwise, and additional handler is required, something like `get_real_front_[mop_name](mop_name, m_objs, params)`.



# How slow is `evaluate_mop`?

The aim of using `evaluate_mop.py` is to unify the evaluation of test problems using a single function (right!, `evaluate_mop`). Although, you may be wondering how slow is this approach compared to, for instance, making a call to a function. In other words, what is the overhead associated to `evaluate_mop.py`. To answer this question, `test_evaluate_mop_zdt.py` was developed. Run the following commands from`ipython` inside `rocket/problems`:
```python
%run test_evaluate_mop_zdt.py                # a figure will appear, just close it

%timeit test_evaluate_problem(sample_chroms) # (1) our approach
10000 loops, best of 3: 81.3 µs per loop

%timeit problem(sample_chroms)               # (2) a direct call
10000 loops, best of 3: 77.5 µs per loop
```
If you are curious, these are the functions we just evaluated:
```python

```
So, what happened? We evaluated two function calls: (1) our approach and (2) a direct call to a test problem (`zdt1`). The advantage of (1) is that it can be used as a single entry point to evaluate other test problems. The disadvantage is that it needs to do other tasks before evaluating a matrix of solutions (i.e., get the problem name, check if the problem is implemented, etc.). In other words, we introduced a little overhead to keep things as simple as possible. The advantage of (2) is that additional tasks are not needed (no overhead). However, it could become a mess when the number of test problems grows. To avoid it, we suggest to use `evaluate_mop` instead.

**Bottom line** The time required for (1) and (2) is pretty similar, so the overhead is not so much. Run `test_evaluate_mop_dtlz.py` as discussed above to test DTLZ problems with `m_objs=5`.
```python
%run test_evaluate_mop_dtlz.py

%timeit test_evaluate_problem(chroms, 5)
100 loops, best of 3: 6.61 ms per loop

%timeit problem(chroms, 5)
100 loops, best of 3: 6.79 ms per loop
```
# References
- [Zitzler00] *Comparison of Multiobjective Evolutionary Algorithms: Empirical Results*
- [Deb05] *Scalable Test Problems for Evolutionary Multiobjective Optimization*
- [Huband06] *A Review of Multiobjective Test Problems and a Scalable Test Problem Toolkit*
- [Liu14] *Decomposition of a Multiobjective Optimization Problem Into a Number of Simple Multiobjective Subproblems*
- [Zhou16] *Are All the Subproblems Equally Important? Resource Allocation in Decomposition-Based Multiobjective Evolutionary Algorithms*
- [Jain12] *An Improved Adaptive Approach for Elitist Nondominated Sorting Genetic Algorithm for Many-Objective Optimization (A$^{2}$-NSGA-III)*