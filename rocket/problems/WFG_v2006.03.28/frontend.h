// frontend.h
#ifndef WFG_FRONTEND
#define WFG_FRONTEND

extern "C" void eval_wfg_fn (int fn_id, double *chrom, double *objs, int n_genes, int m_objs, int k);

#endif
