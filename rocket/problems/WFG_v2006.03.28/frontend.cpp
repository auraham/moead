/*
| frontend.cpp
| 
| Use this file as a frontend for your MOEAs in python.
| 
| Instead of calling this C++ function
| 
|   vector< double > Problems::WFG1 (const vector< double >& z, const int k, const int M)
| 
| use this C function
|   void eval_wfg_fn (int fn_id, double *chrom, double *objs, int n_genes, int m_objs, int k)
| 
| the return is void, but the objective vector is save in objs.
| We assume that both chrom and objs have been allocated first! (ie they are not null pointers)
| 
| Input
| fn_id     wfg function id [1-9]
| chrom     (n_genes, ) array
| objs      (m_objs, ) array
| n_genes   number of decision variables
| m_objs    number of objectives
| k         wfg parameter
| 
| output
| void      the result is saved in objs
| 
* */

#include <iostream>
#include <cstdlib>
#include <sstream>
#include <vector>
#include <cassert>
#include <cmath>
#include <stdio.h>
#include "frontend.h"
#include "Toolkit/ExampleProblems.h"
#include "Toolkit/TransFunctions.h"


using namespace WFG::Toolkit;
using namespace WFG::Toolkit::Examples;
using std::vector;
using std::string;


vector <double> array2vector (double *x, int size);

vector <double> array2vector (double *x, int size) {
    
    vector <double> y(size);
    
    int i;
    for (i=0; i<size; i++)
        y[i] = x[i];
        
    return y;
}

void eval_wfg_fn (int fn_id, double *chrom, double *objs, int n_genes, int m_objs, int k) {
    
    // fn_id check
    if (fn_id < 1 || fn_id > 9) {
        printf ("error, fn_id should be [1-9] (fn_id: %d)!\n", fn_id);
        return;
    }
    
    // objs check
    if (objs == NULL || chrom == NULL) {
        printf ("error, objs should be allocated before calling wfg functions!\n");
    }
    
    // cast: double * to vector <double>
    vector<double> x = array2vector (chrom, n_genes);
    
    // result
    vector<double> y;
    
    // compute
    switch (fn_id) {
        
        case 1:
            y = Problems::WFG1 (x, k, m_objs); break;
        case 2:
            y = Problems::WFG2 (x, k, m_objs); break;
        case 3:
            y = Problems::WFG3 (x, k, m_objs); break;
        case 4:
            y = Problems::WFG4 (x, k, m_objs); break;
        case 5:
            y = Problems::WFG5 (x, k, m_objs); break;
        case 6:
            y = Problems::WFG6 (x, k, m_objs); break;
        case 7:
            y = Problems::WFG7 (x, k, m_objs); break;
        case 8:
            y = Problems::WFG8 (x, k, m_objs); break;
        case 9:
            y = Problems::WFG9 (x, k, m_objs); break;
        
    }
    
    // save value
    int i;
    for (i=0; i<m_objs; i++)
        objs[i] = y[i];
    
}
