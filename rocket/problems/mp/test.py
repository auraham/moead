# test.py
from __future__ import print_function
import numpy as np

import os, sys, argparse

# add lab's path to sys.path
lab_path = os.path.abspath("../../../")
sys.path.insert(0, lab_path)

from rocket.problems.mp import eval_mp_fn_matrix
from rocket.plot import plot_pops

# get_pareto_solutions_dtlz.py
from rocket.helpers.mesh import mesh_2, mesh_3, mesh_4, mesh_7, mesh_9, mesh_14
from rocket.helpers import get_pareto_solutions_dtlz

def get_pareto_solutions_imbs(n_genes, m_objs, p):
    """
    Returns pareto optimal solutions for imbs for m_objs in (2,3,5,8,10) objectives.
    @todo: add support for more objectives
    
    Input
    n_genes     int, number of decision variables
    m_objs      int, number of objectives
    p           int, spacing
    
    Output
    chroms      (rows, n_genes) matrix of pareto optimal solutions
    """
    
    matrix = None
    
    if m_objs == 2:
        span = np.linspace(0, 1, p)
        matrix = span.reshape((span.shape[0], 1))
        
    elif m_objs == 3:
        matrix = mesh_2(p)
    
    elif m_objs == 4:
        matrix = mesh_3(p)
    
    elif m_objs == 5:
        matrix = mesh_4(p)
        
    elif m_objs == 8:
        matrix = mesh_7(p)
        
    elif m_objs == 10:
        matrix = mesh_9(p)
        
    elif m_objs == 15:
        matrix = mesh_14(p)
        
    else:
        print("m_objs: %s is not defined for get_pareto_solutions_dtlz" % m_objs)       # @todo: add log support here
        return None
    
    rows, cols = matrix.shape
        
    # optimal value for distance parameters
    #opt_x = 0.5
    
    chroms = np.ones((rows, n_genes))
    
    
    # position (diversity) variables
    for col in range(cols):                     # rewrite the first cols of chroms
        chroms[:, col] = matrix[:, col]         # with the content of matrix
    
    
    # optimal value for convergence variables
    j = cols                                    # j position variables
    per_row = 1
    opt_x = chroms[:, :j].sum(axis=per_row) * 0.5   # (pop_size, ), suma de las primeras j variables 
    
    # distance (convergence) variables
    for k in range(j, n_genes):
        chroms[:, k] = opt_x.copy()                 # the lask variables of each decision vector contains the same fixed value (opt_x)
        
    return chroms.copy()
    
if __name__ == "__main__":
    
    fn_id       = 8     # use 1-8 
    n_genes     = 10    #
    m_objs      = 3     # use 3 for plotting
    p           = 10 if m_objs <= 3 else 4
    
    chroms = get_pareto_solutions_dtlz(n_genes, m_objs, p)      # they are the same as mp's optimal solutions
    objs   = eval_mp_fn_matrix(fn_id, chroms, m_objs)
    m_objs = objs.shape[1]
    
    # plot
    mop_name = "MP"+str(fn_id)
    label = mop_name
    
    pops = (objs, )
    labels = (label, )
    plot_pops(pops, labels, title=mop_name, lower_lims=(0,0,0), upper_lims=(1,1,1))


    # check hyperplane condition
    sum_per_rows = objs.sum(axis=1)
    total_sum = sum_per_rows.sum()
    print("sum per rows: ", sum_per_rows)   # [1, 1, ..., 1]
    print("total sum: %d, expected: %d" %  (total_sum, len(sum_per_rows)))   # pop_size
    
    
    
