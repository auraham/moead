# common.py
import numpy as np
by_row = 1

def mp_g(Z, k):
    """
    multimodal function g (taken from dtlz1)
    """
    r = 100 * (k+(((Z - 0.5)**2) - np.cos(20*np.pi*(Z-0.5))).sum(axis=by_row))
    return r.copy()


def mp_h(X, Z, j, k):
    """
    imbalance function h (taken from imb4)
    """
    
    pop_size, n_genes = X.shape
    
    X1       = X[:, 0]                  # first variable
    Xn       = X[:, j:]                 # j+1 to last variables
    h        = np.zeros((pop_size, ))   # return value
    
    # evaluate condition
    case_a = (2./3 <= X1) & (X1 <= 1.0)     # imbalance
    case_b = ~case_a
    
    # multimodality
    g = mp_g(Z, k)
    
    # save
    h[case_a]   = 0
    h[case_b]   = g[case_b]
    
    return h.copy()
