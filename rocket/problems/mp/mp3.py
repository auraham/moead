# mp3.py
import numpy as np
from rocket.problems.mp.mp1 import mp1
from rocket.problems.mp.common import mp_g
by_row = 1

def mp3(X, m, M):
    
    # separation of X
    n = X.shape[1]
    j = M - 1
    k = n - j
    
    Y = X[:, :-k]
    Z = X[:, -k:]
    
    # evaluate m-th objective
    f = mp1(X, m, M)                    # (pop_size, ) vector with m-th objective values
    
    # evaluate function g
    g = mp_g(Z, k)                      # (pop_size, )
    
    # transform m-th objective
    new_f = (0.5*(1+g)) - f             # (pop_size, )
    
    return new_f.copy()
