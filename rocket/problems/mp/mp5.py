# mp5.py: based on dtlz3.py
import numpy as np
from rocket.problems.mp.common import mp_g
by_row = 1

def mp5(X, m, M):
    
    r = None
    
    # separation of X
    n = X.shape[1]
    j = M - 1
    k = n - j
    
    Y = X[:, :-k]
    Z = X[:, -k:]
    
    # multimodality
    g = mp_g(Z, k)

    # objective 0
    if m == 0:
        r = (1 + g) * np.cos(Y*np.pi/2).prod(axis=by_row)
    
    # objectives [1, ..., M-2]
    if m in range(1, M-1):
        r = (1 + g) * np.cos(Y[:, :M-m-1]*np.pi/2).prod(axis=by_row) * np.sin(Y[:, M-m-1]*np.pi/2)

    # objective M-1
    if m == M-1:
        r = (1 + g) * np.sin((Y[:, 0] * np.pi) / 2.0)
        
    return r.copy()
