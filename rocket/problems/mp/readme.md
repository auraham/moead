# About this package

This package contains our new benchmark based on:

- DTLZ [Deb05]
- IMB [Liu17]
- inv-DTLZ [Jain13]


The proposed benchmark, called MP (Multiobjective Problem), is detailed below.


| Problem | Base            | Properties                            | Geometry |
| ------- | --------------- | ------------------------------------- | -------- |
| MP1     | DTLZ1           | Multimodal                            | Linear   |
| MP2     | DTLZ1, IMB4     | Multimodal, Imbalance                 | Linear   |
| MP3     | inv-DTLZ1       | Multimodal, Inverted shape            | Linear   |
| MP4     | IMB4, inv-DTLZ1 | Multimodal, Imbalance, Inverted shape | Linear   |

| Problem | Base               | Properties                            | Geometry |
| ------- | ------------------ | ------------------------------------- | -------- |
| MP5     | DTLZ3              | Multimodal                            | Concave  |
| MP6     | DTLZ1, DTLZ3, IMB4 | Multimodal, Imbalance                 | Concave  |
| MP7     | inv-DTLZ2          | Multimodal, Inverted shape            | Concave  |
| MP8     | IMB4, inv-DTLZ2    | Multimodal, Imbalance, Inverted shape | Concave  |



**Update** this table summarizes the new benchmark:

| Name | Multimodality      | Imbalance          | Irregular shape    | Linear             | Concave            | Base                       |
| ---- | ------------------ | ------------------ | ------------------ | ------------------ | ------------------ | -------------------------- |
| MP1  | :heavy_check_mark: |                    |                    | :heavy_check_mark: |                    | DTLZ1                      |
| MP2  | :heavy_check_mark: | :heavy_check_mark: |                    | :heavy_check_mark: |                    | DTLZ1, IMB4                |
| MP3  | :heavy_check_mark: |                    | :heavy_check_mark: | :heavy_check_mark: |                    | inv-DTLZ1                  |
| MP4  | :heavy_check_mark: | :heavy_check_mark: | :heavy_check_mark: | :heavy_check_mark: |                    | inv-DTLZ1, IMB4            |
| MP5  | :heavy_check_mark: |                    |                    |                    | :heavy_check_mark: | DTLZ3                      |
| MP6  | :heavy_check_mark: | :heavy_check_mark: |                    |                    | :heavy_check_mark: | DTL3, IMB4                 |
| MP7  | :heavy_check_mark: |                    | :heavy_check_mark: |                    | :heavy_check_mark: | inv-DTLZ1, inv-DTLZ2       |
| MP8  | :heavy_check_mark: | :heavy_check_mark: | :heavy_check_mark: |                    | :heavy_check_mark: | inv-DTLZ1, inv-DTLZ2, IMB4 |



# Pareto fronts

![](fig_benchmark_regions.png)

This figure shows the Pareto fronts of the proposed benchmark for $m=3$ objectives. Favored parts of the Pareto front of problems with imbalance (MP2, MP4, MP6, MP8) are highlighted. To generate this figure, run `labs/imbalance/experiments/test_mp/visualize.py`.



# References

- [Deb05] *Scalable Test Problems for Evolutionary Multiobjective Optimization*
- [Liu17] *Investigating the Effect of Imbalance Between Convergence and Diversity in Evolutionary Multi-objective Algorithms*
- [Jain13] *An Improved Adaptive Approach for Elitist Nondominated Sortng Genetic Algorithm for Many-Objective Optimization*



