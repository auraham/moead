# mp8.py
import numpy as np
from rocket.problems.mp.mp6 import mp6
from rocket.problems.mp.common import mp_h
by_row = 1

def mp8(X, m, M):
    
    # separation of X
    n = X.shape[1]
    j = M - 1
    k = n - j
    
    Y = X[:, :-k]
    Z = X[:, -k:]
    
    # evaluate m-th objective
    f = mp6(X, m, M)
    
    # evaluate function h
    h = mp_h(X, Z, j, k)
    
    # transform m-th objective
    new_f = None
    
    """
    # from [Jain13], it did not work
    if m == M-1:
        new_f = (1+h) - (f**2)      # last objective
    else:
        new_f = (1+h) - (f**4)      # first M-1 objectives
    """
    
    # from PlatEMO[Tian17], it works!
    new_f = (1+h) - f
        
    return new_f.copy()
