# mp1: based on dtlz1.py
import numpy as np
from rocket.problems.mp.common import mp_h
by_row = 1

def mp2(X, m, M):
    
    r = None
    
    # separation of X
    n = X.shape[1]
    j = M - 1
    k = n - j
    
    Y = X[:, :-k]
    Z = X[:, -k:]
    
    # multimodality + imbalance
    h = mp_h(X, Z, j, k)
    
    # objective 0
    if m == 0:
        r = (1.0 + h) * 0.5 * (Y.prod(axis=by_row))
    
    # objectives [1, .., m-2]
    if m in range(1, M-1):
        r = (1.0 + h) * 0.5 * (Y[:, :M-m-1].prod(axis=by_row)) * (1 - Y[:, M-m-1])
    
    # objective M-1
    if m == M-1:
        r = (1.0 + h) * 0.5 * (1 - Y[:, 0])

    return r.copy()
