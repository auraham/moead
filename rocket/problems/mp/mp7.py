# mp7.py
import numpy as np
from rocket.problems.mp.mp5 import mp5
from rocket.problems.mp.common import mp_g
by_row = 1

def mp7(X, m, M):
    
    # separation of X
    n = X.shape[1]
    j = M - 1
    k = n - j
    
    Y = X[:, :-k]
    Z = X[:, -k:]
    
    # evaluate m-th objective
    f = mp5(X, m, M)
    
    # evaluate function g
    g = mp_g(Z, k)
    
    # transform m-th objective
    new_f = None
    
    """
    # from [Jain13], it did not work
    if m == M-1:
        new_f = (1+g) - (f**2)      # last objective
    else:
        new_f = (1+g) - (f**4)      # first M-1 objectives
    """
    
    # from PlatEMO[Tian17], it works!
    new_f = (1+g) - f
        
    return new_f.copy()
