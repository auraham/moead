# mp4.py
import numpy as np
from rocket.problems.mp.mp2 import mp2
from rocket.problems.mp.common import mp_h
by_row = 1

def mp4(X, m, M):
    
    # separation of X
    n = X.shape[1]
    j = M - 1
    k = n - j
    
    Y = X[:, :-k]
    Z = X[:, -k:]
    
    # evaluate m-th objective
    f = mp2(X, m, M)                # (pop_size, ) vector with m-th objective values
    
    # evaluate function h
    h = mp_h(X, Z, j, k)            # (pop_size, )
    
    # transform m-th objective
    new_f = (0.5*(1 + h)) - f       # (pop_size, )
    
    return new_f.copy()
    
