# frontend.py
from __future__ import print_function
import numpy as np
from rocket.problems.mp.mp1 import mp1
from rocket.problems.mp.mp2 import mp2
from rocket.problems.mp.mp3 import mp3
from rocket.problems.mp.mp4 import mp4
from rocket.problems.mp.mp5 import mp5
from rocket.problems.mp.mp6 import mp6
from rocket.problems.mp.mp7 import mp7
from rocket.problems.mp.mp8 import mp8

def eval_mp_fn_matrix(fn_id, chroms, m_objs):
    
    objs = None
    
    fns = {
        1: mp1, 
        2: mp2, 
        3: mp3, 
        4: mp4, 
        5: mp5, 
        6: mp6, 
        7: mp7, 
        8: mp8, 
        }
    
    # fn_id check
    if fn_id not in fns:
        print("Error on fn_id value (fn_id: %d)" % fn_id)
        
    pop_size = chroms.shape[0]
    objs = np.zeros((pop_size, m_objs))
    
    for m in range(m_objs):
        objs[:, m] = fns[fn_id](chroms, m, m_objs)

    return objs.copy()
