# frontend.py
from __future__ import print_function
import numpy as np
from rocket.problems.zhou16.t1 import T1
from rocket.problems.zhou16.t2 import T2

def eval_zhou16_fn_matrix(fn_id, chroms):
    
    objs = None
    
    # fn_id check
    if fn_id not in (1, 2):
        print("Error on fn_id value (fn_id: %d)" % fn_id)
        return
        
    if fn_id == 1:
        objs = T1(chroms)
    
    if fn_id == 2:
        objs = T2(chroms)
    

    return objs.copy()
    
    
if __name__ == "__main__":
    
    fn_id           = 2         # use 1, 2
    pop_size        = 100
    n_genes         = 10
    X               = None
    
    if fn_id == 1:
        
        # optimal
        X = np.zeros((pop_size, n_genes))
        X[:, 0] = np.linspace(0, 1, pop_size)
        
        for i in range(1, n_genes):
            X[:, i] = np.sin(6.0*np.pi*X[:, 0] + (i+1)*np.pi/n_genes)
        
    if fn_id == 2:
        
        # optimal
        X = np.zeros((pop_size, n_genes))
        X[:, 0] = np.linspace(0, 1, pop_size)
        
        factor = (4.0/5.0) * X[:, 0]
        for i in range(1, n_genes):
            X[:, i] = np.sin(6.0*np.pi*X[:, 0] + (i+1)*np.pi/n_genes) * factor
        
    
    chroms = X
    objs   = eval_zhou16_fn_matrix(fn_id, chroms)
    
    # plot
    title = "T"+str(fn_id)
    import matplotlib.pyplot as plt
    fig = plt.figure(title)
    ax = fig.add_subplot(111)
    ax.set_title(title)
    ax.plot(objs[:, 0], objs[:, 1], "bo")
    plt.show()
    
    
