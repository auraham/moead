# About this package #

This package contains the implementation of the problems (T1, T2) described in [Zhou16].



# Errata

The Equations of T1 and T2 in [Zhou16] are based on some problems described in [Li09] (see the problem F2). However, I think that the definition of T1 and T2 has a little typo (a power of 2 was omited).

- In [Li09], every summatory has this format:

```
sum([...]**2)
```

where `[...]` involves a difference of `sin` or `cos` functions. Although, the important point is the use of a power of 2. This ensures that `sum()` will return positive values only.

- In [Zhou16], the summatory do not use any power of 2:

```
sum([...])
```

In that case, `sum()` can (and will) return either negative or positive values.

In order to solve that issue and return positive values only, we add that power of 2. The following snippet (`t1.py`, `t2.py`) contains this fix:

```python
# t1.py
for i, j in enumerate(J1):
	suma[:, i] = X[:, j] - np.sin(6.0*np.pi*X1 + (j+1)*np.pi/n_genes)
    suma[:, i] = suma[:, i]**2  # this line solves the issue

sumat = suma.sum(axis=1)		# sum of squares
```





# References

-  [Zhou16] *Are All the Subproblems Equally Important? Resource Allocation in Decomposition-Based Multiobjective Evolutionary Algorithms*.
- [Li09] *Multiobjective Optimization Problems With Complicated Pareto Sets, MOEA/D and NSGA-II*.



