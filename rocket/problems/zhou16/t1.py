# t1.py
from __future__ import print_function
import numpy as np

def T1(X):
    
    pop_size = X.shape[0]
    objs = np.zeros((pop_size, 2))
    
    objs[:, 0] = t1_f1(X)
    objs[:, 1] = t1_f2(X)
    
    return objs.copy()
    
def t1_f1(X):
    
    pop_size, n_genes = X.shape
    
    X1 = X[:, 0]
    J1 = np.arange(2, n_genes, 2)
    
    suma = np.zeros((pop_size, len(J1)))
    
    for i, j in enumerate(J1):
        
        suma[:, i] = X[:, j] - np.sin(6.0*np.pi*X1 + (j+1)*np.pi/n_genes)
        suma[:, i] = suma[:, i]**2
        
    sumat = suma.sum(axis=1)
    
    r = X1 + 2.0/len(J1) * sumat
    
    return r.copy()
    
    
def t1_f2(X):
    
    pop_size, n_genes = X.shape
    
    X1 = X[:, 0]
    J2 = np.arange(1, n_genes, 2)
    
    suma = np.zeros((pop_size, len(J2)))
    
    for i, j in enumerate(J2):
        
        suma[:, i] = X[:, j] - np.sin(6.0*np.pi*X1 + (j+1)*np.pi/n_genes)
        suma[:, i] = suma[:, i]**2
        
    sumat = suma.sum(axis=1)
    
    r = 1 - np.sqrt(X1) + 2.0/len(J2) * sumat
    
    return r.copy()
    
    
def get_t1_pareto_front(pop_size):
    
    Z = np.zeros((pop_size, 2))
    Z[:, 0] = np.linspace(0, 1, pop_size)
    Z[:, 1] = 1 - np.sqrt(Z[:, 0])
    
    return Z.copy()
    
    
if __name__ == "__main__":
    
    pop_size = 100
    n_genes = 10
    
    # optimal
    X = np.zeros((pop_size, n_genes))
    X[:, 0] = np.linspace(0, 1, pop_size)
    
    for i in range(1, n_genes):
        X[:, i] = np.sin(6.0*np.pi*X[:, 0] + (i+1)*np.pi/n_genes)
        
    Z = T1(X)
    PF = get_t1_pareto_front(pop_size)
    
    import matplotlib.pyplot as plt
    
    fig = plt.figure("t1")
    ax = fig.add_subplot(111)
    ax.cla()
    
    ax.plot(Z[:, 0], Z[:, 1], "bo", label="Approximation")
    ax.plot(PF[:, 0], PF[:, 1], "g", label="Real front")
    plt.show()
    
        
    
    
    
    
    
    
    
    
