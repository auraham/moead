# About this benchmark
These problems are defined in *Performance of Decomposition-Based Many-Objective Algorithms Strongly Depends on Pareto Front Shapes*, Ishibuchi et al. 2016
