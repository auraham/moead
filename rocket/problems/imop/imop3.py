# imop3: based on dtlz1.py, inv-dtlz1 (transform imop1)
import numpy as np
from rocket.problems.imop.imop1 import imop1, dtlz1_g as imop1_g
by_row = 1

def imop3(X, m, M):
    
    f = imop1(X, m, M)          # (pop_size, ) m-th objective vector values
    
    # separation of X
    n = X.shape[1]          # n_genes
    j = M - 1               # num of position variables
    k = n - j               # num of distance variables
    
    Y = X[:, :-k]
    Z = X[:, -k:]
    
    g = imop1_g(Z, k)       # (pop_size, )
    
    
    # transform m-th objective
    factor = 0.5*(1 + g)
    new_f  = factor - f
    
    return new_f.copy()
        

