# imop1: based on dtlz1.py
import numpy as np
by_row = 1

def imop1(X, m, M):
    
    return dtlz1(X, m, M)
    
# ----

def dtlz1(X, m, M):
    
    r = None
    
    # separation of X
    n = X.shape[1]
    j = M - 1
    k = n - j
    
    Y = X[:, :-k]
    Z = X[:, -k:]
    
    g = dtlz1_g(Z, k)
    
    # objective 0
    if m == 0:
        r = (1.0 + g) * 0.5 * (Y.prod(axis=by_row))
    
    # objectives [1, .., m-2]
    if m in range(1, M-1):
        r = (1.0 + g) * 0.5 * (Y[:, :M-m-1].prod(axis=by_row)) * (1 - Y[:, M-m-1])
    
    # objective M-1
    if m == M-1:
        r = (1.0 + g) * 0.5 * (1 - Y[:, 0])

    return r

def dtlz1_g(Z, k):
    r = 100 * (k+(((Z - 0.5)**2) - np.cos(20*np.pi*(Z-0.5))).sum(axis=by_row))
    return r
