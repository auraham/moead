# frontend.py
from __future__ import print_function
import numpy as np
from rocket.problems.imop.imop1 import imop1
from rocket.problems.imop.imop2 import imop2
from rocket.problems.imop.imop3 import imop3
from rocket.problems.imop.imop4 import imop4

def eval_imop_fn_matrix(fn_id, chroms, m_objs):
    
    objs = None
    
    # fn_id check
    if fn_id not in (1, 2, 3, 4):
        print("Error on fn_id value (fn_id: %d)" % fn_id)
        
    fns = {
        1: imop1, 
        2: imop2, 
        3: imop3, 
        4: imop4, 
        }
    
    pop_size = chroms.shape[0]
    objs = np.zeros((pop_size, m_objs))
    
    for m in range(m_objs):
        objs[:, m] = fns[fn_id](chroms, m, m_objs)

    return objs.copy()
    
