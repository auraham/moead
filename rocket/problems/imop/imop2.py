# imop2: based on dtlz1.py, imbs4.py
import numpy as np
by_row = 1

def imop2(X, m, M):
    
    r = None
    
    # separation of X
    n = X.shape[1]
    j = M - 1
    k = n - j
    
    Y = X[:, :-k]
    Z = X[:, -k:]
    
    # @note: the g function must be change to introduce imbalance
    ##g = dtlz1_g(Z, k)
    g = imop_imb4_g(X, Z, j, k)
    
    # objective 0
    if m == 0:
        r = (1.0 + g) * 0.5 * (Y.prod(axis=by_row))
    
    # objectives [1, .., m-2]
    if m in range(1, M-1):
        r = (1.0 + g) * 0.5 * (Y[:, :M-m-1].prod(axis=by_row)) * (1 - Y[:, M-m-1])
    
    # objective M-1
    if m == M-1:
        r = (1.0 + g) * 0.5 * (1 - Y[:, 0])

    return r

def dtlz1_g(Z, k):
    r = 100 * (k+(((Z - 0.5)**2) - np.cos(20*np.pi*(Z-0.5))).sum(axis=by_row))
    return r
    
    
def imop_imb4_g(X, Z, j, k):
    """
    This function is similar to imb4_g. The main difference is the
    computing of the vector g
    """
    
    pop_size, n_genes = X.shape
    
    X1       = X[:, 0]                  # first variable
    Xn       = X[:, j:]                 # j+1 to last variables
    g        = np.zeros((pop_size, ))   # return value
    
    # evaluate condition (the imbalance is here)
    case_a = (2./3 <= X1) & (X1 <= 1.0) 
    case_b = ~case_a
    
    
    # ---- this block was for imb4_g ----
    """
    # case b
    per_row = 1
    coss    = 2*np.cos((np.pi*X1)/2)                    # (pop_size, )
    sumTi   = (X[:, :j]).sum(axis=per_row) / 2          # (pop_size, ), suma las primeras j columnas (ie. las primeras j variables)
    sumTi   = sumTi.reshape((pop_size, 1))              # (pop_size, 1) para hacer la suma por columna
    Ti      = Xn - sumTi                                # (pop_size, n-2)
    prod1   = -0.9 * (Ti**2)
    prod2   = np.abs(Ti)**0.6
    suma    = coss * (prod1 + prod2).sum(axis=per_row)
    
    # save
    g[case_a]   = 0
    g[case_b]   = suma[case_b]
    """
    # ---
    
    
    # ---- this new block is for dtlz1_g ----
    g_multimodal = dtlz1_g(Z, k)
    
    # save
    g[case_a]   = 0
    g[case_b]   = g_multimodal[case_b]
    # ----
    
    return g.copy()
    

