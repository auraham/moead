# imop4: based on dtlz1.py, inv-dtlz1 (transform imop2)
import numpy as np
from rocket.problems.imop.imop2 import imop2, imop_imb4_g as imop2_g
by_row = 1

def imop4(X, m, M):
    
    f = imop2(X, m, M)          # (pop_size, ) m-th objective vector values
    
    # separation of X
    n = X.shape[1]          # n_genes
    j = M - 1               # num of position variables
    k = n - j               # num of distance variables
    
    Y = X[:, :-k]
    Z = X[:, -k:]
    
    g = imop2_g(X, Z, j, k)       # (pop_size, )
    
    
    # transform m-th objective
    factor = 0.5*(1 + g)
    new_f  = factor - f
    
    return new_f.copy()
        

