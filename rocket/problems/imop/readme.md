# About this package

This package contains our new benchmark based on:

- DTLZ [Deb05]
- IMB [Liu17]
- inv-DTLZ [Jain13]


The proposed benchmark, called IMOP (Irregular MOP), is detailed below.


| Problem | Base            | Properties                      | Geometry |
| ------- | --------------- | ------------------------------- | -------- |
| IMOP1   | DTLZ1           | Multimodal                      | Linear   |
| IMOP2   | IMB4            | Multimodal, Imbalance           | Linear   |
| IMOP3   | inv-DTLZ1       | Multimodal, Inverted            | Linear   |
| IMOP4   | IMB4, inv-DTLZ1 | Multimodal, Imbalance, Inverted | Linear   |

| Problem | Base            | Properties                      | Geometry |
| ------- | --------------- | ------------------------------- | -------- |
| IMOP5   | DTLZ3           | Multimodal                      | Concave  |
| IMOP6   | IMB4            | Multimodal, Imbalance           | Concave  |
| IMOP7   | inv-DTLZ2       | Multimodal, Inverted            | Concave  |
| IMOP8   | IMB4, inv-DTLZ2 | Multimodal, Imbalance, Inverted | Concave  |





# References

- [Deb05] *Scalable Test Problems for Evolutionary Multiobjective Optimization*
- [Liu17] *Investigating the Effect of Imbalance Between Convergence and Diversity in Evolutionary Multi-objective Algorithms*
- [Jain13] *An Improved Adaptive Approach for Elitist Nondominated Sortng Genetic Algorithm for Many-Objective Optimization*



