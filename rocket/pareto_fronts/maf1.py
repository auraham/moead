# maf1.py
from __future__ import print_function
from rocket.pareto_fronts.simplex_lattice import simplex_lattice
import numpy as np


def get_front_maf1(m_objs):
    """
    Pareto front of MaF1
    """
    
    """
    matlab
    f = UniformPoint(input,Global.M);
    varargout = {1-f};
    """
    
    # uniform set of vectors
    front = 1 - simplex_lattice(m_objs)
    
    return front.copy()
