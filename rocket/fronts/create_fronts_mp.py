# create_fronts_mp.py
from __future__ import print_function
from mesh import mesh_2, mesh_3, mesh_4, mesh_7, mesh_9, mesh_14
from rocket.helpers import get_n_genes, get_pareto_solutions_mp
from rocket.problems import evaluate_mop
import numpy as np

import os
script_path = os.path.dirname(os.path.abspath(__file__))

  
if __name__ == "__main__":
    
    # mop_name, optimal_chroms_generator
    params = (
        ("mp1", get_pareto_solutions_mp),
        ("mp2", get_pareto_solutions_mp),
        ("mp3", get_pareto_solutions_mp),
        ("mp4", get_pareto_solutions_mp),
        ("mp5", get_pareto_solutions_mp),
        ("mp6", get_pareto_solutions_mp),
        ("mp7", get_pareto_solutions_mp),
        ("mp8", get_pareto_solutions_mp),
    )


    # m_objs, p spacing
    spacing = {
        2 : 100,
        3 : 15,
        4 : 12,
        5 : 10,
        8 : 4,
        10: 3,
        15: 2,
    }
    
    for m_objs in (2, 3, 4, 5, 8, 10, 15):
        
        for item in params:
            
            # unpack
            mop_name, chroms_generator = item
            p = spacing[m_objs]
            
            n_genes, k, l = get_n_genes(mop_name, m_objs)
            
            chroms = chroms_generator(n_genes, m_objs, p)
            objs   = evaluate_mop(chroms.copy(), m_objs, {"name": mop_name})

            # save
            filename = "front_%s_m_%d.npz" % (mop_name, m_objs)
            filepath = os.path.join(script_path, "files", filename)
            # uncomment this line to overwrite current fronts
            #np.savez(filepath, objs=objs, p=np.array([p]))              # save objs and p in the same file

            
            
    # plot!
    from rocket.plot import load_rcparams, plot_pops
    
    mop_name = "mp1"
    m_objs = 3
    
    # unpack
    npzfile = np.load("files/front_%s_m_%d.npz" % (mop_name, m_objs))
    objs    = npzfile["objs"]
    p       = npzfile["p"][0]
    m_objs  = objs.shape[1]
    
    pops = (objs, )
    labels = ("pop", )
    
    plot_pops(pops, labels, title=mop_name.upper())
    
    
