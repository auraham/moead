# create_fronts_inv_dtlz.py
from __future__ import print_function
from mesh import mesh_2, mesh_3, mesh_4, mesh_7, mesh_9, mesh_14
from rocket.helpers import get_n_genes, get_pareto_solutions_dtlz
from rocket.problems import evaluate_mop
import numpy as np

import os
script_path = os.path.dirname(os.path.abspath(__file__))

  
if __name__ == "__main__":
    
    # mop_name, optimal_chroms_generator
    params = (
        ("inv-dtlz1", get_pareto_solutions_dtlz),
    )


    # m_objs, p spacing
    spacing = {
        2 : 100,
        3 : 15,
        4 : 12,
        5 : 10,
        8 : 4,
        10: 3,
        15: 2,
    }
    
    for m_objs in (2, 3, 4, 5, 8, 10, 15):
        
        for item in params:
            
            # unpack
            mop_name, chroms_generator = item
            p = spacing[m_objs]
            
            n_genes, k, l = get_n_genes(mop_name, m_objs)
            
            chroms = chroms_generator(n_genes, m_objs, p)
            objs   = evaluate_mop(chroms.copy(), m_objs, {"name": mop_name})

            # save
            filename = "front_%s_m_%d.npz" % (mop_name, m_objs)
            filepath = os.path.join(script_path, "files", filename)
            # uncomment this line to overwrite current fronts
            #np.savez(filepath, objs=objs, p=np.array([p]))              # save objs and p in the same file

            
            """
            # check
            check, expected = 0, 0
            if mop_name == "inv-dtlz1":
                check    = objs.sum(axis=1).sum()                       # it should be equal to rows/2 (chroms.shape[0]/2)
                expected = chroms.shape[0]/2.0
                
                if (check - expected) > 1:
                    print("error on %s! check: %.2f, expected: %.2f " % 
                        (mop_name, check, expected))                    # @todo: add log support here
            
            elif mop_name in ("inv-dtlz2", "inv-dtlz3", "inv-dtlz4"):
                check    = (objs**2).sum(axis=1).sum()                  # it should be equal to rows (chroms.shape[0])
                expected = chroms.shape[0]
                
                if (check - expected) > 1:
                    print("error on %s! check: %.2f, expected: %.2f " % 
                        (mop_name, check, expected))                    # @todo: add log support here
            
            print("m: %2s, n: %2s, k: %2s, l: %2s, rows: %d, filepath: %s, check: %010.2f, exp: %.2f" % 
                (m_objs, n_genes, k, l, chroms.shape[0], filepath, check, expected))
            
            """
            
    # plot!
    import matplotlib.pyplot as plt
    from mpl_toolkits.mplot3d import Axes3D
    
    mop_name = "inv-dtlz1"
    
    # unpack
    npzfile = np.load("files/front_%s_m_2.npz" % mop_name)
    objs    = npzfile["objs"]
    p       = npzfile["p"][0]
    m_objs  = objs.shape[1]
    
    fig = plt.figure(mop_name)
    ax = fig.add_subplot(111, projection="3d") if m_objs == 3 else fig.add_subplot(111)
    ax.set_title(mop_name)
    ax.plot(objs[:, 0], objs[:, 1], objs[:, 2], "bo") if m_objs == 3 else ax.plot(objs[:, 0], objs[:, 1], "bo")
    plt.show()
    
    
    mop_name = "dtlz1"
    
    # unpack
    npzfile = np.load("files/front_%s_m_2.npz" % mop_name)
    objs    = npzfile["objs"]
    p       = npzfile["p"][0]
    m_objs  = objs.shape[1]
    
    ax.plot(objs[:, 0], objs[:, 1], objs[:, 2], "go") if m_objs == 3 else ax.plot(objs[:, 0], objs[:, 1], "go")
    
    if m_objs == 3:
        ax.view_init(15, 45)
    
    plt.show()
    
