"""
create_simplex_vectors.py
"""

import numpy as np
import sys
import os

eps = 10e-6

def int2kary(x, basek, digits):
    
    if x >= basek**digits:
        raise ValueError("Number in int2kary is too large")
    
    val     = digits-1
    kary    = np.zeros(digits, )
    i       = 0
    
    while x:
        
        if x >= basek**val:
            
            kary[i] += 1
            x -= basek**val
            
        else:
            
            val -= 1
            i += 1
            
    return kary.copy()

def create_simplex_vectors(dims, spacing, add_eps=False):
    """
    Create a set of vectors distributed into a simplex.
    
    Input
    dims        int, Number of dimensions (eg objectives)
    spacing     int, Spacing among vectors
    add_eps     bool, Add a little const value to avoid zero vectors such as v=[0., 0., 1]
    
    Output
    vectors     (n_vectors, dims) matrix of vectors
    """
    
    k = dims
    s = spacing
    i = 0
    c = 0
    vector_list = []
    
    while i < (s+1)**k:
        
        suma = 0
        count = int2kary(i, s+1, k)
        suma += count.sum()
        
        if suma == s:
            
            vector = np.zeros(k, )
            
            for j in range(0, k):
                
                vector[j] = (count[j]*1.0) / s

            vector_list.append(vector)
            c += 1
        
        i += 1

    # cast to matrix
    vectors = np.array(vector_list)
    
    # add eps
    if add_eps:
        
        # change values
        to_fill = vectors == 0
        vectors[to_fill] = eps
        
        # normalize
        nvectors = np.array([ v/v.sum() for v in vectors ])

        return nvectors.copy()
        
    return vectors.copy()

def get_simplex_vectors(rows, cols, spacing=1):
    """
    Return a predefined matrix of vectors, if such matrix is available.
    
    Input
    rows        int, number of rows 
    cols        int, number of cols (ie dims)
    spacing     int, spacing among vectors
    
    Output
    vectors     (rows, cols) matrix
    """

    current_path = os.path.dirname(os.path.realpath(__file__))
    
    filename = "simplex_vectors_rows_%d_cols_%d.txt" % (rows, cols)
    filepath = os.path.join(current_path, filename)
    
    vectors = None
    
    if os.path.isfile(filepath):
        
        vectors = np.genfromtxt(filepath)
        
    else:

        print("There is no predefined matrix of size (%d, %d). A new matrix will be created instead." % (rows, cols))

        # create new matrix
        vectors = create_simplex_vectors(cols, spacing, add_eps=False)
        
        # save new matrix
        filename = "simplex_vectors_rows_%d_cols_%d.txt" % (vectors.shape[0], vectors.shape[1])
        filepath = os.path.join(current_path, filename)
        np.savetxt(filepath, vectors, fmt="%.10f")
        
    return vectors.copy()
    

if __name__ == "__main__":
    
    dims = 3
    spacing = 10
    vectors = create_simplex_vectors(dims, spacing, False)
    
    # plotting
    import matplotlib.pyplot as plt
    from mpl_toolkits.mplot3d import Axes3D

    fig = plt.figure("simplex vectors")
    ax = fig.add_subplot(111, projection="3d")
    ax.cla()
    
    ax.plot(vectors[:, 0], vectors[:, 1], vectors[:, 2], "bo")
    plt.show()
    
