# About this directory

This directory contains fronts for:

- DTLZ
- DTLZ-spin
- WFG
- WFG-spin

Each front is stored in `npz` format to save two variables:

- `objs` , a `(n_points, m_objs)` numpy array. This variable contains the front.
- `p`, a `(1, )` numpy array. This is an optional value used for plotting a meshgrid with this parameter.

Both DLTZ and DTLZ-spin fronts were generated using `create_fronts_dtlz.py` and `create_fronts_dtlz_spin.py` in `rocket/fronts`. WFG fronts were downloaded from [jMetal][http://jmetal.sourceforge.net/problems.html] (for `m_objs=2,3` only). To convert these fronts from `.pf` to `.npz`, use `convert_wfg_fronts.py`.