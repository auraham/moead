# convert_wfg_fronts.py
from __future__ import print_function
import numpy as np

if __name__ == "__main__":

    # problems
    problems = ("WFG1", "WFG2", "WFG3", "WFG4", "WFG5", "WFG6", "WFG7", "WFG8", "WFG9")

    for m_objs in (2, 3):
        
        for mop_name in problems:

            # load
            filename = "%s.%dD.pf" % (mop_name, m_objs)
            objs     = np.genfromtxt(filename, delimiter=" ")

            # save
            filename = "front_%s_m_%d.npz" % (mop_name.lower(), m_objs)
            # uncomment this line to overwrite current fronts
            #np.savez(filename, objs=objs, p=np.array([None]))              # save objs and p in the same file
            
            print(filename)
            
            

    # test
    from rocket.fronts import get_real_front
    from rocket.plot import plot_real_front
    
    real_front, p = get_real_front("wfg2", 3)
    
    plot_real_front (real_front, p=p)

    
    

