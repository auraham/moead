# mesh.py
import numpy as np

def mesh_2(p):
    
    x = np.linspace(0, 1, p)
    
    a, b = np.meshgrid(x, x)
    
    matrix = np.vstack((a.flatten(),
                        b.flatten()
                        )).T
                        
    return matrix.copy()

def mesh_3(p):
    
    x = np.linspace(0, 1, p)
    
    a, b, c = np.meshgrid(x, x, x)
    
    matrix = np.vstack((a.flatten(),
                        b.flatten(),
                        c.flatten(),
                        )).T
                        
    return matrix.copy()
    
def mesh_4(p):
    
    x = np.linspace(0, 1, p)
    
    a, b, c, d = np.meshgrid(x, x, x, x)
    
    matrix = np.vstack((a.flatten(),
                        b.flatten(),
                        c.flatten(),
                        d.flatten()
                        )).T
                        
    return matrix.copy()

def mesh_7(p):
    
    x = np.linspace(0, 1, p)
    
    a, b, c, d, e, f, g = np.meshgrid(x, x, x, x, x, x, x)
    
    matrix = np.vstack((a.flatten(),
                        b.flatten(),
                        c.flatten(),
                        d.flatten(),
                        e.flatten(),
                        f.flatten(),
                        g.flatten()
                        )).T
                        
    return matrix.copy()

def mesh_9(p):
    
    x = np.linspace(0, 1, p)
    
    a, b, c, d, e, f, g, h, i = np.meshgrid(x, x, x, x, x, x, x, x, x)
    
    matrix = np.vstack((a.flatten(),
                        b.flatten(),
                        c.flatten(),
                        d.flatten(),
                        e.flatten(),
                        f.flatten(),
                        g.flatten(),
                        h.flatten(),
                        i.flatten()
                        )).T
                        
    return matrix.copy()
    
def mesh_14(p):
    
    x = np.linspace(0, 1, p)
    
    a, b, c, d, e, f, g, h, i, j, k, l, m, n = np.meshgrid(x, x, x, x, x, x, x, x, x, x, x, x, x, x)
                                                            
    
    matrix = np.vstack((a.flatten(),
                        b.flatten(),
                        c.flatten(),
                        d.flatten(),
                        e.flatten(),
                        f.flatten(),
                        g.flatten(),
                        h.flatten(),
                        i.flatten(),
                        j.flatten(),
                        k.flatten(),
                        l.flatten(),
                        m.flatten(),
                        n.flatten(),
                        )).T
                        
    return matrix.copy()


