# create_fronts_zhou16.py
from __future__ import print_function
from rocket.helpers import get_pareto_solutions_zhou16, get_n_genes
from rocket.problems import evaluate_mop
import numpy as np

import os
script_path = os.path.dirname(os.path.abspath(__file__))
    
    
if __name__ == "__main__":
    
    
    params = (
        (1, "t1"),
        (2, "t2"),
    )
    
    pop_size = 300
    m_objs = 2          # no parametrizable
    
    for fn_id, mop_name in params:
    
        n_genes, _, _ = get_n_genes(mop_name, m_objs)
        chroms = get_pareto_solutions_zhou16(fn_id, pop_size, n_genes)
        objs   = evaluate_mop(chroms.copy(), m_objs, {"name": mop_name})

        # save
        filename = "front_%s_m_%d.npz" % (mop_name, m_objs)
        filepath = os.path.join(script_path, "files", filename)
        
        # uncomment this line to overwrite current fronts
        #np.savez(filepath, objs=objs, p=np.array([pop_size]))              # save objs and p in the same file

    # ---
    
            
    # plot!
    import matplotlib.pyplot as plt
    from mpl_toolkits.mplot3d import Axes3D
    
    mop_name = "t2"
    
    # unpack
    npzfile = np.load("files/front_%s_m_2.npz" % mop_name)
    objs    = npzfile["objs"]
    m_objs  = objs.shape[1]
    
    fig = plt.figure(mop_name)
    ax = fig.add_subplot(111)
    ax.set_title(mop_name)
    ax.plot(objs[:, 0], objs[:, 1], "bo")
    plt.show()
    
