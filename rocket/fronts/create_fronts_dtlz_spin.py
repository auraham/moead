# create_fronts_dtlz.py
from __future__ import print_function
from mesh import mesh_2, mesh_4, mesh_7, mesh_9
from rocket.helpers import get_n_genes, spin
from rocket.problems import evaluate_mop
from rocket.fronts import get_real_front
import numpy as np

import os
script_path = os.path.dirname(os.path.abspath(__file__))


if __name__ == "__main__":
    
    # base_mop_name
    params = (
        "dtlz1",    # @note: solo dtlz1 esta soportado debido al metodo de rotacion, cuando sea generalizado, se descomentaran los demas problemas
        "dtlz2",
        "dtlz3",
        "dtlz4",
        #("dtlz5",                               # @todo: to implement
        )
    
    # (radians, degrees)
    thetas = (                  
            #(np.pi * (0.25), "45"),
            #(np.pi * (0.50), "90"),
            #(np.pi * (0.75), "135"),
            (np.pi,          "180"),    # @note: solo se soporta este angulo de rotacion
            
            #(np.pi * (1.25), "225"),
            #(np.pi * (1.50), "270"),
            #(np.pi * (1.75), "315"),
            #(np.pi * (2),    "360"),
            )
    
    
    #for m_objs in (2, 3, 5, 8, 10):                # @todo: currently, we only known how to rotate in 3D, but we do not how to do it with more dimensions!
    for m_objs in (3, ):
        
        for theta, degree in thetas:
        
            for base_mop_name in params:
                
                # we use this trick
                # instead of evaluate dtlz4 function, we use dtl2
                # both functions (mops) generate the same hypersphere surface in the objective space,
                # but when using dtlz4 that surface will be less dense
                tmp_mop_name = base_mop_name
                if tmp_mop_name == "dtlz4":
                    tmp_mop_name = "dtlz2"
                        
                # original front
                front_real, p = get_real_front(tmp_mop_name, m_objs)
                
                # rotated front
                front_spin    = spin(front_real, degree)
                
                
                # save
                mop_name = "%s-spin-%s" % (base_mop_name, degree)
                filename = "front_%s_m_%d.npz" % (mop_name, m_objs)
                filepath = os.path.join(script_path, "files", filename)
                
                # check
                if (front_spin.min(axis=0) < 0).any():
                    print("warning: front %s contains negative values!" % mop_name)
               
                
                # uncomment to overwrite current fronts
                #np.savez(filepath, objs=front_spin, p=np.array([p]))              # save objs and p in the same file
                
                
            
    # plot!
    import matplotlib.pyplot as plt
    from mpl_toolkits.mplot3d import Axes3D
    
    mop_name = "dtlz4-spin-180"
    
    # unpack
    npzfile = np.load("files/front_%s_m_3.npz" % mop_name)
    objs    = npzfile["objs"]
    p       = npzfile["p"][0]
    m_objs  = objs.shape[1]
    
    from rocket.plot import plot_real_front
    plot_real_front(real_front=objs, p=p, title=mop_name)
    
    """
    fig = plt.figure(mop_name)
    ax = fig.add_subplot(111, projection="3d") if m_objs == 3 else fig.add_subplot(111)
    ax.set_title(mop_name)
    ax.plot(objs[:, 0], objs[:, 1], objs[:, 2], "bo") if m_objs == 3 else ax.plot(objs[:, 0], objs[:, 1], "bo")
    plt.show()
    """
