# create_fronts_min_dtlz.py
from __future__ import print_function
from mesh import mesh_2, mesh_4, mesh_7, mesh_9
from rocket.helpers import get_n_genes
from rocket.problems import evaluate_mop
import numpy as np

import os
script_path = os.path.dirname(os.path.abspath(__file__))

def get_pareto_solutions_min_dtlz1_3(n_genes, m_objs, p):
    """
    Returns pareto optimal solutions for min-dtlz1 and min-dtlz3 for m_objs in (2,3,5,8,10) objectives.
    @todo: add support for more objectives
    
    Input
    n_genes     int, number of decision variables
    m_objs      int, number of objectives
    p           int, spacing
    
    Output
    chroms      (rows, n_genes) matrix of pareto optimal solutions
    """
    
    matrix = None
    
    if m_objs == 2:
        span = np.linspace(0, 1, p)
        matrix = span.reshape((span.shape[0], 1))
    
    elif m_objs == 3:
        matrix = mesh_2(p)
    
    elif m_objs == 5:
        matrix = mesh_4(p)
    
    elif m_objs == 8:
        matrix = mesh_7(p)
    
    elif m_objs == 10:
        matrix = mesh_9(p)
    
    else:
        print("m_objs: %s is not defined for get_pareto_solutions_min_dtlz" % m_objs)       # @todo: add log support here
        return None
        
    rows, cols = matrix.shape
    
    # optimal value for distance parameters
    opt_x = 0.04977
    
    chroms = np.ones((rows, n_genes))*opt_x
    
    for col in range(cols):                     # rewrite the first cols of chroms
        chroms[:, col] = matrix[:, col]         # with the content of matrix
        
    return chroms.copy()
    
    

def get_pareto_solutions_min_dtlz2_5(n_genes, m_objs, p):
    """
    Returns pareto optimal solutions for min-dtlz2, min-dtlz4, and min-dtlz5 for m_objs in (2,3,5,8,10) objectives.
    @todo: add support for more objectives
    
    Input
    n_genes     int, number of decision variables
    m_objs      int, number of objectives
    p           int, spacing
    
    Output
    chroms      (rows, n_genes) matrix of pareto optimal solutions
    """
    
    matrix = None
    
    if m_objs == 2:
        span = np.linspace(0, 1, p)
        matrix = span.reshape((span.shape[0], 1))
    
    elif m_objs == 3:
        matrix = mesh_2(p)
    
    elif m_objs == 5:
        matrix = mesh_4(p)
    
    elif m_objs == 8:
        matrix = mesh_7(p)
    
    elif m_objs == 10:
        matrix = mesh_9(p)
    
    else:
        print("m_objs: %s is not defined for get_pareto_solutions_min_dtlz" % m_objs)       # @todo: add log support here
        return None
        
    rows, cols = matrix.shape
    
    # optimal value for distance parameters
    # (it could be either 0 or 1)
    opt_x = 0
    
    chroms = np.ones((rows, n_genes))*opt_x
    
    for col in range(cols):
        chroms[:, col] = matrix[:, col]
        
    return chroms.copy()
    
if __name__ == "__main__":
    
    # mop_name, optimal_chroms_generator
    params = (
        #("min-dtlz1", get_pareto_solutions_min_dtlz1_3),
        #("min-dtlz2", get_pareto_solutions_min_dtlz2_5),
        #("min-dtlz3", get_pareto_solutions_min_dtlz1_3),
        ("min-dtlz4", get_pareto_solutions_min_dtlz2_5),
        #("min-dtlz5", get_pareto_solutions_min_dtlz2_5),   # @todo: we don't know if this front is correct
    )
    
    # m_objs, p spacing
    spacing = {
        2 : 100,
        3 : 15,
        5 : 10,
        8 : 4, 
        10: 3, 
    }
    
    
    for m_objs in (2, 3, 5, 8, 10):
        
        for item in params:
            
            # unpack
            mop_name, chroms_generator = item
            p = spacing[m_objs]
            
            n_genes, k, l = get_n_genes(mop_name, m_objs)
            
            # we use this trick
            # instead of evaluate dtlz4 function, we use dtl2
            # both functions (mops) generate the same hypersphere surface in the objective space,
            # but when using dtlz4 that surface will be less dense
            tmp_mop_name = mop_name
            if tmp_mop_name == "min-dtlz4":
               tmp_mop_name = "min-dtlz2"
               
            chroms = chroms_generator(n_genes, m_objs, p)
            objs   = evaluate_mop(chroms.copy(), m_objs, {"name": tmp_mop_name})   
            
            # save
            filename = "front_%s_m_%d.npz" % (mop_name, m_objs)
            filepath = os.path.join(script_path, "files", filename)
            
            # uncomment to override current fronts
            #np.savez(filepath, objs=objs, p=np.array([p]))              # save objs and p in the same file
            
            # @todo: how to check if the front is correct?
            
            print("m: %2s, n: %2s, k: %2s, l: %2s, filepath: %s" % 
                (m_objs, n_genes, k, l, filepath))
                
    # plot!
    import matplotlib.pyplot as plt
    from mpl_toolkits.mplot3d import Axes3D
    
    mop_name = "min-dtlz4"
    
    # unpack
    npzfile = np.load("files/front_%s_m_3.npz" % mop_name)
    objs    = npzfile["objs"]
    p       = npzfile["p"][0]
    m_objs  = objs.shape[1]
    
    fig = plt.figure(mop_name)
    ax = fig.add_subplot(111, projection="3d") if m_objs == 3 else fig.add_subplot(111)
    ax.set_title(mop_name)
    ax.plot(objs[:, 0], objs[:, 1], objs[:, 2], "bo") if m_objs == 3 else ax.plot(objs[:, 0], objs[:, 1], "bo")
    plt.show()
            
            
            
    

    
    
    
    
    
    
    
    
    
