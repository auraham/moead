# get_real_front.py
from __future__ import print_function
from rocket.helpers import check_n_genes, spin
import numpy as np

import os
script_path = os.path.dirname(os.path.abspath(__file__))


def get_real_front_dtlz_spin(mop_name, m_objs, n_genes, params):
    """
    This function returns the real front of dtlz-spin problems
    It is almost the same as get_real_front, but handles other conditions 
    
    Input
    mop_name    string, name of problem
    m_objs      int, number of objectives
    n_genes     int, number of decision variables
    params      dict, additional parameters
    
    Output
    front       (n_points, m_objs) matrix,
                or None if the front is not available
    p           spacing parameter (needed for plotting)
    
    """
    
    if m_objs != 3:
        print("Front %s for m_objs: %s is not available" % 
            (mop_name, m_objs))                                         # @todo: add log support here
    
    degrees  = params["degrees"]
    filename = "front_%s-%d_m_%d.npz" % (mop_name, degrees, m_objs)
    filepath = os.path.join(script_path, "files", filename)
    
    if os.path.isfile(filepath):
        
        # n_genes check (only for "min-dtlz")
        if check_n_genes(mop_name, m_objs, n_genes):
        
            # load front from file
            npzfile = np.load(filepath)
            
            front   = npzfile["objs"]
            p       = npzfile["p"][0]
            
            return front.copy(), p
            
    else:
        
        # @todo: descomentar este bloque cuando el metodo de rotacion sea general para rotar a un grado diferente de 180
        """
        # this block allow us to get a front with a user-defined rotation angle (like 20 degrees)
        base_mop = mop_name.split("-")[0]
        filename = "front_%s_m_%d.npz" % (base_mop, m_objs)
        filepath = os.path.join(script_path, "files", filename)
        
        # load front from file
        npzfile = np.load(filepath)
        
        
        # original front
        front   = npzfile["objs"]
        p       = npzfile["p"][0]
        
        # rotate front
        rotated_front   = spin(front, degrees)
        
        print("Rotating %s %d degrees..." % (base_mop, degrees))      # @todo: add log support here
        
        return rotated_front.copy(), p
        """
        
        print("Front %s for m_objs: %s is not available" % 
            (mop_name, m_objs))                                         # @todo: add log support here
    
    
    return None, None
    
def get_real_front_wfg_spin(mop_name, m_objs, n_genes, params):
    """
    This function returns the real front of wfg-spin problems
    It is almost the same as get_real_front, but handles other conditions 
    
    Input
    mop_name    string, name of problem
    m_objs      int, number of objectives
    n_genes     int, number of decision variables
    params      dict, additional parameters
    
    Output
    front       (n_points, m_objs) matrix,
                or None if the front is not available
    p           spacing parameter (needed for plotting)
    
    """
    
    if m_objs != 3:
        print("Front %s for m_objs: %s is not available" % 
            (mop_name, m_objs))                                         # @todo: add log support here
    
    degrees  = params["degrees"]
    filename = "front_%s-%d_m_%d.npz" % (mop_name, degrees, m_objs)
    filepath = os.path.join(script_path, "files", filename)
    
    if os.path.isfile(filepath):
        
        # n_genes check (only for "min-dtlz")
        if check_n_genes(mop_name, m_objs, n_genes):
        
            # load front from file
            npzfile = np.load(filepath)
            
            front   = npzfile["objs"]
            p       = npzfile["p"][0]
            
            return front.copy(), p
            
    else:
        
        # @todo: descomentar este bloque cuando el metodo de rotacion sea general para rotar a un grado diferente de 180
        """
        # this block allow us to get a front with a user-defined rotation angle (like 20 degrees)
        base_mop = mop_name.split("-")[0]
        filename = "front_%s_m_%d.npz" % (base_mop, m_objs)
        filepath = os.path.join(script_path, "files", filename)
        
        # load front from file
        npzfile = np.load(filepath)
        
        
        # original front
        front   = npzfile["objs"]
        p       = npzfile["p"][0]
        
        # rotate front
        rotated_front   = spin(front, degrees)
        
        print("Rotating %s %d degrees..." % (base_mop, degrees))      # @todo: add log support here
        
        return rotated_front.copy(), p
        """
        
        print("Front %s for m_objs: %s is not available" % 
            (mop_name, m_objs))                                         # @todo: add log support here
    
    
    return None, None
    

def get_real_front(mop_name, m_objs, n_genes=None, params={}):
    """
    Return a matrix with the real front of mop_name.
    
    Input
    mop_name    string, name of problem
    m_objs      int, number of objectives
    n_genes     int, number of decision variables
    params      dict, additional parameters
    
    Output
    front       (n_points, m_objs) matrix,
                or None if the front is not available
    p           spacing parameter (needed for plotting)
    """
    
    # @todo: what to do with redundant problems?
    filename = "front_%s_m_%d.npz" % (mop_name, m_objs)
    filepath = os.path.join(script_path, "files", filename)
    
    
    # added for dtlz-spin
    #if mop_name in ("dtlz1-spin", "dtlz2-spin", "dtlz3-spin", "dtlz4-spin"):        #@todo: add regex support here
    # @note: solo se soporta dtlz1 debido al metodo de rotacion
    if mop_name in ("dtlz1-spin", "dtlz2-spin", "dtlz3-spin", "dtlz4-spin"):
        
        front, p =  get_real_front_dtlz_spin(mop_name, m_objs, n_genes, params)
        return front, p
    
    # added for wfg-spin
    if mop_name in ("wfg1-spin", "wfg2-spin", "wfg3-spin", "wfg4-spin", "wfg5-spin", "wfg6-spin", "wfg7-spin", "wfg8-spin", "wfg9-spin"):
        
        front, p =  get_real_front_wfg_spin(mop_name, m_objs, n_genes, params)
        return front, p
    
    # for other fronts...
    if os.path.isfile(filepath):
        
        # n_genes check (only for "min-dtlz")
        if check_n_genes(mop_name, m_objs, n_genes):
        
            # load front from file
            npzfile = np.load(filepath)
            
            front   = npzfile["objs"]
            p       = npzfile["p"][0]
            
            return front.copy(), p
        
        else:
            
            return None, None
            
    else:
        
        print("Front %s for m_objs: %s is not available" % 
            (mop_name, m_objs))                                         # @todo: add log support here
    
    return None, None
    
if __name__ == "__main__":
    
    from rocket.helpers import get_n_genes
    import matplotlib.pyplot as plt
    from mpl_toolkits.mplot3d import Axes3D
        
    # min-dtlz3
    mop_name = "min-dtlz3"
    m_objs = 3

    n_genes, __, __ = get_n_genes(mop_name, m_objs)
    #objs, p         = get_real_front(mop_name, m_objs)                 # incorrect: n_genes is needed
    #objs, p         = get_real_front(mop_name, m_objs, n_genes+1)      # incorrect: the value of n_genes is wrong
    objs, p         = get_real_front(mop_name, m_objs, n_genes)         # correct
    
    if objs is not None:
        
        fig = plt.figure(mop_name)
        ax = fig.add_subplot(111, projection="3d")
        ax.set_title(mop_name)
        
        # reshape
        X = objs[:, 0].reshape(p, p)
        Y = objs[:, 1].reshape(p, p)
        Z = objs[:, 2].reshape(p, p)
        
        ax.plot_wireframe(X, Y, Z, rstride=2, cstride=2, color="b")
        
        ax.view_init(30, 45)
        plt.show()


    # dtlz3-spin
    mop_name = "dtlz3-spin"
    m_objs = 3
    params = {"degrees": 180}
    #params = {"degrees": 45}

    n_genes, __, __ = get_n_genes(mop_name, m_objs)
    objs, p         = get_real_front(mop_name, m_objs, n_genes, params) 
    
    if objs is not None:
        
        title = "%s-%d" % (mop_name, params["degrees"])
        fig = plt.figure(title)
        ax = fig.add_subplot(111, projection="3d")
        ax.set_title(title)
        
        # reshape
        X = objs[:, 0].reshape(p, p)
        Y = objs[:, 1].reshape(p, p)
        Z = objs[:, 2].reshape(p, p)
        
        ax.plot_wireframe(X, Y, Z, rstride=2, cstride=2, color="b")
        
        ax.view_init(30, 45)
        plt.show()
