"""
create_vectors.py
This script creates the weights for baseline algorithms.

Based on
http://computacion.cs.cinvestav.mx/~rhernandez/mombi/

Sample Output:

creating weights/weights_m_10.txt
n vectors: 275          these two numbers
total sum: 275.0        should be almost the same!
"""
from __future__ import print_function
from simplex_vectors import create_simplex_vectors
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from rocket.helpers import spin

if __name__ == "__main__":
    
    # (dims, spacing h1, spacing h2)
    params = (
            (2, 39, 0),     # 100 vectors
            #(2, 239, 0),     # 100 vectors
            #(3, 19, 0),     # 100 vectors
            #(3, 1, 0),     # 100 vectors
            #(3, 5, 0),     # 100 vectors
            
            #(2, 99, 0),     # 100 vectors
            #(3, 10, 0),      #  66 vectors
            #(3, 11, 0),      #  78 vectors
            #(3, 12, 0),      #  91 vectors
            #(3, 25, 0),      #  91 vectors
            #(5, 6, 0),      # 210 vectors
            #(8, 3, 2),      # 156 vectors
            #(10, 3, 2),     # 275 vectors
            #(15, 2, 1),     # 135 vectors
            )
    
    for dims, h1, h2 in params:
        
        
        # create first layer
        w = create_simplex_vectors(dims, h1, add_eps=False)
        
        # create second layer (optional)
        if h2:
            
            v = create_simplex_vectors(dims, h2, add_eps=True)
            merged = vstack((w, v))
            w = merged.copy()
            
        # check by rows
        print("n vectors:", w.shape[0])
        print("total sum:", w.sum(axis=1).sum())        # each row sums (approx) 1, so n rows sums (approx) n
        
        filename = "weights_m_%d_%d.txt" % (dims, w.shape[0])
        print("creating", filename)
        
        # uncomment to save
        np.savetxt(filename, w, fmt="%.10f")
        print("")
        
        
        if w.shape[0] == 91 and dims == 3:
            
            # set A
            filename = "weights_m_%d_a.txt" % (dims, )
            # uncomment to save
            #np.savetxt(filename, w, fmt="%.10f")
            print("creating", filename)
            
            # set B (scaled)
            filename = "weights_m_%d_b.txt" % (dims, )
            # uncomment to save
            #np.savetxt(filename, w*0.5, fmt="%.10f")
            print("creating", filename)
        
            # set D (spin)
            filename = "weights_m_%d_d.txt" % (dims, )
            # uncomment to save
            #np.savetxt(filename, spin(w, degrees=180), fmt="%.10f")
            print("creating", filename)
        
    
    # plotting
    fig = plt.figure("vectors")
    ax = fig.add_subplot(111, projection="3d")
    ax.cla()
    
    w = np.genfromtxt("weights_m_3_3.txt")
    ax.plot(w[:, 0], w[:, 1], w[:, 2], marker="o", color="#DF6B6B", ls="none")
    ax.view_init(30, 45)
    plt.show()
    
