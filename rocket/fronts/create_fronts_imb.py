# create_fronts_zhou16.py
from __future__ import print_function
#from rocket.problems import evaluate_mop
from rocket.fronts.simplex_vectors import create_simplex_vectors
import numpy as np

import os
script_path = os.path.dirname(os.path.abspath(__file__))

def get_pareto_front_imb1(m_objs, pop_size, p):
    
    front = np.zeros((pop_size, m_objs))
    front[:, 0] = np.linspace(0, 1, pop_size)
    front[:, 1] = 1 - np.sqrt(front[:, 0])
    
    return front.copy()

def get_pareto_front_imb4(m_objs, pop_size, p):
    
    front = create_simplex_vectors(m_objs, p, add_eps=False)
        
    return front.copy()
    


    
if __name__ == "__main__":
    
    # mop_name, m_objs, pop_size, p, function
    params = (
        ("imb1", 2, 406,  None, get_pareto_front_imb1),     # 406 vectors
        #("imb4", 3, None, 27,   get_pareto_front_imb4),     # 406 vectors
    )
    
    for mop_name, m_objs, pop_size, p, generator in params:
    
        #n_genes, _, _ = get_n_genes(mop_name, m_objs)
        #chroms = get_pareto_solutions_zhou16(fn_id, pop_size, n_genes)
        #objs   = evaluate_mop(chroms.copy(), m_objs, {"name": mop_name})
        
        objs = generator(m_objs, pop_size, p)

        # save
        filename = "front_%s_m_%d.npz" % (mop_name, m_objs)
        filepath = os.path.join(script_path, "files", filename)
        
        # uncomment this line to overwrite current fronts
        #np.savez(filepath, objs=objs, p=np.array([p]))              # save objs and p in the same file

    # ---
    
            
    # plot!
    import matplotlib.pyplot as plt
    from mpl_toolkits.mplot3d import Axes3D
    
    mop_name = "imb1"
    m_objs   = 2        # change according mop_name1
    
    # unpack
    npzfile = np.load("files/front_%s_m_%d.npz" % (mop_name, m_objs))
    objs    = npzfile["objs"]
    p       = npzfile["p"][0]
    m_objs  = objs.shape[1]
    
    fig = plt.figure(mop_name)
    ax = fig.add_subplot(111) if m_objs == 2 else fig.add_subplot(111, projection="3d")
    ax.set_title(mop_name)
    ax.plot(objs[:, 0], objs[:, 1], "bo") if m_objs == 2 else ax.plot(objs[:, 0], objs[:, 1], objs[:, 2], "bo")
    plt.show()
    
