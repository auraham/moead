# About this package

`fronts` contains functions to retrieve the real front of the problems available in `rocket.problems`:

- `get_real_front(mop_name, m_objs, n_genes=None, params={})` allows us to retrive the real front of `mop_name` with `m_objs`. Some fronts require to define the value of `n_genes`, for instance, `"min-dtlz"`. The dictionary `params` will be used for additional parameters for other problems.

**Example** The following example shows how to retrieve the front of `"min-dtlz3"` and `"dtlz3"`. The size of the front of `"min-dtlz3"` depends on the value of `n_genes`; in other words, the size of the front for `n_genes=10` is not the same as the size for `n_genes=20`. So, we need to supply also the value of `n_genes`. On the other hand, the size of the front of `"dtlz3"` does not depend on the number of decision variables (`n_genes`); in other words, the size  of the front for `n_genes=10` is the same as the size for `n_genes=20`.

In this script, we supply the value of `n_genes` for `"min-dtlz3"`, whereas this parameter is ommited when the front of `"dtlz3"` is requested.

```python
from rocket.fronts import get_real_front
from rocket.helpers import get_n_genes
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

# first plot
mop_name = "min-dtlz3"
m_objs = 3

n_genes, __, __ = get_n_genes(mop_name, m_objs)
#objs, p         = get_real_front(mop_name, m_objs)                 # incorrect: n_genes is needed
#objs, p         = get_real_front(mop_name, m_objs, n_genes+1)      # incorrect: the value of n_genes is wrong
objs, p         = get_real_front(mop_name, m_objs, n_genes)         # correct

if objs is not None:
    
    fig = plt.figure(mop_name)
    ax = fig.add_subplot(111, projection="3d")
    ax.set_title(mop_name)

    # reshape
    X = objs[:, 0].reshape(p, p)
    Y = objs[:, 1].reshape(p, p)
    Z = objs[:, 2].reshape(p, p)

    ax.plot_wireframe(X, Y, Z, rstride=2, cstride=2, color="b")
        
    ax.view_init(30, 45)
    plt.show()
    
# second plot
mop_name = "dtlz3"
m_objs = 3

objs, p = get_real_front(mop_name, m_objs)         # correct, n_genes is not needed

if objs is not None:
    
    fig = plt.figure(mop_name)
    ax = fig.add_subplot(111, projection="3d")
    ax.set_title(mop_name)

    # reshape
    X = objs[:, 0].reshape(p, p)
    Y = objs[:, 1].reshape(p, p)
    Z = objs[:, 2].reshape(p, p)

    ax.plot_wireframe(X, Y, Z, rstride=2, cstride=2, color="b")
        
    ax.view_init(30, 45)
    plt.show()
```

![Result](min-dtlz3.png)

![Result](dtlz3.png)

# How it works?

The real fronts are stored in `ROCKET_PATH/fronts/files` in `npz` format. These files have this pattern:

```python
front_MOPNAME_m_OBJS.npz
```

To create these files, we use the following scripts:

- `create_fronts_dtlz.py` for DTLZ.
- `create_fronts_min_dtlz.py` for min-DTLZ.

**Note** For practical purposes, `get_real_front` is the only function you will need from this package. In the practice, you would be  interested on getting a front, not to construct it from stratch. Although, this `readme` explains the construction process for DTLZ and min-DTLZ.

# Script `create_fronts_dtlz.py`

This script creates the real front files of the DTLZ problems. Before explaining how it works, it is convenient to remember the *form* of a Pareto optimal solution of DTLZ:
```python
x = [x_1, x_2,               ...,  x_{n-1}, x_{n}]
    d position (diversity)         c distance (convergence)
    variables                      variables
```
Each solution (`x` vector) contains `n` decision variables (`d` diversity and `c` convergence variables). The optimal value for distance (convergence) variables is a fixed value. For DTLZ1, DTLZ2, DTLZ3, and DTLZ4, such fixed value is `0.5`. The values for position (diversity) variables should span the range `[0, 1]`.  The number of diversity variable is `m_objs-1`. So, for a problem with `n=9` decision variables, `m_objs=5` objectives, the number of convergence and diversity variables are `c=5`, and `d=4`, respectively.

To ease this calculation, use `get_n_genes` as follows:
```
get_n_genes("dtlz1", 5)
(9, 5, 4)				# n: 9, c: 5, d: 4
```

*Enough! Now explain me how to create a matrix of Pareto optimal solutions.* Consider the following example, where `m_objs=5`, `n_genes=9`, `c=5`, `d=4`, and the problem is `"dtlz1"`. This is how a matrix (`chroms`) of Pareto optimal solutions looks like:

```python
from rocket.helpers import get_n_genes
from create_fronts_dtlz import get_pareto_solutions_dtlz

m_objs = 5
n_genes, __, __ = get_n_genes("dtlz1", m_objs)
chroms = get_pareto_solutions_dtlz(n_genes, m_objs, 5)

# print the first 10 vectors only
print(chroms[:10, :])                                
[[ 0.    0.    0.    0.    0.5   0.5   0.5   0.5   0.5 ]
 [ 0.    0.    0.    0.25  0.5   0.5   0.5   0.5   0.5 ]
 [ 0.    0.    0.    0.5   0.5   0.5   0.5   0.5   0.5 ]
 [ 0.    0.    0.    0.75  0.5   0.5   0.5   0.5   0.5 ]
 [ 0.    0.    0.    1.    0.5   0.5   0.5   0.5   0.5 ]
 [ 0.    0.    0.25  0.    0.5   0.5   0.5   0.5   0.5 ]
 [ 0.    0.    0.25  0.25  0.5   0.5   0.5   0.5   0.5 ]
 [ 0.    0.    0.25  0.5   0.5   0.5   0.5   0.5   0.5 ]
 [ 0.    0.    0.25  0.75  0.5   0.5   0.5   0.5   0.5 ]
 [ 0.    0.    0.25  1.    0.5   0.5   0.5   0.5   0.5 ]]
```
Note that:
- The first `d=4` columns (`d = m_objs-1`) correspond to the diversity segment of each vector. In other words, the first `d` columns must span the range `[0, 1]` to get a good coverage (i.e. diversity). In order to cover such range uniformly, we use `np.meshgrid` (see `mesh.py` to look up the code). Also, note that the number of rows of `chroms` depends on (1) the number of objectives `m_objs` and (2) the spacing parameter `p`. As `p` grows, the number of rows increases, especially when the value of `m_objs` is high. In the previous example, `p=5` and the shape of `chroms` is `(625, 5)`.
- The last `c=5` columns correspond to the convergence segment of each vector and contains a fixed value of `0.5`. That's it.

So, the *hardest part* (Coldplay joke intended) is how to define the diversity segment. To do this, we use `mesh.py` to create that part of the `chroms` matrix. For completeness, here is the code of `get_pareto_solutions_dtlz` to create a Pareto optimal set of solutions.

```python
def get_pareto_solutions_dtlz(n_genes, m_objs, p):
    
    matrix = None
    
    if m_objs == 2:
        span = np.linspace(0, 1, p)
        matrix = span.reshape((span.shape[0], 1))
        
    elif m_objs == 3:
        matrix = mesh_2(p)
    
    elif m_objs == 5:
        matrix = mesh_4(p)
        
    elif m_objs == 8:
        matrix = mesh_7(p)
        
    elif m_objs == 10:
        matrix = mesh_9(p)
        
    else:
        print("m_objs: %s is not defined for get_pareto_solutions_dtlz" % m_objs)       # @todo: add log support here
        return None
    
    rows, cols = matrix.shape
        
    # optimal value for distance parameters
    opt_x = 0.5
    
    chroms = np.ones((rows, n_genes))*opt_x
    
    for col in range(cols):                     # rewrite the first cols of chroms
        chroms[:, col] = matrix[:, col]         # with the content of matrix
        
    return chroms.copy()
```







# A possible issue with `wfg1-spin-180`

When `create_fronts_wfg_spin.py` is executed, I got this warning:

```
warning: front wfg1-spin-180 contains negative values!
```

That is, our transformation method `spin` (`rocket.helpers.rotation.py`) caused a negative value after rotating `wfg1`.

After checking on `ipython`, we have found this:



```
 %run create_fronts_wfg_spin.py
warning: front wfg1-spin-180 contains negative values!

In [4]: objs
Out[4]: 
array([[  2.94671637e+00,   2.94572949e+00,   9.86880274e-04],
       [  2.93077002e+00,   2.92854977e+00,   2.22025020e-03],
       [  2.92540550e+00,   2.92145896e+00,   3.94654134e-03],
       ..., 
       [  2.05992204e+00,   1.27336449e+00,   7.86656143e-01],
       [  2.97078354e+00,   2.97078341e+00,   9.87197927e-05],
       [  2.92279561e+00,   2.92195206e+00,   1.82974525e-03]])

In [5]: objs.min(axis=0)
Out[5]: array([  1.02923721e-01,   1.02680862e-01,   8.88178420e-16])

In [6]: objs.min(axis=0)>0
Out[6]: array([ True,  True,  True], dtype=bool)

In [7]: objs.min(axis=0)<0
Out[7]: array([False, False, False], dtype=bool)

In [8]: %run create_fronts_wfg_spin.py
warning: front wfg1-spin-180 contains negative values!

In [9]: objs.min(axis=0)>0
Out[9]: array([ True, False,  True], dtype=bool)

In [10]: objs[:, 1] < 0
Out[10]: array([False, False, False, ..., False, False, False], dtype=bool)

In [11]: res = objs[:, 1] < 0

In [12]: res
Out[12]: array([False, False, False, ..., False, False, False], dtype=bool)

In [13]: res.sum()
Out[13]: 1

In [14]: ind = arange(0, len(res))

In [15]: ind = arange(0, len(res), dtype=int)

In [16]: ind[res]
Out[16]: array([614])

In [17]: objs[614]
Out[17]: array([  1.99981256e+00,  -4.12110230e-08,   1.99981260e+00])

In [18]: objs[614, 1]
Out[18]: -4.1211023038556505e-08

In [19]: "%.10f" % objs[614, 1]
Out[19]: '-0.0000000412'

```

The 614-th objectve vector of `wfg1-spin-180` front has these values:



```
In [17]: objs[614]
Out[17]: array([  1.99981256e+00,  -4.12110230e-08,   1.99981260e+00])
```



From these, the second objective is negative, `-0.0000000412`:

```
In [18]: objs[614, 1]
Out[18]: -4.1211023038556505e-08

In [19]: "%.10f" % objs[614, 1]
Out[19]: '-0.0000000412'
```



Although this value is almost zero, I do not know if this issue could lead to another problem (e.g. another front below the original-rotated front).





# TODO

- Create fronts using weight vectors instead of using Pareto optimal solutions.
- Check fronts of `inv-dtlz1` for more than 3 objectives. The script `create_fronts_inv_dtlz.py` can create fronts for more than 3 objectives, however, I do not know if they are correct. For now, only `front_inv-dtlz1_m_3.npz` is under mercurial.
- Show the difference among fronts of `dtlz1`, `dtlz1-spin`, and `inv-dtlz1`.
- Why the front of `inv-dtlz1` and `dtlz1` for `m_objs=2` is the same?.