# MOEA/D - Multi-Objective Evolutionary Algorithm based on Decomposition 

This repository contains a Python implementation of MOEA/D, proposed by Zhang and Li [Zhang07].



## Demo

Run this command to test MOEA/D for solving a benchmark problem (`dtlz1`) with three objecitves:

```
python3 main.py -p dtlz1 -m 3 -d true
```

Then, run this script for creating an animation of the search process:

```
python3 anim_pop.py -p dtlz1
```

You will see this animation:

![](demo_dtlz1.gif)



## References

- [Zhang07] MOEA/D: A Multiobjective Evolutionary Algorithm Based on Decomposition.



# Contact

Auraham Camacho `auraham.cg@gmail.com`