# main.py
# %run main.py -p dtlz1 -m 3 -d true
# python3 main.py -p dtlz1 -m 3 -d true
from __future__ import print_function
import numpy as np
import os, sys, argparse
np.seterr(all='raise')

# add rocket
script_path = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, script_path)

from rocket.moea import MOEAD
from rocket.helpers import get_n_genes, get_bounds
from rocket.plot import load_rcparams

if __name__ == "__main__":
    
    # generations per problem instance (format: {m_objs:iters})
    main_budget = {  
        "dtlz1"     : {3: 400, 5: 500, 8: 800, 10: 1000},
        "dtlz2"     : {3: 300, 5: 500, 8: 800, 10: 1000},
        "dtlz3"     : {3: 500, 5: 500, 8: 800, 10: 1000},
        "inv-dtlz1" : {3: 300, 5: 500, 8: 800, 10: 1000},
        "maf1"      : {3: 500, 5: 500, 8: 800, 10: 1000},
        "maf2"      : {3: 300, 5: 500, 8: 800, 10: 1000},
        "maf3"      : {3: 500, 5: 500, 8: 800, 10: 1000},
        "maf4"      : {3: 300, 5: 500, 8: 800, 10: 1000},
        "maf5"      : {3: 300, 5: 500, 8: 800, 10: 1000},
    }

    # input arguments
    ap = argparse.ArgumentParser()
    ap.add_argument("-p", "--mop_name",             required=True, help="mop name: dtlz1")
    ap.add_argument("-m", "--m_objs",               required=True, help="m_objs: 3, 5, 8, 10", type=int)
    ap.add_argument("-d", "--debug",                required=True, help="debug run: true, false", choices=("true", "false"))
    args = vars(ap.parse_args())
    
    # path for saving results
    output_path = "results"
    
    runs = 1                # number of independent runs
    step = 100              # save pop every step generations for posterior hv calculation
    plot_output = False     # plot final population
    
    if args["debug"] == "true":

        # for plotting
        load_rcparams()
        plot_output = True
        
    # budget for this instance
    iters = main_budget[args["mop_name"]][args["m_objs"]]
    
    # -- from here is up to you -- 

    # load weights
    filename = os.path.join(script_path, "data", "weights_m_%d.txt" % args["m_objs"])
    weights = np.genfromtxt(filename)
    pop_size = weights.shape[0]
    
    # debug
    print("mop_name:    %s" % args["mop_name"])
    print("m_objs:      %d" % args["m_objs"])
    print("pop_size:    %d" % pop_size)
    print("iters:       %d" % iters)
    
    # get n_genes (number of decision variables)
    n_genes, wfg_k, wfg_l = get_n_genes(args["mop_name"], args["m_objs"])
    
    params = {
        "runs"              : runs,                                                     # number of independent runs
        "iters"             : iters,                                                    # number of generations
        "m_objs"            : args["m_objs"],                                           # number of objectives
        "pop_size"          : pop_size,                                                 # number of individuals
        "params_mop"        : {"name": args["mop_name"], "k": wfg_k, "degrees":180},    # mop parameters
        "params_crossover"  : {"name": "sbx", "prob": 1.0, "eta": 20},                  # crossover parameters
        "params_mutation"   : {"name": "polymutation", "prob": 1./n_genes, "eta": 15},  # mutation parameters
        "params_stats"      : {"output_path": output_path, "step": step},               # stats parameters
        "verbose"           : True,                                                     # log verbosity
        
        # MOEAD params
        "neigh_size"        : 20,
        "pop_weights"       : weights,
        "params_form"       : {"name": "pbi",  "pbi-theta": 5},
    }
    
    # set name
    params["name"]  = "moead-pbi"
    params["label"] = "MOEA/D-PBI"
            
    # execution
    moea = MOEAD(params)
    moea.evaluate(plot_output=plot_output)
